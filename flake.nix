{
  description = "personal NixOS configurations with flakes";

  inputs = {
    commitlint-config.url = "git+https://codeberg.org/samuelsung/commitlint-config";
    commitlint-config.inputs.nixpkgs.follows = "nixpkgs";

    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";

    dmenu.url = "git+https://codeberg.org/samuelsung/dmenu";

    dwm.url = "git+https://codeberg.org/samuelsung/dwm";
    dwm.inputs.nixpkgs.follows = "nixpkgs";

    fetchfetch.url = "git+https://codeberg.org/samuelsung/fetchfetch";

    flake-parts.url = "github:hercules-ci/flake-parts";

    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    impermanence.url = "github:nix-community/impermanence";

    misc.url = "git+https://codeberg.org/samuelsung/misc?ref=release-24.11";

    music-playlist.url = "git+https://codeberg.org/samuelsung/music-playlist";
    music-playlist.flake = false;

    neovim-flake.url = "git+https://codeberg.org/samuelsung/neovim-flake?ref=release-24.11";

    nixos-generators.url = "github:nix-community/nixos-generators";
    nixos-generators.inputs.nixpkgs.follows = "nixpkgs";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-master.url = "nixpkgs/master";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";

    nur.url = "github:nix-community/NUR";

    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";

    slock.url = "git+https://codeberg.org/samuelsung/slock";
    slock.inputs.nixpkgs.follows = "nixpkgs";

    sops-nix.url = "github:Mic92/sops-nix";

    st.url = "git+https://codeberg.org/samuelsung/st";
    st.inputs.nixpkgs.follows = "nixpkgs";

    shuf-less-more.url = "git+https://codeberg.org/samuelsung/shuf-less-more";

    wallpapers.url = "git+https://codeberg.org/samuelsung/wallpapers";

    zen-browser-flake.url = "github:0xc000022070/zen-browser-flake";
    zen-browser-flake.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        ./apps
        ./checks
        ./devenv
        ./hardwares
        ./machines
        ./modules
        ./secrets
      ];
      systems = [ "x86_64-linux" ];

      perSystem = { system, ... }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          config = { allowUnfree = true; };
        };
      };
    };
}
