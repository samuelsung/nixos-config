{ lib, ... }:

{
  _module.args.colors = {
    catppuccin = import ./catppuccin/catppuccin.nix;
    nord = import ./nord/nord.nix;
    lib = {
      hexOf = { r, g, b }:
        let
          toHexString = i: if i < 16 then "0${lib.toHexString i}" else lib.toHexString i;
        in
        "${toHexString r}${toHexString g}${toHexString b}";
    };
  };
}
