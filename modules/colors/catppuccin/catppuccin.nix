{
  latte = {
    rosewater = { r = 220; g = 138; b = 120; };
    flamingo = { r = 221; g = 120; b = 120; };
    pink = { r = 234; g = 118; b = 203; };
    mauve = { r = 136; g = 57; b = 239; };
    red = { r = 210; g = 15; b = 57; };
    maroon = { r = 230; g = 69; b = 83; };
    peach = { r = 254; g = 100; b = 11; };
    yellow = { r = 223; g = 142; b = 29; };
    green = { r = 64; g = 160; b = 43; };
    teal = { r = 23; g = 146; b = 153; };
    sky = { r = 4; g = 165; b = 229; };
    sapphire = { r = 32; g = 159; b = 181; };
    blue = { r = 30; g = 102; b = 245; };
    lavender = { r = 114; g = 135; b = 253; };
    text = { r = 76; g = 79; b = 105; };
    subtext1 = { r = 92; g = 95; b = 119; };
    subtext0 = { r = 108; g = 111; b = 133; };
    overlay2 = { r = 124; g = 127; b = 147; };
    overlay1 = { r = 140; g = 143; b = 161; };
    overlay0 = { r = 156; g = 160; b = 176; };
    surface2 = { r = 172; g = 176; b = 190; };
    surface1 = { r = 188; g = 192; b = 204; };
    surface0 = { r = 204; g = 208; b = 218; };
    base = { r = 239; g = 241; b = 245; };
    mantle = { r = 230; g = 233; b = 239; };
    crust = { r = 220; g = 224; b = 232; };
  };

  frappe = {
    rosewater = { r = 242; g = 213; b = 207; };
    flamingo = { r = 238; g = 190; b = 190; };
    pink = { r = 244; g = 184; b = 228; };
    mauve = { r = 202; g = 158; b = 230; };
    red = { r = 231; g = 130; b = 132; };
    maroon = { r = 234; g = 153; b = 156; };
    peach = { r = 239; g = 159; b = 118; };
    yellow = { r = 229; g = 200; b = 144; };
    green = { r = 166; g = 209; b = 137; };
    teal = { r = 129; g = 200; b = 190; };
    sky = { r = 153; g = 209; b = 219; };
    sapphire = { r = 133; g = 193; b = 220; };
    blue = { r = 140; g = 170; b = 238; };
    lavender = { r = 186; g = 187; b = 241; };
    text = { r = 198; g = 208; b = 245; };
    subtext1 = { r = 181; g = 191; b = 226; };
    subtext0 = { r = 165; g = 173; b = 206; };
    overlay2 = { r = 148; g = 156; b = 187; };
    overlay1 = { r = 131; g = 139; b = 167; };
    overlay0 = { r = 115; g = 121; b = 148; };
    surface2 = { r = 98; g = 104; b = 128; };
    surface1 = { r = 81; g = 87; b = 109; };
    surface0 = { r = 65; g = 69; b = 89; };
    base = { r = 48; g = 52; b = 70; };
    mantle = { r = 41; g = 44; b = 60; };
    crust = { r = 35; g = 38; b = 52; };
  };

  macchiato = {
    rosewater = { r = 244; g = 219; b = 214; };
    flamingo = { r = 240; g = 198; b = 198; };
    pink = { r = 245; g = 189; b = 230; };
    mauve = { r = 198; g = 160; b = 246; };
    red = { r = 237; g = 135; b = 150; };
    maroon = { r = 238; g = 153; b = 160; };
    peach = { r = 245; g = 169; b = 127; };
    yellow = { r = 238; g = 212; b = 159; };
    green = { r = 166; g = 218; b = 149; };
    teal = { r = 139; g = 213; b = 202; };
    sky = { r = 145; g = 215; b = 227; };
    sapphire = { r = 125; g = 196; b = 228; };
    blue = { r = 138; g = 173; b = 244; };
    lavender = { r = 183; g = 189; b = 248; };
    text = { r = 202; g = 211; b = 245; };
    subtext1 = { r = 184; g = 192; b = 224; };
    subtext0 = { r = 165; g = 173; b = 203; };
    overlay2 = { r = 147; g = 154; b = 183; };
    overlay1 = { r = 128; g = 135; b = 162; };
    overlay0 = { r = 110; g = 115; b = 141; };
    surface2 = { r = 91; g = 96; b = 120; };
    surface1 = { r = 73; g = 77; b = 100; };
    surface0 = { r = 54; g = 58; b = 79; };
    base = { r = 36; g = 39; b = 58; };
    mantle = { r = 30; g = 32; b = 48; };
    crust = { r = 24; g = 25; b = 38; };
  };

  mocha = {
    rosewater = { r = 245; g = 224; b = 220; };
    flamingo = { r = 242; g = 205; b = 205; };
    pink = { r = 245; g = 194; b = 231; };
    mauve = { r = 203; g = 166; b = 247; };
    red = { r = 243; g = 139; b = 168; };
    maroon = { r = 235; g = 160; b = 172; };
    peach = { r = 250; g = 179; b = 135; };
    yellow = { r = 249; g = 226; b = 175; };
    green = { r = 166; g = 227; b = 161; };
    teal = { r = 148; g = 226; b = 213; };
    sky = { r = 137; g = 220; b = 235; };
    sapphire = { r = 116; g = 199; b = 236; };
    blue = { r = 137; g = 180; b = 250; };
    lavender = { r = 180; g = 190; b = 254; };
    text = { r = 205; g = 214; b = 244; };
    subtext1 = { r = 186; g = 194; b = 222; };
    subtext0 = { r = 166; g = 173; b = 200; };
    overlay2 = { r = 147; g = 153; b = 178; };
    overlay1 = { r = 127; g = 132; b = 156; };
    overlay0 = { r = 108; g = 112; b = 134; };
    surface2 = { r = 88; g = 91; b = 112; };
    surface1 = { r = 69; g = 71; b = 90; };
    surface0 = { r = 49; g = 50; b = 68; };
    base = { r = 30; g = 30; b = 46; };
    mantle = { r = 24; g = 24; b = 37; };
    crust = { r = 17; g = 17; b = 27; };
  };
}
