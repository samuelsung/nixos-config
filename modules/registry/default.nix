{ inputs, ... }:

{
  flake.nixosModules.registry =
    { config, lib, ... }:
    let
      inherit (lib) mapAttrs mkIf;
    in
    {
      nix.registry = mkIf config.desktop.enable (mapAttrs
        (_: flake: { inherit flake; })
        {
          inherit (inputs)
            misc
            neovim-flake
            nixpkgs
            nixpkgs-master
            nixpkgs-unstable
            nur;
        });
    };
}
