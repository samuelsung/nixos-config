{ lib, ... }:

let
  inherit (lib) mkOption toHexString types;

  mkPrimaryOption = name: mkOption {
    type = types.ints.between 0 255;
    description = "${name} value of the color";
  };

  colorType = types.submodule (args:
    let inherit (args.config) r g b; in
    {
      options = {
        r = mkPrimaryOption "red";
        g = mkPrimaryOption "green";
        b = mkPrimaryOption "blue";
        rgbhex = mkOption {
          type = types.str;
          default = "${toHexString r}${toHexString g}${toHexString b}";
          readOnly = true;
        };
      };
    });
in
{
  options.colors = {
    colors = mkOption {
      type = types.attrsOf colorType;
      default = { };
      description = "colection of colors";
    };

    lib = mkOption {
      type = types.attrs;
    };
  };

  config.colors.lib = {
    inherit colorType;
  };

  imports = [
    ./colors-nord.nix
  ];
}
