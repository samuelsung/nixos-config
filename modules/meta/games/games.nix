{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.utils.games.enable = mkEnableOption "games";
}
