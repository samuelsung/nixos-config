{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.utils.developments.enable = mkEnableOption "development utils";
}
