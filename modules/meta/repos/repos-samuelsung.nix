{
  repos = {
    entertainment.ff14ss = {
      repoPath = "samuelsung/ff14ss";
      remotes.origin = {
        fetch = "https://gitea.samuelsung.net/samuelsung/ff14ss.git";
        push = "https://gitea.samuelsung.net/samuelsung/ff14ss.git";
      };
    };

    entertainment.final-fantasy-xiv-config = {
      repoPath = "samuelsung/final-fantasy-xiv-config";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/final-fantasy-xiv-config.git";
        push = "git@codeberg.org:samuelsung/final-fantasy-xiv-config.git";
      };
    };

    entertainment.ffxiv-tools.remotes.origin.fetch =
      "https://github.com/valarnin/ffxiv-tools.git";

    entertainment.IpsuShade.remotes.origin.fetch =
      "https://github.com/ipsusu/IpsuShade.git";

    hyprland.hypr-contrib.remotes.origin.fetch =
      "https://github.com/hyprwm/contrib.git";

    misc.Canary.remotes.origin.fetch =
      "https://github.com/Apsu/Canary.git";

    misc.cvt_modeline_calculator_12.remotes.origin.fetch =
      "https://github.com/kevinlekiller/cvt_modeline_calculator_12.git";

    misc.danbooru.remotes.origin.fetch =
      "https://github.com/danbooru/danbooru.git";

    misc.rspotify.remotes.origin.fetch =
      "https://github.com/ramsayleung/rspotify.git";

    nix.devenv.remotes.origin.fetch =
      "https://github.com/cachix/devenv.git";

    nix.direnv.remotes.origin.fetch =
      "https://github.com/direnv/direnv.git";

    nix.home-manager.remotes.origin.fetch =
      "https://github.com/nix-community/home-manager.git";

    nix.impermanence.remotes.origin.fetch =
      "https://github.com/nix-community/impermanence.git";

    nix.nix-on-droid.remotes.origin.fetch =
      "https://github.com/nix-community/nix-on-droid.git";

    nix.nixos-generators.remotes.origin.fetch =
      "https://github.com/nix-community/nixos-generators.git";

    nix.nixos-hardware.remotes.origin.fetch =
      "https://github.com/NixOS/nixos-hardware.git";

    nix.nixpkgs.remotes.origin.fetch =
      "https://github.com/NixOS/nixpkgs.git";

    nix.nixvim.remotes.origin.fetch =
      "https://github.com/nix-community/nixvim.git";

    nix."pre-commit-hooks.nix".remotes.origin.fetch =
      "https://github.com/cachix/pre-commit-hooks.nix.git";

    nix.terranix.remotes.origin.fetch =
      "https://github.com/terranix/terranix.git";

    nix.terranix-examples.remotes.origin.fetch =
      "https://github.com/terranix/terranix-examples.git";

    personal.adv360-config = {
      repoPath = "samuelsung/adv360-config.git";

      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/adv360-config.git";
        push = "git@codeberg.org:samuelsung/adv360-config.git";
      };
      remotes.upstream.fetch = "https://github.com/KinesisCorporation/Adv360-Pro-ZMK.git";
      bare = true;
    };

    personal.advent-of-code-solutions = {
      repoPath = "samuelsung/advent-of-code-solutions.git";

      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/advent-of-code-solutions.git";
        push = "git@codeberg.org:samuelsung/advent-of-code-solutions.git";
      };
      bare = true;
    };

    personal.commitlint-config = {
      repoPath = "samuelsung/commitlint-config.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/commitlint-config.git";
        push = "git@codeberg.org:samuelsung/commitlint-config.git";
      };
      bare = true;
    };

    personal.daily-practice = {
      repoPath = "samuelsung/daily-practice.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/daily-practice.git";
        push = "git@codeberg.org:samuelsung/daily-practice.git";
      };
      bare = true;
    };

    personal.dmenu = {
      repoPath = "samuelsung/dmenu.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/dmenu.git";
        push = "git@codeberg.org:samuelsung/dmenu.git";
      };
      remotes.upstream.fetch = "https://git.suckless.org/dmenu";
      bare = true;
    };

    personal.docker-config = {
      repoPath = "samuelsung/docker-config.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/docker-config.git";
        push = "git@codeberg.org:samuelsung/docker-config.git";
      };
      bare = true;
    };

    personal.dwl = {
      repoPath = "samuelsung/dwl.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/dwl.git";
        push = "git@codeberg.org:samuelsung/dwl.git";
      };
      remotes.upstream.fetch = "https://github.com/djpohly/dwl.git";
      bare = true;
    };

    personal.dwm = {
      repoPath = "samuelsung/dwm.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/dwm.git";
        push = "git@codeberg.org:samuelsung/dwm.git";
      };
      remotes.upstream.fetch = "https://git.suckless.org/dwm";
      bare = true;
    };

    personal.ergogen-config = {
      repoPath = "samuelsung/ergogen-config.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/ergogen-config.git";
        push = "git@codeberg.org:samuelsung/ergogen-config.git";
      };
      bare = true;
    };

    personal.fetchfetch = {
      repoPath = "samuelsung/fetchfetch.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/fetchfetch.git";
        push = "git@codeberg.org:samuelsung/fetchfetch.git";
      };
      bare = true;
    };

    personal.final-fantasy-xiv-sync = {
      repoPath = "samuelsung/final-fantasy-xiv-sync.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/final-fantasy-xiv-sync.git";
        push = "git@codeberg.org:samuelsung/final-fantasy-xiv-sync.git";
      };
      bare = true;
    };

    personal.misc = {
      repoPath = "samuelsung/misc.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/misc.git";
        push = "git@codeberg.org:samuelsung/misc.git";
      };
      bare = true;
    };

    personal.moe2nix = {
      repoPath = "samuelsung/moe2nix.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/moe2nix.git";
        push = "git@codeberg.org:samuelsung/moe2nix.git";
      };
      bare = true;
    };

    personal.music-playlist = {
      repoPath = "samuelsung/music-playlist";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/music-playlist.git";
        push = "git@codeberg.org:samuelsung/music-playlist.git";
      };
    };

    personal.neovim-flake = {
      repoPath = "samuelsung/neovim-flake.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/neovim-flake.git";
        push = "git@codeberg.org:samuelsung/neovim-flake.git";
      };
      bare = true;
    };

    personal.nixos-config = {
      repoPath = "samuelsung/nixos-config.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/nixos-config.git";
        push = "git@codeberg.org:samuelsung/nixos-config.git";
      };
      bare = true;
    };

    personal.personal-site = {
      repoPath = "samuelsung/personal-site.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/personal-site.git";
        push = "git@codeberg.org:samuelsung/personal-site.git";
      };
      bare = true;
    };

    personal.personal-site-old = {
      repoPath = "samuelsung/personal-site-old.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/personal-site-old.git";
        push = "git@codeberg.org:samuelsung/personal-site-old.git";
      };
      bare = true;
      private = true;
    };

    personal.personal-site-reboot = {
      repoPath = "samuelsung/personal-site-reboot.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/personal-site-reboot.git";
        push = "git@codeberg.org:samuelsung/personal-site-reboot.git";
      };
      bare = true;
    };

    personal.rdanbooru = {
      repoPath = "samuelsung/rdanbooru.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/rdanbooru.git";
        push = "git@codeberg.org:samuelsung/rdanbooru.git";
      };
      bare = true;
    };

    personal.slock = {
      repoPath = "samuelsung/slock.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/slock.git";
        push = "git@codeberg.org:samuelsung/slock.git";
      };
      remotes.upstream.fetch = "https://git.suckless.org/slock";
      bare = true;
    };

    personal.st = {
      repoPath = "samuelsung/st.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/st.git";
        push = "git@codeberg.org:samuelsung/st.git";
      };
      remotes.upstream.fetch = "https://git.suckless.org/st";
      bare = true;
    };

    personal.termbooru = {
      repoPath = "samuelsung/termbooru.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/termbooru.git";
        push = "git@codeberg.org:samuelsung/termbooru.git";
      };
      bare = true;
    };

    personal.terraform-config = {
      repoPath = "samuelsung/terraform-config.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/terraform-config.git";
        push = "git@codeberg.org:samuelsung/terraform-config.git";
      };
      bare = true;
    };

    personal.wallpapers = {
      repoPath = "samuelsung/wallpapers.git";
      remotes.origin = {
        fetch = "https://codeberg.org/samuelsung/wallpapers.git";
        push = "git@codeberg.org:samuelsung/wallpapers.git";
      };
      bare = true;
    };

    react.react-hook-form.remotes.origin.fetch =
      "https://github.com/react-hook-form/react-hook-form.git";

    vim.LazyVim.remotes.origin.fetch =
      "https://github.com/LazyVim/LazyVim.git";

    vim."lualine.nvim".remotes.origin.fetch =
      "https://github.com/nvim-lualine/lualine.nvim.git";

    vim.neorg.remotes.origin.fetch =
      "https://github.com/nvim-neorg/neorg.git";

    vim."neorg.wiki".remotes.origin.fetch =
      "https://github.com/nvim-neorg/neorg.wiki.git";

    vim.nvim-lspconfig.remotes.origin.fetch =
      "https://github.com/neovim/nvim-lspconfig.git";

    vim.nvim-treesitter.remotes.origin.fetch =
      "https://github.com/nvim-treesitter/nvim-treesitter.git";

    vim."telescope.nvim".remotes.origin.fetch =
      "https://github.com/nvim-telescope/telescope.nvim.git";

    zmk.Adv360-Pro-ZMK.remotes.origin.fetch =
      "https://github.com/KinesisCorporation/Adv360-Pro-ZMK.git";

    zmk.duckyb-zmk-sweep.remotes.origin.fetch =
      "https://github.com/duckyb/zmk-sweep.git";

    zmk.toniz4-zmk-config.remotes.origin.fetch =
      "https://github.com/toniz4/zmk-config.git";
  };
}
