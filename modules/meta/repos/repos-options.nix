{ lib, ... }:

let
  inherit (lib) mkOption types;

  remoteType = types.submodule ({ name, config, ... }: {
    options = {
      remote = mkOption {
        type = types.str;
        default = name;
        readOnly = true;
        description = "name of the remote.";
      };

      fetch = mkOption {
        type = types.str;
        description = "url for fetching";
      };

      push = mkOption {
        type = types.str;
        default = config.fetch;
        description = "url for pushing";
      };
    };
  });

  repoType = folder: types.submodule ({ name, ... }: {
    options = {
      repoPath = mkOption {
        type = types.str;
        default = "${folder}/${name}";
        description = "path of the repository";
      };

      remotes = mkOption {
        type = types.attrsOf remoteType;
        default = { };
        description = "list of remotes";
      };

      bare = mkOption {
        type = types.bool;
        default = false;
        description = "clone as bare repo";
      };

      private = mkOption {
        type = types.bool;
        default = false;
        description = "is a private repo";
      };
    };
  });

  folderType = types.submodule ({ name, ... }: {
    options = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "enable this repo module";
      };
    };

    freeformType = types.attrsOf (repoType name);
  });
in
{
  options.repos = mkOption {
    type = types.attrsOf folderType;
    default = { };
  };
}
