{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.utils.managements.enable = mkEnableOption "management utils.";
}
