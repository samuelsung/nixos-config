{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.bluraySupport.enable = mkEnableOption "bluray support";
}
