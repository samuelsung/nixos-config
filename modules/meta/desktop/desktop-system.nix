{ config, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf;
in
{
  options.desktop.enable = mkEnableOption "desktop support";

  options.services.wayland.enable = mkEnableOption "wayland";

  config.environment.systemPackages = mkIf config.services.wayland.enable [
    pkgs.wl-clipboard
  ];

  config.assertions = [
    {
      assertion = config.services.wayland.enable -> config.desktop.enable;
      message = ''
        hm desktop support is needed when enabling hm wayland support.
      '';
    }
  ];
}
