{ config, lib, osConfig, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.desktop.enable = mkEnableOption "desktop support";

  config.assertions = [{
    # or true mean just keep the assertion when osConfig is missing
    assertion = config.desktop.enable -> (osConfig.desktop.enable or true);
    message = ''
      System desktop support is needed when enabling hm desktop support.
    '';
  }];
}
