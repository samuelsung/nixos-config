{
  flake.homeManagerModules.meta-desktop-home = ./desktop-home.nix;
  flake.nixosModules.meta-desktop-system = ./desktop-system.nix;
}
