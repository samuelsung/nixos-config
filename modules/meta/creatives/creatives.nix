{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.utils.creatives.enable = mkEnableOption "creative utils";
}
