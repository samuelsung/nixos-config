{
  imports = [
    ./basics
    ./bluray
    ./colors
    ./communications
    ./creatives
    ./desktop
    ./developments
    ./games
    ./managements
    ./repos
    ./secrets
  ];
}
