{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.utils.communications.enable = mkEnableOption "communication utils";
}
