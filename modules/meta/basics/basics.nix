{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.utils.basics.enable = mkEnableOption "basic utils";
}
