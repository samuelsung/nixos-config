{ lib
, osConfig ? null
, ...
}:

let
  inherit (lib) mkOption types;
in
{
  options.secrets.enable = mkOption {
    type = types.bool;
    default = osConfig.secrets.enable or false;
    description = "secrets support";
  };
}
