{ lib, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.secrets.enable = mkEnableOption "secrets support";
}
