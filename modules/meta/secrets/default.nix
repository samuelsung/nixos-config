{
  flake.homeManagerModules.meta-secrets = ./secrets-home-manager.nix;
  flake.nixosModules.meta-secrets = ./secrets-system.nix;
}
