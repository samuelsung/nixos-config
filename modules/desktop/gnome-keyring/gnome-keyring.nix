{ config, lib, ... }:

let
  inherit (lib) mkIf optionalString;
in
mkIf config.desktop.enable {
  services.gnome.gnome-keyring.enable = true;
  programs.seahorse.enable = true;
  security.pam.services = {
    login = {
      enableGnomeKeyring = true;
    };
  };

  home-manager.sharedModules = [
    ({ config, osConfig, ... }: {
      home.persistence."/persist/${config.home.homeDirectory}" = mkIf osConfig.impermanence.enable {
        directories = [
          { directory = ".local/share/keyrings"; method = "symlink"; }
        ];
        allowOther = true;
      };

      systemd.user.tmpfiles.rules = [
        "d ${optionalString osConfig.impermanence.enable "/persist/"}${config.home.homeDirectory}/.local/share/keyrings 0700 - - -"
      ];
    })
  ];
}
