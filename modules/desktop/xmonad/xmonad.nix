{ config, lib, pkgs, wm-lib, ... }:

with lib;
let
  inherit (wm-lib.consts) modmasks;
  inherit (wm-lib.shortcuts) shortcutsToTrees;
  inherit (wm-lib.shortcuts.types) shortcutOf mouseKey key;
  cfg = config.xsession.windowManager.xmonad;
  wmCfg = config.windowManager;

  fromXK = replaceStrings [ "XF86XK_" "XK_" "Button" ] [ "xF86XK_" "xK_" "button" ];

  fromModMask = replaceStrings [ "Mod" ] [ "mod" ];

  # string => Tree<keyType, shortcutsType> => string
  sTreeToStr = indent: { key, value, children }:
    let
      modMasks = concatStringsSep " .|. " ((map fromModMask key.mods) ++ [ "0" ]);

      command =
        if children != [ ]
        then "submap . ${sTreesToStr indent children}"
        else value.action;
    in
    " ((${modMasks}, ${fromXK key.key}), ${command})";

  # string => Tree<keyType, shortcutsType>[] => string
  sTreesToStr = indent: trees:
    ''
      M.fromList $
      ${indent}[${concatMapStringsSep "\n${indent}," (sTreeToStr "${indent}    ") trees}
      ${indent}]'';
in
{
  options.xsession.windowManager.xmonad = {
    terminal = mkOption {
      description = "command to spawn a new terminal with the command";
      type = with types; functionTo str;
    };

    workspaces = mkOption {
      type = types.listOf types.str;
      default = [ "opt1" "opt2" ];
      description = "xmoand's workspace list";
    };

    mod = mkOption {
      type = types.enum modmasks;
      default = "Mod4Mask";
      description = ''
        modKey of the wm
      '';
    };

    mouseShortcuts = mkOption {
      type = types.attrsOf (shortcutOf mouseKey types.str);
      default = { };
      description = "xmoand's mouse shortcuts";
    };

    shortcuts = mkOption {
      type = types.attrsOf (shortcutOf key types.str);
      default = { };
      description = "xmoand's keyboard shortcuts";
    };

    visuals = {
      border.width = mkOption {
        description = "windows border (in px)";
      };

      border.active.color = mkOption {
        type = types.str;
        description = "border color on active window";
      };

      border.inactive.color = mkOption {
        type = types.str;
        description = "border color on inactive window";
      };

      border.radius = mkOption {
        type = types.int;
        description = "border radius (in px)";
      };

      padding.width = mkOption {
        type = types.int;
        description = "wm padding width";
      };
    };

    layouts.master = {
      nmaster = mkOption {
        type = types.int;
        default = 1;
        description = "number of master window";
      };

      ratio = mkOption {
        type = types.float;
        default = 0.5;
        description = "ratio of the split out of 100";
      };

      delta = mkOption {
        type = types.float;
        default = 0.03;
        description = "increment when resizing panes";
      };
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      dbus
    ];

    desktop.enable = true;
    services.polybar.enable = true;
    services.polybar.wantedBy = [ "xmonad-session.target" ];
    services.xserver.enable = true;
    services.xserver.windowManager.command = ''
      systemctl --user start xmonad-session.target
      ${config.xsession.windowManager.command}
    '';

    systemd.user.targets.xmonad-session = {
      Unit = {
        Description = "xmonad session";
        Documentation = [ "man:systemd.special(7)" ];
        BindsTo = [ "graphical-session.target" "x-session.target" ];
        Wants = [ "graphical-session-pre.target" ];
        After = [ "graphical-session-pre.target" "x-session.target" ];
      };
    };

    xsession.windowManager.xmonad = {
      enableContribAndExtras = true;
      extraPackages = haskellPackages: [
        haskellPackages.dbus
        pkgs.dbus
      ];

      shortcuts = flip mapAttrs wmCfg.shortcuts (_: s: s // {
        action =
          if s.inTerminal
          then "spawn \"${cfg.terminal { cmd = s.action; }}\""
          else "spawn \"${s.action}\"";
      });

      mouseShortcuts = flip mapAttrs wmCfg.mouseShortcuts (_: s: s // {
        action =
          if s.inTerminal
          then "spawn \"${cfg.terminal { cmd = s.action; }}\""
          else "spawn \"${s.action}\"";
      });

      config = pkgs.writeTextFile {
        name = "xmonad.hs";
        text = ''
          import XMonad hiding ( (|||) )
          import Data.Monoid
          import System.Exit
          import XMonad.Util.SpawnOnce
          import XMonad.Util.Run
          import XMonad.Hooks.ManageDocks
          import XMonad.Hooks.ManageHelpers
          import XMonad.Hooks.InsertPosition
          import XMonad.Util.CustomKeys
          import XMonad.Actions.DwmPromote
          import XMonad.Hooks.EwmhDesktops
          import XMonad.Layout.Fullscreen hiding (fullscreenEventHook)
          import XMonad.Layout.Spacing
          import XMonad.Hooks.DynamicLog
          import XMonad.Actions.Submap
          import XMonad.Actions.CopyWindow
          import XMonad.Util.EZConfig
          import XMonad.Util.WorkspaceCompare
          import XMonad.Util.Loggers
          import XMonad.Actions.RotSlaves
          import XMonad.Actions.CycleWS
          import XMonad.Actions.PhysicalScreens
          import XMonad.Layout.LayoutCombinators
          import XMonad.Layout.ToggleLayouts
          import XMonad.Layout.NoBorders
          import XMonad.Util.Scratchpad
          import Graphics.X11.ExtraTypes.XF86
          
          
          import qualified XMonad.StackSet as W
          import qualified Data.Map        as M
          
          import qualified DBus as D
          import qualified DBus.Client as D
          
          -- The preferred terminal program, which is used in a binding below and by
          -- certain contrib modules.
          --
          myTerminal      = "${cfg.terminal { }}"
          
          -- Whether focus follows the mouse pointer.
          myFocusFollowsMouse :: Bool
          myFocusFollowsMouse = False
          
          -- Whether clicking on a window to focus also passes the click to the window
          myClickJustFocuses :: Bool
          myClickJustFocuses = False
          
          -- Width of the window border in pixels.
          --
          myBorderWidth   = ${toString cfg.visuals.border.width}
          
          -- modMask lets you specify which modkey you want to use. The default
          -- is mod1Mask ("left alt").  You may also consider using mod3Mask
          -- ("right alt"), which does not conflict with emacs keybindings. The
          -- "windows key" is usually mod4Mask.
          --
          myModMask       = ${fromModMask cfg.mod}
          
          -- The default number of workspaces (virtual screens) and their names.
          -- By default we use numeric strings, but any string may be used as a
          -- workspace name. The number of workspaces is determined by the length
          -- of this list.
          --
          -- A tagging example:
          --
          -- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
          --
          myWorkspaces    = [${concatMapStringsSep "," (name: "\"${name}\"") cfg.workspaces}]
          
          -- Border colors for unfocused and focused windows, respectively.
          --
          
          myNormalBorderColor  = "#${cfg.visuals.border.inactive.color}"
          myFocusedBorderColor = "#${cfg.visuals.border.active.color}"
          
          ------------------------------------------------------------------------
          -- Key bindings. Add, modify or remove key bindings here.
          --
          myKeys conf = ${sTreesToStr "    " (shortcutsToTrees (attrValues cfg.shortcuts))}
              -- mkKeymap conf $
              -- [ ("M-M1-C-S-q",        io (exitWith ExitSuccess))
              -- , ("M-C-S-q",           spawn "xmonad --recompile; xmonad --restart")]
              -- Resize viewed windows to the correct size
              -- , ((modm,               xK_n     ), refresh)
              -- Move focus to the master window
              --, ((modm,               xK_m     ), windows W.focusMaster  )
              -- Swap the focused window and the master window
              -- , ((modm,               xK_space), windows W.swapMaster)
              -- Toggle the status bar gap
              -- Use this binding with avoidStruts from Hooks.ManageDocks.
              -- See also the statusBar function from Hooks.DynamicLog.
              --
              -- Quit xmonad
              -- Run xmessage with a summary of the default keybindings (useful for beginners)
              --, ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
              --]
              -- ++
              -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
              -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
              --
              -- [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
                  -- | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
                  -- , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
          
          
          ------------------------------------------------------------------------
          -- Mouse bindings: default actions bound to mouse events
          --
          myMouseBindings conf = ${sTreesToStr "    " (shortcutsToTrees (attrValues cfg.mouseShortcuts))}

              -- you may also bind events to the mouse scroll wheel (button4 and button5)

          ------------------------------------------------------------------------
          -- Layouts:
          
          -- You can specify and transform your layouts by modifying these values.
          -- If you change layout bindings be sure to use 'mod-shift-space' after
          -- restarting (with 'mod-q') to reset your layout state to the new
          -- defaults, as xmonad preserves your old layout settings by default.
          --
          -- The available layouts.  Note that each layout is separated by |||,
          -- which denotes layout choice.
          --
          ${
          let
            paddingWidth = toString cfg.visuals.padding.width;
            border = "(Border ${paddingWidth} ${paddingWidth} ${paddingWidth} ${paddingWidth})";
          in
          ''
          myLayout = avoidStruts $ spacingRaw False ${border} True ${border} True $ toggleLayouts (noBorders Full) (tiled ||| Mirror tiled)
            where
               -- default tiling algorithm partitions the screen into two panes
               tiled   = Tall nmaster delta ratio
          
               -- The default number of windows in the master pane
               nmaster = ${toString cfg.layouts.master.nmaster}
          
               -- Default proportion of screen occupied by master pane
               ratio   = ${toString cfg.layouts.master.ratio}
          
               -- Percent of screen to increment by when resizing panes
               delta   = ${toString cfg.layouts.master.delta}
          ''}
          
          ------------------------------------------------------------------------
          -- Window rules:
          
          -- Execute arbitrary actions and WindowSet manipulations when managing
          -- a new window. You can use this to, for example, always float a
          -- particular program, or have a client always appear on a particular
          -- workspace.
          --
          -- To find the property name associated with a program, use
          -- > xprop | grep WM_CLASS
          -- and click on the client you're interested in.
          --
          -- To match on the WM_NAME, you can use 'title' in the same way that
          -- 'className' and 'resource' are used below.
          --
          myManageHook = composeAll
              [ className =? "MPlayer"        --> doFloat
              , className =? "Gimp"           --> doFloat
              , resource  =? "desktop_window" --> doIgnore
              , resource  =? "kdesktop"       --> doIgnore ]
          
          ------------------------------------------------------------------------
          -- Event handling
          
          -- * EwmhDesktops users should change this to ewmhDesktopsEventHook
          --
          -- Defines a custom handler function for X Events. The function should
          -- return (All True) if the default handler is to be run afterwards. To
          -- combine event hooks use mappend or mconcat from Data.Monoid.
          --
          myEventHook = mempty
          
          ------------------------------------------------------------------------
          -- Status bars and logging
          
          -- Perform an arbitrary action on each internal state change or X event.
          -- See the 'XMonad.Hooks.DynamicLog' extension for examples.
          
          -- Override the PP values as you would otherwise, adding colors etc depending
          -- on  the statusbar used-
          --
          --
          invertWrap::String -> WorkspaceId -> String
          invertWrap inner outer = outer ++ inner ++ outer
          
          myLogHook::D.Client -> PP
          myLogHook dbus = def { ppOutput = dbusOutput dbus
            , ppCurrent = invertWrap "::ws_current::"
            , ppVisible = invertWrap "::ws_visible::"
            , ppHidden = invertWrap "::ws_hidden::"
            , ppHiddenNoWindows = invertWrap "::ws_hiddenNoWindows::"
            , ppUrgent = invertWrap "::ws_urgent::"
            , ppTitle = wrap "::title::" "::title::"
            , ppLayout = wrap "::layout::" "::layout::"
            , ppSep = " "
            , ppWsSep = " "
            , ppSort = (. scratchpadFilterOutWorkspace) <$> getSortByXineramaRule
            , ppExtras = [logConst $ concat $ map (wrap "::order::" "::order") myWorkspaces]
          }
          
          -- Emit a DBus signal on log updates
          dbusOutput :: D.Client -> String -> IO ()
          dbusOutput dbus str = do
              let signal = (D.signal objectPath interfaceName memberName) {
                      D.signalBody = [D.toVariant $ str]
                  }
              D.emit dbus signal
            where
              objectPath = D.objectPath_ "/org/xmonad/Log"
              interfaceName = D.interfaceName_ "org.xmonad.Log"
              memberName = D.memberName_ "Update"
          
          ------------------------------------------------------------------------
          -- Startup hook
          
          -- Perform an arbitrary action each time xmonad starts or is restarted
          -- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
          -- per-workspace layout choices.
          --
          -- By default, do nothing.
          myStartupHook = return ()
          
          ------------------------------------------------------------------------
          -- Now run xmonad with all the defaults we set up.
          
          -- Run xmonad with the settings you specify. No need to modify this.
          --
          main = do
            dbus <- D.connectSession
              -- Request access to the DBus name
            D.requestName dbus (D.busName_ "org.xmonad.Log")
              [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
          
            xmonad $ addEwmhWorkspaceSort (pure scratchpadFilterOutWorkspace) . ewmh $ docks def {
              -- simple stuff
              terminal           = myTerminal,
              focusFollowsMouse  = myFocusFollowsMouse,
              clickJustFocuses   = myClickJustFocuses,
              borderWidth        = myBorderWidth,
              modMask            = myModMask,
              workspaces         = myWorkspaces,
              normalBorderColor  = myNormalBorderColor,
              focusedBorderColor = myFocusedBorderColor,
          
              -- key bindings
              keys               = myKeys,
              mouseBindings      = myMouseBindings,
          
              -- hooks, layouts
              layoutHook         = myLayout,
              manageHook         = composeAll [
                -- fullscreenManageHook,
                insertPosition Master Newer,
                (scratchpadManageHook $ W.RationalRect 0.125 0.125 0.75 0.75),
                -- (isFullscreen --> doFullFloat),
                myManageHook],
              handleEventHook    = myEventHook, -- <+> fullscreenEventHook,
              logHook            = dynamicLogWithPP (myLogHook dbus),
              startupHook        = myStartupHook
            }
        '';
      };
    };
  };
}
