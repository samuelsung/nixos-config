localFlake:

{ config, lib, samuelsung-lib, ... }:

let
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
  inherit (lib) flip concatStrings listToAttrs imap1 nameValuePair optionalString;
  inherit (samuelsung-lib.misc) mod;

  cfg = config.xsession.windowManager.xmonad;
in
{
  services.xserver.variables = {
    TERMINAL = "${config.programs.ghostty.package}/bin/ghostty";
    TERM = "xterm-ghostty";
  };

  xsession.windowManager.xmonad = {
    terminal = { name ? "", cmd ? "" }:
      concatStrings [
        "${config.programs.ghostty.package}/bin/ghostty"
        (optionalString (name != "") " -n ${name}")
        (optionalString (cmd != "") " -e ${cmd}")
      ];

    workspaces = [ "coding" "notes" "web" "music" "chat" "opt0" "opt1" "opt2" "opt3" ];

    mod = "Mod4Mask";

    visuals = {
      border.width = 2;
      border.radius = 3;

      border.active.color = hexOf catppuccin.mocha.mauve;
      border.inactive.color = hexOf catppuccin.mocha.base;
      padding.width = 3;
    };

    mouseShortcuts = {
      mouseFocusResize = {
        action = "(\\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster)";
        keys = [
          { mods = [ cfg.mod ]; key = "Button3"; }
        ];
      };

      mouseFocusTop = {
        action = "(\\w -> focus w >> windows W.shiftMaster)";
        keys = [
          { mods = [ cfg.mod ]; key = "Button2"; }
        ];
      };

      mouseFocusMove = {
        action = "(\\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)";
        keys = [
          { mods = [ cfg.mod ]; key = "Button1"; }
        ];
      };
    };

    shortcuts = {
      terminal = {
        action = ''spawn "${cfg.terminal { }}"'';
        keys = [
          { mods = [ cfg.mod ]; key = "XK_Return"; }
        ];
      };

      dmenu-run = {
        action = ''spawn "${config.programs.dmenu.package}/bin/dmenu_run"'';
        keys = [
          { mods = [ cfg.mod ]; key = "XK_d"; }
        ];
      };

      killWindow = {
        action = "kill";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_q"; }
        ];
      };

      promoteWindow = {
        action = "dwmpromote";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_space"; }
        ];
      };

      focusUp = {
        action = "windows W.focusUp";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_e"; }
        ];
      };

      focusDown = {
        action = "windows W.focusDown";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_n"; }
        ];
      };

      fullscreen = {
        action = "sendMessage ToggleStruts >> toggleWindowSpacingEnabled >> toggleScreenSpacingEnabled >> sendMessage ToggleLayout";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_f"; }
        ];
      };

      hideBar = {
        action = "sendMessage ToggleStruts";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_b"; }
        ];
      };

      prevWorkspace = {
        action = "moveTo Prev hiddenWS";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_m"; }
        ];
      };

      nextWorkspace = {
        action = "moveTo Next hiddenWS";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_comma"; }
        ];
      };

      prevScreen = {
        action = "onPrevNeighbour horizontalScreenOrderer W.view";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_k"; }
        ];
      };

      nextScreen = {
        action = "onNextNeighbour horizontalScreenOrderer W.view";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_period"; }
        ];
      };

      movePrevWorkspace = {
        action = "shiftTo Prev hiddenWS >> moveTo Prev hiddenWS";
        keys = [
          { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_m"; }
        ];
      };

      moveNextWorkspace = {
        action = "shiftTo Next hiddenWS >> moveTo Next hiddenWS";
        keys = [
          { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_comma"; }
        ];
      };

      movePrevScreen = {
        action = "onPrevNeighbour horizontalScreenOrderer W.shift >> onPrevNeighbour horizontalScreenOrderer W.view";
        keys = [
          { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_k"; }
        ];
      };

      moveNextScreen = {
        action = "onNextNeighbour horizontalScreenOrderer W.shift >> onNextNeighbour horizontalScreenOrderer W.view";
        keys = [
          { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_period"; }
        ];
      };

      setWindowTile = {
        action = "withFocused $ windows . W.sink";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_t"; }
        ];
      };

      incMasterNode = {
        action = "sendMessage (IncMasterN 1)";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_u"; }
        ];
      };

      descMasterNode = {
        action = "sendMessage (IncMasterN (-1))";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_l"; }
        ];
      };

      masterExpand = {
        action = "sendMessage Expand";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_i"; }
        ];
      };

      masterShrink = {
        action = "sendMessage Shrink";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_h"; }
        ];
      };

      nextLayout = {
        action = "sendMessage NextLayout";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_Tab"; }
        ];
      };

      rotateUpStack = {
        action = "rotAllUp";
        keys = [
          { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_e"; }
        ];
      };

      rotateDownStack = {
        action = "rotAllDown";
        keys = [
          { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_n"; }
        ];
      };

      # terminalScratchpad = {
      #   action = withNav "nav_launch_terminal_scratchpad";
      #   keys = [
      #     { mods = [ cfg.mod ]; key = "XK_s"; }
      #   ];
      # };
    } // (listToAttrs
      ((flip imap1 cfg.workspaces (i: w:
        nameValuePair "workspaces${toString i}" {
          action = "(windows (W.view \"${w}\"))";
          keys = [
            { mods = [ cfg.mod ]; key = "XK_${toString (mod i 10)}"; }
          ];
        }))
      ++ (flip imap1 cfg.workspaces (i: w:
        nameValuePair "moveWorkspaces${toString i}" {
          action = "(windows (W.shift \"${w}\") >> windows (W.view \"${w}\"))";
          keys = [
            { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_${toString (mod i 10)}"; }
          ];
        }))));
  };
}
