{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.desktop-xmonad-samuelsung = importApply
    ./xmonad-samuelsung.nix
    { inherit colors; };

  flake.homeManagerModules.desktop-xmonad = ./xmonad.nix;
}
