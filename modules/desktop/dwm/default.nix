{ flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.desktop-dwm = ./dwm.nix;

  flake.homeManagerModules.desktop-dwm-samuelsung = importApply
    ./dwm-samuelsung.nix
    { inherit moduleWithSystem; };
}
