{ config, lib, pkgs, ... }:

let
  cfg = config.xsession.windowManager.dwm;
  inherit (lib) mkIf mkOption types;
in
{
  options.xsession.windowManager.dwm = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.dwm;
    };
  };

  config = mkIf cfg.enable {
    desktop.enable = true;
    services.xserver.enable = true;

    home.packages = [ cfg.package ];
  };
}
