localFlake:

localFlake.moduleWithSystem ({ inputs' }:
{
  xsession.windowManager.dwm.package =
    inputs'.dwm.packages.dwm;
})
