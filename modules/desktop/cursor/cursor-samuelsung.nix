{ pkgs, ... }:

{
  gtk.cursorTheme = {
    package = pkgs.catppuccin-cursors.mochaDark;
    name = "catppuccin-mocha-dark-cursors";
    size = 16;
  };
}
