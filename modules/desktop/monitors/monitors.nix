{ desktop-lib, lib, ... }:

let
  inherit (lib) mkOption types;
  inherit (desktop-lib) xwOf;

  monitorType = types.submodule {
    options = {
      name = mkOption {
        type = xwOf types.str;
        description = "name of the monitor";
      };

      h = mkOption {
        type = types.number;
        description = "hight of the monitor";
      };

      v = mkOption {
        type = types.number;
        description = "vertical dimension of the monitor";
      };

      x = mkOption {
        type = types.number;
        description = "x coordinate of the monitor";
      };

      y = mkOption {
        type = types.number;
        description = "y coordinate of the monitor";
      };

      refreshRate = mkOption {
        type = xwOf types.number;
        description = "refresh rate of the monitor";
      };

      rotation = mkOption {
        type = types.number;
        default = 0;
        description = "degree of rotation of the monitor";
      };

      primary = mkOption {
        type = types.bool;
        default = false;
        description = "whether the monitor is the primary one";
      };
    };
  };
in
{
  options.hardware.video.monitors = mkOption {
    type = types.listOf monitorType;
    default = [ ];
    description = "monitor settings";
  };
}
