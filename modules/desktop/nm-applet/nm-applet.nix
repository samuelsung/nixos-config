{ config, lib, osConfig, ... }:

let
  inherit (lib) mkDefault;
  networkmanagerEnable = osConfig.networking.networkmanager.enable or false;
in
{
  services.network-manager-applet.enable = mkDefault (networkmanagerEnable && config.desktop.enable);
}
