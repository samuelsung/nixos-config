localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ config, pkgs, lib, ... }:

let
  inherit (lib) mkOption types makeBinPath;
  cfg = config.desktop.wallpapers;
in
{
  options.desktop.wallpapers = {
    package = mkOption {
      default = inputs'.wallpapers.packages.wallpapers;
      type = types.package;
    };

    random-wallpaper =
      mkOption {
        type = types.package;
        default = pkgs.writeScriptBin "random-wallpaper" ''
          PATH=${makeBinPath [ pkgs.coreutils inputs'.shuf-less-more.packages.shuf-less-more ]}:$PATH

          WALLPAPERS="${cfg.package}"

          ls $WALLPAPERS | \
          shuf-less-more --profile wallpapers -n $1 --bias 80 | \
          sed "s@^@$WALLPAPERS/@g"
        '';
        readOnly = true;
      };
  };
})
