{ flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.desktop-wallpapers = importApply ./wallpapers.nix { inherit moduleWithSystem; };
}
