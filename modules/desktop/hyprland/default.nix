{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.desktop-hyprland.imports = [
    ./hyprland-hm-keymaps.nix
    ./hyprland-hm.nix
  ];

  flake.homeManagerModules.desktop-hyprland-samuelsung = importApply ./hyprland-hm-samuelsung.nix {
    inherit colors;
  };
}
