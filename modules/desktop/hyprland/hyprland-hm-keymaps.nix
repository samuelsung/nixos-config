{ config, lib, wm-lib, ... }:

let
  inherit (lib) attrValues concatMapStringsSep flip replaceStrings mapAttrs mkOption;
  inherit (lib.types) attrsOf enum str;
  inherit (wm-lib.consts) modmasks;
  inherit (wm-lib.shortcuts) shortcutsToTrees;
  inherit (wm-lib.shortcuts.types) key mouseKey shortcutOf;

  wmCfg = config.windowManager;
  cfg = config.wayland.windowManager.hyprland;

  fromXK = replaceStrings [
    "XF86XK_"
    "XK_"
    "Button1"
    "Button2"
    "Button3"
    "Button4"
    "Button5"
  ] [
    "XF86"
    ""
    "mouse:272"
    "mouse:273"
    "mouse:274"
    "mouse:275"
    "mouse:276"
  ];

  fromModMask = mod: {
    ShiftMask = "SHIFT";
    LockMask = "CAPS";
    ControlMask = "CTRL";
    Mod1Mask = "ALT";
    Mod2Mask = "MOD2";
    Mod3Mask = "MOD3";
    Mod4Mask = "MOD4";
    Mod5Mask = "MOD5";
  }.${mod} or mod;

  sTreeToStr = { key, value, children }:
    let
      ms = concatMapStringsSep "_" fromModMask key.mods;
      k = fromXK key.key;
    in
    if children == [ ]
    then ''
      binde = ${ms},${k},${value.action}
      bind = ${ms},${k},submap,reset
    ''
    else ''
      bind = ${ms},${k},submap,${ms}${k}
      submap=${ms}${k}
      ${concatMapStringsSep "\n" sTreeToStr children}
      submap=reset
    '';

  mTreeToStr = { key, value, ... }:
    let
      ms = concatMapStringsSep "_" fromModMask key.mods;
      k = fromXK key.key;
    in
    ''
      bindm = ${ms},${k},${value.action}
    '';
in
{
  options.wayland.windowManager.hyprland = {
    mod = mkOption {
      type = enum modmasks;
      default = "Mod4Mask";
      description = ''
        modKey of the hyprland
      '';
    };

    mouseShortcuts = mkOption {
      type = attrsOf (shortcutOf mouseKey str);
      default = { };
      description = "hyprland's mouse shortcuts";
    };

    shortcuts = mkOption {
      type = attrsOf (shortcutOf key str);
      default = { };
      description = "hyprland's keyboard shortcuts";
    };
  };

  config.wayland.windowManager.hyprland = {
    shortcuts = flip mapAttrs wmCfg.shortcuts (_: s: s // {
      action =
        if s.inTerminal
        then "exec,${cfg.terminal { cmd = s.action; }}"
        else "exec,${s.action}";
    });

    mouseShortcuts = flip mapAttrs wmCfg.mouseShortcuts (_: s: s // {
      action =
        if s.inTerminal
        then "exec,${cfg.terminal { cmd = s.action; }}"
        else "exec,${s.action}";
    });
  };

  config.wayland.windowManager.hyprland.extraConfig = ''
    ${concatMapStringsSep "\n" sTreeToStr (shortcutsToTrees (attrValues cfg.shortcuts))}
    ${concatMapStringsSep "\n" mTreeToStr (shortcutsToTrees (attrValues cfg.mouseShortcuts))}
  '';
}
