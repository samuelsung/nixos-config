localFlake:

{ config, lib, pkgs, osConfig, samuelsung-lib, ... }:

let
  inherit (lib) concatMap concatStrings concatStringsSep flip listToAttrs mkIf nameValuePair optionalString range;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
  inherit (samuelsung-lib.misc) mod;
  cfg = config.wayland.windowManager.hyprland;

  mapColor = rgbhex: "rgb(${rgbhex})";
in
{
  programs.swaylock.enable = true;
  programs.waybar.enable = mkIf cfg.enable true;
  programs.waybar.systemd.enable = true;
  programs.waybar.systemd.target = "hyprland-session.target";
  services.sway-notification-center.enable = true;
  services.sway-notification-center.systemd.enable = true;
  services.sway-notification-center.systemd.targets = [ "hyprland-session.target" ];
  services.swww.enable = mkIf cfg.enable true;
  services.swww.systemd.enable = true;
  services.swww.systemd.target = "hyprland-session.target";
  services.swww.random-wallpaper-systemd.enable = mkIf cfg.enable true;
  services.swww.random-wallpaper-systemd.monitorsScript = ''
    ${pkgs.hyprland}/bin/hyprctl monitors -j | ${pkgs.jq}/bin/jq -r '.[].name' || exit 1
  '';

  programs.rofi.enable = true;
  programs.rofi.package = pkgs.rofi-wayland;

  wayland.windowManager.hyprland.systemd.variables = [
    "TERMINAL"
  ];

  wayland.windowManager.hyprland = {
    terminal = { name ? "", cmd ? "" }:
      concatStrings [
        "${config.programs.ghostty.package}/bin/ghostty"
        (optionalString (name != "") " -n ${name}")
        (optionalString (cmd != "") " -e ${cmd}")
      ];
  };

  wayland.windowManager.hyprland.mouseShortcuts = {
    mouseFocusResize = {
      action = "resizewindow";
      keys = [
        { mods = [ cfg.mod ]; key = "Button3"; }
      ];
    };

    mouseFocusTop = {
      action = "bringactivetotop";
      keys = [
        { mods = [ cfg.mod ]; key = "Button2"; }
      ];
    };

    mouseFocusMove = {
      action = "movewindow";
      keys = [
        { mods = [ cfg.mod ]; key = "Button1"; }
      ];
    };
  };

  wayland.windowManager.hyprland.shortcuts = {
    terminal = {
      action = "exec,${cfg.terminal { }}";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_Return"; }
      ];
    };

    chat-overlay = {
      action = "exec,hyprctl dispatch togglespecialworkspace chat";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_o"; }
        { key = "XK_c"; }
      ];
    };

    notes-overlay = {
      action = "exec,hyprctl dispatch togglespecialworkspace notes";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_o"; }
        { key = "XK_n"; }
      ];
    };

    music-overlay = {
      action = "exec,hyprctl dispatch togglespecialworkspace music";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_o"; }
        { key = "XK_m"; }
      ];
    };

    workspaceToCurrentMonitor = {
      action = "exec,${pkgs.writeShellScript "workspace-to-current-monitor.sh" ''
        PATH=${lib.makeBinPath [ pkgs.gnused pkgs.gnugrep pkgs.hyprland config.programs.rofi.package pkgs.jq ]}
        workspace=$(hyprctl workspaces -j | jq -r ".[].name" | rofi -dmenu -p workspaces)

        if echo "$workspace" | grep -q "^special\(.*\)\?"; then
          hyprctl dispatch togglespecialworkspace "$(echo $workspace | sed "s@special:\?@@g")"
        else
          hyprctl dispatch moveworkspacetomonitor "''${workspace}" current \
            && hyprctl dispatch workspace "''${workspace}"
        fi
      ''}";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_w"; }
        { key = "XK_w"; }
      ];
    };

    renameWorkspace =
      let
        choices = [
          "coding"
          "games"
          "music"
          "web"
        ];
      in
      {
        action = "exec,${pkgs.writeShellScript "rename-workspace.sh" ''
        PATH=${lib.makeBinPath [ pkgs.gnused pkgs.gnugrep pkgs.hyprland config.programs.rofi.package pkgs.jq ]}
        workspace=$(printf "${concatStringsSep "\n" choices}" | rofi -dmenu -p rename-workspace)

        hyprctl dispatch renameworkspace "$(hyprctl activeworkspace -j | jq -r '.id')" "''${workspace}"
      ''}";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_w"; }
          { key = "XK_r"; }
        ];
      };

    rofi-dmenu-run = {
      action = "exec,${config.programs.rofi.package}/bin/rofi -show drun";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_d"; }
      ];
    };

    capscreen-clipboard = {
      action = "exec,${pkgs.grimblast}/bin/grimblast copysave area";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_c"; }
        { key = "XK_c"; }
      ];
    };

    capscreen-save = {
      action = "exec,${pkgs.grimblast}/bin/grimblast save area";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_c"; }
        { key = "XK_s"; }
      ];
    };

    capscreen-clipboard-printscreen = {
      action = "exec,${pkgs.grimblast}/bin/grimblast copy area";
      keys = [
        { key = "XK_Print"; }
      ];
    };

    notification = {
      action = "exec,${pkgs.swaynotificationcenter}/bin/swaync-client -t -sw";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_p"; }
      ];
    };

    lockscreen = {
      action = "exec,${config.programs.swaylock.package}/bin/swaylock";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_z"; }
      ];
    };

    killWindow = {
      action = "killactive";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_q"; }
      ];
    };

    promoteWindow = {
      action = "layoutmsg, swapwithmaster auto";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_space"; }
      ];
    };

    focusUp = {
      action = "layoutmsg,cycleprev";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_e"; }
      ];
    };

    focusDown = {
      action = "layoutmsg,cyclenext";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_n"; }
      ];
    };

    fullscreen = {
      action = "fullscreen, 0";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_f"; }
      ];
    };

    # hideBar = {
    #   action = withNav "nav_hide_bar";
    #   keys = [
    #     { mods = [ cfg.mod ]; key = "XK_b"; }
    #   ];
    # };

    prevWorkspace = {
      action = "exec, ${pkgs.hyprnome}/bin/hyprnome --previous";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_m"; }
      ];
    };

    nextWorkspace = {
      action = "exec, ${pkgs.hyprnome}/bin/hyprnome";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_comma"; }
      ];
    };

    prevScreen = {
      action = "focusmonitor, l";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_k"; }
      ];
    };

    nextScreen = {
      action = "focusmonitor, r";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_period"; }
      ];
    };

    movePrevWorkspace = {
      action = "exec, ${pkgs.hyprnome}/bin/hyprnome --previous --move";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_m"; }
      ];
    };

    moveNextWorkspace = {
      action = "exec, ${pkgs.hyprnome}/bin/hyprnome --move";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_comma"; }
      ];
    };

    movePrevScreen = {
      action = "movewindow,mon:l";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_k"; }
      ];
    };

    moveNextScreen = {
      action = "movewindow,mon:r";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_period"; }
      ];
    };

    setWindowTile = {
      action = "togglefloating,";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_t"; }
      ];
    };

    incMasterNode = {
      action = "layoutmsg, addmaster";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_u"; }
      ];
    };

    descMasterNode = {
      action = "layoutmsg, removemaster";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_l"; }
      ];
    };

    masterExpand = {
      action = "splitratio, +0.03";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_i"; }
      ];
    };

    masterShrink = {
      action = "splitratio, -0.03";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_h"; }
      ];
    };

    nextLayout = {
      action = "layoutmsg, orientationnext";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_Tab"; }
      ];
    };

    rotateUpStack = {
      action = "layoutmsg, swapprev";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_e"; }
      ];
    };

    rotateDownStack = {
      action = "layoutmsg, swapnext";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_n"; }
      ];
    };
  } // (listToAttrs (flip concatMap (range 1 10) (i: [
    (nameValuePair "workspaces${toString i}" {
      action = "workspace, ${toString i}";
      keys = [
        { mods = [ cfg.mod ]; key = "XK_${toString (mod i 10)}"; }
      ];
    })

    (nameValuePair "moveWorkspaces${toString i}" {
      action = "movetoworkspace, ${toString i}";
      keys = [
        { mods = [ cfg.mod "Mod1Mask" ]; key = "XK_${toString (mod i 10)}"; }
      ];
    })
  ])));

  wayland.windowManager.hyprland.settings = {
    env = [
      "TERMINAL,${cfg.terminal { }}"
    ];
    general = {
      border_size = 2;
      gaps_in = 3;
      gaps_out = 3 * 2;
      # gaps_workspaces = 0;
      "col.active_border" = mapColor (hexOf catppuccin.mocha.mauve);
      "col.inactive_border" = mapColor (hexOf catppuccin.mocha.base);
      # "col.nogroup_border" = mapColor (hexOf catppuccin.mocha.mauve);
      # "col.nogroup_border_active" = mapColor (hexOf catppuccin.mocha.base);
      # "col.group_border" = mapColor (hexOf catppuccin.mocha.surface2);
      # "col.group_border_active" = mapColor (hexOf catppuccin.mocha.yellow);
      layout = "master";
    };

    decoration = {
      # See https://wiki.hyprland.org/Configuring/Variables/ for more
      rounding = 10;
      active_opacity = 1;
      inactive_opacity = 1;
      fullscreen_opacity = 1;

      blur.enabled = false;

      shadow.enabled = true;
      shadow.range = 20;
      shadow.render_power = 3;
      shadow.ignore_window = true;
      shadow.offset = "5, 5";

      dim_inactive = false;
    };

    animations = {
      enabled = true;
      animation = [
        "border, 1, 2, default"
        "fade, 1, 4, default"
        "windows, 1, 3, default, popin 80%"
        "workspaces, 1, 2, default, slide"
      ];
    };

    input = {
      kb_layout = osConfig.services.xserver.xkb.layout;
      kb_variant = osConfig.services.xserver.xkb.variant;
      kb_options = osConfig.services.xserver.xkb.options;

      repeat_rate = 25; # per second
      repeat_delay = 600; # in ms

      sensitivity = 0; # -1.0 - 1.0, 0 means no modification.
      natural_scroll = false;

      follow_mouse = 2;

      # float_switch_override_focus	If enabled (1 or 2), focus will change to the window under the cursor when changing from tiled-to-floating and vice versa. If 2, focus will also follow mouse on float-to-float switches.	int	1
      touchpad = {
        disable_while_typing = true;
        natural_scroll = false;
        tap-to-click = true;
      };
    };

    gestures = {
      workspace_swipe = true;
      workspace_swipe_fingers = 3;
      workspace_swipe_invert = true;
      workspace_swipe_create_new = false;
      workspace_swipe_forever = true;
    };

    misc = {
      disable_hyprland_logo = true;
      vfr = true;
      vrr = 1;
      mouse_move_enables_dpms = true;
      key_press_enables_dpms = true;
      # disable_autoreload = true;
      focus_on_activate = true;
      new_window_takes_over_fullscreen = 1;
    };

    binds.allow_workspace_cycles = true;

    cursor.inactive_timeout = 2;

    master = {
      new_on_top = true;
      mfact = 0.55;
      orientation = "left";
    };
  };
}
