{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) concatStringsSep mkIf mkOption;
  inherit (lib.types) functionTo str;
  cfg = config.wayland.windowManager.hyprland;
in
{
  options.wayland.windowManager.hyprland = {
    terminal = mkOption {
      description = "command to spawn a new terminal with the command";
      type = functionTo str;
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; mkIf cfg.enable [
      grim
      qt6.qtwayland
      libsForQt5.qt5.qtwayland
    ];

    wayland.windowManager.hyprland = {
      systemd.variables = [
        "DISPLAY"
        "WAYLAND_DISPLAY"
        "HYPRLAND_INSTANCE_SIGNATURE"
        "XDG_CURRENT_DESKTOP"
        "XDG_SESSION_TYPE"
      ];

      # TODO(samuelsung): home.pointCursor
      settings.env =
        let
          inherit (config.gtk) cursorTheme;
        in
        [
          "XCURSOR_THEME,${cursorTheme.name}"
          "XCURSOR_SIZE,${toString cursorTheme.size}"
        ];

      settings.monitor = map
        ({ name, h, v, x, y, refreshRate, rotation, ... }:
          concatStringsSep "," [
            name.w
            "${toString h}x${toString v}@${toString refreshRate.w}"
            "${toString x}x${toString y}"
            "1"
            "transform"
            (toString (rotation / 90))
          ]
        )
        osConfig.hardware.video.monitors;
    };
  };
}
