{
  flake.homeManagerModules.desktop-fonts = ./fonts.nix;
  flake.homeManagerModules.desktop-fonts-samuelsung = ./fonts-samuelsung.nix;
}
