{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
in
mkIf config.desktop.enable {
  home.packages = with pkgs; [
    fira-code
    recursive
    (nerdfonts.override {
      fonts = [ "FiraCode" ];
    })
  ];
}
