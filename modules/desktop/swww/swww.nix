{ config, lib, pkgs, ... }:

let
  inherit (lib) makeBinPath mkEnableOption mkIf mkMerge mkOption types;
  inherit (types) lines package str;
  cfg = config.services.swww;
in
{
  options.services.swww = {
    enable = mkEnableOption "swww";

    package = mkOption {
      default = pkgs.swww;
      type = package;
      description = "package of swww";
    };

    systemd.enable = mkEnableOption "systemd support for swww";

    systemd.target = mkOption {
      type = str;
      default = "graphical-session.target";
      example = "sway-session.target";
      description = "the systemd target that will automatically start the service";
    };

    random-wallpaper-systemd.enable = mkEnableOption
      "systemd service for refreshing wallpaper using wallpapers in desktop.wallpaper";

    random-wallpaper-systemd.monitorsScript = mkOption {
      type = lines;
      description = "systemd service for refreshing wallpaper using wallpapers in desktop.wallpaper";
    };

    random-wallpaper-systemd.script = mkOption {
      type = package;
      default = pkgs.writeShellScriptBin "swww-random-wallpaper" ''
        PATH=${makeBinPath [ config.desktop.wallpapers.random-wallpaper cfg.package pkgs.coreutils ]}:$PATH
        get_monitors() {
          ${cfg.random-wallpaper-systemd.monitorsScript}
        }

        monitors=$(get_monitors) || exit 1
        wallpapers=$(random-wallpaper "$(echo "$monitors" | wc -l)") || exit 1

        paste \
        <(echo "$wallpapers") \
        <(echo "$monitors") \
          | while read wallpaper monitor; do {
            swww img -o "$monitor" "$wallpaper" --transition-type any --transition-duration 2 --transition-fps 144 --transition-step 90
            sleep 2s
          }; done
      '';
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      home.packages = [ cfg.package ];
    })

    (mkIf (cfg.enable && cfg.systemd.enable) {
      systemd.user.services.swww = {
        Unit = {
          Description = cfg.package.meta.description or pkgs.swww.meta.description;
          After = [ "graphical-session-pre.target" ];
          PartOf = [ "graphical-session.target" ];
        };

        Install.WantedBy = [ cfg.systemd.target ];

        Service = {
          ExecStart = "${cfg.package}/bin/swww-daemon";
          Restart = "always";
          RestartSec = 3;
        };
      };
    })

    (mkIf (cfg.enable && cfg.random-wallpaper-systemd.enable) {
      systemd.user.services."swww-random-wallpaper" = {
        Unit = {
          Description = "swww-random-wallpaper";
          After = [ "swww.service" "graphical-session-pre.target" ];
          PartOf = [ "graphical-session.target" ];
        };
        Install.WantedBy = [ cfg.systemd.target ];
        Service = {
          Type = "oneshot";
          ExecStart = "${cfg.random-wallpaper-systemd.script}/bin/swww-random-wallpaper";
          Restart = "on-failure";
          RestartSec = 1;
        };
      };

      systemd.user.timers."swww-random-wallpaper" = {
        Unit = {
          Description = "swww-random-wallpaper";
        };
        Timer = {
          Unit = "swww-random-wallpaper.service";
          OnUnitInactiveSec = "5min";
        };
        Install.WantedBy = [ "timers.target" ];
      };
    })
  ];
}
