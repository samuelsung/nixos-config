{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.desktop-polybar = ./polybar.nix;
  flake.homeManagerModules.desktop-polybar-samuelsung = importApply
    ./polybar-samuelsung.nix
    { inherit colors; };
}
