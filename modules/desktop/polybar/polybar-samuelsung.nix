localFlake:

{ config, pkgs, ... }:

let
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
  inherit (config.xsession.windowManager) xmonad;
in
{
  services.polybar.settings = {
    "bar/primaryBar" = {
      monitor = "\${env:monitor:}";
      monitor-strict = false;
      monitor-exact = true;
      override-redirect = false;
      bottom = false;
      fixed-center = true;
      width = "100%";
      height = 21;
      offset-x = 0;
      offset-y = 0;
      background = "\${colors.mainbackground}";
      foreground = "\${colors.mainforeground}";
      radius = "0.0";
      line-size = 1;
      border-left-size = xmonad.visuals.padding.width * 2;
      border-right-size = xmonad.visuals.padding.width * 2;
      border-top-size = xmonad.visuals.padding.width * 2;
      border-color = "\${colors.trans}";
      padding = 1;
      module-margin = 0;
      modules-left = "workspace";
      modules-center = "date";
      modules-right = "battery memory cpu pulseaudio";
      separator = " | ";
      spacing = 0;
      dim-value = "1.0";
      tray-position = "\${env:TRAY_POSITION:right}";
      tray-detached = false;
      tray-maxsize = 16;
      tray-background = "\${colors.mainbackground}";
      tray-offset-x = 0;
      tray-offset-y = 0;
      tray-padding = 0;
      tray-scale = "1.0";
      wm-restack = "generic";
      dpi-x = 96;
      dpi-y = 96;
      enable-ipc = false;

      font = [
        "Fira Code:size=10:style=Medium;2"
        "FiraCode Nerd Font Mono:size=20:style=Regular;4"
        "Noto Sans Mono CJK JP:size=10:style=Regular;1"
        "Noto Color Emoji:scale=10;2"
      ];
    };

    colors = {
      mainbackground = "#D0${hexOf catppuccin.mocha.base}";
      mainforeground = "#${hexOf catppuccin.mocha.text}";
      trans = "#00000000";
    };

    "global/wm" = {
      margin-bottom = 0;
      margin-top = 0;
    };

    "settings" = {
      throttle-output = 5;
      throttle-output-for = 10;
      throttle-input-for = 30;
      screenchange-reload = false;
      compositing-background = "source";
      compositing-foreground = "over";
      compositing-overline = "over";
      compositing-underline = "over";
      compositing-border = "over";
      pseudo-transparency = false;
    };

    "module/battery" = {
      type = "internal/battery";
      full-at = 99;
      battery = "BAT0";
      adapter = "AC";
      poll-interval = 5;
      time-format = "%H:%M";
      format-charging = "<animation-charging> <label-charging>";
      format-discharging = "<ramp-capacity> <label-discharging>";
      label-charging = "Charging %percentage%%";
      label-discharging = "Discharging %percentage%%";
      label-full = "Fully charged";
      ramp-capacity = [ "" "" "" "" "" ];
      bar-capacity-width = 10;
      animation-charging = [ "" "" "" "" "" ];
      animation-charging-framerate = 750;
      animation-discharging = [ "" "" "" "" "" ];
      animation-discharging-framerate = 500;
    };

    "module/cpu" = {
      type = "internal/cpu";
      interval = "0.5";
      format = "<label>";
      label = "CPU %percentage%%";
    };

    "module/date" = {
      type = "internal/date";
      interval = "1.0";
      date = "%Y %b %d";
      time = "%I:%M";
      date-alt = "%Y %b %d (%a)";
      time-alt = "%I:%M:%S%p";
      format = "<label>";
      label = "%date% %time%";
    };

    "module/memory" = {
      type = "internal/memory";
      interval = 3;
      format = "<label>";
      label = "RAM %gb_used%/%gb_total%";
    };

    "module/pulseaudio" = {
      type = "internal/pulseaudio";
      use-ui-max = false;
      interval = 5;
      format-volume = "<ramp-volume> <label-volume>";
      format-muted = "<label-muted>";
      label-muted = "🔇 muted";
      ramp-volume = [ "🔈" "🔉" "🔊" ];
    };

    "module/workspace" = {
      type = "custom/script";
      exec = "${pkgs.xmonad-ws}/bin/xmonad-ws";
      tail = true;
    };
  };
}
