{ config, lib, pkgs, ... }:

let
  inherit (lib) makeBinPath mkForce mkIf mkOption types;
  cfg = config.services.polybar;

  launchPolybarScript = pkgs.writeShellScript "launch-polybar" ''
    PATH=${makeBinPath (with pkgs; [
      coreutils
      gnugrep
      xorg.xrandr
    ])}''${PATH:+:}$PATH

    pm=$(xrandr --query | grep " connected" | grep "primary" | cut -d" " -f1)
    npm=$(xrandr --query | grep " connected" | grep -v "primary" | cut -d" " -f1)

    if [[ -v pm ]]; then
      echo "Primary monitor is set"

      for monitor in $pm; do
        # echo "---" | tee -a /tmp/polybar.log #/tmp/polybar2.log /tmp/polybar3.log
        TRAY_POSITION=right monitor=$monitor polybar primaryBar &
        # >>/tmp/polybar.log 2>&1 &
      done

      for monitor in $npm; do
        # echo "---" | tee -a /tmp/polybar.log #/tmp/polybar2.log /tmp/polybar3.log
        TRAY_POSITION=none monitor=$monitor polybar primaryBar &
        #>>/tmp/polybar.log 2>&1 &
      done
    else
      echo "Primary monitor is not set"

      for monitor in $npm; do
        echo "---" | tee -a /tmp/polybar.log #/tmp/polybar2.log /tmp/polybar3.log
        TRAY_POSITION=right monitor=$monitor polybar primaryBar &
        # >>/tmp/polybar.log 2>&1 &
      done
    fi

    echo "Bars launched..."
  '';
in
{
  options.services.polybar = {
    wantedBy = mkOption {
      type = with types; listOf str;
      default = [ ];
      description = ''
        list of services/targets that that trigger polybar's systemd service
      '';
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed when enabling polybar
        '';
      }
    ];

    services.polybar = {
      package = pkgs.polybarFull;

      script = "${launchPolybarScript}";
    };

    systemd.user.services.polybar.Install.WantedBy = mkForce cfg.wantedBy;
  };
}
