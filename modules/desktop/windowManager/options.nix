{ lib, wm-lib, ... }:

let
  inherit (lib) mkOption types;
  inherit (wm-lib.consts)
    modmasks;
  inherit (wm-lib.shortcuts.types)
    key
    mouseKey
    shortcutOf;
in
{
  options.windowManager = {
    mod = mkOption {
      type = types.enum modmasks;
      default = "Mod4Mask";
      description = ''
        modKey of the wm
      '';
    };

    # see xorg.xorgproto.src /include/X11/keysymdef.h
    mouseShortcuts = mkOption {
      type = types.attrsOf (shortcutOf mouseKey types.str);
      default = { };
      description = "wm's mouse shortcuts";
    };

    # see xorg.xorgproto.src /include/X11/keysymdef.h
    shortcuts = mkOption {
      type = types.attrsOf (shortcutOf key types.str);
      default = { };
      description = "wm's keyboard shortcuts";
    };
  };
}
