{ config, pkgs, ... }:

let
  cfg = config.windowManager;
in
{
  windowManager = {
    mod = "Mod4Mask";

    shortcuts = {
      pulsemixer = {
        action = "${pkgs.pulsemixer}/bin/pulsemixer";
        inTerminal = true;
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_a"; }
        ];
      };

      notes = {
        action = "${pkgs.joplin-desktop}/bin/joplin-desktop";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_n"; }
        ];
      };

      newsboat = {
        action = "${pkgs.newsboat}/bin/newsboat";
        inTerminal = true;
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_r"; }
        ];
      };

      vifm = {
        action = "vifm $HOME";
        inTerminal = true;
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_f"; }
        ];
      };

      qutebrowser = {
        action = "${config.programs.qutebrowser.package}/bin/qutebrowser";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_w"; }
        ];
      };

      dmenuEmoji = {
        action = "dmenu-emoji";
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_e"; }
        ];
      };

      ncmpcpp = {
        action = "${pkgs.ncmpcpp}/bin/ncmpcpp";
        inTerminal = true;
        keys = [
          { mods = [ cfg.mod ]; key = "XK_a"; }
          { key = "XK_m"; }
        ];
      };
    };
  };
}
