{ lib, samuelsung-lib, wm-lib, ... }:

let
  inherit (lib) mkEnableOption mkOption sort;
  inherit (lib.types) enum listOf submodule;
  inherit (wm-lib.consts)
    # see xorg.xorgproto.src /include/X11/keysymdef.h
    modmasks mouseButtons keySymDefs xf86KeySyms;
  inherit (samuelsung-lib.tree) listToTree;
in
{
  shortcutsToTrees = listToTree
    ({ keys, ... }: keys)
    (a: b:
      let sortMod = sort (c: d: c < d); in
      a.key == b.key && (sortMod a.mods) == (sortMod b.mods));

  types.mouseKey = submodule {
    options.mods = mkOption {
      default = [ ];
      type = listOf (enum modmasks);
    };

    options.key = mkOption {
      type = enum mouseButtons;
    };
  };

  types.key = submodule {
    options.mods = mkOption {
      default = [ ];
      type = listOf (enum modmasks);
    };

    options.key = mkOption {
      type = enum (keySymDefs ++ xf86KeySyms);
    };
  };

  types.shortcutOf = keyType: actionType: submodule {
    options.action = mkOption {
      type = actionType;
    };

    options.inTerminal = mkEnableOption "run command in terminal";

    options.keys = mkOption {
      type = listOf keyType;
    };
  };
}
