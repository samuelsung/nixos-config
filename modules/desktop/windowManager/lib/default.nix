{ lib, samuelsung-lib, wm-lib, ... }:

{
  _module.args.wm-lib = {
    consts = import ./consts;
    shortcuts = import ./shortcuts.nix {
      inherit lib samuelsung-lib wm-lib;
    };
  };
}
