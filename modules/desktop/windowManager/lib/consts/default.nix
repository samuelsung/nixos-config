{
  actions = import ./actions.nix;
  keySymDefs = import ./keysymdef.nix;
  xf86KeySyms = import ./XF86keysym.nix;
  mouseButtons = import ./mouseButton.nix;
  modmasks = import ./modmask.nix;
}
