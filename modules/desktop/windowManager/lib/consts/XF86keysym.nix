[
  # 0x1008FF01 # Mode Switch Lock
  "XF86XK_ModeLock"

  # 0x1008FF02 # Monitor/panel brightness
  "XF86XK_MonBrightnessUp"

  # 0x1008FF03 # Monitor/panel brightness
  "XF86XK_MonBrightnessDown"

  # 0x1008FF04 # Keyboards may be lit
  "XF86XK_KbdLightOnOff"

  # 0x1008FF05 # Keyboards may be lit
  "XF86XK_KbdBrightnessUp"

  # 0x1008FF06 # Keyboards may be lit
  "XF86XK_KbdBrightnessDown"

  # 0x1008FF07 # Monitor/panel brightness
  "XF86XK_MonBrightnessCycle"

  # 0x1008FF10 # System into standby mode
  "XF86XK_Standby"

  # 0x1008FF11 # Volume control down
  "XF86XK_AudioLowerVolume"

  # 0x1008FF12 # Mute sound from the system
  "XF86XK_AudioMute"

  # 0x1008FF13 # Volume control up
  "XF86XK_AudioRaiseVolume"

  # 0x1008FF14 # Start playing of audio >
  "XF86XK_AudioPlay"

  # 0x1008FF15 # Stop playing audio
  "XF86XK_AudioStop"

  # 0x1008FF16 # Previous track
  "XF86XK_AudioPrev"

  # 0x1008FF17 # Next track
  "XF86XK_AudioNext"

  # 0x1008FF18 # Display user's home page
  "XF86XK_HomePage"

  # 0x1008FF19 # Invoke user's mail program
  "XF86XK_Mail"

  # 0x1008FF1A # Start application
  "XF86XK_Start"

  # 0x1008FF1B # Search
  "XF86XK_Search"

  # 0x1008FF1C # Record audio application
  "XF86XK_AudioRecord"

  # 0x1008FF1D # Invoke calculator program
  "XF86XK_Calculator"

  # 0x1008FF1E # Invoke Memo taking program
  "XF86XK_Memo"

  # 0x1008FF1F # Invoke To Do List program
  "XF86XK_ToDoList"

  # 0x1008FF20 # Invoke Calendar program
  "XF86XK_Calendar"

  # 0x1008FF21 # Deep sleep the system
  "XF86XK_PowerDown"

  # 0x1008FF22 # Adjust screen contrast
  "XF86XK_ContrastAdjust"

  # 0x1008FF23 # Rocker switches exist up
  "XF86XK_RockerUp"

  # 0x1008FF24 # and down
  "XF86XK_RockerDown"

  # 0x1008FF25 # and let you press them
  "XF86XK_RockerEnter"

  # 0x1008FF26 # Like back on a browser
  "XF86XK_Back"

  # 0x1008FF27 # Like forward on a browser
  "XF86XK_Forward"

  # 0x1008FF28 # Stop current operation
  "XF86XK_Stop"

  # 0x1008FF29 # Refresh the page
  "XF86XK_Refresh"

  # 0x1008FF2A # Power off system entirely
  "XF86XK_PowerOff"

  # 0x1008FF2B # Wake up system from sleep
  "XF86XK_WakeUp"

  # 0x1008FF2C # Eject device (e.g. DVD)
  "XF86XK_Eject"

  # 0x1008FF2D # Invoke screensaver
  "XF86XK_ScreenSaver"

  # 0x1008FF2E # Invoke web browser
  "XF86XK_WWW"

  # 0x1008FF2F # Put system to sleep
  "XF86XK_Sleep"

  # 0x1008FF30 # Show favorite locations
  "XF86XK_Favorites"

  # 0x1008FF31 # Pause audio playing
  "XF86XK_AudioPause"

  # 0x1008FF32 # Launch media collection app
  "XF86XK_AudioMedia"

  # 0x1008FF33 # Display "My Computer" window
  "XF86XK_MyComputer"

  # 0x1008FF34 # Display vendor home web site
  "XF86XK_VendorHome"

  # 0x1008FF35 # Light bulb keys exist
  "XF86XK_LightBulb"

  # 0x1008FF36 # Display shopping web site
  "XF86XK_Shop"

  # 0x1008FF37 # Show history of web surfing
  "XF86XK_History"

  # 0x1008FF38 # Open selected URL
  "XF86XK_OpenURL"

  # 0x1008FF39 # Add URL to favorites list
  "XF86XK_AddFavorite"

  # 0x1008FF3A # Show "hot" links
  "XF86XK_HotLinks"

  # 0x1008FF3B # Invoke brightness adj. UI
  "XF86XK_BrightnessAdjust"

  # 0x1008FF3C # Display financial site
  "XF86XK_Finance"

  # 0x1008FF3D # Display user's community
  "XF86XK_Community"

  # 0x1008FF3E # "rewind" audio track
  "XF86XK_AudioRewind"

  # 0x1008FF3F # ???
  "XF86XK_BackForward"

  # 0x1008FF40 # Launch Application
  "XF86XK_Launch0"

  # 0x1008FF41 # Launch Application
  "XF86XK_Launch1"

  # 0x1008FF42 # Launch Application
  "XF86XK_Launch2"

  # 0x1008FF43 # Launch Application
  "XF86XK_Launch3"

  # 0x1008FF44 # Launch Application
  "XF86XK_Launch4"

  # 0x1008FF45 # Launch Application
  "XF86XK_Launch5"

  # 0x1008FF46 # Launch Application
  "XF86XK_Launch6"

  # 0x1008FF47 # Launch Application
  "XF86XK_Launch7"

  # 0x1008FF48 # Launch Application
  "XF86XK_Launch8"

  # 0x1008FF49 # Launch Application
  "XF86XK_Launch9"

  # 0x1008FF4A # Launch Application
  "XF86XK_LaunchA"

  # 0x1008FF4B # Launch Application
  "XF86XK_LaunchB"

  # 0x1008FF4C # Launch Application
  "XF86XK_LaunchC"

  # 0x1008FF4D # Launch Application
  "XF86XK_LaunchD"

  # 0x1008FF4E # Launch Application
  "XF86XK_LaunchE"

  # 0x1008FF4F # Launch Application
  "XF86XK_LaunchF"

  # 0x1008FF50 # switch to application, left
  "XF86XK_ApplicationLeft"

  # 0x1008FF51 # switch to application, right
  "XF86XK_ApplicationRight"

  # 0x1008FF52 # Launch bookreader
  "XF86XK_Book"

  # 0x1008FF53 # Launch CD/DVD player
  "XF86XK_CD"

  # 0x1008FF54 # Launch Calculater
  "XF86XK_Calculater"

  # 0x1008FF55 # Clear window, screen
  "XF86XK_Clear"

  # 0x1008FF56 # Close window
  "XF86XK_Close"

  # 0x1008FF57 # Copy selection
  "XF86XK_Copy"

  # 0x1008FF58 # Cut selection
  "XF86XK_Cut"

  # 0x1008FF59 # Output switch key
  "XF86XK_Display"

  # 0x1008FF5A # Launch DOS (emulation)
  "XF86XK_DOS"

  # 0x1008FF5B # Open documents window
  "XF86XK_Documents"

  # 0x1008FF5C # Launch spread sheet
  "XF86XK_Excel"

  # 0x1008FF5D # Launch file explorer
  "XF86XK_Explorer"

  # 0x1008FF5E # Launch game
  "XF86XK_Game"

  # 0x1008FF5F # Go to URL
  "XF86XK_Go"

  # 0x1008FF60 # Logitech iTouch- don't use
  "XF86XK_iTouch"

  # 0x1008FF61 # Log off system
  "XF86XK_LogOff"

  # 0x1008FF62 # ??
  "XF86XK_Market"

  # 0x1008FF63 # enter meeting in calendar
  "XF86XK_Meeting"

  # 0x1008FF65 # distinguish keyboard from PB
  "XF86XK_MenuKB"

  # 0x1008FF66 # distinguish PB from keyboard
  "XF86XK_MenuPB"

  # 0x1008FF67 # Favourites
  "XF86XK_MySites"

  # 0x1008FF68 # New (folder, document...
  "XF86XK_New"

  # 0x1008FF69 # News
  "XF86XK_News"

  # 0x1008FF6A # Office home (old Staroffice)
  "XF86XK_OfficeHome"

  # 0x1008FF6B # Open
  "XF86XK_Open"

  # 0x1008FF6C # ??
  "XF86XK_Option"

  # 0x1008FF6D # Paste
  "XF86XK_Paste"

  # 0x1008FF6E # Launch phone; dial number
  "XF86XK_Phone"

  # 0x1008FF70 # Compaq's Q - don't use
  "XF86XK_Q"

  # 0x1008FF72 # Reply e.g., mail
  "XF86XK_Reply"

  # 0x1008FF73 # Reload web page, file, etc.
  "XF86XK_Reload"

  # 0x1008FF74 # Rotate windows e.g. xrandr
  "XF86XK_RotateWindows"

  # 0x1008FF75 # don't use
  "XF86XK_RotationPB"

  # 0x1008FF76 # don't use
  "XF86XK_RotationKB"

  # 0x1008FF77 # Save (file, document, state
  "XF86XK_Save"

  # 0x1008FF78 # Scroll window/contents up
  "XF86XK_ScrollUp"

  # 0x1008FF79 # Scrool window/contentd down
  "XF86XK_ScrollDown"

  # 0x1008FF7A # Use XKB mousekeys instead
  "XF86XK_ScrollClick"

  # 0x1008FF7B # Send mail, file, object
  "XF86XK_Send"

  # 0x1008FF7C # Spell checker
  "XF86XK_Spell"

  # 0x1008FF7D # Split window or screen
  "XF86XK_SplitScreen"

  # 0x1008FF7E # Get support (??)
  "XF86XK_Support"

  # 0x1008FF7F # Show tasks
  "XF86XK_TaskPane"

  # 0x1008FF80 # Launch terminal emulator
  "XF86XK_Terminal"

  # 0x1008FF81 # toolbox of desktop/app.
  "XF86XK_Tools"

  # 0x1008FF82 # ??
  "XF86XK_Travel"

  # 0x1008FF84 # ??
  "XF86XK_UserPB"

  # 0x1008FF85 # ??
  "XF86XK_User1KB"

  # 0x1008FF86 # ??
  "XF86XK_User2KB"

  # 0x1008FF87 # Launch video player
  "XF86XK_Video"

  # 0x1008FF88 # button from a mouse wheel
  "XF86XK_WheelButton"

  # 0x1008FF89 # Launch word processor
  "XF86XK_Word"

  # 0x1008FF8A
  "XF86XK_Xfer"

  # 0x1008FF8B # zoom in view, map, etc.
  "XF86XK_ZoomIn"

  # 0x1008FF8C # zoom out view, map, etc.
  "XF86XK_ZoomOut"

  # 0x1008FF8D # mark yourself as away
  "XF86XK_Away"

  # 0x1008FF8E # as in instant messaging
  "XF86XK_Messenger"

  # 0x1008FF8F # Launch web camera app.
  "XF86XK_WebCam"

  # 0x1008FF90 # Forward in mail
  "XF86XK_MailForward"

  # 0x1008FF91 # Show pictures
  "XF86XK_Pictures"

  # 0x1008FF92 # Launch music application
  "XF86XK_Music"

  # 0x1008FF93 # Display battery information
  "XF86XK_Battery"

  # 0x1008FF94 # Enable/disable Bluetooth
  "XF86XK_Bluetooth"

  # 0x1008FF95 # Enable/disable WLAN
  "XF86XK_WLAN"

  # 0x1008FF96 # Enable/disable UWB
  "XF86XK_UWB"

  # 0x1008FF97 # fast-forward audio track
  "XF86XK_AudioForward"

  # 0x1008FF98 # toggle repeat mode
  "XF86XK_AudioRepeat"

  # 0x1008FF99 # toggle shuffle mode
  "XF86XK_AudioRandomPlay"

  # 0x1008FF9A # cycle through subtitle
  "XF86XK_Subtitle"

  # 0x1008FF9B # cycle through audio tracks
  "XF86XK_AudioCycleTrack"

  # 0x1008FF9C # cycle through angles
  "XF86XK_CycleAngle"

  # 0x1008FF9D # video: go one frame back
  "XF86XK_FrameBack"

  # 0x1008FF9E # video: go one frame forward
  "XF86XK_FrameForward"

  # 0x1008FF9F # display, or shows an entry for time seeking
  "XF86XK_Time"

  # 0x1008FFA0 # Select button on joypads and remotes
  "XF86XK_Select"

  # 0x1008FFA1 # Show a view options/properties
  "XF86XK_View"

  # 0x1008FFA2 # Go to a top-level menu in a video
  "XF86XK_TopMenu"

  # 0x1008FFA3 # Red button
  "XF86XK_Red"

  # 0x1008FFA4 # Green button
  "XF86XK_Green"

  # 0x1008FFA5 # Yellow button
  "XF86XK_Yellow"

  # 0x1008FFA6 # Blue button
  "XF86XK_Blue"

  # 0x1008FFA7 # Sleep to RAM
  "XF86XK_Suspend"

  # 0x1008FFA8 # Sleep to disk
  "XF86XK_Hibernate"

  # 0x1008FFA9 # Toggle between touchpad/trackstick
  "XF86XK_TouchpadToggle"

  # 0x1008FFB0 # The touchpad got switched on
  "XF86XK_TouchpadOn"

  # 0x1008FFB1 # The touchpad got switched off
  "XF86XK_TouchpadOff"

  # 0x1008FFB2 # Mute the Mic from the system
  "XF86XK_AudioMicMute"

  # 0x1008FFB3 # User defined keyboard related action
  "XF86XK_Keyboard"

  # 0x1008FFB4 # Toggle WWAN (LTE, UMTS, etc.) radio
  "XF86XK_WWAN"

  # 0x1008FFB5 # Toggle radios on/off
  "XF86XK_RFKill"

  # 0x1008FFB6 # Select equalizer preset, e.g. theatre-mode
  "XF86XK_AudioPreset"

  # 0x1008FFB7 # Toggle screen rotation lock on/off
  "XF86XK_RotationLockToggle"

  # 0x1008FFB8 # Toggle fullscreen
  "XF86XK_FullScreen"

  # 0x1008FE01
  "XF86XK_Switch_VT_1"

  # 0x1008FE02
  "XF86XK_Switch_VT_2"

  # 0x1008FE03
  "XF86XK_Switch_VT_3"

  # 0x1008FE04
  "XF86XK_Switch_VT_4"

  # 0x1008FE05
  "XF86XK_Switch_VT_5"

  # 0x1008FE06
  "XF86XK_Switch_VT_6"

  # 0x1008FE07
  "XF86XK_Switch_VT_7"

  # 0x1008FE08
  "XF86XK_Switch_VT_8"

  # 0x1008FE09
  "XF86XK_Switch_VT_9"

  # 0x1008FE0A
  "XF86XK_Switch_VT_10"

  # 0x1008FE0B
  "XF86XK_Switch_VT_11"

  # 0x1008FE0C
  "XF86XK_Switch_VT_12"

  # 0x1008FE20 # force ungrab
  "XF86XK_Ungrab"

  # 0x1008FE21 # kill application with grab
  "XF86XK_ClearGrab"

  # 0x1008FE22 # next video mode available
  "XF86XK_Next_VMode"

  # 0x1008FE23 # prev. video mode available
  "XF86XK_Prev_VMode"

  # 0x1008FE24 # print window tree to log
  "XF86XK_LogWindowTree"

  # 0x1008FE25 # print all active grabs to log
  "XF86XK_LogGrabInfo"

  # _EVDEVK(0x0F4) # v3.16 KEY_BRIGHTNESS_AUTO
  "XF86XK_BrightnessAuto"

  # _EVDEVK(0x0F5) # v2.6.23 KEY_DISPLAY_OFF
  "XF86XK_DisplayOff"

  # _EVDEVK(0x166) # KEY_INFO
  "XF86XK_Info"

  # _EVDEVK(0x177) # v5.1  KEY_ASPECT_RATIO
  "XF86XK_AspectRatio"

  # _EVDEVK(0x185) # KEY_DVD
  "XF86XK_DVD"

  # _EVDEVK(0x188) # KEY_AUDIO
  "XF86XK_Audio"

  # _EVDEVK(0x192) # KEY_CHANNELUP
  "XF86XK_ChannelUp"

  # _EVDEVK(0x193) # KEY_CHANNELDOWN
  "XF86XK_ChannelDown"

  # _EVDEVK(0x19B) # KEY_BREAK
  "XF86XK_Break"

  # _EVDEVK(0x1A0) # v2.6.20 KEY_VIDEOPHONE
  "XF86XK_VideoPhone"

  # _EVDEVK(0x1A4) # v2.6.20 KEY_ZOOMRESET
  "XF86XK_ZoomReset"

  # _EVDEVK(0x1A6) # v2.6.20 KEY_EDITOR
  "XF86XK_Editor"

  # _EVDEVK(0x1A8) # v2.6.20 KEY_GRAPHICSEDITOR
  "XF86XK_GraphicsEditor"

  # _EVDEVK(0x1A9) # v2.6.20 KEY_PRESENTATION
  "XF86XK_Presentation"

  # _EVDEVK(0x1AA) # v2.6.20 KEY_DATABASE
  "XF86XK_Database"

  # _EVDEVK(0x1AC) # v2.6.20 KEY_VOICEMAIL
  "XF86XK_Voicemail"

  # _EVDEVK(0x1AD) # v2.6.20 KEY_ADDRESSBOOK
  "XF86XK_Addressbook"

  # _EVDEVK(0x1AF) # v2.6.20 KEY_DISPLAYTOGGLE
  "XF86XK_DisplayToggle"

  # _EVDEVK(0x1B0) # v2.6.24 KEY_SPELLCHECK
  "XF86XK_SpellCheck"

  # _EVDEVK(0x1B6) # v2.6.24 KEY_CONTEXT_MENU
  "XF86XK_ContextMenu"

  # _EVDEVK(0x1B7) # v2.6.26 KEY_MEDIA_REPEAT
  "XF86XK_MediaRepeat"

  # _EVDEVK(0x1B8) # v2.6.38 KEY_10CHANNELSUP
  "XF86XK_10ChannelsUp"

  # _EVDEVK(0x1B9) # v2.6.38 KEY_10CHANNELSDOWN
  "XF86XK_10ChannelsDown"

  # _EVDEVK(0x1BA) # v2.6.39 KEY_IMAGES
  "XF86XK_Images"

  # _EVDEVK(0x1BC) # v5.10 KEY_NOTIFICATION_CENTER
  "XF86XK_NotificationCenter"

  # _EVDEVK(0x1BD) # v5.10 KEY_PICKUP_PHONE
  "XF86XK_PickupPhone"

  # _EVDEVK(0x1BE) # v5.10 KEY_HANGUP_PHONE
  "XF86XK_HangupPhone"

  # _EVDEVK(0x1D0) # KEY_FN
  "XF86XK_Fn"

  # _EVDEVK(0x1D1) # KEY_FN_ESC
  "XF86XK_Fn_Esc"

  # _EVDEVK(0x1E5) # v5.10 KEY_FN_RIGHT_SHIFT
  "XF86XK_FnRightShift"

  # _EVDEVK(0x200) # v2.6.28 KEY_NUMERIC_0
  "XF86XK_Numeric0"

  # _EVDEVK(0x201) # v2.6.28 KEY_NUMERIC_1
  "XF86XK_Numeric1"

  # _EVDEVK(0x202) # v2.6.28 KEY_NUMERIC_2
  "XF86XK_Numeric2"

  # _EVDEVK(0x203) # v2.6.28 KEY_NUMERIC_3
  "XF86XK_Numeric3"

  # _EVDEVK(0x204) # v2.6.28 KEY_NUMERIC_4
  "XF86XK_Numeric4"

  # _EVDEVK(0x205) # v2.6.28 KEY_NUMERIC_5
  "XF86XK_Numeric5"

  # _EVDEVK(0x206) # v2.6.28 KEY_NUMERIC_6
  "XF86XK_Numeric6"

  # _EVDEVK(0x207) # v2.6.28 KEY_NUMERIC_7
  "XF86XK_Numeric7"

  # _EVDEVK(0x208) # v2.6.28 KEY_NUMERIC_8
  "XF86XK_Numeric8"

  # _EVDEVK(0x209) # v2.6.28 KEY_NUMERIC_9
  "XF86XK_Numeric9"

  # _EVDEVK(0x20A) # v2.6.28 KEY_NUMERIC_STAR
  "XF86XK_NumericStar"

  # _EVDEVK(0x20B) # v2.6.28 KEY_NUMERIC_POUND
  "XF86XK_NumericPound"

  # _EVDEVK(0x20C) # v4.1  KEY_NUMERIC_A
  "XF86XK_NumericA"

  # _EVDEVK(0x20D) # v4.1  KEY_NUMERIC_B
  "XF86XK_NumericB"

  # _EVDEVK(0x20E) # v4.1  KEY_NUMERIC_C
  "XF86XK_NumericC"

  # _EVDEVK(0x20F) # v4.1  KEY_NUMERIC_D
  "XF86XK_NumericD"

  # _EVDEVK(0x210) # v2.6.33 KEY_CAMERA_FOCUS
  "XF86XK_CameraFocus"

  # _EVDEVK(0x211) # v2.6.34 KEY_WPS_BUTTON
  "XF86XK_WPSButton"

  # _EVDEVK(0x215) # v2.6.39 KEY_CAMERA_ZOOMIN
  "XF86XK_CameraZoomIn"

  # _EVDEVK(0x216) # v2.6.39 KEY_CAMERA_ZOOMOUT
  "XF86XK_CameraZoomOut"

  # _EVDEVK(0x217) # v2.6.39 KEY_CAMERA_UP
  "XF86XK_CameraUp"

  # _EVDEVK(0x218) # v2.6.39 KEY_CAMERA_DOWN
  "XF86XK_CameraDown"

  # _EVDEVK(0x219) # v2.6.39 KEY_CAMERA_LEFT
  "XF86XK_CameraLeft"

  # _EVDEVK(0x21A) # v2.6.39 KEY_CAMERA_RIGHT
  "XF86XK_CameraRight"

  # _EVDEVK(0x21B) # v3.10 KEY_ATTENDANT_ON
  "XF86XK_AttendantOn"

  # _EVDEVK(0x21C) # v3.10 KEY_ATTENDANT_OFF
  "XF86XK_AttendantOff"

  # _EVDEVK(0x21D) # v3.10 KEY_ATTENDANT_TOGGLE
  "XF86XK_AttendantToggle"

  # _EVDEVK(0x21E) # v3.10 KEY_LIGHTS_TOGGLE
  "XF86XK_LightsToggle"

  # _EVDEVK(0x230) # v3.13 KEY_ALS_TOGGLE
  "XF86XK_ALSToggle"

  # _EVDEVK(0x240) # v3.16 KEY_BUTTONCONFIG
  "XF86XK_Buttonconfig"

  # _EVDEVK(0x241) # v3.16 KEY_TASKMANAGER
  "XF86XK_Taskmanager"

  # _EVDEVK(0x242) # v3.16 KEY_JOURNAL
  "XF86XK_Journal"

  # _EVDEVK(0x243) # v3.16 KEY_CONTROLPANEL
  "XF86XK_ControlPanel"

  # _EVDEVK(0x244) # v3.16 KEY_APPSELECT
  "XF86XK_AppSelect"

  # _EVDEVK(0x245) # v3.16 KEY_SCREENSAVER
  "XF86XK_Screensaver"

  # _EVDEVK(0x246) # v3.16 KEY_VOICECOMMAND
  "XF86XK_VoiceCommand"

  # _EVDEVK(0x247) # v4.13 KEY_ASSISTANT
  "XF86XK_Assistant"

  # _EVDEVK(0x250) # v3.16 KEY_BRIGHTNESS_MIN
  "XF86XK_BrightnessMin"

  # _EVDEVK(0x251) # v3.16 KEY_BRIGHTNESS_MAX
  "XF86XK_BrightnessMax"

  # _EVDEVK(0x260) # v3.18 KEY_KBDINPUTASSIST_PREV
  "XF86XK_KbdInputAssistPrev"

  # _EVDEVK(0x261) # v3.18 KEY_KBDINPUTASSIST_NEXT
  "XF86XK_KbdInputAssistNext"

  # _EVDEVK(0x262) # v3.18 KEY_KBDINPUTASSIST_PREVGROUP
  "XF86XK_KbdInputAssistPrevgroup"

  # _EVDEVK(0x263) # v3.18 KEY_KBDINPUTASSIST_NEXTGROUP
  "XF86XK_KbdInputAssistNextgroup"

  # _EVDEVK(0x264) # v3.18 KEY_KBDINPUTASSIST_ACCEPT
  "XF86XK_KbdInputAssistAccept"

  # _EVDEVK(0x265) # v3.18 KEY_KBDINPUTASSIST_CANCEL
  "XF86XK_KbdInputAssistCancel"

  # _EVDEVK(0x266) # v4.7  KEY_RIGHT_UP
  "XF86XK_RightUp"

  # _EVDEVK(0x267) # v4.7  KEY_RIGHT_DOWN
  "XF86XK_RightDown"

  # _EVDEVK(0x268) # v4.7  KEY_LEFT_UP
  "XF86XK_LeftUp"

  # _EVDEVK(0x269) # v4.7  KEY_LEFT_DOWN
  "XF86XK_LeftDown"

  # _EVDEVK(0x26A) # v4.7  KEY_ROOT_MENU
  "XF86XK_RootMenu"

  # _EVDEVK(0x26B) # v4.7  KEY_MEDIA_TOP_MENU
  "XF86XK_MediaTopMenu"

  # _EVDEVK(0x26C) # v4.7  KEY_NUMERIC_11
  "XF86XK_Numeric11"

  # _EVDEVK(0x26D) # v4.7  KEY_NUMERIC_12
  "XF86XK_Numeric12"

  # _EVDEVK(0x26E) # v4.7  KEY_AUDIO_DESC
  "XF86XK_AudioDesc"

  # _EVDEVK(0x26F) # v4.7  KEY_3D_MODE
  "XF86XK_3DMode"

  # _EVDEVK(0x270) # v4.7  KEY_NEXT_FAVORITE
  "XF86XK_NextFavorite"

  # _EVDEVK(0x271) # v4.7  KEY_STOP_RECORD
  "XF86XK_StopRecord"

  # _EVDEVK(0x272) # v4.7  KEY_PAUSE_RECORD
  "XF86XK_PauseRecord"

  # _EVDEVK(0x273) # v4.7  KEY_VOD
  "XF86XK_VOD"

  # _EVDEVK(0x274) # v4.7  KEY_UNMUTE
  "XF86XK_Unmute"

  # _EVDEVK(0x275) # v4.7  KEY_FASTREVERSE
  "XF86XK_FastReverse"

  # _EVDEVK(0x276) # v4.7  KEY_SLOWREVERSE
  "XF86XK_SlowReverse"

  # _EVDEVK(0x277) # v4.7  KEY_DATA
  "XF86XK_Data"

  # _EVDEVK(0x278) # v4.12 KEY_ONSCREEN_KEYBOARD
  "XF86XK_OnScreenKeyboard"

  # _EVDEVK(0x279) # v5.5  KEY_PRIVACY_SCREEN_TOGGLE
  "XF86XK_PrivacyScreenToggle"

  # _EVDEVK(0x27A) # v5.6  KEY_SELECTIVE_SCREENSHOT
  "XF86XK_SelectiveScreenshot"

  # _EVDEVK(0x290) # v5.5  KEY_MACRO1
  "XF86XK_Macro1"

  # _EVDEVK(0x291) # v5.5  KEY_MACRO2
  "XF86XK_Macro2"

  # _EVDEVK(0x292) # v5.5  KEY_MACRO3
  "XF86XK_Macro3"

  # _EVDEVK(0x293) # v5.5  KEY_MACRO4
  "XF86XK_Macro4"

  # _EVDEVK(0x294) # v5.5  KEY_MACRO5
  "XF86XK_Macro5"

  # _EVDEVK(0x295) # v5.5  KEY_MACRO6
  "XF86XK_Macro6"

  # _EVDEVK(0x296) # v5.5  KEY_MACRO7
  "XF86XK_Macro7"

  # _EVDEVK(0x297) # v5.5  KEY_MACRO8
  "XF86XK_Macro8"

  # _EVDEVK(0x298) # v5.5  KEY_MACRO9
  "XF86XK_Macro9"

  # _EVDEVK(0x299) # v5.5  KEY_MACRO10
  "XF86XK_Macro10"

  # _EVDEVK(0x29A) # v5.5  KEY_MACRO11
  "XF86XK_Macro11"

  # _EVDEVK(0x29B) # v5.5  KEY_MACRO12
  "XF86XK_Macro12"

  # _EVDEVK(0x29C) # v5.5  KEY_MACRO13
  "XF86XK_Macro13"

  # _EVDEVK(0x29D) # v5.5  KEY_MACRO14
  "XF86XK_Macro14"

  # _EVDEVK(0x29E) # v5.5  KEY_MACRO15
  "XF86XK_Macro15"

  # _EVDEVK(0x29F) # v5.5  KEY_MACRO16
  "XF86XK_Macro16"

  # _EVDEVK(0x2A0) # v5.5  KEY_MACRO17
  "XF86XK_Macro17"

  # _EVDEVK(0x2A1) # v5.5  KEY_MACRO18
  "XF86XK_Macro18"

  # _EVDEVK(0x2A2) # v5.5  KEY_MACRO19
  "XF86XK_Macro19"

  # _EVDEVK(0x2A3) # v5.5  KEY_MACRO20
  "XF86XK_Macro20"

  # _EVDEVK(0x2A4) # v5.5  KEY_MACRO21
  "XF86XK_Macro21"

  # _EVDEVK(0x2A5) # v5.5  KEY_MACRO22
  "XF86XK_Macro22"

  # _EVDEVK(0x2A6) # v5.5  KEY_MACRO23
  "XF86XK_Macro23"

  # _EVDEVK(0x2A7) # v5.5  KEY_MACRO24
  "XF86XK_Macro24"

  # _EVDEVK(0x2A8) # v5.5  KEY_MACRO25
  "XF86XK_Macro25"

  # _EVDEVK(0x2A9) # v5.5  KEY_MACRO26
  "XF86XK_Macro26"

  # _EVDEVK(0x2AA) # v5.5  KEY_MACRO27
  "XF86XK_Macro27"

  # _EVDEVK(0x2AB) # v5.5  KEY_MACRO28
  "XF86XK_Macro28"

  # _EVDEVK(0x2AC) # v5.5  KEY_MACRO29
  "XF86XK_Macro29"

  # _EVDEVK(0x2AD) # v5.5  KEY_MACRO30
  "XF86XK_Macro30"

  # _EVDEVK(0x2B0) # v5.5  KEY_MACRO_RECORD_START
  "XF86XK_MacroRecordStart"

  # _EVDEVK(0x2B1) # v5.5  KEY_MACRO_RECORD_STOP
  "XF86XK_MacroRecordStop"

  # _EVDEVK(0x2B2) # v5.5  KEY_MACRO_PRESET_CYCLE
  "XF86XK_MacroPresetCycle"

  # _EVDEVK(0x2B3) # v5.5  KEY_MACRO_PRESET1
  "XF86XK_MacroPreset1"

  # _EVDEVK(0x2B4) # v5.5  KEY_MACRO_PRESET2
  "XF86XK_MacroPreset2"

  # _EVDEVK(0x2B5) # v5.5  KEY_MACRO_PRESET3
  "XF86XK_MacroPreset3"

  # _EVDEVK(0x2B8) # v5.5  KEY_KBD_LCD_MENU1
  "XF86XK_KbdLcdMenu1"

  # _EVDEVK(0x2B9) # v5.5  KEY_KBD_LCD_MENU2
  "XF86XK_KbdLcdMenu2"

  # _EVDEVK(0x2BA) # v5.5  KEY_KBD_LCD_MENU3
  "XF86XK_KbdLcdMenu3"

  # _EVDEVK(0x2BB) # v5.5  KEY_KBD_LCD_MENU4
  "XF86XK_KbdLcdMenu4"

  # _EVDEVK(0x2BC) # v5.5  KEY_KBD_LCD_MENU5
  "XF86XK_KbdLcdMenu5"
]
