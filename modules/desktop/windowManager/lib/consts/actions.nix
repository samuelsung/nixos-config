let
  inherit (builtins) genList;
in
[
  "nav_kill_window"
  "nav_promote_window"
  "nav_focus_up"
  "nav_focus_down"
  "nav_fullscreen"
  "nav_hide_bar"
  "nav_prev_workspace"
  "nav_next_workspace"
  "nav_prev_screen"
  "nav_next_screen"
  "nav_move_prev_workspace"
  "nav_move_next_workspace"
  "nav_move_prev_screen"
  "nav_move_next_screen"
  "nav_set_window_tile"
  "nav_inc_master_node"
  "nav_desc_master_node"
  "nav_master_shrink"
  "nav_master_expand"
  "nav_next_layout"
  "nav_rotate_up_stack"
  "nav_rotate_down_stack"
  "nav_mouse_focus_resize"
  "nav_mouse_focus_top"
  "nav_mouse_focus_move"
  "nav_launch_terminal_scratchpad"
]
++ (genList (i: "nav_workspace_${toString (i + 1)}") 10)
++ (genList (i: "nav_move_workspace_${toString (i + 1)}") 10)
