[
  # 1<<0 # Shift
  "ShiftMask"

  # 1<<1 # CapsLock
  "LockMask"

  # 1<<2 # Ctrl
  "ControlMask"

  # 1<<3 # Alt
  "Mod1Mask"

  # 1<<4 # Num Lock
  "Mod2Mask"

  # 1<<5 # Scroll Lock
  "Mod3Mask"

  # 1<<6 # Windows
  "Mod4Mask"

  # 1<<7 # AltGr
  "Mod5Mask"
]
