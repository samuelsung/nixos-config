{
  flake.homeManagerModules.desktop-windowManager.imports = [
    ./lib
    ./options.nix
    ./targets.nix
  ];

  flake.homeManagerModules.desktop-windowManager-samuelsung = ./windowManager-samuelsung.nix;
}
