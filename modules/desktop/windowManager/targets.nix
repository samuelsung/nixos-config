{ lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  systemd.user.targets.tray = mkDefault {
    Unit = {
      Description = "System Tray Target";
      Requires = [ "graphical-session-pre.target" ];
    };
  };
}
