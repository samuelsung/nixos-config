{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.gtk;
in
{
  home.packages = mkIf cfg.enable [
    pkgs.lxappearance
  ];

  xresources.properties = mkIf (cfg.enable && cfg.cursorTheme != null) {
    "Xcursor.theme" = cfg.cursorTheme.name;
    "Xcursor.size" = mkIf
      (cfg.cursorTheme.size != null)
      cfg.cursorTheme.size;
  };

  gtk.enable = mkDefault config.desktop.enable;
  gtk.font = mkDefault {
    package = pkgs.noto-fonts-cjk-sans;
    name = "Noto Sans CJK JP 10";
  };

  gtk.gtk2.extraConfig = ''
    gtk-toolbar-style=GTK_TOOLBAR_BOTH
    gtk-toolbar-icon-size=GTK_ICON_SIZE_BUTTON
    gtk-button-images=1
    gtk-menu-images=1
    gtk-enable-event-sounds=1
    gtk-enable-input-feedback-sounds=1
    gtk-xft-antialias=1
    gtk-xft-hinting=1
    gtk-xft-hintstyle="hintfull"
    gtk-xft-rgba="rgb"
  '';

  gtk.gtk3.extraConfig = {
    gtk-toolbar-style = "GTK_TOOLBAR_BOTH";
    gtk-toolbar-icon-size = "GTK_ICON_SIZE_BUTTON";
    gtk-button-images = 1;
    gtk-menu-images = 1;
    gtk-enable-event-sounds = 1;
    gtk-enable-input-feedback-sounds = 1;
    gtk-xft-antialias = 1;
    gtk-xft-hinting = 1;
    gtk-xft-hintstyle = "hintfull";
    gtk-xft-rgba = "rgb";
  };

  gtk.gtk4.extraConfig = {
    gtk-toolbar-style = "GTK_TOOLBAR_BOTH";
    gtk-toolbar-icon-size = "GTK_ICON_SIZE_BUTTON";
    gtk-button-images = 1;
    gtk-menu-images = 1;
    gtk-enable-event-sounds = 1;
    gtk-enable-input-feedback-sounds = 1;
    gtk-xft-antialias = 1;
    gtk-xft-hinting = 1;
    gtk-xft-hintstyle = "hintfull";
    gtk-xft-rgba = "rgb";
  };

  home.sessionVariables = mkIf (cfg.enable && cfg.theme != null) {
    GTK_THEME = cfg.theme.name;
  };

  systemd.user.sessionVariables = mkIf (cfg.enable && cfg.theme != null) {
    GTK_THEME = cfg.theme.name;
  };
}
