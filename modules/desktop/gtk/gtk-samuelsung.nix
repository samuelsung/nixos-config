{ pkgs, ... }:

{
  gtk.theme = {
    name = "catppuccin-mocha-mauve-standard+rimless";
    package = pkgs.catppuccin-gtk.override {
      accents = [ "mauve" ];
      size = "standard";
      tweaks = [ "rimless" ];
      variant = "mocha";
    };
  };

  gtk.iconTheme = {
    package = pkgs.flat-remix-icon-theme;
    name = "Flat-Remix-Blue-Dark";
  };

  gtk.gtk3.bookmarks = [
    "file:///home/samuelsung/nextcloud"
    "smb://freenas.local/main_set/main_set main_set on freenas.local"
  ];
}
