{ lib, config, pkgs, ... }:

let
  inherit (lib) attrNames concatStringsSep filterAttrs mkIf;
  normalUsers = attrNames (filterAttrs (_: v: v.isNormalUser) config.users.users);
in
mkIf config.programs.light.enable {
  system.activationScripts.setLightMinimum = ''
    for u in ${concatStringsSep " " normalUsers}; do {
      ${pkgs.sudo}/bin/sudo -u "$u" ${pkgs.light}/bin/light -N 1 # set minimum brightness
    }; done;
  '';

  home-manager.sharedModules = [
    {
      windowManager.shortcuts = {
        incBrightness = {
          action = "${pkgs.light}/bin/light -A 10";
          keys = [{ key = "XF86XK_MonBrightnessUp"; }];
        };

        descBrightness = {
          action = "${pkgs.light}/bin/light -U 10";
          keys = [{ key = "XF86XK_MonBrightnessDown"; }];
        };
      };
    }
  ];

  users.presets.commonNormalUserGroup = [ "video" ];
}
