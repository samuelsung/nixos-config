{ config, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.webcam;
in
{
  options.webcam.enable = mkEnableOption "webcam support";

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      v4l-utils # control webcam params
    ];
  };
}
