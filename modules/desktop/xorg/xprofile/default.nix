{ config, lib, pkgs, ... }:

let
  inherit (lib) attrNames concatMapStringsSep escapeShellArgs mkIf mkOption optionalString types unique;
  cfg = config.services.xserver;
in
{
  options.services.xserver = {
    profileExtra = mkOption {
      type = types.lines;
      default = "";
      description = "Extra shell commands to run before session start.";
    };

    importedVariables = mkOption {
      type = types.listOf (types.strMatching "[a-zA-Z_][a-zA-Z0-9_]*");
      apply = unique;
      example = [ "GDK_PIXBUF_ICON_LOADER" ];
      visible = false;
      description = ''
        Environment variables to import into the user systemd
        session. The will be available for use by graphical
        services.
      '';
    };

    variables = mkOption {
      type = types.attrsOf types.str;
      default = { };
    };
  };

  config = mkIf cfg.enable {
    services.xserver.importedVariables = [
      "DBUS_SESSION_BUS_ADDRESS"
      "DISPLAY"
      "XAUTHORITY"
      "XDG_DATA_DIRS"
      "XDG_RUNTIME_DIR"
      "XDG_SESSION_ID"
      "TERMINAL"
    ];

    home.file.".xprofile".text = ''
      . "${config.home.profileDirectory}/etc/profile.d/hm-session-vars.sh"

      ${concatMapStringsSep "\n" (k: "export ${k}=\"${cfg.variables.${k}}\"") (attrNames cfg.variables)}

      systemctl --user stop graphical-session.target graphical-session-pre.target

      ${optionalString (cfg.importedVariables != [ ]) ''
        ${pkgs.dbus}/bin/dbus-update-activation-environment --systemd ${escapeShellArgs cfg.importedVariables}
      ''}

      ${cfg.profileExtra}
    '';
  };
}
