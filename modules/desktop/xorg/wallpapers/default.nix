{ config, lib, pkgs, ... }:

let
  inherit (lib) makeBinPath mkIf;
  cfg = config.services.xserver;

  x-random-wallpaper = pkgs.writeShellScriptBin "x-random-wallpaper" ''
    PATH=${makeBinPath [
      config.desktop.wallpapers.random-wallpaper
      pkgs.coreutils
      pkgs.gnugrep
      pkgs.gnused
      pkgs.feh
      pkgs.xorg.xrandr
      pkgs.findutils
    ]}:$PATH

    random-wallpaper "$(xrandr --listactivemonitors | grep '+' | wc -l)" \
      | xargs feh --bg-fill
  '';
in
mkIf cfg.enable {
  home.packages = with pkgs; [
    feh
  ];

  systemd.user.services.x-random-wallpaper = {
    Unit = {
      Description = "Set wallpaper loop using feh";
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "x-session.target" ];
    };

    Install.WantedBy = [ "x-session.target" ];

    Service = {
      Type = "oneshot";
      ExecStart = "${x-random-wallpaper}/bin/x-random-wallpaper";
      Restart = "on-failure";
      RestartSec = 1;
    };
  };

  systemd.user.timers."x-random-wallpaper" = {
    Unit = {
      Description = "x-random-wallpaper";
    };
    Timer = {
      Unit = "x-random-wallpaper.service";
      OnUnitInactiveSec = "5min";
    };
    Install.WantedBy = [ "timers.target" ];
  };
}
