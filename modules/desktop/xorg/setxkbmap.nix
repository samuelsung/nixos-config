{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf optional;
in
mkIf config.services.xserver.enable {
  systemd.user.services = {
    setxkbmap = {
      Unit = {
        Description = "Set up keyboard in X";
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" "x-session.target" ];
      };

      Install.WantedBy = [ "x-session.target" ];

      Service = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStart = with config.home.keyboard;
          let
            args = optional (layout != null) "-layout '${layout}'"
              ++ optional (variant != null) "-variant '${variant}'"
              ++ optional (model != null) "-model '${model}'"
              ++ [ "-option ''" ] ++ map (v: "-option '${v}'") options;
          in
          "${pkgs.xorg.setxkbmap}/bin/setxkbmap ${toString args}";
      };
    };

    xplugd = {
      Unit = {
        Description = "Rerun setxkbmap.service when I/O is changed";
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" "x-session.target" ];
      };

      Install.WantedBy = [ "x-session.target" ];

      Service = {
        Type = "forking";
        ExecStart =
          let
            script = pkgs.writeShellScript "xplugrc" ''
              case "$1,$3" in
                keyboard,connected)
                systemctl --user restart setxkbmap.service
                ;;
              esac
            '';
          in
          "${pkgs.xplugd}/bin/xplugd ${script}";
      };
    };
  };
}
