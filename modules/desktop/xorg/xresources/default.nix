{ config, lib, pkgs, ... }:

let
  inherit (lib) mapAttrs;
  colors = mapAttrs (_: v: "#${v.rgbhex}") config.colors.colors;
in
{
  xresources.properties = {
    "dwm.normbordercolor" = colors.nord0;
    "dwm.normbgcolor" = colors.nord0;
    "dwm.normfgcolor" = colors.nord4;
    "dwm.selbordercolor" = colors.nord9;
    "dwm.selbgcolor" = colors.nord9;
    "dwm.selfgcolor" = colors.nord1;
  };

  services.xserver.profileExtra = ''
    ${pkgs.xorg.xrdb}/bin/xrdb -merge ${config.xresources.path}
  '';
}
