{ config, lib, osConfig, ... }:

let
  inherit (lib) escapeShellArgs mkIf mkOption optionalString types;
  cfg = config.services.xserver;
in
{
  options.services.xserver = {
    windowManager.command = mkOption {
      type = types.str;
      description = "window manager/de to start";
    };
  };

  config = mkIf (cfg.enable && osConfig.services.xserver.displayManager.startx.enable) {
    home.file.".xinitrc".text =
      ''
        . "${config.home.file.".xprofile".source}"

        ${cfg.windowManager.command}
        err=$?

        systemctl --user stop graphical-session.target
        systemctl --user stop graphical-session-pre.target

        # Wait until the units actually stop.
        while [ -n "$(systemctl --user --no-legend --state=deactivating list-units)" ]; do
          sleep 0.5
        done

        ${optionalString (cfg.importedVariables != [ ]) ''
          systemctl --user unset-environment ${escapeShellArgs cfg.importedVariables}
        ''}

        exit $err
      '';
  };
}
