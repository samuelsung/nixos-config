{ config, lib, osConfig, ... }:

let
  inherit (lib) mkEnableOption;
in
{
  options.services.xserver.enable = mkEnableOption "xserver";

  config.assertions = [
    {
      assertion = config.services.xserver.enable -> osConfig.services.xserver.enable or true;
      message = ''
        System xserver support is needed when enabling hm xserver support.
      '';
    }

    {
      assertion = config.services.xserver.enable -> config.desktop.enable;
      message = ''
        hm desktop support is needed when enabling hm xserver support.
      '';
    }
  ];

  imports = [
    ./core
    ./setxkbmap.nix
    ./wallpapers
    ./xinit
    ./xprofile
    ./xresources
  ];
}
