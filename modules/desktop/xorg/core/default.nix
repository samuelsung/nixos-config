{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
  cfg = config.services.xserver;
in
mkIf cfg.enable {
  home.packages = with pkgs; [
    xsel
    xorg.xwininfo
    xorg.xdpyinfo
    xorg.xbacklight
    xclip
    xdotool
    xss-lock
    xlayoutdisplay
    xterm

    lxrandr # monitor settings
  ];

  systemd.user.targets.x-session = {
    Unit = {
      Description = "x session";
      Documentation = [ "man:systemd.special(7)" ];
      BindsTo = [ "graphical-session.target" ];
      Wants = [ "graphical-session-pre.target" ];
      After = [ "graphical-session-pre.target" ];
    };
  };

  services.xserver.profileExtra = ''
    ${pkgs.xorg.xset}/bin/xset s off
    ${pkgs.xorg.xset}/bin/xset -dpms
    ${pkgs.xorg.xsetroot}/bin/xsetroot -cursor_name left_ptr
  '';
}
