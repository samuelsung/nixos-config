{
  flake.homeManagerModules.desktop-xorg = ./xorg-home-manager.nix;
  flake.nixosModules.desktop-xorg = ./xorg.nix;
}
