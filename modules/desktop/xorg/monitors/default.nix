{ config, ... }:

let
  # https://github.com/kevinlekiller/cvt_modeline_calculator_12
  modelines = h: v: refreshRate:
    if h == 1920 && v == 1080 && refreshRate == 60 then ''
      Modeline "1920x1080_60.00_rb2"  133.32  1920 1928 1960 2000  1080 1097 1105 1111 +hsync -vsync
      Option "PreferredMode" "1920x1080_60.00_rb2"
    ''
    else if h == 1920 && v == 1080 && refreshRate == 120 then ''
      Modeline "1920x1080_120.00_rb2"  274.56  1920 1928 1960 2000  1080 1130 1138 1144 +hsync -vsync
      Option "PreferredMode" "1920x1080_120.00_rb2"
    ''
    else if h == 1920 && v == 1080 && refreshRate == 144 then ''
      Modeline "1920x1080_144.00_rb2"  333.22  1920 1928 1960 2000  1080 1143 1151 1157 +hsync -vsync
      Option "PreferredMode" "1920x1080_144.00_rb2"
    ''
    else if h == 2560 && v == 1440 && refreshRate == 60 then ''
      Modeline "2560x1440_60.00_rb2"  234.59  2560 2568 2600 2640  1440 1467 1475 1481 +hsync -vsync
      Option "PreferredMode"  "2560x1440_60.00_rb2"
    ''
    else if h == 2560 && v == 1440 && refreshRate == 120 then ''
      Modeline "2560x1440R"  497.25  2560 2608 2640 2720  1440 1443 1448 1525 +hsync -vsync
      Option "PreferredMode"  "2560x1440R"
    ''
    # Modeline "2560x1440_120.00_rb2"  483.12  2560 2568 2600 2640  1440 1511 1519 1525 +hsync -vsync
    # Option "PreferredMode"  "2560x1440_120.00_rb2"
    else if h == 2560 && v == 1440 && refreshRate == 144 then ''
      Modeline "2560x1440_144.00_rb2"  586.59  2560 2568 2600 2640  1440 1529 1537 1543 +hsync -vsync
      Option "PreferredMode"  "2560x1440_144.00_rb2" 
    ''
    else if h == 1920 && v == 1200 && refreshRate == 60 then ''
      Modeline "1920x1200_60.00_rb2"  148.20  1920 1928 1960 2000  1200 1221 1229 1235 +hsync -vsync
      Option "PreferredMode"  "1920x1200_60.00_rb2"
    ''
    else throw "${toString h}x${toString v}@${toString refreshRate} not supported";

  rotations = degree:
    if degree == 0 then ""
    else if degree == 90 then ''
      Option "Rotate" "left"
    ''
    else if degree == -90 || degree == 270 then ''
      Option "Rotate" "right"
    ''
    else if degree == 180 || degree == -180 then ''
      Option "Rotate" "inverted"
    ''
    else throw "degree ${toString degree} not supported";
in
{
  services.xserver.xrandrHeads = map
    ({ name, h, v, x, y, refreshRate, primary, rotation }: {
      output = name.x;
      inherit primary;
      monitorConfig = ''
        Option "Position" "${toString x} ${toString y}"
        ${modelines h v refreshRate.x}
        ${rotations rotation}
      '';
    })
    config.hardware.video.monitors;
}
