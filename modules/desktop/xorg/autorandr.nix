{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{ services.autorandr.enable = mkDefault config.services.xserver.enable; }
