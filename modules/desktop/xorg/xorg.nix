{ config, ... }:

{
  config.assertions = [{
    assertion = config.services.xserver.enable -> config.desktop.enable;
    message = ''
      System desktop support is needed when enabling xserver support.
    '';
  }];

  imports = [
    ./autorandr.nix
    ./monitors
  ];
}
