{ config, lib, ... }:

let
  inherit (lib) mkDefault mkForce mkIf;
  cfg = config.services.picom;
in
{
  services.picom = {
    enable = mkDefault config.services.xserver.enable;

    fade = false;

    shadow = true;
    shadowOffsets = [ 0 0 ];
    shadowOpacity = 0.5;

    wintypes = {
      dock.shadow = false;
      dnd.shadow = false;
      popup_menu.opacity = 0.92;
      dropdown_menu.opacity = 0.92;
    };

    backend = "glx";

    vSync = true;

    settings = {
      glx-no-stencil = true;
      glx-no-rebind-pixmap = true;
      no-use-damage = true;
      shadow-radius = 5;
      unredir-if-possible = false;
    };
  };

  systemd.user.services.picom = mkIf cfg.enable {
    Unit = mkForce {
      Description = "Picom X11 compositor";
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "x-session.target" ];
      X-Restart-Triggers = [
        (toString config.xdg.configFile."picom/picom.conf".source)
      ];
    };

    Install.WantedBy = mkForce [ "x-session.target" ];
  };
}
