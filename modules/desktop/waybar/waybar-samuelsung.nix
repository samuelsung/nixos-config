localFlake:

{ config, pkgs, ... }:
let
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
  mapRgba = { r, g, b }: a: "rgba(${toString r}, ${toString g}, ${toString b}, ${toString a})";

  mapColorSpan = color: content:
    "<span color=\"#${hexOf color}\">${content}</span>";
in
{
  programs.waybar.settings.mainBar = {
    layer = "top";
    position = "top";
    height = 20;
    # "width": 1280, // Waybar width
    spacing = 4;
    margin-left = 6;
    margin-right = 6;
    margin-top = 6;
    margin-bottom = 0;

    # Choose the order of the modules
    modules-left = [ "hyprland/workspaces" "tray" "mpd" ];
    modules-center = [ "clock" ];
    modules-right = [ "idle_inhibitor" "pulseaudio" "backlight" "battery" "custom/notification" ];

    "hyprland/workspaces" = {
      on-click = "activate";
    };

    # Modules configuration
    keyboard-state = {
      numlock = true;
      capslock = true;
      format = "{name} {icon}";
      format-icons = {
        locked = "";
        unlocked = "";
      };
    };

    mpd = {
      format = "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩";
      format-disconnected = " Disconnected";
      format-stopped = "{conserror umeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped";
      unknown-tag = "N/A";
      interval = 2;
      consume-icons = {
        on = " ";
      };
      random-icons = {
        off = mapColorSpan catppuccin.mocha.red " ";
        on = mapColorSpan catppuccin.mocha.green " ";
      };

      repeat-icons.on = " ";
      single-icons.on = "1 ";

      state-icons = {
        paused = mapColorSpan catppuccin.mocha.red "";
        playing = mapColorSpan catppuccin.mocha.green "";
      };

      tooltip-format = "MPD (connected)";
      tooltip-format-disconnected = "MPD (disconnected)";

      inherit (config.services.mpd.network) port;
      server = "localhost";
      on-click-right = "${pkgs.mpc_cli}/bin/mpc --port ${toString config.services.mpd.network.port} toggle";
      on-click = "$TERMINAL -e ${config.programs.ncmpcpp.package}/bin/ncmpcpp";
    };

    idle_inhibitor = {
      format = "{icon}";
      format-icons = {
        activated = "";
        deactivated = "";
      };
    };

    tray = {
      # icon-size = 21;
      spacing = 6;
    };

    clock.format = "{:%Y %b %d %a %R}";

    cpu = {
      format = " {usage}%";
      tooltip = false;
    };

    memory.format = " {}%";

    temperature = {
      # thermal-zone = 2;
      # hwmon-path = "/sys/class/hwmon/hwmon2/temp1_input";
      critical-threshold = 80;
      # format-critical = "{temperatureC}°C {icon}";
      format = "{icon} {temperatureC}°C";
      format-icons = [ "" "" "" ];
    };

    backlight = {
      format = "{icon} {percent}%";
      format-icons = [ "" "" "" "" "" "" "" "" "" ];
    };

    battery = {
      states = {
        # "good" = 95;
        warning = 30;
        critical = 15;
      };
      format = "{icon} {capacity}% {time}";
      format-charging = " {capacity}% {time}";
      format-plugged = " {capacity}%";
      # format-alt = "{icon} {time}";
      # format-good = ""; // An empty format will hide the module
      # format-full = "";
      format-icons = [ "" "" "" "" "" ];
    };

    network = {
      # interface = "wlp2*"; # (Optional) To force the use of this interface
      format-wifi = " {essid} ({signalStrength}%)";
      format-ethernet = " {ipaddr}/{cidr}";
      tooltip-format = " {ifname} via {gwaddr}";
      format-linked = " {ifname} (No IP)";
      format-disconnected = "⚠ Disconnected";
      format-alt = "{ifname}: {ipaddr}/{cidr}";
    };

    pulseaudio = {
      # scroll-step = 1, # %, can be a float
      format = "${mapColorSpan catppuccin.mocha.green "{icon}"} {volume}% {format_source}";
      format-bluetooth = "${mapColorSpan catppuccin.mocha.green "{icon}"} {volume}% {format_source}";
      format-bluetooth-muted = "${mapColorSpan catppuccin.mocha.red ""} {icon} {format_source}";
      format-muted = "${mapColorSpan catppuccin.mocha.red ""} {format_source}";
      format-source = "${mapColorSpan catppuccin.mocha.green ""} {volume}%";
      format-source-muted = mapColorSpan catppuccin.mocha.red "";
      format-icons = {
        headphone = "";
        hands-free = "";
        headset = "";
        phone = "";
        portable = "";
        car = "";
        default = [ "" "" "" ];
      };
      on-click = "$TERMINAL -e ${pkgs.pulsemixer}/bin/pulsemixer";
    };

    "custom/notification" = {
      tooltip = true;
      format = "{icon}";
      format-icons = {
        notification = "${mapColorSpan catppuccin.mocha.red "<sup></sup>"}";
        none = "";
        dnd-notification = "${mapColorSpan catppuccin.mocha.red "<sup></sup>"}";
        dnd-none = "";
        inhibited-notification = "${mapColorSpan catppuccin.mocha.red "<sup></sup>"}";
        inhibited-none = "";
        dnd-inhibited-notification = "${mapColorSpan catppuccin.mocha.red "<sup></sup>"}";
        dnd-inhibited-none = "";
      };
      return-type = "json";
      exec = "${pkgs.swaynotificationcenter}/bin/swaync-client -swb";
      on-click = "${pkgs.coreutils}/bin/sleep 0.1 && ${pkgs.swaynotificationcenter}/bin/swaync-client -t -sw";
      on-click-right = "${pkgs.coreutils}/bin/sleep 0.1 && ${pkgs.swaynotificationcenter}/bin/swaync-client -d -sw";
      escape = true;
    };
  };

  programs.waybar.style = ''
    * {
        border: none;
        font-family: "FiraCode Nerd Font Mono";
        font-weight: bold;
        min-height: 0;
    }

    window#waybar,
    #clock,
    #custom-self-remainder,
    #custom-self-remainder2,
    #mpd,
    #workspaces button,
    #tray, 
    #pulseaudio,
    #cpu,
    #network,
    #memory,
    #temperature,
    #backlight,
    #idle_inhibitor,
    #battery,
    #custom-notification {
      padding: 0.25em 0.5em 0.25em 0.5em;
      border-radius: 0.5em;
      background-color: ${mapRgba catppuccin.mocha.base 0.85};
      color: ${mapRgba catppuccin.mocha.text 1};
    }

    window#waybar {
      background-color: transparent;
    }

    #workspaces {
    }

    #workspaces button:not(:first-child) {
      margin-left: 0.25em;
    }
  '';
}
