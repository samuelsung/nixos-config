{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.desktop-waybar-samuelsung = importApply ./waybar-samuelsung.nix {
    inherit colors;
  };
}
