{ colors, flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules.desktop-slock = importApply ./slock.nix {
    inherit inputs;
  };

  flake.homeManagerModules.desktop-slock-samuelsung = importApply ./slock-samuelsung.nix {
    inherit colors;
  };
}
