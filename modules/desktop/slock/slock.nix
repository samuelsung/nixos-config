localFlake:

{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
mkIf config.desktop.enable {
  nixpkgs.overlays = [
    localFlake.inputs.slock.overlays.default
  ];

  programs.slock.enable = true;
}
