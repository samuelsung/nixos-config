localFlake:

{ lib, ... }:
let
  inherit (lib) concatMapAttrs mapAttrs;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;

  mapColors = mapAttrs (_: v: "#${hexOf v}");

  mapCatppuccin = flavour: with (mapColors flavour);
    {
      color0 = base;
      color4 = sky;
      color1 = red;
      color3 = peach;
    };
in
{
  xresources.properties = concatMapAttrs
    (k: v: { "slock.${k}" = v; })
    (mapCatppuccin catppuccin.mocha);
}
