{ lib, ... }:

let
  inherit (lib) mkOption types;
  inherit (types) submodule;
in
{
  _module.args.desktop-lib = {
    xwOf = type: submodule ({ config, ... }: {
      options.x = mkOption {
        inherit type;
        description = "value in x.org";
      };

      options.w = mkOption {
        inherit type;
        default = config.x;
        description = "value in wayland";
      };
    });
  };
}
