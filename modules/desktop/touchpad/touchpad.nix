{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
mkIf config.desktop.enable {
  # Touchpad Support
  services.libinput = {
    enable = true;
    touchpad = {
      tapping = true;
      naturalScrolling = false;
      disableWhileTyping = true;
    };
  };
}
