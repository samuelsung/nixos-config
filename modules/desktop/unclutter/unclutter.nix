{ config, lib, ... }:

let
  inherit (lib) mkDefault mkForce;
in
{
  services.unclutter.enable = mkDefault config.services.xserver.enable;
  services.unclutter.timeout = mkDefault 2;

  systemd.user.services.unclutter = {
    Unit = mkForce {
      Description = "unclutter";
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "graphical-session.target" "x-session.target" ];
    };

    Install.WantedBy = mkForce [ "x-session.target" ];
  };
}
