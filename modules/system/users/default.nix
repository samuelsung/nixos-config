{ config, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules.system-users = importApply ./users.nix { inherit config; };
}
