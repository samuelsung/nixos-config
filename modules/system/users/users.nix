localFlake:

{ config, lib, ... }:

let
  inherit (lib)
    concatMapStringsSep
    filterAttrs
    mapAttrsToList
    mkEnableOption
    mkDefault
    mkIf
    mkMerge
    mkOption
    types;
  cfg = config.users;
  hmOptions = config.home-manager.users;
  normalUsers = mapAttrsToList
    (_: u: u.name)
    (filterAttrs (_: v: v.isNormalUser) config.users.users);
in
{
  options.users.presets = {
    root.enable = mkOption {
      default = true;
      readOnly = true;
      description = "enable user root";
    };
    samuelsung.enable = mkEnableOption "user samuelsung";
    samuelsung_play.enable = mkEnableOption "user samuelsung_play";
    commonNormalUserGroup = mkOption {
      type = with types; listOf str;
      default = [ ];
      description = "groups share amount all normal users";
    };
  };

  config = {
    # To change uid, run
    # usermod -u <new_uid> foo # assign new uid to the user
    # find / -user <old_uid> -exec chown -h foo {} \; # apply the new uid to files

    # To change username
    # FIRST, uninstall home manager config
    # nix-env -e home-manager-path

    # run
    # usermod -l <new_name> <old_name>

    # move home folder
    # mv /home/<old_name> /home/<new_name>

    # fixed failed symlink which references the absolute path
    # new_name=<new_name>
    # old_name=<old_name>
    # find . -type l | while read -r p; do {
    #     if readlink "$p" | grep "/home/${old_name}/" > /dev/null; then {
    #         from=$p
    #         echo "from: $from"
    #         to=$(readlink "$p" | sed "s@/home/${old_name}/@/home/${new_name}/@g")
    #         echo "to: $to"
    #
    #         ln -sf "$to" "$from"
    #     }; fi
    # }; done

    users.users =
      let
        samuelsung_common = {
          isNormalUser = true;
          initialPassword = mkDefault "p@ssw0rd";
          extraGroups = mkMerge [
            [
              "wheel" # Enable ‘sudo’ for the user.
              "samuelsung_common"
              "vault_common"
            ]

            cfg.presets.commonNormalUserGroup
          ];
          openssh.authorizedKeys.keyFiles = [
            ../../../samuelsung.public-keys
          ];
        };
      in
      {
        samuelsung = mkIf cfg.presets.samuelsung.enable
          (mkMerge [
            samuelsung_common
            { uid = 1001; }
          ]);

        samuelsung_play = mkIf cfg.presets.samuelsung_play.enable
          (mkMerge [
            samuelsung_common
            { uid = 1000; }
          ]);

        root.initialPassword = mkDefault "p@ssw0rd";
      };

    home-manager.users.samuelsung = mkIf cfg.presets.samuelsung.enable {
      imports = [ localFlake.config.flake.homeManagerModules.samuelsung ];
    };
    home-manager.users.samuelsung_play = mkIf cfg.presets.samuelsung_play.enable {
      imports = [ localFlake.config.flake.homeManagerModules.samuelsung ];
    };

    # To change gid, run
    # groupmod -g <new_gid> foo # assign new gid to the group
    # find / -group <old_gid> -exec chgrp -h foo {} \; # apply the new gid to files
    users.groups.samuelsung_common.gid = 996;

    users.groups.vault_common.gid = 1030;

    system.activationScripts = {
      setupHomeFolder = {
        text = concatMapStringsSep "\n"
          (user: ''
            install -d -m 0700 -o ${user} ${hmOptions.${user}.xdg.configHome}
            install -d -m 0700 -o ${user} ${hmOptions.${user}.home.homeDirectory}/.local
            install -d -m 0700 -o ${user} ${hmOptions.${user}.xdg.dataHome}
          '')
          normalUsers;
        deps = [ "users" ];
        supportsDryActivation = true;
      };

      setupSecrets = {
        text = ""; # It will just append empty line at the end, its fine
        deps = [ "setupHomeFolder" ];
      };
    };
  };
}
