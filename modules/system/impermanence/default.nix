{ flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules.system-impermanence = importApply ./impermanence.nix { inherit inputs; };
}
