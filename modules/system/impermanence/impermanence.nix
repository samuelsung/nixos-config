localFlake:

{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.impermanence;
in
{
  imports = [
    localFlake.inputs.impermanence.nixosModules.impermanence
  ];

  options.impermanence.enable = mkEnableOption "impermanence";

  config = {
    programs.fuse.userAllowOther = mkIf cfg.enable true;

    boot.tmp = {
      useTmpfs = mkIf cfg.enable true;
      tmpfsSize = "95%";
    };

    environment.persistence."/persist" = mkIf cfg.enable {
      hideMounts = true;
      directories = [
        "/var/log"
        "/var/lib/nixos"
      ];
      files = [
        "/etc/machine-id"
      ];
    };
  };
}
