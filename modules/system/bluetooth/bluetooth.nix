{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
  cfg = config.hardware.bluetooth;
in
mkIf cfg.enable {
  hardware.bluetooth = {
    powerOnBoot = true;
    package = pkgs.bluez;
  };

  environment.persistence."/persist" = mkIf config.impermanence.enable {
    hideMounts = true;
    directories = [
      "/var/lib/bluetooth"
    ];
  };
}
