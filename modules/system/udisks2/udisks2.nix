{ config, lib, ... }:

let
  inherit (lib) mkDefault mkMerge;
in
mkMerge [
  { services.udisks2.enable = mkDefault config.desktop.enable; }
]
