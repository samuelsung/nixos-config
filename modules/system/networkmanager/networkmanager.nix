{ pkgs, config, lib, ... }:

let
  inherit (lib) mkForce mkIf;
  cfg = config.networking.networkmanager;
in
mkIf cfg.enable {
  # networkmanager
  networking.wireless.enable = mkForce false;
  networking.networkmanager = {
    wifi.powersave = true;

    plugins = with pkgs; [
      networkmanager-openvpn
    ];
  };

  users.presets.commonNormalUserGroup = [ "networkmanager" ];

  environment.persistence."/persist" = mkIf config.impermanence.enable {
    directories = [
      # all configurations, such as wifi
      "/etc/NetworkManager/system-connections"
    ];
  };
}
