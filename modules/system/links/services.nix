{ config, pkgs, lib, ... }:

let
  inherit (lib) attrValues toposort concatMapStringsSep mkIf optionalString;
  cfg = config.services.links;

  toposortedLinks = (toposort
    (a: b: a.target == b.source)
    (attrValues cfg.links)).result;

  configureScript = pkgs.writeShellScript "links-configure" (
    concatMapStringsSep "\n"
      (link: ''
        [ ! -e "${link.target}" ] && [ ! -h "${link.target}" ] \
          && ln ${optionalString (!link.hardLink) "-s"} "${link.source}" "${link.target}" \
          && echo "${if !link.hardLink then "sym" else "hard"} link ${link.target} linked." \
          || echo "file target ${link.target} exists, skip."
      '')
      toposortedLinks
  );

in
{
  config = mkIf cfg.enable {
    systemd.services.links-configure = {
      description = "links configuration";
      documentation = [ ];
      after = [ "local-fs.target" "remote-fs.target" ];
      requires = [ "local-fs.target" "remote-fs.target" ];
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStart = "${configureScript}";
      };
    };
  };
}
