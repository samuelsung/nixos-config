{ lib, config, ... }:

with lib;
let
  cfg = config.networking.fix-interfaces;
in
{
  options.networking.fix-interfaces = mkOption {
    type = with types; attrsOf str;
    default = { };
    description = ''
      an attrsets that where:
        key: name
        value: mac address
    '';
  };

  config =
    let
      genRule = name: mac: ''
        SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="${mac}", NAME="${name}"
      '';
      concated = concatStringsSep "\n" (mapAttrsToList genRule cfg);
    in
    {
      services.udev.extraRules = concated;

      boot.initrd.services.udev.rules = concated;
    };
}
