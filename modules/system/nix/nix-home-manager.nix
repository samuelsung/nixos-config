{ config, lib, osConfig, ... }:

let
  inherit (lib) concatStringsSep mkDefault mkIf reverseList;
in
{
  nixpkgs.config.allowUnfree = mkDefault true;

  systemd.user.sessionVariables = {
    # SEE: nixpkgs/nixos/modules/programs/environment.nix
    NIX_USER_PROFILE_DIR = "/nix/var/nix/profiles/per-user/${config.home.username}";
    NIX_PROFILES = mkIf
      ((osConfig.environment or { }) ? profiles)
      (concatStringsSep " " (reverseList osConfig.environment.profiles));
  };
}
