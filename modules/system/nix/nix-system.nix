{ config, lib, pkgs, ... }:

let
  inherit (lib) types mkDefault mkMerge mkIf mkOption;
  cfg = config.nix;
in
{
  options.nix = {
    flake.enable = mkOption {
      type = types.bool;
      default = true;
      description = ''
        use flake for nix
      '';
    };

    space-optimize.enable = mkOption {
      type = types.bool;
      default = true;
      description = ''
        enable gc and other space optimize settings
      '';
    };
  };

  config = mkMerge [
    {
      nixpkgs.config.allowUnfree = mkDefault true;
      nix = {
        channel.enable = false;
        settings = {
          # The substituter will be appended to the default substituters when fetching packages.
          extra-substituters = [
            "https://minio.scorch.samuelsung.net/nix-cache/"
          ];
          extra-trusted-public-keys = [
            "samuelsung.woodpecker-codeberg.helios.samuelsung.net-1:rK/W/WLv59Qy34JHgUbgQ2guMonGi281WZJ+b6lnkoc="
          ];
        };
      };
    }

    (mkIf cfg.flake.enable {
      nix = {
        package = pkgs.nix;
        extraOptions = ''
          experimental-features = nix-command flakes
        '';
      };
    })

    (mkIf cfg.space-optimize.enable {
      nix = {
        settings.auto-optimise-store = true;

        gc = {
          automatic = true;
          dates = "weekly";
          options = "--delete-older-than 30d";
        };
      };
    })
  ];
}
