{
  flake.homeManagerModules.system-nix = ./nix-home-manager.nix;
  flake.nixosModules.system-nix = ./nix-system.nix;
}
