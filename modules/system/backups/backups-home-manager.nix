{ lib, ... }:

let
  inherit (lib) mkOption;
  inherit (lib.types) listOf str;
in
{
  options.home.backups = {
    paths = mkOption {
      type = listOf str;
      default = [ ];
    };

    exclude = mkOption {
      type = listOf str;
      default = [ ];
    };
  };
}
