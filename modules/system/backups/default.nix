{
  flake.homeManagerModules.system-backups = ./backups-home-manager.nix;
  flake.nixosModules.system-backups = ./backups-system.nix;
}
