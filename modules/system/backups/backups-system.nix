{ config, lib, ... }:

let
  inherit (lib) attrValues concatMap mkOption;
  inherit (lib.types) listOf str;
in
{
  options.backups = {
    paths = mkOption {
      type = listOf str;
      default = [ ];
    };

    exclude = mkOption {
      type = listOf str;
      default = [ ];
    };
  };

  config.backups.paths =
    concatMap (user: user.home.backups.paths) (attrValues config.home-manager.users);

  config.backups.exclude =
    concatMap (user: user.home.backups.exclude) (attrValues config.home-manager.users);
}
