localFlake:

{ config, lib, utils, pkgs, ... }:

let
  inherit (lib) attrValues concatMapStringsSep mapAttrsToList mkIf;

  configureScript = pkgs.writeShellScript "impermanence-homes" (
    concatMapStringsSep "\n"
      (hmUser:
        let user = config.users.users.${hmUser.home.username}; in
        "install -d -m 0700 -o ${user.name} -g ${user.group} /persist/${user.home}")
      (attrValues config.home-manager.users)
  );
in
{
  imports = [
    localFlake.inputs.home-manager.nixosModules.home-manager
  ];

  home-manager.useGlobalPkgs = false;
  home-manager.useUserPackages = true;

  home-manager.sharedModules = [{
    # set the same stateVersion in home-manger
    home.stateVersion = config.system.stateVersion;
    home.packages = [ pkgs.home-manager ];
    imports = [ localFlake.config.flake.homeManagerModules.default ];

    _module.args = { inherit utils; };
  }];

  systemd.services.impermanence-homes = mkIf config.impermanence.enable {
    description = "home folders for impermanence";
    documentation = [ ];
    after = [ "local-fs.target" ];
    requires = [ "local-fs.target" ];
    wantedBy = mapAttrsToList
      (_: user: "home-manager-${utils.escapeSystemdPath user.home.username}.service")
      config.home-manager.users;
    restartIfChanged = true;
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStart = "${configureScript}";
    };
  };
}
