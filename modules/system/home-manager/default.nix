{ config, flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules.system-home-manager = importApply ./home-manager.nix {
    inherit config inputs;
  };
}
