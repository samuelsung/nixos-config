{ lib, osConfig, ... }:

let
  inherit (builtins) filter isString split;
  inherit (lib) mkDefault;
  xCfg = osConfig.services.xserver;
in
{
  home.keyboard = {
    layout = mkDefault xCfg.xkb.layout;
    options = mkDefault (filter isString (split "," xCfg.xkb.options));
    variant = mkDefault xCfg.xkb.variant;
  };
}
