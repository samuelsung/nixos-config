{
  home-manager.sharedModules = [
    ./user.nix
  ];

  services.xserver.xkb = {
    layout = "us";
    options = "ctrl:nocaps";
    variant = "colemak";
  };
}
