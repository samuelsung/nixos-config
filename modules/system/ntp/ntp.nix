{ config, lib, ... }:

let
  inherit (lib) mkEnableOption mkIf;
in
{
  options.services.timesyncd.highRootDistance = mkEnableOption "high root distance";

  config.services.timesyncd.extraConfig =
    mkIf config.services.timesyncd.highRootDistance
      "RootDistanceMaxSec=30";
}
