{ config, lib, pkgs, ... }:

let
  inherit (lib) mapAttrsToList mkOption types;
in
{
  options.environment.binAliases = mkOption {
    type = with types; attrsOf str;
    default = { };
  };

  config.environment.systemPackages = mapAttrsToList
    (package: command: pkgs.writeShellScriptBin package ''
      ${command} $@
    '')
    config.environment.binAliases;
}
