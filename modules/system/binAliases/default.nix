{
  flake.homeManagerModules.system-binAliases = ./binAliases-home-manager.nix;
  flake.nixosModules.system-binAliases = ./binAliases-system.nix;
}
