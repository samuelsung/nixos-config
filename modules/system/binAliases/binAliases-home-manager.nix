{ config, lib, pkgs, ... }:

let
  inherit (lib) mapAttrsToList mkOption types;
in
{
  options.home.binAliases = mkOption {
    type = with types; attrsOf str;
    default = { };
  };

  config.home.packages = mapAttrsToList
    (package: command: pkgs.writeShellScriptBin package ''
      ${command} $@
    '')
    config.home.binAliases;
}
