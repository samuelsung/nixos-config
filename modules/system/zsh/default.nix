{
  flake.homeManagerModules.system-zsh-samuelsung = ./zsh-samuelsung;
  flake.nixosModules.system-zsh = ./zsh-system.nix;
}
