{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkDefault optionalString;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.zsh;
in
{
  programs.oh-my-posh.enableZshIntegration = cfg.enable;
  programs.ghostty.enableZshIntegration = cfg.enable;

  programs.zsh = {
    enable = mkDefault true;
    autosuggestion.enable = true;
    enableCompletion = true;

    sessionVariables = {
      FZF_BASE = "${pkgs.fzf}/bin/fzf";
      FZF_DEFAULT_COMMAND = "${pkgs.ripgrep}/bin/rg --files --hidden";
      KEYTIMEOUT = 1;
      YSU_MESSAGE_FORMAT = "$(${pkgs.ncurses}/bin/tput setaf 4)You should: %command ➜  %alias$(${pkgs.ncurses}/bin/tput sgr0)";
    };

    shellAliases = {
      wget = "wget --hsts-file=\"${config.xdg.cacheHome}/wget-hsts\"";
      grep = "grep --color=auto";
      c = "cd";
    };

    autocd = true;
    defaultKeymap = "viins";
    dotDir = ".config/zsh";

    history = {
      expireDuplicatesFirst = false;
      extended = true; # also save timestamp
      ignoreDups = false;
      ignoreSpace = false;
      path = "${optionalString impermanence "/persist/"}${config.xdg.dataHome}/zsh/zsh_history";
      save = 100000000;
      share = true; # Share command history between zsh sessions.
      size = 100000000;
    };

    initExtra =
      ''
        source ${pkgs.zsh-fast-syntax-highlighting}/share/zsh/site-functions/fast-syntax-highlighting.plugin.zsh
        fast-theme -q ${pkgs.catppuccin-zsh-fsh}/themes/catppuccin-mocha.ini 2>/dev/null
        source ${pkgs.zsh-you-should-use}/share/zsh/plugins/you-should-use/you-should-use.plugin.zsh

        zstyle ':completion:*' menu select
        # Auto complete with case insenstivity
        zstyle ':completion:*' matcher-list '''''' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

        # Include hidden files in autocomplete:
        _comp_options+=(globdots)

        zmodload zsh/complist

        # Edit line in editor
        autoload edit-command-line; zle -N edit-command-line
        bindkey -M vicmd '^v' edit-command-line

        ## menuselect
        ### Use vim keys in tab complete menu:
        bindkey -M menuselect 'h' vi-backward-char
        bindkey -M menuselect 'e' vi-up-line-or-history
        bindkey -M menuselect 'i' vi-forward-char
        bindkey -M menuselect 'n' vi-down-line-or-history

        ## Auto Suggestion
        bindkey -v '^?' backward-delete-char
        bindkey '^ ' autosuggest-accept

        ## vim mode
        ### I use colemak 🙂
        bindkey -M vicmd 'h' vi-backward-char
        bindkey -M vicmd 'e' vi-up-line-or-history
        bindkey -M vicmd 'i' vi-forward-char
        bindkey -M vicmd 'n' vi-down-line-or-history
        bindkey -M vicmd 'u' vi-insert
        bindkey -M vicmd 'U' vi-insert-bol
        bindkey -M vicmd 'l' undo
        bindkey -M vicmd 'L' redo
        bindkey -M vicmd 'k' vi-repeat-search
        bindkey -M vicmd 'K' vi-rev-repeat-search
        bindkey -M visual 'n' down-line
        bindkey -M visual 'e' up-line
        bindkey -M visual 'h' vi-backward-char
        bindkey -M visual 'i' vi-forward-char
        bindkey -M viopp 'n' down-line
        bindkey -M viopp 'e' up-line
        bindkey -M viopp 'h' vi-backward-char
        bindkey -M viopp 'i' vi-forward-char

        ### Change cursor shape for different vi modes.
        function zle-keymap-select {
          if [[ ''${KEYMAP} == vicmd ]] ||
             [[ $1 = 'block' ]]; then
            echo -ne '\e[1 q'

          elif [[ ''${KEYMAP} == main ]] ||
               [[ ''${KEYMAP} == viins ]] ||
               [[ ''${KEYMAP} = '''''' ]] ||
               [[ $1 = 'beam' ]]; then
            echo -ne '\e[5 q'
          fi
        }
        zle -N zle-keymap-select

        ### ci", ci', ci`, di", etc
        autoload -U select-quoted
        zle -N select-quoted
        for m in visual viopp; do
          for c in {a,i}{\',\",\`}; do
            bindkey -M $m $c select-quoted
          done
        done

        ### ci{, ci(, ci<, di{, etc
        autoload -U select-bracketed
        zle -N select-bracketed
        for m in visual viopp; do
          for c in {a,i}''${(s..)^:-'()[]{}<>bB'}; do
            bindkey -M $m $c select-bracketed
          done
        done

        zle-line-init() {
            zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
            echo -ne "\e[5 q"
        }
        zle -N zle-line-init

        # Use beam shape cursor on startup.
        echo -ne '\e[5 q'
        # Use beam shape cursor for each new prompt.
        preexec() { echo -ne '\e[5 q' ;}

        # enable commentings in interactive shell
        # SEE(samuelsung): https://unix.stackexchange.com/questions/557486/allowing-comments-in-interactive-zsh-commands
        setopt interactive_comments
      '';
  };
}
