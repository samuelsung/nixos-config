{ pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    enableCompletion = true;
  };

  environment.pathsToLink = [ "/share/zsh" ];

  users.defaultUserShell = pkgs.zsh;
}
