{
  flake.homeManagerModules.system-direnv = ./direnv.nix;
  flake.nixosModules.system-direnv-system-setup = ./direnv-system-setup.nix;
}
