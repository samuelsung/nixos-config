{ config, lib, pkgs, osConfig, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge mkOption mkOrder types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.direnv;
in
{
  options.utils.developments.nix.enable = mkOption {
    type = types.bool;
    default = config.utils.developments.enable;
    description = "nix development utils";
  };

  config = mkMerge [
    {
      programs.direnv.enable = mkDefault config.utils.developments.nix.enable;
    }

    (mkIf cfg.enable {
      programs.direnv.nix-direnv.enable = true;
      programs.git.ignores = [
        ".direnv"
        ".envrc"
        ".pre-commit-config.yaml"
      ];

      programs.zsh.initExtraBeforeCompInit = mkMerge [
        (mkOrder 1010 ''
          export DIRENV_LOG_FORMAT="$(${pkgs.ncurses}/bin/tput setaf 8)direnv: %s$(${pkgs.ncurses}/bin/tput sgr0)";
          (( ''${+commands[direnv]} )) && emulate zsh -c "$(direnv export zsh)"
        '')
        (mkOrder 1020 ''
          (( ''${+commands[direnv]} )) && emulate zsh -c "$(direnv hook zsh)"
        '')
      ];

      home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
        directories = [
          ".local/share/direnv/allow"
        ];
        allowOther = true;
      };
    })
  ];
}
