{ config, lib, ... }:

let
  inherit (builtins) any attrValues;
  inherit (lib) mkIf;
  anyOneUseDirenv = any
    (userCfg: userCfg.programs.direnv.enable)
    (attrValues config.home-manager.users);
in
mkIf anyOneUseDirenv {
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';
}
