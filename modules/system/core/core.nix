{ lib, pkgs, ... }:

let
  inherit (lib) mkDefault;
in
{
  # Time Zone
  time.timeZone = mkDefault "Asia/Hong_Kong";

  # Locale
  i18n.defaultLocale = mkDefault "en_US.UTF-8";

  boot.loader.grub.theme = pkgs.catppuccin-grub;

  environment.systemPackages = with pkgs; [
    # compression
    gzip
    zip
    unzip
    unrar
    dpkg
    cpio
    p7zip # 7z
    bindfs
    parted
    cryptsetup

    # utils
    git
    wget
    curl
    jq
    coreutils-full
    dbus # read dbus singal
    pciutils # lspci...
    psmisc # killall, pstree...
    openssl
    htop
    acl # for file permission
    file # get file type
    lm_sensors # system temps
    dig # check dns
  ];
}
