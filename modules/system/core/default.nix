{ flake-parts-lib, colors, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules.system-console = importApply ./console.nix { inherit colors; };
  flake.nixosModules.system-core = ./core.nix;
}
