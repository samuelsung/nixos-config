{ colors }:

let
  inherit (colors.lib) hexOf;
  mapHex = map hexOf;

  # deadnix: skip
  _nord = with colors; [
    nord1
    nord11
    nord14
    nord13
    nord9
    nord15
    nord8
    nord5
    nord3
    nord11
    nord14
    nord13
    nord9
    nord15
    nord7
    nord6
  ];

  mapCatppuccin = flavour: with flavour; [
    base
    red
    green
    yellow
    blue
    pink
    teal
    subtext1
    surface2
    red
    green
    yellow
    blue
    pink
    teal
    subtext0
  ];
in
{
  console = {
    font = "Lat2-Terminus16";
    colors = mapHex (mapCatppuccin colors.catppuccin.mocha);
    useXkbConfig = true;
  };
}
