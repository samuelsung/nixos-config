{ config, pkgs, lib, ... }:

with builtins;
with lib;
let
  cfg = config.services.facl;

  toposortedFolders = (toposort
    (a: b: hasPrefix a.folderPath b.folderPath)
    (attrValues cfg.folders)).result;

  getSetSpecialModeCommend =
    path: { suid
          , sgidOnFiles
          , sgidOnDirectories
          , sticky
          , recursive
          }:
    let
      setRecursive = isEnable: if isEnable then "-R" else "";
      setMode = isEnable: if isEnable then "+" else "-";
    in
    ''
      # suid
      chmod ${setRecursive recursive} u${setMode suid}s "${path}"

      # sgid
      ${if sgidOnFiles != sgidOnDirectories && recursive then ''
        find "${path}/" -type d -exec chmod g${setMode sgidOnDirectories}s {} +
        find "${path}/" -type f -exec chmod g${setMode sgidOnFiles}s {} +
      '' else ''
        [ ! -f "${path}" ] || chmod ${setRecursive recursive} g${setMode sgidOnFiles}s "${path}"
        [ ! -d "${path}" ] || chmod ${setRecursive recursive} g${setMode sgidOnDirectories}s "${path}"
      ''}

      # sticky
      chmod ${setRecursive recursive} ${setMode sticky}t "${path}"
    '';

  getSetModeCommand =
    path: { type
          , for ? ""
          , default ? false
          , modes
          , recursive
          }:
    let
      hasMode = m: elem m modes;
      r = if hasMode "r" then "r" else "-";
      w = if hasMode "w" then "w" else "-";
      x = if hasMode "x" then "x" else if hasMode "X" then "X" else "-";
    in
    ''
      setfacl ${if default then "-d" else ""} ${if recursive then "-R" else ""} -m "${type}:${for}:${r}${w}${x}" ${path}
    '';

  configureScript = pkgs.writeShellScriptBin "facl-configure" ''
    # current and desired state
    NEEDED_FOLDERS=$(echo ${concatStringsSep " " (mapAttrsToList (_: { hash, ... }: hash) cfg.folders)} | tr " " "\n")
    REGISTERED_FOLDERS=$(cat "$HOME/registered_folders" 2>/dev/null || true)

    # difference between current and desired state
    NEW_FOLDERS=$(grep -vxF -f <(echo "$REGISTERED_FOLDERS") <(echo "$NEEDED_FOLDERS") || true)
    OLD_FOLDERS=$(grep -vxF -f <(echo "$NEEDED_FOLDERS") <(echo "$REGISTERED_FOLDERS") || true)

    ${concatStringsSep "\n" (map (folder: ''
      if echo "$NEW_FOLDERS" | grep -xq ${folder.hash} || ${if cfg.partialUpdate then "false" else "true"}; then
        mkdir -p "${folder.folderPath}"

        ${if folder.ownerUid != null then ''
          chown ${if folder.applyRecursively then "-R" else ""} "${toString folder.ownerUid}" "${folder.folderPath}"
        '' else ""}

        ${if folder.groupGid != null then ''
          chgrp ${if folder.applyRecursively then "-R" else ""} "${toString folder.groupGid}" "${folder.folderPath}"
        '' else ""}

        ${getSetSpecialModeCommend folder.folderPath {
          suid = folder.suid.enable;
          sgidOnFiles = folder.sgid.enableOnFiles;
          sgidOnDirectories = folder.sgid.enableOnDirectories;
          sticky = folder.sticky.enable;
          recursive = folder.applyRecursively;
        }}

        setfacl ${if folder.applyRecursively then "-R" else ""} --remove-all ${folder.folderPath}

        ${concatStringsSep "\n" (flatten (map
          ({ total, type, defaultAcl, ... }: (mapAttrsToList
            (specificName: specificModes: (
              getSetModeCommand folder.folderPath {
                inherit type;
                for = specificName;
                modes = specificModes;
                default = defaultAcl;
                recursive = folder.applyRecursively;
              }
            ))
            total
          ))
          ((attrValues folder.acls) ++ (attrValues folder.defaultAcls))
        ))}

        echo "folder ${folder.hash} facl configuated."
      fi
    '')
    toposortedFolders)}

    # store needed folders
    mkdir -p "$HOME";
    echo "$NEEDED_FOLDERS" > "$HOME/registered_folders"
  '';

in
{
  config = mkIf cfg.enable {
    systemd.services.facl = {
      description = "facl configuration";
      documentation = [ ];
      after = [ "local-fs.target" "remote-fs.target" "persist-nix-persist.service" ];
      requires = [ "local-fs.target" "remote-fs.target" ];
      wantedBy = [ "multi-user.target" ];
      environment = {
        HOME = "/var/lib/facl-configure";
      };
      path = with pkgs; [
        acl
      ];
      restartIfChanged = true;
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStart = "${configureScript}/bin/facl-configure";
      };
    };

    environment.persistence."/persist" = mkIf config.impermanence.enable {
      hideMounts = true;
      directories = [
        "/var/lib/facl-configure"
      ];
    };
  };
}
