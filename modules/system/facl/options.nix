{ config, lib, ... }:

with builtins;
with lib;
let
  cfg = config.services.facl;

  inherit (config.users) users groups;

  addCheckDesc = desc: elemType: check: types.addCheck elemType check
    // { description = "${elemType.description} (with check: ${desc})"; };

  everyKey = test: set: all test (attrNames set);

  isUser = name: hasAttr name config.users.users;
  isGroup = name: hasAttr name config.users.groups;

  userAttrsOf = V: (addCheckDesc "is all user" (types.attrsOf V) (everyKey isUser));
  groupAttrsOf = V: (addCheckDesc "is all group" (types.attrsOf V) (everyKey isGroup));

  permissionType = types.listOf (types.enum [ "r" "w" "x" "X" ]);

  mkNamedAclOption =
    { default
    , specificAttrsOf ? null
    , description
    , defaultAcl ? false
    }: mkOption {
      type = types.submodule [
        ({ name, config, ... }: {
          options = {
            defaultAcl = mkOption {
              type = types.bool;
              default = defaultAcl;
              readOnly = true;
              description = ''
                if this acl is a default acl of the target
              '';
            };

            type = mkOption {
              type = types.str;
              default = name;
              readOnly = true;
              description = ''
                type of the acl (i.e. user/group/other/mask)
              '';
            };

            all = mkOption {
              type = permissionType;
              inherit default;
              description = ''
                the permission(s) for all uesr (say "user::rwx")
              '';
            };

            total = mkOption {
              type = types.attrsOf permissionType;
              default = { "" = config.all; } // (if hasAttr "specific" config then config.specific else { });
              readOnly = true;
              description = ''
                the union of all and specific acls
              '';
            };

          } // (if specificAttrsOf != null then {
            specific = mkOption {
              type = specificAttrsOf permissionType;
              default = { };
              description = ''
                the permission(s) for a specific uesr (say "user:(name):rwx")
              '';
            };
          } else { });
        })
      ];
      default = { };
      inherit description;
    };

  mkAclOption = { defaultAcl ? false, description }: mkOption {
    type = types.submodule {
      options = {
        user = mkNamedAclOption {
          inherit defaultAcl;
          default = [ "r" "w" "X" ];
          specificAttrsOf = userAttrsOf;
          description = ''
            user acl settings
          '';
        };

        group = mkNamedAclOption {
          inherit defaultAcl;
          default = [ "r" "X" ];
          specificAttrsOf = groupAttrsOf;
          description = ''
            group acl settings
          '';
        };

        other = mkNamedAclOption {
          inherit defaultAcl;
          default = [ ];
          description = ''
            other acl settings
          '';
        };

        mask = mkNamedAclOption {
          inherit defaultAcl;
          default = [ "r" "w" "x" ];
          description = ''
            mask acl settings
          '';
        };
      };
    };
    default = { };
    inherit description;
  };

  folderModule = types.submodule
    ({ name, config, ... }: {
      options = {
        folderPath = mkOption {
          default = name;
          type = types.path;
          description = ''
            the path of the target
          '';
        };

        owner = mkOption {
          type = with types; nullOr str;
          default = null;
          description = ''
            the name of the owner of the target
          '';
        };

        ownerUid = mkOption {
          type = with types; nullOr int;
          default = if config.owner != null then users.${config.owner}.uid else null;
          readOnly = true;
          description = ''
            the uid of the owner of the target
          '';
        };

        group = mkOption {
          type = with types; nullOr str;
          default = null;
          description = ''
            the name of the group of the target
          '';
        };

        groupGid = mkOption {
          type = with types; nullOr int;
          default = if config.group != null then groups.${config.group}.gid else null;
          readOnly = true;
          description = ''
            the gid of the group of the target
          '';
        };

        applyRecursively = mkOption {
          type = types.bool;
          default = false;
          description = ''
            apply the acl recursively
          '';
        };

        suid.enable = mkOption {
          type = types.bool;
          default = false;
          description = ''
            enable suid for the target
          '';
        };

        sgid.enableOnFiles = mkOption {
          type = types.bool;
          default = false;
          description = ''
            enable sgid for the target if target is a file
          '';
        };

        sgid.enableOnDirectories = mkOption {
          type = types.bool;
          default = false;
          description = ''
            enable sgid for the target if target is a directories
          '';
        };

        sticky.enable = mkOption {
          type = types.bool;
          default = false;
          description = ''
            enable sticky for the target
          '';
        };

        acls = mkAclOption {
          description = ''
            acl settings for the target
          '';
        };

        defaultAcls = mkAclOption {
          defaultAcl = true;
          description = ''
            default acl settings for the target
          '';
        };

        childrenPath = mkOption {
          type = with types; listOf str;
          default = filter
            (hasPrefix "${name}/")
            (attrNames cfg.folders);
          readOnly = true;
          description = ''
            all the paths that inside this target
          '';
        };

        dependenciesHash = mkOption {
          type = with types; listOf str;
          default =
            let
              dependencies = filter
                (k: hasPrefix "${k}/" name)
                (attrNames cfg.folders);
            in
            map
              (k: cfg.folders.${k}.hash)
              dependencies;
          readOnly = true;
          description = ''
            a list of all dependencies (parents) hashes.
          '';
        };

        hash = mkOption {
          type = types.str;
          default = "${name}_${substring 0 12 (
            hashString "md5" (unsafeDiscardStringContext (toJSON (
              filterAttrs (k: _: k != "hash") cfg.folders.${name}
            )))
          )}";
          readOnly = true;
          description = ''
            the hash of the configuration of the folder
          '';
        };
      };
    });
in
{
  options.services.facl = {
    enable = mkEnableOption "facl";

    partialUpdate = mkOption {
      type = types.bool;
      default = true;
      description = ''
        only update folders that the acl had been changed (default is true)
      '';
    };

    folders = mkOption {
      default = [ ];
      description = ''
        list of folders to apply facl on.
      '';
      type = types.attrsOf folderModule;
    };
  };
}
