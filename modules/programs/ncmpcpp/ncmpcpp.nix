{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf;
in
{
  programs.ncmpcpp.enable = mkDefault (config.utils.basics.enable && config.desktop.enable);

  programs.ncmpcpp.bindings = [
    { key = "@"; command = "show_server_info"; }
    { key = "#"; command = "dummy"; } # toggle_bitrate_visibility
    { key = ":"; command = "execute_command"; }
    { key = "mouse"; command = "mouse_event"; }

    { key = "left"; command = "previous_column"; } # master_screen # volume_down
    { key = "right"; command = "next_column"; } # slave_screen # volume_up

    { key = "tab"; command = "slave_screen"; } # next_screen
    { key = "tab"; command = "master_screen"; }

    { key = "shift-tab"; command = "dummy"; } # previous_screen

    { key = "enter"; command = "enter_directory"; }
    { key = "enter"; command = "toggle_output"; }
    { key = "enter"; command = "run_action"; }
    { key = "enter"; command = "play_item"; }

    { key = "space"; command = "add_item_to_playlist"; }
    # { key = "space"; command = "toggle_lyrics_update_on_song_change"; }
    { key = "space"; command = "toggle_visualization_type"; }

    { key = "1"; command = "show_playlist"; }
    { key = "2"; command = "show_browser"; }
    { key = "2"; command = "change_browse_mode"; }
    { key = "3"; command = "show_search_engine"; }
    { key = "3"; command = "reset_search_engine"; }
    { key = "4"; command = "show_media_library"; }
    { key = "4"; command = "toggle_media_library_columns_mode"; }
    { key = "5"; command = "show_playlist_editor"; }
    { key = "6"; command = "show_tag_editor"; }
    { key = "7"; command = "show_outputs"; }
    { key = "8"; command = "show_visualizer"; }

    { key = "+"; command = "dummy"; } # volume_up
    { key = "_"; command = "dummy"; } # volume_down

    { key = "y"; command = "save_tag_changes"; }
    { key = "y"; command = "start_searching"; }

    { key = "Y"; command = "dummy"; } # toggle_replay_gain_mode

    { key = "a"; command = "add_selected_items"; }
    { key = "A"; command = "add"; }

    { key = "I"; command = "show_artist_info"; }

    { key = "u"; command = "update_database"; }

    { key = "U"; command = "dummy"; } # toggle_playing_song_centering

    { key = "d"; command = "delete_playlist_items"; }
    { key = "d"; command = "delete_browser_items"; }
    { key = "d"; command = "delete_stored_playlist"; }

    { key = "delete"; command = "delete_playlist_items"; }
    { key = "delete"; command = "delete_browser_items"; }
    { key = "delete"; command = "delete_stored_playlist"; }

    { key = "n"; command = "scroll_down"; }

    { key = "ctrl-n"; command = "move_sort_order_down"; }
    { key = "ctrl-n"; command = "move_selected_items_down"; }

    { key = "escape"; command = "remove_selection"; }

    { key = "e"; command = "scroll_up"; }

    { key = "ctrl-e"; command = "move_sort_order_up"; }
    { key = "ctrl-e"; command = "move_selected_items_up"; }

    { key = "o"; command = "jump_to_playing_song"; }

    { key = "t"; command = "select_item"; }
    { key = "T"; command = "select_found_items"; }

    { key = "H"; command = "move_selected_items_to"; }

    { key = "M"; command = "dummy"; } # move_selected_items_to

    { key = "v"; command = "select_range"; } # reverse_selection
    { key = "V"; command = "dummy"; } # remove_selection
    { key = "ctrl-v"; command = "dummy"; } # select_range

    { key = "E"; command = "edit_lyrics"; }
    { key = "E"; command = "jump_to_tag_editor"; }
    { key = "E"; command = "jump_to_playlist_editor"; }

    { key = "E"; command = "edit_song"; }
    { key = "E"; command = "edit_library_tag"; }
    { key = "E"; command = "edit_library_album"; }
    { key = "E"; command = "edit_directory_name"; }
    { key = "E"; command = "edit_playlist_name"; }

    { key = "ctrl-b"; command = "jump_to_media_library"; }
    { key = "B"; command = "select_album"; }

    { key = "M"; command = "jump_to_media_library"; }

    { key = "~"; command = "dummy"; } # jump_to_media_library

    { key = "h"; command = "previous_column"; }
    { key = "h"; command = "jump_to_parent_directory"; }
    { key = "h"; command = "replay_song"; }

    { key = "f"; command = "seek_forward"; }

    { key = "b"; command = "seek_backward"; }

    { key = "ctrl-f"; command = "apply_filter"; }

    { key = "F"; command = "dummy"; } # fetch_lyrics_in_background

    { key = "i"; command = "next_column"; }
    { key = "i"; command = "enter_directory"; }
    { key = "i"; command = "play_item"; }

    { key = "k"; command = "next_found_item"; }
    { key = "K"; command = "previous_found_item"; }

    { key = ","; command = "dummy"; }
    { key = "."; command = "dummy"; }

    { key = "g"; command = "move_home"; } # jump_to_position_in_song
    { key = "G"; command = "move_end"; }

    { key = "w"; command = "dummy"; } # toggle_find_mode

    { key = "ctrl-g"; command = "show_song_info"; }

    { key = "ctrl-p"; command = "set_selected_items_priority"; }

    { key = "["; command = "scroll_up_album"; }
    { key = "]"; command = "scroll_down_album"; }

    { key = "{"; command = "scroll_up_artist"; }
    { key = "}"; command = "scroll_down_artist"; }

    { key = "f1"; command = "dummy"; } # show_help
    { key = "ctrl-h"; command = "show_help"; }

    { key = ">"; command = "next"; }
    { key = "<"; command = "previous"; }

    { key = "r"; command = "toggle_repeat"; }
    { key = "l"; command = "show_lyrics"; }

    { key = "L"; command = "dummy"; } # toggle_lyrics_fetcher

    { key = "R"; command = "add_random_items"; }

    { key = "ctrl-r"; command = "refetch_lyrics"; } # reverse_playlist

    { key = "p"; command = "pause"; }
    { key = "P"; command = "stop"; } # toggle_display_mode
    { key = "s"; command = "toggle_single"; }
    { key = "c"; command = "toggle_consume"; }
    { key = "C"; command = "clear_playlist"; }
    { key = "C"; command = "clear_main_playlist"; }
    { key = "z"; command = "toggle_random"; }
    { key = "Z"; command = "shuffle"; }
    { key = "x"; command = "toggle_crossfade"; }
    { key = "X"; command = "set_crossfade"; }

    { key = "ctrl-s"; command = "sort_playlist"; }
    { key = "ctrl-s"; command = "toggle_browser_sort_mode"; }
    { key = "ctrl-s"; command = "toggle_media_library_sort_mode"; }

    { key = "S"; command = "save_playlist"; }

    { key = "ctrl-l"; command = "toggle_screen_lock"; }

    { key = "ctrl-t"; command = "toggle_library_tag_type"; } # toggle_add_mode

    { key = "/"; command = "find"; }
    { key = "/"; command = "find_item_forward"; }
    { key = "?"; command = "find"; }
    { key = "?"; command = "find_item_backward"; }

    { key = "\\\\"; command = "dummy"; } # toggle_interface
    { key = "|"; command = "dummy"; } # toggle_mouse
    { key = "!"; command = "dummy"; } # toggle_separators_between_albums
    { key = "alt-l"; command = "dummy"; } # toggle_fetching_lyrics_in_background
    { key = "="; command = "dummy"; } # show_clock

    { key = "q"; command = "quit"; }
  ];

  programs.ncmpcpp.settings = {
    mpd_port = mkIf config.services.mpd.enable (toString config.services.mpd.network.port);

    playlist_disable_highlight_delay = 0;

    # Basics
    external_editor = config.home.sessionVariables.EDITOR;
    use_console_editor = "yes";
    system_encoding = "utf-8";
    regular_expressions = "extended";
    allow_for_physical_item_deletion = "no";
    space_add_mode = "always_add";

    ## Song List ##
    # song_columns_list_format = "(10)[blue]{l} (30)[green]{t} (30)[magenta]{a} (30)[yellow]{b}";
    song_list_format = "{$7%a - $9}{$5%t$9}|{$5%f$9}$R{$6%b $9}{$3%l$9}";
    song_columns_list_format = "(20)[blue]{a} (5f)[red]{d} (6f)[green]{NE} (50)[white]{t|f:Title} (20)[cyan]{b} (7f)[magenta]{l}";

    # Layout
    now_playing_prefix = "$u";
    now_playing_suffix = "$/u";

    user_interface = "alternative";

    display_volume_level = "no";
    display_bitrate = "yes";

    # Motion
    cyclic_scrolling = "no";
    autocenter_mode = "no";
    ## Seeking ##
    incremental_seeking = "yes";
    seek_time = "1";

    # Lyrics
    store_lyrics_in_song_dir = "yes";
    follow_now_playing_lyrics = "yes";
    fetch_lyrics_for_current_song_in_background = "yes";
  };
}
