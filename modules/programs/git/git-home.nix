{ config, lib, osConfig, ... }:

let
  inherit (lib) mkDefault mkIf optionalString;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.git;
in
{
  programs.git = {
    enable = mkDefault config.utils.basics.enable;
    delta.enable = true;
    extraConfig = {
      pull.rebase = false;
      push.autoSetupRemote = true;
      merge.conflictstyle = "diff3";

      commit.verbose = true;

      log.showsignature = false;

      core.quotepath = false;
      safe.directory = "*";
    };
  };

  home.shellAliases = mkIf cfg.enable {
    g = "git";
    ga = "git add";
    gaa = "git add --all";
    gb = "git branch";
    gc = "git commit -v";
    gco = "git checkout";
    gd = "git diff";
    gdc = "git diff --cached";
    gf = "git fetch";
    gl = "git pull";
    gm = "git merge";
    gp = "git push";
    grb = "git rebase";
    gst = "git stash";
  };

  home.persistence."/persist/${config.home.homeDirectory}" =
    mkIf (impermanence && cfg.enable) {
      directories = [
        { directory = "repos"; method = "symlink"; }
      ];
      allowOther = true;
    };

  home.backups.paths = [
    "${optionalString impermanence "/persist"}/home/${config.home.username}/repos"
  ];

  systemd.user.tmpfiles.rules =
    mkIf cfg.enable [
      "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/repos 0755 - - -"
    ];
}
