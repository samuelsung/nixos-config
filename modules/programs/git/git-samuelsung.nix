{
  programs.git = {
    userEmail = "code@samuelsung.net";
    userName = "Samuel Sung";

    extraConfig = {
      commit.gpgsign = true;
      user.signingkey = "D570C81D32940386";
    };
  };
}
