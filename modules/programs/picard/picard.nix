{ config, lib, pkgs, ... }:

let
  cfg = config.programs.picard;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.picard = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.creatives.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.picard;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };
}
