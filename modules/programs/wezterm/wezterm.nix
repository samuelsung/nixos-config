{ config, lib, pkgs, ... }:

let
  inherit (lib) mkAfter mkBefore mkDefault mkMerge;
in
{
  programs.wezterm.enable = mkDefault (config.utils.basics.enable && config.desktop.enable);
  programs.wezterm.package = pkgs.unstable.wezterm;

  programs.wezterm.extraConfig = mkMerge [
    (mkBefore ''
      local config = {}

      if wezterm.config_builder then
        config = wezterm.config_builder()
      end
    '')

    (mkAfter ''
      return config
    '')
  ];
}
