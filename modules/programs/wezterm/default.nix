{
  flake.homeManagerModules.programs-wezterm = ./wezterm.nix;
  flake.homeManagerModules.programs-wezterm-samuelsung = ./wezterm-samuelsung.nix;
}
