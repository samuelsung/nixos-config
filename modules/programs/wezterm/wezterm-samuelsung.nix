{
  programs.wezterm.extraConfig = ''
    config.enable_wayland = true

    config.font = wezterm.font_with_fallback({
      {
        family = "Recursive Mono Casual Static",
        weight = "Medium",
        harfbuzz_features = {
          "dlig", -- enable ligatures
          "ss01", -- simplified 'a'
          "ss02", -- simplified 'g'
          "ss03", -- simplified 'f'
          "ss04", -- simplified 'i'
          "ss05", -- simplified 'l'
          "ss06", -- simplified 'r'
          -- "ss07", -- simplified itatlic diaponals
          "ss08", -- No-serif 'L', 'Z'
          "ss09", -- simplified 6, 9
          "ss10", -- dotted zero
          "ss11", -- simplified 1
          "ss12", -- simplified @
        },
      },
      {
        family = "FiraCode Nerd Font Mono",
        weight = "Medium",
        stretch = "Normal",
        style = "Normal",
        harfbuzz_features = { "cv06", "cv29", "cv31", "ss07", "ss09", "ss25", "ss26", "ss32" }
      },
      { family = "Noto Sans Mono CJK JP", weight = "Regular", stretch = "Normal", style = "Normal", scale = 0.8 },
      { family = "Noto Color Emoji", weight="Regular", stretch="Normal", style="Normal" }
    })

    config.font_size = 12

    config.window_padding = {
      left = 2,
      right = 2,
      top = 0,
      bottom = 0,
    }

    -- NOTE(samuelsung): this should not be required after enable_wayland = true
    config.xcursor_theme = "Catppuccin-Mocha-Dark-Cursors"

    config.color_scheme = "Catppuccin Mocha"

    config.window_background_opacity = 0.75
    config.enable_tab_bar = false
    config.window_close_confirmation = 'NeverPrompt'
    config.warn_about_missing_glyphs = false
    config.check_for_updates = false

    config.disable_default_key_bindings = true

    config.keys = {
      {
        key = 'i',
        mods = 'ALT',
        action = wezterm.action.IncreaseFontSize,
      },

      {
        key = 'h',
        mods = 'ALT',
        action = wezterm.action.DecreaseFontSize,
      },

      {
        key = 'r',
        mods = 'ALT',
        action = wezterm.action.ResetFontSize,
      },

      {
        key = 'e',
        mods = 'ALT',
        action = wezterm.action.ScrollByLine(-3),
      },

      {
        key = 'n',
        mods = 'ALT',
        action = wezterm.action.ScrollByLine(3),
      },

      {
        key = 'v',
        mods = 'ALT',
        action = wezterm.action.PasteFrom('Clipboard'),
      },

      {
        key = 'c',
        mods = 'ALT',
        action = wezterm.action.CopyTo('Clipboard'),
      },
    }
  '';
}
