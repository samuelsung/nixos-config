{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.programs.tmux;
in
{
  home.packages = mkIf cfg.enable [
    pkgs.tmux-sessionizer
  ];

  programs.tmux = {
    enable = mkDefault config.utils.basics.enable;
    package = pkgs.tmux;
    aggressiveResize = false;
    baseIndex = 1;
    clock24 = false;
    mouse = true;
    customPaneNavigationAndResize = false;
    disableConfirmationPrompt = false;
    escapeTime = 0;
    extraConfig =
      ''
        set -g status-bg default
        set -g status-style bg=default

        set -g status-left-length 50

        # C-b is not acceptable -- Vim uses it
        set-option -g prefix C-a
        bind-key C-a last-window
        bind-key a send-prefix

        # colemak mappings
        bind h select-pane -L
        bind n select-pane -D
        bind e select-pane -U
        bind i select-pane -R

        bind -T copy-mode-vi h send-keys -X cursor-left
        bind -T copy-mode-vi n send-keys -X cursor-down
        bind -T copy-mode-vi e send-keys -X cursor-up
        bind -T copy-mode-vi i send-keys -X cursor-right
        bind -T copy-mode-vi k send-keys -X search-again
        bind -T copy-mode-vi K send-keys -X search-reverse

        bind C-i next-window
        bind C-h previous-window

        # Smart pane switching with awareness of Vim splits.
        # See: https://github.com/christoomey/vim-tmux-navigator
        is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
            | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|l?n?vim?x?|fzf)(diff)?$'"

        # I'm used to my <c-w>hkjl/<c-w>hnei
        bind-key -Troot 'C-w' if-shell "$is_vim" 'send-keys C-w' 'switch-client -T tablew'

        bind-key -Ttablew 'h' 'select-pane -L'
        bind-key -Ttablew 'n' 'select-pane -D'
        bind-key -Ttablew 'e' 'select-pane -U'
        bind-key -Ttablew 'i' 'select-pane -R'

        bind -T copy-mode-vi 'C-w' switch-client -Ttablew

        bind-key t run-shell "tmux neww ${pkgs.tmux-sessionizer}/bin/tmux-sessionizer"

        set -g default-terminal "tmux-256color"
        set -ag terminal-overrides ",$TERM:RGB"

        set-window-option -g mode-keys vi
        bind-key -Tcopy-mode-vi v send -X begin-selection
        bind-key -Tcopy-mode-vi y send -X copy-selection-and-cancel

        # fix pane_current_path on new window and splits
        unbind \"
        unbind %
        bind c new-window
        bind v split-window -h
        bind s split-window
      '';

    historyLimit = 5000;
    keyMode = "vi";
    newSession = false;
    plugins = [
      pkgs.tmuxPlugins.catppuccin
    ];

    resizeAmount = 5;
    reverseSplit = false;
    secureSocket = false;
    sensibleOnTop = true;
    terminal = "st-256color";
    tmuxinator.enable = true;
    tmuxp.enable = false;
  };

  home.file.".config/tmuxinator".source = mkIf cfg.enable ./tmuxinator;
}
