{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf readFile;
  cfg = config.programs.anki;
in
{
  services.picom.opacityRules = mkIf cfg.enable [
    "94:class_g = 'Anki'"
  ];

  wayland.windowManager.hyprland.settings.windowrulev2 = mkIf cfg.enable [
    "opacity 0.94,class:(anki)"
  ];

  programs.anki.recolor = {
    enable = true;
    config = readFile "${pkgs.anki-recolor}/themes/(dark) Catppuccin Mocha.json";
  };
}
