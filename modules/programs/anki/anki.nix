{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  inherit (types) bool nullOr str;

  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.anki;

  anki-recolor-with-meta-json = pkgs.symlinkJoin {
    inherit (pkgs.anki-recolor) name;
    paths = [ pkgs.anki-recolor ];
    buildInputs = [ pkgs.makeWrapper pkgs.jq ];
    postBuild =
      if cfg.recolor.config == null then ''
        ln -s "${config.xdg.dataHome}/Anki2/recolor-meta.json" $out/meta.json
      '' else ''
        jq -s '{ "config": (.[0] + .[1]) }' \
        "${pkgs.writeText "recolor-conifg" cfg.recolor.config}" \
        <(echo '{ "version": { "major": -1, "minor": -1 } }') \
        >$out/meta.json
      '';
  };
in
{
  options.programs.anki = {
    enable = mkOption {
      type = bool;
      default = config.desktop.enable;
      description = "anki";
    };

    recolor = {
      enable = mkOption {
        type = bool;
        default = false;
        description = "anki recolor";
      };

      config = mkOption {
        type = nullOr str;
        default = null;
        description = "anki recolor's meta.json config";
      };
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.anki
    ];

    home.file = mkIf cfg.recolor.enable {
      ".local/share/Anki2/addons21/ReColor".source =
        anki-recolor-with-meta-json;
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".local/share/Anki2"
      ];
      allowOther = true;
    };
  };
}
