{
  flake.homeManagerModules.programs-anki = ./anki.nix;
  flake.homeManagerModules.programs-anki-samuelsung = ./anki-samuelsung.nix;
}
