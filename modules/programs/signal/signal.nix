{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.communications.signal;
in
{
  options.utils.communications.signal.enable = mkOption {
    type = types.bool;
    default = config.utils.communications.enable && config.desktop.enable;
    description = ''
      enable signal
    '';
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      signal-desktop
    ];

    systemd.user.services.signal-desktop = {
      Unit = {
        Description = pkgs.signal-desktop.name;
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" ];
      };

      Install.WantedBy = [ "graphical-session.target" ];

      Service = {
        ExecStart = "${pkgs.signal-desktop}/bin/signal-desktop --start-in-tray";
        Restart = "always";
        RestartSec = 3;
      };
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/Signal"
      ];
      allowOther = true;
    };
  };
}
