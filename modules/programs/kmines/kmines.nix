{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.kmines;
in
{
  options.utils.games.kmines.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable kmines
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for kmines
        '';
      }
    ];

    home.packages = with pkgs; [
      kmines
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      files = [
        ".config/kminesrc"
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/kminesrc"
    ];

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "f ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.config/kminesrc 0755 - - -"
      ];
  };
}
