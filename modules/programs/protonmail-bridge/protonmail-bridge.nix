{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.utils.communications.protonmail-bridge;
in
{
  options.utils.communications.protonmail-bridge.enable = mkOption {
    type = types.bool;
    default = config.utils.communications.enable;
    description = ''
      enable protonmail-bridge
    '';
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      protonmail-bridge
    ];
  };
}
