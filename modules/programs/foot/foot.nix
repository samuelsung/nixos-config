{ config, lib, osConfig, ... }:

let
  inherit (lib) mkDefault;
in
{
  programs.foot.enable = mkDefault
    (config.utils.basics.enable && osConfig.services.wayland.enable);
}
