localFlake:

{ lib, ... }:

let
  inherit (lib) concatStringsSep mapAttrs;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;

  mapColors = mapAttrs (_: hexOf);

  mapCatppuccin = flavour: with (mapColors flavour); {
    regular0 = surface1;
    regular1 = red;
    regular2 = green;
    regular3 = yellow;
    regular4 = blue;
    regular5 = pink;
    regular6 = teal;
    regular7 = subtext1;
    bright0 = surface2;
    bright1 = red;
    bright2 = green;
    bright3 = yellow;
    bright4 = blue;
    bright5 = pink;
    bright6 = teal;
    bright7 = subtext0;
    background = base;
    foreground = text;
  };
in
{
  programs.foot.settings = {
    main = {
      font = concatStringsSep "," [
        "FiraCode Nerd Font Mono:pixelsize=16:antialias=true:autohint=true:style=Medium"
        "Noto Sans Mono CJK JP:pixelsize=14:style=Regular"
        "Noto Color Emoji:pixelsize=16:style=Regular"
      ];
    };

    key-bindings = {
      # scrollback-up-page=Shift+Page_Up
      # scrollback-up-half-page=none
      # scrollback-up-line=none
      # scrollback-down-page=Shift+Page_Down
      # scrollback-down-half-page=none
      # scrollback-down-line=none
      # clipboard-copy=Control+Shift+c XF86Copy
      # clipboard-paste=Control+Shift+v XF86Paste
      # primary-paste=Shift+Insert
      # search-start=Control+Shift+r
      font-increase = "Mod1+e";
      font-decrease = "Mod1+n";
      font-reset = "Mod1+r";
      # spawn-terminal=Control+Shift+n
      # minimize=none
      # maximize=none
      # fullscreen=none
      # pipe-visible=[sh -c "xurls | fuzzel | xargs -r firefox"] none
      # pipe-scrollback=[sh -c "xurls | fuzzel | xargs -r firefox"] none
      # pipe-selected=[xargs -r firefox] none
      # show-urls-launch=Control+Shift+u
      # show-urls-copy=none
      # show-urls-persistent=none
      # prompt-prev=Control+Shift+z
      # prompt-next=Control+Shift+x
      # unicode-input=none
      # noop=none
    };

    cursor = {
      color = "${hexOf catppuccin.mocha.base} ${hexOf catppuccin.mocha.text}";
    };

    colors = (mapCatppuccin catppuccin.mocha) // {
      alpha = 0.75;
    };
  };
}
