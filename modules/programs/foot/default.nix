{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-foot = ./foot.nix;
  flake.homeManagerModules.programs-foot-samuelsung = importApply ./foot-samuelsung.nix { inherit colors; };
}
