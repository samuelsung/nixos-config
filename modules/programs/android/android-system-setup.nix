{ config, lib, ... }:

let
  inherit (builtins) attrNames;
  inherit (lib) filterAttrs listToAttrs mkIf nameValuePair;
  androidUsers = attrNames (filterAttrs
    (_: userCfg: userCfg.utils.developments.android.enable)
    config.home-manager.users);
  anyOneUseAndroid = androidUsers != [ ];
in
mkIf anyOneUseAndroid {
  programs.adb.enable = true;

  users.users = listToAttrs (map
    (name: nameValuePair name {
      extraGroups = [ "adbusers" ];
    })
    androidUsers);
}
