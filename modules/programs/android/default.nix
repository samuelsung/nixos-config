{
  flake.homeManagerModules.programs-android = ./android.nix;
  flake.nixosModules.programs-android-system-setup = ./android-system-setup.nix;
}
