{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionals types;
  cfg = config.utils.developments.android;
in
{
  options.utils.developments.android.enable = mkOption {
    type = types.bool;
    default = config.utils.developments.enable;
    description = "enable android utils.";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; optionals config.desktop.enable [
      android-studio
    ];
  };
}
