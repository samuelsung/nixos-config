{
  flake.homeManagerModules.programs-zathura = ./zathura.nix;
  flake.homeManagerModules.programs-zathura-samuelsung = ./zathura-samuelsung.nix;
}
