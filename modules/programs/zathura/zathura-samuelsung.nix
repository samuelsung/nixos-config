{ pkgs, ... }:

{
  programs.zathura = {
    options = {
      recolor = true;
    };

    mappings = {
      h = "scroll left";
      n = "scroll down";
      e = "scroll up";
      i = "scroll right";
      k = "search forward";
      K = "search backward";
    };

    extraConfig = ''
      include ${pkgs.catppuccin-zathura}/catppuccin-mocha
    '';
  };
}
