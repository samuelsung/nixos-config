{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.programs.zathura;
in
{
  programs.zathura.enable = mkDefault (config.utils.basics.enable && config.desktop.enable);
  programs.zathura.options = {
    sandbox = "strict";
    selection-clipboard = "clipboard";
  };

  xdg.mimeApps = mkIf cfg.enable {
    defaultApplications = {
      "application/pdf" = "org.pwmt.zathura.desktop";
    };
    associations.added = {
      "application/pdf" = [ "org.pwmt.zathura.desktop" ];
    };
  };
}
