{ pkgs, lib, config, osConfig, ... }:

let
  inherit (lib) mkIf mkOption types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.bitwarden;
in
{
  options.programs.bitwarden.enable = mkOption {
    default = config.utils.basics.enable && config.desktop.enable;
    type = types.bool;
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      bitwarden
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/Bitwarden"
      ];
      allowOther = true;
    };
  };
}
