{ config, lib, pkgs, ... }:

let
  cfg = config.programs.deadbeef;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.deadbeef = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default =
        if cfg.withPlugins
        then pkgs.deadbeef-with-plugins
        else pkgs.deadbeef;
    };

    withPlugins = mkOption {
      type = types.bool;
      default = true;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };
}
