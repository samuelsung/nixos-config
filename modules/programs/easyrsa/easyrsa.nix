{ pkgs, config, lib, ... }:

with lib;
let
  cfg = config.programs.easyrsa;
in
{
  options.programs.easyrsa = {
    enable = mkEnableOption "easyrsa"; # not enable by default
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.easyrsa
      pkgs.openssl
    ];

    # To use the following vars, first export the variable
    # first export the path as EASYRSA_VARS_FILE
    environment.variables = {
      "EASYRSA_VARS_FILE" = "/etc/easyrsa/vars";
    };

    # NOTE(samuelsung): keep the environment variable with -E

    # NOTE(samuelsung): To generate base folders for easyrsa
    # sudo -E easyrsa init-pki # pki stands for "Public Key Infrastructure", which is just the folder structure of easyrsa

    # NOTE(samuelsung): To generate ca:
    # sudo -E easyrsa build-ca
    # now the ca cert should be located at pki/ca.crt

    # NOTE(samuelsung): To generate server certificate:
    # sudo -E easyrsa build-server-full <name> nopass
    # now the cert should be located at pki/issued/<name>.crt

    # NOTE(samuelsung): To generate client certificate:
    # sudo -E easyrsa build-client-full <name> nopass
    # now the cert should be located at pki/issued/<name>.crt

    # NOTE(samuelsung): To generate diffie-hellman param:
    # sudo -E easyrsa gen-dh
    # now the param should be located at pki/dh.pem

    environment.etc."easyrsa/vars" = {
      text = ''
        set_var EASYRSA_DN              "cn_only"
        set_var EASYRSA_KEY_SIZE        4096
        set_var EASYRSA_ALGO            ec
        set_var EASYRSA_CURVE           secp521r1
        set_var EASYRSA_DIGEST          "sha512"
        set_var EASYRSA_NS_SUPPORT      "no"
        set_var EASYRSA_CA_EXPIRE       7500
        set_var EASYRSA_CERT_EXPIRE     365
      '';
      mode = "0400";
    };
  };
}
