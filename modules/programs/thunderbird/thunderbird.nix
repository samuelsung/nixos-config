{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.utils.communications.thunderbird;
in
{
  options.utils.communications.thunderbird.enable = mkOption {
    type = types.bool;
    default = config.utils.communications.enable && config.desktop.enable;
    description = ''
      enable thunderbird
    '';
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      thunderbird
    ];
  };
}
