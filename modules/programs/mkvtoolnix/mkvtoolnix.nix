{ config, lib, pkgs, ... }:

let
  cfg = config.programs.mkvtoolnix;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.mkvtoolnix = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.creatives.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.mkvtoolnix;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };
}
