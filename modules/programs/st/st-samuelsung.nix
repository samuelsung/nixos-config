localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ lib, ... }:
let
  inherit (lib) concatMapAttrs mapAttrs;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;

  mapColors = mapAttrs (_: v: "#${hexOf v}");

  mapCatppuccin = flavour: with (mapColors flavour);
    {
      "color0" = surface1;
      "color1" = red;
      "color2" = green;
      "color3" = yellow;
      "color4" = blue;
      "color5" = pink;
      "color6" = teal;
      "color7" = subtext1;
      "color8" = surface2;
      "color9" = red;
      "color10" = green;
      "color11" = yellow;
      "color12" = blue;
      "color13" = pink;
      "color14" = teal;
      "color15" = subtext0;
      "focusedBackground" = base;
      "unfocusedBackground" = base;
      "foreground" = text;
      "cursorColor" = rosewater;
      "reverseCursorColor" = rosewater;
    };
in
{
  programs.st.package =
    inputs'.st.packages.st-samuelsung;

  xresources.properties = concatMapAttrs
    (k: v: { "st.${k}" = v; })
    ((mapCatppuccin catppuccin.mocha) // {
      "alphaFocused" = "0.75";
      "alphaUnfocused" = "0.75";
    });
})
