{ colors, flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-st = ./st.nix;
  flake.homeManagerModules.programs-st-samuelsung = importApply ./st-samuelsung.nix { inherit colors moduleWithSystem; };
}
