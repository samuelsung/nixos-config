{ config, lib, pkgs, ... }:

let
  cfg = config.programs.st;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.st = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.st;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };
}
