{ config, lib, pkgs, ... }:

let
  inherit (lib) flip listToAttrs mkIf mkOption nameValuePair types;
  cfg = config.programs.nsxiv;
in
{
  options.programs.nsxiv.enable = mkOption {
    default = config.utils.basics.enable && config.desktop.enable;
    type = types.bool;
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      nsxiv
    ];

    xdg.mimeApps.defaultApplications = listToAttrs (map (flip nameValuePair [ "nsxiv.desktop" ]) [
      "image/bmp"
      "image/gif"
      "image/jpeg"
      "image/jpg"
      "image/png"
      "image/tiff"
      "image/x-bmp"
      "image/x-portable-anymap"
      "image/x-portable-bitmap"
      "image/x-portable-graymap"
      "image/x-tga"
      "image/x-xpixmap"
      "image/webp"
      "image/heic"
      "image/svg+xml"
      "application/postscript"
      "image/jp2"
      "image/jxl"
      "image/avif"
      "image/heif"
    ]);

    xdg.configFile."nsxiv/exec/key-handler".source = toString (pkgs.writeShellScript "key-handler" ''
      while read file
      do
        case "$1" in
        "C-r")
          ${pkgs.imagemagick}/bin/convert -rotate 90 "$file" "$file" ;;
        "C-c")
          echo -n "$file" | xclip -selection clipboard ;;
        "C-w")
          nitrogen --save --set-zoom-fill "$file" ;;
        esac
      done
    '');
  };
}
