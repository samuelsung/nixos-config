{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.utils.games.chiaki;
in
{
  options.utils.games.chiaki.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable chiaki
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for chiaki
        '';
      }
    ];

    home.packages = with pkgs; [
      chiaki
    ];
  };
}
