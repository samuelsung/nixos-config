{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.lutris;
in
{
  options.utils.games.lutris.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable lutris
    '';
  };


  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for lutris
        '';
      }
    ];

    home.packages = with pkgs; [
      lutris
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        { directory = ".config/lutris"; method = "symlink"; }
        { directory = ".local/share/lutris"; method = "symlink"; }
        ".cache/winetricks"
        { directory = "games"; method = "symlink"; }
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/lutris"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.local/share/lutris"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.cache/winetricks"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/games"
    ];

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.config/lutris 0755 - - -"
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.local/share/lutris 0755 - - -"
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/games 0755 - - -"
      ];
  };
}
