{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.figma-linux;
in
{
  options.programs.figma-linux.enable = mkOption {
    type = types.bool;
    default = config.utils.developments.enable && config.desktop.enable;
    description = ''
      Enable figma-linux.
    '';
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      figma-linux
    ];

    xdg.mimeApps.defaultApplications."x-scheme-handler/figma" =
      [ "figma-linux.desktop" ];
    xdg.mimeApps.associations.added."x-scheme-handler/figma" =
      [ "figma-linux.desktop" ];
  };
}
