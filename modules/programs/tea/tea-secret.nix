{ config, lib, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.tea;
in
{
  options.programs.tea.secretFile = mkOption {
    type = types.nullOr types.str;
    default = null;
    description = "the connecting secret of tea";
  };

  config = {
    systemd.user.tmpfiles.rules = mkIf (cfg.secretFile != null) [
      "L ${config.xdg.configHome}/tea/config.yml - - - - ${cfg.secretFile}"
    ];
  };
}
