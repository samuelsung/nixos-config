{
  flake.homeManagerModules.programs-tea.imports = [
    ./tea-secret.nix
    ./tea.nix
  ];
}
