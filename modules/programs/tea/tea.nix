{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.tea;
  patchedTea = pkgs.tea.overrideAttrs (oldAttrs: {
    nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ pkgs.installShellFiles ];
    postInstall = (oldAttrs.postInstall or "") + ''
      substituteInPlace contrib/autocomplete.zsh --replace "\$PROG" "tea";
      substituteInPlace contrib/autocomplete.sh --replace "\$PROG" "tea";

      mv contrib/autocomplete.sh contrib/autocomplete.bash

      installShellCompletion contrib/autocomplete.zsh
      installShellCompletion contrib/autocomplete.bash
    '';
  });

in
{
  options.programs.tea = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable;
      description = "enable tea";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      patchedTea
    ];

    programs.git.aliases = {
      tea = "!tea";
    };
  };
}
