{ pkgs, lib, config, osConfig, ... }:

let
  inherit (lib) mkIf mkOption types;
  impermanence = osConfig.impermanence.enable;
  cfg = config.programs.remmina;
in
{
  options.programs.remmina.enable = mkOption {
    default = config.utils.basics.enable && config.desktop.enable;
    type = types.bool;
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      remmina
      libvncserver
      freerdp
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/remmina"
        ".local/share/remmina"
      ];
      allowOther = true;
    };
  };
}
