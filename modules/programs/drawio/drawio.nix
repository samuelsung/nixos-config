{ pkgs, lib, config, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.drawio;
in
{
  options.programs.drawio.enable = mkOption {
    default = config.utils.basics.enable && config.desktop.enable;
    type = types.bool;
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      drawio
    ];
  };
}
