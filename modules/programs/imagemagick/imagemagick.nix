{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.imagemagick;
in
{
  options.programs.imagemagick = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
      description = "imagemagick";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.imagemagick
    ];
  };
}
