localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ config, lib, pkgs, osConfig, ... }:

let
  inherit (lib) mkIf optionalString;
  impermanence = osConfig.impermanence.enable or false;
in
mkIf config.utils.basics.enable {
  home.packages = with pkgs; [
    inputs'.shuf-less-more.packages.shuf-less-more
    moreutils
    termdown # tui timer
  ];

  home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
    directories = [
      { directory = ".local/state/shuf-less-more"; method = "symlink"; }
    ];
    allowOther = true;
  };

  home.backups.paths = [
    "${optionalString impermanence "/persist"}/home/${config.home.username}/.local/state/shuf-less-more"
  ];

  systemd.user.tmpfiles.rules = [
    "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.local/state/shuf-less-more 0750 - - -"
  ];
})
