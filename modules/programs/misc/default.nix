{ flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-misc = importApply ./misc.nix { inherit moduleWithSystem; };
}
