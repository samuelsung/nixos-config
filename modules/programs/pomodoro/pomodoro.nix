{ pkgs, config, lib, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.pomodoro;
in
{
  options.programs.pomodoro = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
      description = "pomodoro script";
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      pomodoro
    ];
  };
}
