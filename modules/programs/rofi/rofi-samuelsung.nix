localFlake:

{ pkgs, ... }:

let
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
in
{
  programs.rofi.theme = toString (pkgs.writeText "runner.rasi" ''
    * {
      BG: #${hexOf catppuccin.mocha.base}dd;
      BGA: #${hexOf catppuccin.mocha.mauve}ff;
      FG: #${hexOf catppuccin.mocha.text}ff;
      BDR: #${hexOf catppuccin.mocha.mauve}ff;
      SEL: #${hexOf catppuccin.mocha.base}ff;
      ACT: #${hexOf catppuccin.mocha.maroon}ff;
      ACTT: #${hexOf catppuccin.mocha.base}ff;
      UGT: #F28FADff;
      IMG: #FAE3B0ff;
    }

    ${builtins.readFile ./runner.rasi}
  '');
}
