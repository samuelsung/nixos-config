{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-rofi-samuelsung = importApply ./rofi-samuelsung.nix {
    inherit colors;
  };
}
