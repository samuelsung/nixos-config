{ lib, config, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge;
  cfg = config.services.flameshot;
in
mkMerge [
  { services.flameshot.enable = mkDefault (config.utils.basics.enable && config.desktop.enable); }

  (mkIf cfg.enable {
    home.file.".config/flameshot/flameshot.config".source = ./flameshot.ini;
  })
]
