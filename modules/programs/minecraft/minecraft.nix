{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.minecraft;
in
{
  options.utils.games.minecraft.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable minecraft
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for minecraft
        '';
      }
    ];

    home.packages = with pkgs; [
      prismlauncher
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        { directory = ".local/share/PrismLauncher"; method = "symlink"; }
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.local/share/PrismLauncher"
    ];

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.local/share/PrismLauncher 0755 - - -"
      ];
  };
}
