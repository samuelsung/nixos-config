{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  programs.eza = {
    enable = mkDefault config.utils.basics.enable;
    icons = "auto";
    git = true;
    extraOptions = [ "--group-directories-first" ];
  };
}
