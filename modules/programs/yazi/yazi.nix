{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge;
  cfg = config.programs.yazi;

  catppuccin-bat = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "bat";
    rev = "699f60fc8ec434574ca7451b444b880430319941";
    sha256 = "sha256-6fWoCH90IGumAMc4buLRWL0N61op+AuMNN9CAR9/OdI=";
  };

  catppuccin-yazi = pkgs.fetchFromGitHub {
    owner = "catppuccin";
    repo = "yazi";
    rev = "5d50620344a0c7b83f6b5b907457b835844831c3";
    sha256 = "sha256-knYanLMpNyCsFm6csI5MzhyBaqE+p17BAbhNpvey5Co=";
  };

  patchYaziToml = name: batTheme: yaziToml: pkgs.runCommand "patched-${name}.toml" { } ''
    ${pkgs.gnused}/bin/sed 's@^syntect_theme = .*@syntect_theme = "${batTheme}"@g' "${yaziToml}" > $out
  '';
in
mkMerge [
  {
    programs.yazi = {
      enable = mkDefault config.utils.basics.enable;
      package = pkgs.unstable.yazi;

      keymap.manager.prepend_keymap = [
        { on = "e"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "n"; run = "arrow 1"; desc = "Move cursor down"; }
        { on = "h"; run = "leave"; desc = "Go back to the parent directory"; }
        { on = "i"; run = "enter"; desc = "Enter the child directory"; }
        { on = "H"; run = "back"; desc = "Go back to the previous directory"; }
        { on = "I"; run = "forward"; desc = "Go forward to the next directory"; }
        { on = "E"; run = "seek -5"; desc = "Seek up 5 units in the preview"; }
        { on = "N"; run = "seek 5"; desc = "Seek down 5 units in the preview"; }
        { on = "k"; run = "find_arrow"; desc = "Goto the next found"; }
        { on = "K"; run = "find_arrow --previous"; desc = "Goto the previous found"; }
      ];

      keymap.tasks.prepend_keymap = [
        { on = "e"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "n"; run = "arrow 1"; desc = "Move cursor down"; }
      ];

      keymap.spot.prepend_keymap = [
        { on = "e"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "n"; run = "arrow 1"; desc = "Move cursor down"; }
        { on = "h"; run = "swipe -1"; desc = "Swipe to the previous file"; }
        { on = "i"; run = "swipe 1"; desc = "Swipe to the next file"; }
      ];

      keymap.pick.prepend_keymap = [
        { on = "k"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "j"; run = "arrow 1"; desc = "Move cursor down"; }
      ];

      keymap.input.prepend_keymap = [
        { on = "u"; run = "insert"; desc = "Enter insert mode"; }
        { on = "U"; run = [ "move first-char" "insert" ]; desc = "Move to the BOL, and enter insert mode"; }

        { on = "h"; run = "move -1"; desc = "Move back a character"; }
        { on = "i"; run = "move 1"; desc = "Move forward a character"; }

        { on = "e"; run = "forward --end-of-word"; desc = "Move forward to the end of the current or next word"; }
        { on = "E"; run = "noop"; }
      ];

      keymap.confirm.prepend_keymap = [
        { on = "<C-e>"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "<C-n>"; run = "arrow 1"; desc = "Move cursor down"; }

        { on = "k"; run = "noop"; }
        { on = "j"; run = "noop"; }
      ];

      keymap.cmp.prepend_keymap = [
        { on = "<C-e>"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "<C-n>"; run = "arrow 1"; desc = "Move cursor down"; }
      ];

      keymap.help.prepend_keymap = [
        { on = "e"; run = "arrow -1"; desc = "Move cursor up"; }
        { on = "n"; run = "arrow 1"; desc = "Move cursor down"; }
      ];
    };
  }
  (mkIf cfg.enable {
    xdg.configFile."yazi/theme.toml".source = patchYaziToml
      "catppuccin-mocha-mauves"
      "${catppuccin-bat}/themes/Catppuccin Mocha.tmTheme"
      "${catppuccin-yazi}/themes/mocha/catppuccin-mocha-mauve.toml";
  })
]
