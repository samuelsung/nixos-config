{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.moonlight;
in
{
  options.utils.games.moonlight.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable moonlight
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for moonlight
        '';
      }
    ];

    home.packages = with pkgs; [
      moonlight-qt
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/Moonlight Game Streaming Project"
      ];
      allowOther = true;
    };
  };
}
