{ config, lib, ... }:

let
  inherit (lib) mkDefault mkEnableOption mkIf;
  cfg = config.programs.notes-samuelsung;
in
{
  options.programs.notes-samuelsung = {
    enable = mkEnableOption "notes of samuelsung";
  };

  config = mkIf cfg.enable {
    repos.notes = {
      enable = true;
      notes-samuelsung = {
        repoPath = "samuelsung/notes";
        bare = false;
        remotes.origin = {
          fetch = mkDefault "gitea@alt-gitea.samuelsung.net:samuelsung/notes.git";
          push = "gitea@alt-gitea.samuelsung.net:samuelsung/notes.git";
        };
      };
    };
  };
}
