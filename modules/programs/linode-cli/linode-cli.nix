{ config, lib, pkgs, ... }:

let
  cfg = config.programs.linode-cli;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.linode-cli = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.managements.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.linode-cli;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };
}
