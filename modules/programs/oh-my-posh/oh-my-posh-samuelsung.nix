{ lib, ... }:

let
  inherit (lib) concatStrings mkDefault;
in
{
  programs.oh-my-posh = {
    enable = mkDefault true;
    settings = {
      disable_notice = true;
      auto_upgrade = false;
      blocks = [
        {
          alignment = "left";
          segments = [
            {
              type = "session";
              template = "{{ if .SSHSession }}{{ .UserName }}@{{ .HostName }}{{ end }}";
              foreground = "p:white";
              background = "p:surface1";
              style = "diamond";
              leading_diamond = "";
              trailing_diamond = " ";
            }
            {
              type = "path";
              foreground = "p:blue";
              properties = {
                home_icon = "~";
                style = "powerlevel";
                max_width = 40;
              };
              template = "{{ .Path }}";
              background = "p:surface1";
              style = "diamond";
              leading_diamond = "";
              trailing_diamond = "";
            }
            {
              type = "git";
              properties = {
                branch_icon = "";
                branch_identical_icon = "";
                branch_gone_icon = "";
                cherry_pick_icon = "ue29b ";
                commit_icon = "uf417 ";
                fetch_status = true;
                fetch_upstream_icon = true;
                merge_icon = "ue727 ";
                no_commits_icon = "uf0c3 ";
                rebase_icon = "ue728 ";
                revert_icon = "uf0e2 ";
                tag_icon = "uf412 ";
                source = "cli";
              };
              template = concatStrings [
                "{{ .UpstreamIcon }}"
                "{{ if .Upstream }}{{ .Upstream }}  {{ end }}"
                "{{ .HEAD }}"
                "{{ if .BranchStatus }} {{ .BranchStatus }}{{ end }}"
                "{{ if gt .StashCount 0 }} *{{ .StashCount }}{{ end }}"
                "{{ if .Working.Changed }}"
                "{{ if .Working.Untracked }} <blue>?{{ .Working.Untracked }}</>{{ end }}"
                "{{ if .Working.Added }} <yellow>+{{ .Working.Added }}</>{{ end }}"
                "{{ if .Working.Modified }} <yellow>!{{ .Working.Modified }}</>{{ end }}"
                "{{ if .Working.Deleted }} <red>-{{ .Working.Deleted }}</>{{ end }}"
                "{{ if .Working.Unmerged }} <red>x{{ .Working.Unmerged }}</>{{ end }}"
                "{{ end }}"
                "{{ if .Staging.Changed }} |"
                "{{ if .Staging.Untracked }} <blue>?{{ .Staging.Untracked }}</>{{ end }}"
                "{{ if .Staging.Added }} <yellow>+{{ .Staging.Added }}</>{{ end }}"
                "{{ if .Staging.Modified }} <yellow>!{{ .Staging.Modified }}</>{{ end }}"
                "{{ if .Staging.Deleted }} <red>-{{ .Staging.Deleted }}</>{{ end }}"
                "{{ if .Staging.Unmerged }} <red>x{{ .Staging.Unmerged }}</>{{ end }}"
                "{{ end }}"
              ];
              foreground = "p:green";
              background = "p:surface1";
              style = "diamond";
              leading_diamond = " ";
              trailing_diamond = "";
            }
          ];
          type = "prompt";
        }
        {
          alignment = "right";
          segments = [
            {
              type = "status";
              foreground = "p:red";
              background = "p:surface1";
              template = "{{ if .Error }}✘ {{ .String }}{{ end }}";
              properties = {
                always_enabled = true;
              };
              style = "diamond";
              leading_diamond = " ";
              trailing_diamond = "";
            }
            {
              type = "nix-shell";
              template = "{{ if ne (.Type) \"unknown\" }} {{ .Type }}{{ end }}";
              foreground = "blue";
              background = "p:surface1";
              style = "diamond";
              leading_diamond = " ";
              trailing_diamond = "";
            }
            {
              type = "executiontime";
              template = "took  {{ .FormattedMs }}";
              foreground = "p:yellow";
              background = "p:surface1";
              properties = {
                threshold = 500;
                style = "austin";
                always_enabled = false;
              };
              style = "diamond";
              leading_diamond = " ";
              trailing_diamond = "";
            }
          ];
          type = "prompt";
        }
        {
          alignment = "left";
          newline = true;
          segments = [
            {
              properties = {
                always_enabled = true;
              };
              foreground = "p:green";
              foreground_templates = [
                "{{ if .Error }}p:red{{ end }}"
              ];
              style = "plain";
              template = "->";
              type = "status";
            }
          ];
          type = "prompt";
        }
      ];
      final_space = true;
      palette = {
        rosewater = "#f5e0dc";
        flamingo = "#f2cdcd";
        pink = "#f5c2e7";
        mauve = "#cba6f7";
        red = "#f38ba8";
        maroon = "#eba0ac";
        peach = "#fab387";
        yellow = "#f9e2af";
        green = "#a6e3a1";
        teal = "#94e2d5";
        sky = "#89dceb";
        sapphire = "#74c7ec";
        blue = "#89b4fa";
        lavender = "#b4befe";
        text = "#cdd6f4";
        subtext1 = "#bac2de";
        subtext0 = "#a6adc8";
        overlay2 = "#9399b2";
        overlay1 = "#7f849c";
        overlay0 = "#6c7086";
        surface2 = "#585b70";
        surface1 = "#45475a";
        surface0 = "#313244";
        base = "#1e1e2e";
        mantle = "#181825";
        crust = "#11111b";
      };
      version = 3;
    };
  };
}
