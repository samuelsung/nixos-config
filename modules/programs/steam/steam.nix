{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.steam;
in
{
  options.utils.games.steam.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable steam
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for steam
        '';
      }
    ];

    home.packages = with pkgs; [
      steam
      unstable.protontricks
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        { directory = ".local/share/Steam"; method = "symlink"; }
      ];
      allowOther = true;
    };

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.local/share/Steam 0755 - - -"
      ];
  };
}
