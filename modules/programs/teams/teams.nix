{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.communications.teams;
in
{
  options.utils.communications.teams.enable = mkOption {
    type = types.bool;
    default = config.utils.communications.enable && config.desktop.enable;
    description = ''
      enable teams
    '';
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      teams-for-linux
    ];

    xdg.mimeApps.defaultApplications = {
      "x-scheme-handler/msteams" = [ "teams-for-linux.desktop" ];
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/teams-for-linux"
      ];
      allowOther = true;
    };
  };
}
