{ config, lib, pkgs, ... }:

let
  cfg = config.programs.gimp;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.gimp = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.creatives.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.gimp;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      cfg.package
    ];
  };
}
