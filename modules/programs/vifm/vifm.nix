{ config, pkgs, osConfig, lib, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge;
  impermanence = osConfig.impermanence.enable or false;

  cfg = config.programs.vifm;
in
mkMerge [
  {
    programs.vifm.enable = mkDefault config.utils.basics.enable;
  }

  (mkIf cfg.enable {
    home.packages = [ pkgs.vifm ];
    home.file.".config/vifm/vifmrc".source = ./vifmrc;
    home.file.".config/vifm/colors".source = ./colors;
    home.file.".local/share/applications/vifm.desktop".source = ./vifm.desktop;

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".local/share/vifm/Trash"
      ];
      allowOther = true;
    };

    xdg.mimeApps.defaultApplications = {
      "inode/directory" = [ "vifm.desktop" ];
      "inode/mount-point" = [ "vifm.desktop" ];
    };
  })

  (mkIf (cfg.enable && config.desktop.enable) {
    home.packages = with pkgs; [
      vifmimg
      fontpreview
    ];

    home.shellAliases = {
      vifm = "vifmrun";
    };
  })
]
