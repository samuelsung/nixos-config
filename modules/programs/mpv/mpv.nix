{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  programs.mpv.enable = mkDefault (config.utils.basics.enable && config.desktop.enable);
}
