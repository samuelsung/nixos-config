{ pkgs, lib, config, osConfig, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.libreoffice;
in
{
  options.programs.libreoffice.enable = mkOption {
    default = config.utils.basics.enable && config.desktop.enable;
    type = types.bool;
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      libreoffice
    ];

    xdg.mimeApps.defaultApplications = {
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = [ "writer.desktop" ];
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf osConfig.impermanence.enable {
      directories = [
        ".config/libreoffice"
      ];
      allowOther = true;
    };
  };
}
