{ config, lib, osConfig, ... }:

let
  inherit (lib) mkOption mkIf optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.developments.rust;
in
{
  options.utils.developments.rust.enable = mkOption {
    type = types.bool;
    default = config.utils.developments.enable;
    description = "enable rust utils/configurations.";
  };

  config = mkIf cfg.enable {
    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        { directory = ".cargo"; method = "symlink"; } # cargo
      ];
      allowOther = true;
    };

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.cargo 0755 - - -"
      ];
  };
}
