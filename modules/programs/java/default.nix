{
  flake.homeManagerModules.programs-java = ./java.nix;
  flake.homeManagerModules.programs-rust = ./rust.nix;
}
