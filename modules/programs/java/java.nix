{ config, lib, osConfig, ... }:

let
  inherit (lib) mkOption mkIf optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.developments.java;
in
{
  options.utils.developments.java.enable = mkOption {
    type = types.bool;
    default = config.utils.developments.enable;
    description = "enable java utils/configurations.";
  };

  config = mkIf cfg.enable {
    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        { directory = ".m2"; method = "symlink"; } # maven
      ];
      allowOther = true;
    };

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.m2 0755 - - -"
      ];
  };
}
