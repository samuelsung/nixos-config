{ config, lib, pkgs, osConfig, ... }:

let
  cfg = config.programs.guitarix;
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;

  pipewire-jack-guitarix = pkgs.symlinkJoin rec {
    name = "guitarix";
    paths = [ cfg.package ];
    buildInputs = [ pkgs.makeWrapper ];
    postBuild = ''
      wrapProgram $out/bin/${name} \
        --prefix LD_LIBRARY_PATH : "${lib.makeLibraryPath [ pkgs.pipewire.jack ]}"
    '';
  };
in
{
  options.programs.guitarix = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.creatives.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.guitarix;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      (if osConfig.services.pipewire.jack.enable
      then pipewire-jack-guitarix
      else cfg.package)
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/guitarix"
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/guitarix"
    ];
  };
}
