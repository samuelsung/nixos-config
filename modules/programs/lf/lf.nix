{ config, lib, pkgs, ... }:

let
  inherit (lib) makeBinPath mkDefault mkIf;
  cfg = config.programs.lf;
in
{
  programs.lf = {
    enable = mkDefault config.utils.basics.enable;
    commands = {
      open = ''
        &{{
          case $(file --mime-type -Lb $f) in
            text/*) lf -remote "send $id \$vi \$fx";;
            *) for f in $fx; do xdg-open $f > /dev/null 2> /dev/null & done;;
          esac
        }}
      '';

      # on-select = ''
      #   &{{
      #     lf -remote "send $id set statfmt \"$(${pkgs.exa}/bin/exa -ld --color=always "$f")\""
      #   }}
      # '';

      on-cd = ''
        &{{
          # display git repository status in your prompt
          source ${pkgs.git}/share/bash-completion/completions/git-prompt.sh
          GIT_PS1_SHOWDIRTYSTATE=auto
          GIT_PS1_SHOWSTASHSTATE=auto
          GIT_PS1_SHOWUNTRACKEDFILES=auto
          GIT_PS1_SHOWUPSTREAM=auto
          GIT_PS1_COMPRESSSPARSESTATE=auto
          git=$(__git_ps1 " [GIT BRANCH:> %s]") || true
          fmt="\033[32;1m%u@%h\033[0m:\033[34;1m%w\033[0m\033[33;1m$git\033[0m"
          lf -remote "send $id set promptfmt \"$fmt\""
        }}
      '';
    };
    keybindings = {
      e = "up";
      n = "down";
      i = "open";
      ZZ = "quit";
      ZQ = "quit";
      DD = "delete";
      l = "unselect";
      u = "rename";
      U = ":rename; cmd-home";
      A = ":rename; cmd-end";
      c = ":rename; cmd-delete-home";
      C = ":rename; cmd-end; cmd-delete-home";
    };
    extraConfig = ''
      set icons
      set sixel
    '';

    previewer.source =
      pkgs.writeShellScript "pv.sh" ''
        case "$(file -Lb --mime-type -- "$1")" in
          image/*)
            ${pkgs.chafa}/bin/chafa -f sixel -s "$2x$3" --animate off --polite on "$1"
          ;;
          application/pdf)
            ${pkgs.poppler_utils}/bin/pdftotext "$1" -
          ;;

          application/zip)
            ${pkgs.unzip}/bin/unzip -l "$1"
          ;;

          application/x-rar)
            ${pkgs.unrar}/bin/unrar l "$1"
          ;;

          application/x-7z-compressed)
            ${pkgs.p7zip}/bin/7z l "$1"
          ;;

          *)
            case "$1" in
              *.tar*)
                ${pkgs.gnutar}/bin/tar tf "$1"
              ;;
              *)
                ${pkgs.highlight}/bin/highlight -O ansi "$1"
              ;;
            esac
              ;;
        esac
      '';
  };

  xdg.configFile."lf/icons" = mkIf cfg.enable {
    source = ./icons;
  };

  programs.zsh.initExtra = mkIf cfg.enable
    ''
      # Use lf to switch directories and bind it to ctrl-o
      lfcd() {
        PATH=${makeBinPath [
          pkgs.coreutils
        ]}''${PATH:+:}$PATH

        tmp=$(mktemp)
        lf -last-dir-path="$tmp" "$@"
        if [ -f "$tmp" ]; then
          dir=$(<"$tmp")
          rm -f "$tmp"
          if [ -d "$dir" ] && [ "$dir" != "$(pwd)" ]; then
            cd "$dir"
          fi
        fi
      }

      bindkey -s '^o' 'lfcd\n'
    '';
}
