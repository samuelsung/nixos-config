{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-swaylock-samuelsung = importApply ./swaylock-samuelsung.nix {
    inherit colors;
  };
  flake.nixosModules.programs-swaylock = ./swaylock.nix;
}
