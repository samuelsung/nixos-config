localFlake:

{ pkgs, ... }:

let
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
in
{
  programs.swaylock.package = pkgs.swaylock-effects;
  programs.swaylock.settings = {
    ignore-empty-password = true;
    grace = 0;

    bs-hl-color = hexOf catppuccin.mocha.mauve;
    caps-lock-bs-hl-color = hexOf catppuccin.mocha.yellow;
    caps-lock-key-hl-color = hexOf catppuccin.mocha.yellow;
    clock = true;
    effect-blur = "7x5";
    effect-vignette = "0.5:0.5";
    fade-in = 0.1;
    font = "FiraCode Nerd Font Mono Med";
    indicator = true;
    indicator-caps-lock = true;
    indicator-radius = 200;
    indicator-thickness = 10;
    inside-caps-lock-color = "${hexOf catppuccin.mocha.base}88";
    inside-clear-color = "${hexOf catppuccin.mocha.base}88";
    inside-color = "${hexOf catppuccin.mocha.base}88";
    inside-ver-color = "${hexOf catppuccin.mocha.base}88";
    inside-wrong-color = "${hexOf catppuccin.mocha.base}88";
    key-hl-color = hexOf catppuccin.mocha.mauve;
    layout-bg-color = hexOf catppuccin.mocha.base;
    layout-border-color = "00000000";
    layout-text-color = hexOf catppuccin.mocha.text;
    line-caps-lock-color = "00000000";
    line-clear-color = "00000000";
    line-color = "00000000";
    line-ver-color = "00000000";
    line-wrong-color = "00000000";
    ring-caps-lock-color = hexOf catppuccin.mocha.yellow;
    ring-clear-color = hexOf catppuccin.mocha.overlay0;
    ring-color = hexOf catppuccin.mocha.overlay0;
    ring-ver-color = hexOf catppuccin.mocha.blue;
    ring-wrong-color = hexOf catppuccin.mocha.red;
    screenshots = true;
    separator-color = "00000000";
    show-keyboard-layout = true;
    text-caps-lock-color = hexOf catppuccin.mocha.yellow;
    text-clear-color = hexOf catppuccin.mocha.text;
    text-color = hexOf catppuccin.mocha.text;
    text-ver-color = hexOf catppuccin.mocha.blue;
    text-wrong-color = hexOf catppuccin.mocha.red;
  };
}
