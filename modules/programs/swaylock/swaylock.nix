{ config, lib, ... }:

let
  inherit (lib) any attrValues mkIf;

  anyOneUseSwaylock = any
    (userCfg: userCfg.programs.swaylock.enable)
    (attrValues config.home-manager.users);
in
{
  config = mkIf anyOneUseSwaylock {
    security.pam.services.swaylock = { };
  };
}
