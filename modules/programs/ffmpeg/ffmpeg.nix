{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.ffmpeg;
in
{
  options.programs.ffmpeg = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
      description = "ffmpeg";
    };

    package = mkOption {
      type = types.package;
      default = pkgs.ffmpeg-full;
      description = "package for ffmpeg";
    };

    aacs.enable = mkOption {
      type = types.bool;
      default = osConfig.bluraySupport.enable;
      description = "aacs support";
    };

    bdplus.enable = mkOption {
      type = types.bool;
      default = osConfig.bluraySupport.enable;
      description = "bdplus support";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      (cfg.package.override {
        libbluray = pkgs.libbluray.override {
          withAACS = cfg.aacs.enable;
          withBDplus = cfg.bdplus.enable;
        };
      })
    ];
  };
}
