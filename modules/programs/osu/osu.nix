{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.osu;
in
{
  options.utils.games.osu.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable osu
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for osu
        '';
      }
    ];

    home.packages = with pkgs; [
      (osu-lazer-bin.override {
        appimageTools = pkgs.appimageTools.override {
          inherit (pkgs) buildFHSEnv;
        };
      })
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".local/share/osu"
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.local/share/osu"
    ];
  };
}
