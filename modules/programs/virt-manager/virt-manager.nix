{ lib, config, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.programs.virt-manager;

  askPassword = osConfig.programs.ssh.askPassword
    or"${pkgs.x11_ssh_askpass}/libexec/x11-ssh-askpass";
in
{
  options.programs.virt-manager.enable = mkOption {
    type = types.bool;
    default = config.utils.basics.enable && config.desktop.enable;
    description = ''
      enable virt-manager
    '';
  };

  config = mkIf cfg.enable {
    home.packages = [
      # NOTE(samuelsung): ensure SSH_ASKPASS is set.
      (pkgs.symlinkJoin {
        inherit (pkgs.virt-manager) name;
        paths = [ pkgs.virt-manager ];
        buildInputs = [ pkgs.makeWrapper ];
        postBuild = "wrapProgram $out/bin/virt-manager --set SSH_ASKPASS ${askPassword}";
      })
    ];

    dconf.settings = {
      "org/virt-manager/virt-manager/connections" = {
        autoconnect = [ "qemu:///system" ];
        uris = [ "qemu:///system" ];
      };
    };
  };
}
