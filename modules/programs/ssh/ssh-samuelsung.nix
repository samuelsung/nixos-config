localFlake:

let
  inherit (localFlake.config.flake.nixosConfigurations) sankureddo;
in

{ config, ... }:

{
  programs.ssh.matchBlocks = {
    gitea-samuelsung-net = {
      host = "gitea.samuelsung.net";
      hostname = "gitea.samuelsung.net";
      port = 22;
      user = "gitea";
      checkHostIP = true;
      extraOptions.Preferredauthentications = "publickey";
    };

    alt-gitea-samuelsung-net = {
      host = "alt-gitea.samuelsung.net";
      hostname = "gitea.samuelsung.net";
      port = 443;
      user = "gitea";
      checkHostIP = true;
      extraOptions.Preferredauthentications = "publickey";
    };

    sankureddo = {
      host = "sankureddo";
      hostname = "sankureddo.local";
      port = 939;
      user = "samuelsung";
      checkHostIP = true;
      forwardAgent = true;
      localForwards = [
        { bind.port = 13000; host.address = "localhost"; host.port = 3000; }
        { bind.port = 14321; host.address = "localhost"; host.port = 4321; }
        { bind.port = 18080; host.address = "localhost"; host.port = 8080; }
        { bind.port = 19000; host.address = "localhost"; host.port = 9000; }
      ];
      remoteForwards = [
        {
          bind.address = "/run/user/${toString sankureddo.config.users.users.samuelsung.uid}/gnupg/S.gpg-agent";
          host.address = "/\${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.extra";
        }
      ];
      extraOptions.Preferredauthentications = "publickey";
      extraOptions.StreamLocalBindUnlink = "yes";
    };

    babuiru = {
      host = "babuiru";
      port = 939;
      user = "samuelsung";
      checkHostIP = true;
      forwardAgent = true;
      localForwards = [
        { bind.port = 13000; host.address = "localhost"; host.port = 3000; }
        { bind.port = 14321; host.address = "localhost"; host.port = 4321; }
        { bind.port = 18080; host.address = "localhost"; host.port = 8080; }
        { bind.port = 19000; host.address = "localhost"; host.port = 9000; }
      ];
      remoteForwards = [
        {
          bind.address = "/run/user/1001/gnupg/S.gpg-agent";
          host.address = "/\${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.extra";
        }
      ];
      extraOptions.Preferredauthentications = "publickey";
      extraOptions.StreamLocalBindUnlink = "yes";
    };
  };
}
