{ pkgs, ... }:

{
  programs.ssh = {
    macs = [
      "umac-128-etm@openssh.com"
      "hmac-sha2-256-etm@openssh.com"
      "hmac-sha2-512-etm@openssh.com"
    ];

    kexAlgorithms = [
      "diffie-hellman-group14-sha256"
      "diffie-hellman-group16-sha512"
      "diffie-hellman-group18-sha512"
      "diffie-hellman-group-exchange-sha256"
      "curve25519-sha256"
      "curve25519-sha256@libssh.org"
    ];

    hostKeyAlgorithms = [
      "rsa-sha2-512"
      "rsa-sha2-256"
      "ssh-ed25519"
    ];

    enableAskPassword = false;
    forwardX11 = false;
  };

  environment.systemPackages = with pkgs; [
    sshfs
  ];
}
