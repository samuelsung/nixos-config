{ config, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules.programs-ssh = ./ssh.nix;
  flake.nixosModules.programs-ssh-known-hosts = ./ssh-known-hosts.nix;
  flake.homeManagerModules.programs-ssh = ./ssh-home-manager.nix;
  flake.homeManagerModules.programs-ssh-samuelsung =
    importApply ./ssh-samuelsung.nix { inherit config; };
}
