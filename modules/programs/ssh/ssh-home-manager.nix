{ config, lib, osConfig, ... }:

let
  inherit (builtins) filter listToAttrs;
  inherit (lib) mkIf nameValuePair optionalString;

  impermanence = osConfig.impermanence.enable or false;

  keyMapFromThisAccount = filter
    ({ from, ... }: from.account == config.home.username)
    osConfig.services.openssh.sshkeys.keyMapFromThisMachine or [ ];
in
{
  programs.ssh = {
    enable = true;
    userKnownHostsFile = mkIf impermanence "/persist/${config.home.homeDirectory}/.ssh/known_hosts";

    matchBlocks = listToAttrs (map
      ({ name, to, keyFilePath, ... }: nameValuePair name {
        inherit (to) host hostname port;
        user = to.account;
        checkHostIP = true;
        identityFile = keyFilePath;
        extraOptions.Preferredauthentications = "publickey";
      })
      keyMapFromThisAccount);
  };

  systemd.user.tmpfiles.rules = map
    ({ keyFilePath, to, ... }: ''
      L ${config.home.homeDirectory}/.ssh/${optionalString (to.account != null) "${to.account}-"}${to.host} - - - - ${keyFilePath}
      L ${config.home.homeDirectory}/.ssh/${optionalString (to.account != null) "${to.account}-"}${to.host}.pub - - - - ${keyFilePath}.pub
    '')
    keyMapFromThisAccount;

  home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
    files = [
      ".ssh/known_hosts"
    ];
    allowOther = true;
  };
}
