{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  programs.fzf.enable = mkDefault config.utils.basics.enable;
}
