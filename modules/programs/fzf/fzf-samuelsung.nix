localFlake:

{ lib, ... }:
let
  inherit (lib) mapAttrs;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;

  mapColors = mapAttrs (_: v: "#${hexOf v}");

  # NOTE(samuelsung): https://github.com/catppuccin/fzf
  mapCatppuccin = flavour: with (mapColors flavour);
    {
      # bg = base;
      "bg+" = surface0;
      fg = text;
      "fg+" = text;
      header = red;
      hl = red;
      "hl+" = red;
      info = mauve;
      marker = rosewater;
      pointer = rosewater;
      prompt = mauve;
      spinner = flamingo;
    };
in
{
  programs.fzf.colors = mapCatppuccin catppuccin.mocha;
}
