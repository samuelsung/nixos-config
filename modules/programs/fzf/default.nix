{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-fzf = ./fzf.nix;
  flake.homeManagerModules.programs-fzf-samuelsung = importApply
    ./fzf-samuelsung.nix
    { inherit colors; };
}
