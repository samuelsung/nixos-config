{ pkgs, lib, config, osConfig, ... }:

let
  inherit (builtins) toJSON;
  inherit (lib) makeBinPath mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.joplin;

  writeJSONFile = name: obj: pkgs.writeTextFile { inherit name; text = toJSON obj; };

  applySettings = pkgs.writeShellScript "applySettings" ''
    umask 177

    PATH=${makeBinPath [ pkgs.coreutils-full pkgs.jq ]}''${PATH:+:}$PATH

    run_command() {
      rtn=$($1)

      if [ "$rtn" = "" ]; then
        echo '""'
      else
        echo "$rtn" | jq -R
      fi
    }

    run_json_args() {
      echo "{"

      first=true

      jq <"$1" -r 'to_entries[] | (.key,.value)' \
        | while IFS= read -r key && IFS= read -r value; do {
        if [ ! "$first" = true ]; then
          echo ","
        fi
        first=false

        echo "$(echo "$key" | jq -R):$(run_command "$value")";
      }; done

      echo "}"
    }

    install -d -m 0700 -o ${config.home.username} ${config.home.homeDirectory}/.config/joplin-desktop
    config_path=${config.home.homeDirectory}/.config/joplin-desktop/settings.json
    current_config=$(cat "$config_path")

    jq -s '.[0] + .[1] + .[2]' \
      <(echo "$current_config") \
      ${writeJSONFile "joplinapp-settings.json" cfg.config} \
      <(run_json_args ${writeJSONFile "joplinapp-settings.json" cfg.scriptConfig}) >"$config_path"
  '';
in
{
  options.programs.joplin = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
      description = "joplin";
    };

    config = mkOption {
      type = types.attrs;
      default = { };
    };

    scriptConfig = mkOption {
      type = types.attrs;
      default = { };
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      joplin-desktop
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/Joplin"
        ".config/joplin-desktop"
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/Joplin"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/joplin-desktop"
    ];

    programs.joplin.config = {
      "$schema" = "https://joplinapp.org/schema/settings.json";
      locale = "en_US";

      "markdown.plugin.softbreaks" = false;
      "markdown.plugin.typographer" = false;
      "spellChecker.language" = "en-US";

      "ui.layout" = {
        key = "root";
        children = [
          {
            key = "leftContainer";
            children = [
              { key = "sideBar"; visible = true; }
              { key = "noteList"; visible = true; }
            ];
            visible = true;
            width = 250;
          }
          { key = "editor"; visible = true; }
        ];
        visible = true;
      };
      noteVisiblePanes = [ "viewer" ];

      "sync.target" = cfg.sync.type;

      showTrayIcon = true;
      startMinimized = true;
      editor = pkgs.writeShellScript "nvim" ''
        $TERMINAL -e ${config.programs.neovim-flake.neovim.finalPackage}/bin/nvim $@
      '';
      timeFormat = "h:mm A";
      theme = 6;
      themeAutoDetect = false;
      "notes.sortOrder.buttonsVisible" = true;
    };

    systemd.user.services.joplin = {
      Unit = {
        Description = pkgs.joplin-desktop.name;
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" ];
      };

      Install.WantedBy = [ "graphical-session.target" ];

      Service = {
        ExecStartPre = "${applySettings}";
        ExecStart = "${pkgs.joplin-desktop}/bin/joplin-desktop";
        Restart = "always";
        RestartSec = 3;
      };
    };
  };
}
