{ config, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf mkOption types;
  cfg = config.programs.joplin;
in
{
  options.programs.joplin = {
    sync = {
      enable = mkEnableOption "joplin syncing";

      type = mkOption {
        type = with types; nullOr int;
        default = null;
      };

      secretPath = mkOption {
        type = types.str;
        description = "the connecting secret of joplin";
      };
    };
  };

  config.programs.joplin.scriptConfig =
    let
      valueInPath = key: "${pkgs.jq}/bin/jq -r .${key} ${cfg.sync.secretPath}";
    in
    mkIf cfg.sync.enable {
      "sync.${toString cfg.sync.type}.path" = valueInPath "path";
      "sync.${toString cfg.sync.type}.url" = valueInPath "url";
      "sync.${toString cfg.sync.type}.region" = valueInPath "region";
      "sync.${toString cfg.sync.type}.username" = valueInPath "username";
      "sync.${toString cfg.sync.type}.password" = valueInPath "password";
    };
}
