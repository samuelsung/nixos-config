{
  flake.homeManagerModules.programs-joplin.imports = [
    ./joplin-secret.nix
    ./joplin.nix
  ];
}
