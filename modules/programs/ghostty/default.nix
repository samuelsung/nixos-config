{
  flake.homeManagerModules.programs-ghostty = ./ghostty.nix;
  flake.homeManagerModules.programs-ghostty-samuelsung = ./ghostty-samuelsung.nix;
  flake.nixosModules.programs-ghostty = ./ghostty-system.nix;
}
