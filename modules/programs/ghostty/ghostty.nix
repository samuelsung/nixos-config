{ config, lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  programs.ghostty = {
    enable = mkDefault (config.utils.basics.enable && config.desktop.enable);
  };
}
