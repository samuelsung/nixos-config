{
  programs.ghostty = {
    clearDefaultKeybinds = true;

    settings = {
      window-decoration = false;
      gtk-titlebar = false;

      gtk-single-instance = true;
      quit-after-last-window-closed = false;

      background-opacity = .75;

      font-family = "Recursive Mono Casual Static";
      font-style = "Medium";
      font-feature = [
        # enable ligatures
        "dlig"
        # simplified 'a'
        "ss01"
        # simplified 'g'
        "ss02"
        # simplified 'f'
        "ss03"
        # simplified 'i'
        "ss04"
        # simplified 'l'
        "ss05"
        # simplified 'r'
        "ss06"
        # simplified itatlic diaponals
        # "ss07"
        # No-serif 'L', 'Z'
        "ss08"
        # simplified 6, 9
        "ss09"
        # dotted zero
        "ss10"
        # simplified 1
        "ss11"
        # simplified @
        "ss12"
      ];

      font-size = 12;

      window-padding-x = 2;
      window-padding-y = 0;


      auto-update = "off";

      confirm-close-surface = false;

      palette = [
        "0=#45475a"
        "1=#f38ba8"
        "2=#a6e3a1"
        "3=#f9e2af"
        "4=#89b4fa"
        "5=#f5c2e7"
        "6=#94e2d5"
        "7=#a6adc8"
        "8=#585b70"
        "9=#f38ba8"
        "10=#a6e3a1"
        "11=#f9e2af"
        "12=#89b4fa"
        "13=#f5c2e7"
        "14=#94e2d5"
        "15=#a6adc8"
      ];
      background = "181825";
      foreground = "cdd6f4";
      cursor-color = "f5e0dc";
      selection-background = "585b70";
      selection-foreground = "cdd6f4";

      keybind = [
        "alt+i=increase_font_size:1"
        "alt+h=decrease_font_size:1"
        "alt+r=reset_font_size"
        "alt+e=scroll_page_lines:-3"
        "alt+n=scroll_page_lines:3"
        "alt+v=paste_from_clipboard"
        "alt+c=copy_to_clipboard"
      ];
    };
  };
}
