{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  cfg = config.utils.communications.zoom;
in
{
  options.utils.communications.zoom.enable = mkOption {
    type = types.bool;
    default = false; # not enable by default
    description = ''
      enable zoom
    '';
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      zoom-us
    ];
  };
}
