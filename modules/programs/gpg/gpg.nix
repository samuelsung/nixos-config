{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.programs.gpg;
in
{
  home.packages = mkIf cfg.enable [
    pkgs.paperkey
    pkgs.pgpdump
  ];

  programs.gpg = {
    enable = mkDefault config.utils.basics.enable;
    mutableKeys = mkDefault false;
    mutableTrust = mkDefault false;

    settings = {
      autostart = mkDefault false;
      # https://github.com/drduh/config/blob/master/gpg.conf
      # https://www.gnupg.org/documentation/manuals/gnupg/GPG-Configuration-Options.html
      # https://www.gnupg.org/documentation/manuals/gnupg/GPG-Esoteric-Options.html
      # Use AES256, 192, or 128 as cipher
      personal-cipher-preferences = "AES256 AES192 AES";
      # Use SHA512, 384, or 256 as digest
      personal-digest-preferences = "SHA512 SHA384 SHA256";
      # Use ZLIB, BZIP2, ZIP, or no compression
      personal-compress-preferences = "ZLIB BZIP2 ZIP Uncompressed";
      # Default preferences for new keys
      default-preference-list = "SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed";
      # SHA512 as digest to sign keys
      cert-digest-algo = "SHA512";
      # SHA512 as digest for symmetric ops
      s2k-digest-algo = "SHA512";
      # AES256 as cipher for symmetric ops
      s2k-cipher-algo = "AES256";
      # UTF-8 support for compatibility
      charset = "utf-8";
      # Show Unix timestamps
      fixed-list-mode = true;
      # No comments in signature
      no-comments = true;
      # No version in output
      no-emit-version = true;
      # Disable banner
      no-greeting = true;
      # Long hexidecimal key format
      keyid-format = "0xlong";
      # Display UID validity
      list-options = "show-uid-validity";
      verify-options = "show-uid-validity";
      # Display all keys and their fingerprints
      with-fingerprint = true;
      # Display key origins and updates
      #with-key-origin
      # Cross-certify subkeys are present and valid
      require-cross-certification = true;
      # Disable caching of passphrase for symmetrical ops
      no-symkey-cache = true;
      # Enable smartcard
      use-agent = true;
      # Disable recipient key ID in messages
      throw-keyids = true;
      # Default/trusted key ID to use (helpful with throw-keyids)
      #default-key 0xFF3E7D88647EBCDB
      #trusted-key 0xFF3E7D88647EBCDB
      # Group recipient keys (preferred ID last)
      #group keygroup = 0xFF00000000000001 0xFF00000000000002 0xFF3E7D88647EBCDB
      # Keyserver URL
      #keyserver hkps://keys.openpgp.org
      #keyserver hkps://keyserver.ubuntu.com:443
      #keyserver hkps://hkps.pool.sks-keyservers.net
      #keyserver hkps://pgp.ocf.berkeley.edu
      # Proxy to use for keyservers
      #keyserver-options http-proxy=http://127.0.0.1:8118
      #keyserver-options http-proxy=socks5-hostname://127.0.0.1:9050
      # Verbose output
      #verbose
      # Show expired subkeys
      #list-options show-unusable-subkeys
    };

    publicKeys = [
      {
        trust = "ultimate";
        text = ''
          -----BEGIN PGP PUBLIC KEY BLOCK-----

          mQINBGOpbooBEADcKuu0BNKt6CrATrznr1zvHkKoc+A5vx7ElelfgvsgRc51MhJd
          KFw9oHvMoR+DcGftSl1wsRTzCiAueoM9JSvnxG3XWmBonoTrpheqwKnP1HZ+38hJ
          GatuSupnz+6iEjKaxY3oyfDbn77s08jParXEPB80RYnOxWSos5pI/01f1UPxPs53
          TQ1jRqkDzXzUG7Cqs/jW6f+YF5co/ZvnGSycKlkzq7NMYNEOI8G4xy6FV+ckjKdO
          LQX/gGX9udmWuTziiRdv4kT2Mr5KiWC/UHGdPCFHoAxFbnCiA85qMI+1OjLIS7Nt
          WwO5maTUujP8aY3nX6UR7n4tLhNO7fCeksFWgQcH9uQLohmHdIElrQ/ayBbPshfb
          pd7M20Pzf/kqM2xgqOdlcouCSV6vhBO6Ei6dyq1UvpHB8Li84b9VwC0pfXfyAz/W
          +sMLrcOWTH7GHbUkIf2LmVYLZrrtGflKwVUt2oFV3udiW7cprdrucL1+1/rdREUw
          UxdDYW73XqeEgYqcYFzMgmit45IweSBuu7ttLqGKV1qBLHoX82PCjBREWV1ak61M
          JUNwSlLuVoEwpLm7co944wyGF7t/UALU3qcl4zRPC3LU3MZA/+dhpNXpLeKzHYcY
          ORaZeNEUYq0r7TuyopK8FtbwHwBiiGrO1gV4WRmZnN3Tyl3VVccdtNC2PQARAQAB
          tCBzYW11ZWxzdW5nIDxjb2RlQHNhbXVlbHN1bmcubmV0PokCVAQTAQgAPgIbAQUL
          CQgHAgIiAgYVCgkICwIEFgIDAQIeBwIXgBYhBOpzeiW+8tMNyLtfH9VwyB0ylAOG
          BQJk/0pXAhkBAAoJENVwyB0ylAOGeJEQALbGDw2bn9KKFeCLvZXIMop49QddDDRx
          Raz4UwJhcce0awCze8Kcg6EPbdq7wygt2a6cKmZJ858mk0DSx5x8cLz5fFL6WNxf
          WMitd85gMF1R7wnjDjBTkyznfGxdy3LgzlIeZjPaVGjpTbLnqZBt+Tsb5/90M9yr
          miQUrXysSv77uvWrAwaZ9Bptw8BbpgLxb/moBVdvjCzaraoFP5ulir9zfibRM/gC
          9udCpCbjFB0Z05FQwM4sDoIFfWF7T9UjUPCoudFAIK4ZTNfgQl/0Bu5nsHpQJhll
          dPA1LdBRwhF0nS2ofLzgpT9sm09TstPPjgc1jFOWbOF1B+sS6gtPRV2y1m0Wtv5b
          KXG8L3ZJLa7D96J1425sKsukQlK1+AkEiGvSqAQhb6W9ut4XWDoHK5IAHFXP9ZRR
          vCdAqaec/ItEZSnmiz30sgFCai8NBbWSOOAQ/b9Wu+Fgl28tOjXo+CHw1uzREFKz
          dynRaObqYFQ5FFiRShek9pOKs/MwpTKXrDvYLTfeazdIDbWydzGP9MpHSRZ/Xyx6
          V2NUjah79cpiCYCngso5lS5E6ZiSZHmMylsBwWDRqQvoC7bwAGbePfBuk9wwwZDY
          gJWNWQTosRODzfvYIuwrpzz671PvywwYj0Fl7UI7fSoHO+js21TS8Im8JSQ9f5kC
          VzHIAVC7bK+ftClzYW11ZWxzdW5nIDxzdW5naG95aW5Ac2FtdWVsc3VuZzE5OTgu
          bmV0PokCTAQTAQoANgIbAQQLCQgHBBUKCQgFFgIDAQACHgUCF4AWIQTqc3olvvLT
          Dci7Xx/VcMgdMpQDhgUCZP9KVgAKCRDVcMgdMpQDhkS8D/0ew2Mja6MSeGB+wEsT
          ovkwrj4CQWAjChB4iSMeJ7LFc4QOvNFgQzSQFY5e6TzGBfVGlLSjQbvoXyaP8oyg
          1uSzULQ+4nBGojMIXC+sb3TTfZjy4kQ7mBcojOzQjn6mJcQwXJoP6OfsxI8ej/tc
          1DV4IqZSp/paQIXccdLk1lYr3nd6N0oussjpwNoS4vd+6bMbE3g6wxICWw/N+5vw
          VeWfKnrsUu2m+F3xR16lOA4zt1GZGukpebEU3uXp8DifQmmCbG+R+n2+ht2gdsm2
          tPAHaZEkx8i/6icywrNGHYLi5FGVg42V0FOt7VDiy9lwK0BvNhjNd1F90LclHktS
          NrlRL7IfBeDMVtFFiW5y6ZC2RIEd2VNQD9oWew1gev3GV96yEpCbPN1lba7lPAlF
          Q5OLiFTrP6ta08yJ37dfTJcP2IhH0/eGZAePN2to/WJBvrsjVCmdBy8yCw828ma2
          88s6TT1DNJHr7Z4R0HkfD7st1xXU84EXLBJpo1LAQ3qgnw29dBmu41dmJb5Bxjb1
          NewRnFPWvrzHvmrT+99PZWJWZohRiS0WYpKCD5PbYFsYHSSmCC36I7f3geN34xgb
          OWPrUm0gt1ZDVEQJCtCw4Xv/sRYDOBnpLlNLSdS4NxBbuqs8ErA6jvqvyE1xSQ7U
          dlrFllPCKd97TpRVXhmeCwonYLQgc2FtdWVsc3VuZyA8c3VuZ2hveWluQGdtYWls
          LmNvbT6JAkwEEwEKADYCGwEECwkIBwQVCgkIBRYCAwEAAh4FAheAFiEE6nN6Jb7y
          0w3Iu18f1XDIHTKUA4YFAmT/Sk0ACgkQ1XDIHTKUA4ZCUhAArs2jBTgxJaGeT9es
          EQMYb9Ve675wvOlMqo5adRdce36Pg+RPdpRODewijS0ts8L3JBF03y/zlNaYVNXG
          pqhXdNf97KzDRQoH+76N4b33tyPNDpkdfEgdHJGrZ7VM6ZuPycWs64gluF6NBKRv
          WY5rVHJncDizL8vVK6jqUyk7iaMn+gRmbOcT8r71QFYD7YT+sKYN0zZ4hEE05h1b
          DW/JBRuxlO/ENuFxSyTwyewRshULJwV9kOUxi72mnqeQVDRu7qX9+wjiyoSAd7VH
          PDru/gDnPibUQubVPD62FGTTGZLu1h8gq9viYoTC+wumMIzWzKw/16g+NZt8S60F
          kuLTzAahNYn5VVQ7xwmFotw9NjoC/z4V7OHQtBchFbRcgiigvXbSQKyDj5tKgPuS
          0X36+G9USk8DJMCLU2YQ3RYaiG/Kh7jk1vVC8oKneQfQ26D0sJZHGG0o+cvJ7NrP
          oZguWjFPyDaT9EM4diSI2Vb4BHJicEHfO0kSyF3/JZAwbOHztqkvSll8VcK6Ctv2
          ymyMTfZxU8KjB6YNTAG5+IhJyONIZVEaUdl2Ypq9pHcnwEg4Djz3oeV4SA2trfqm
          qq6PXRgMGPNgTaOBM7kN8ar1INMV9cua+qBAx8QoJIjF4U9PpBOIAIDy6GObfJYY
          8ynyBr4TJmmyzN4ABsJ3ezC+lIi5Ag0EY6mdhwEQALcirLSlzf4R1qy+qbXBjgxB
          0FinaHPbUk1HzdMZzuh/8QIkIBancimkXGS7Pqvse8AIUZ9/cRsktbVBAccRAZnn
          tl937YCOZ33LJto7nQZagPxB0RQzb8B0Up8afwhmb67VnUlmR8br5i4jtQEcM0Es
          +jb5ExS+fGHOo/DImSBj1GMwQfkHl5Cx680EZOFVjajtP+/doKT5rEYp65CSyT+p
          mldmgmQVUL58ssNnXfOKDYX0uLxrLr2UJx2V5UySp3J4zwAYMU40OKT7e4JoefVZ
          h78VL7rOlNo2IrRMzpSZMUZDM3qGj9j0zpoaejLyNnJhXjmL3eXEnUOQ98WLcnY4
          39T0Bla96BvhZ88sUu0JFg55fxQ3B5XNG273XsGFa/CN3heRk0ILrCNQoqzgmdyZ
          dBMWrC5/c0cXUydN00fHcYRx7KgzfcaBh9X7DAlxXeBiDGxvnagGQ9OHBSnaOOxK
          QsIkv3hLCwa6LQO6DxcWRkaehI7iwdAuC4IUtFTZaXyfn2yLFBesrY+MVtQzrkeY
          t6d+YxeYSYF+fY7TtrSfh40qdnam9/VWTDXs/f3BmehYLYsooysAgjWiMGb58KTZ
          LCflzvcb2PRpiRcAVoa9NB/6zjBpmyLYiUSe4jpvElp5Xpvlxo9FGX6Hjpp46yBk
          zerhokpyBNcOD4A0b7WXABEBAAGJBHIEGAEKACYCGwIWIQTqc3olvvLTDci7Xx/V
          cMgdMpQDhgUCZ4UMbwUJB53V6AJAwXQgBBkBCgAdFiEEbrGCATPCRKHgCnWoigle
          ARBXDHMFAmOpnYcACgkQigleARBXDHNTdA//W8T5OVaT+uu8/Wo8yShrdvx9DPdL
          40YIE8MtqVYiOe2D2kJcYp05ZuEFbCpVgQ7CbYpHwv+Za4ZhBvc6ybVO+Cb+4ijW
          siGTHe6zO47LlkrFXMgNCJqWCluw8xKA/teKZ3kvKNgCgldzGqhoffH+/A83LZ1k
          9WDKUeKrBXTDouEQwaM7MJmoGSfuucvWVh2yvG3rJozvrKFpMJQFU1CxNfCocYu+
          4RYTsV0A5/oyZzCt1xxodVAKdCtjYaojqtbJ6vT6s4XIROFtV8mokjOZ+K9+fJ+r
          FhUN3KlpepD9pvWGkfiCV6ACy+eqlLaXTDk0UylOiWMCZ2XdAWIb35ju2pdJ5oA6
          2sTjtmAc1k4n8kpNLSh2bUQXzP5hpje/B4WvZIy3ESmeKbJD4oLczKbCpZuAObTz
          S/eai8aTulkYrexuZ3lFYkZJ2Nj925/Nc5hxPUJ6Y4+k2WXIkp563Pxn7ZJ9XTkt
          dWQyFT6XBR6l+L7EWzVz34iMcI+WlvpFrQqoWQxkDNdgBXT2UNHoPtEPmLo0k/cf
          VUkf7ocB0fp2lihmx+lWg1/llJrl5hwnA+W9zdXTqIr0nGNgmo7o9rD6xd7srLhi
          zGRwzBF16fMT65ULexfApTv631C2gv1HYff+PU8A0BJGLHpuJ+TuRO7/4UQmNe9d
          mlSMziEFREIpSsAJENVwyB0ylAOGG/AQALvqexJvq1e3b0/nWwpoFgwDn9AD79bL
          O23hbtghmWfzCnM2LxZgdZbz/hcaZJLtIkElG0F83/h+hl0bDaKft9UnTBgu9Oyf
          XaNZJ1zaWOXt1gVTudS0lIn2BJdEuPdY/Y7Fe2j0M90KHEpcNkSOTJyRhohprmmQ
          MoM2rBEuVyAU2BVcNFkwuct+N1CmFz3KvEiD4ndi2CoQDB94yTvFvVX0TB6ZqkZL
          O8/U4lLdTkU1DjjIaYSrbY8BFn+25cgWe5gGlE3YkAGP0nAIsvZFyAdKzcVMR4/a
          1pnrkJjJ0+HS6jIa9CoJfV+El0AVhHr07YEwnix18TqinHkkoZtEH+hCYQSGM9Ez
          B1+iAuJ3TRPuySHdpsjafp9OUNiNYADPStXslimacrkLJ0el2/xfReYI71NS8aJb
          w3SUze73063Xhm7L+ntPOBmmNhAU/+F2DLQazgbvqVFRrYoS6Sh9RqMRryf00JVl
          n3qYasow2gktZQ9jQPWmU22fb1tje+nwj39nJLlNlfkihHFsRy2997rXNSChlJCQ
          r3F9PgURTgSf+MCipWXDVlfjJKM79L9d96hyJZ9o4CTISqe/bYT0LLActthwG2jg
          9S2vp57bp14d541LSy/4KoatdSLWwKJt3ex+8CC02dJ0R1n0Ci+xZJp/BBmjz293
          wC053h/OwZe8uQINBGOpnbUBEACrG6DFgukHziSwbXx9hHttBX99etrropLPGJ0j
          yn9VB4SpeQAtDwcLg3OFpugJy4AqQ3Ziyul9CoNnkTvsz2qSmMRiXp0wrPA569V9
          FLDgQzwZ/8Eb2xwZc362DrsOUJDCMGzhhNsDbgzmb7L3MPddVVZexVEMIv+avYXv
          VeGuDqCuZKNpbn+bBOq+eeU3HmXkGshAJyRYPZpqSaM1MxAv7TcCwRv/VJAkh/aI
          QLCzwkGcy+JyoDxQi5szB1ZLEjbtAVID9TiPAdTEnbiaffxVEHfjzBVnCauhSqmG
          ZzIPAeuwb6sed7NqP1MaSGPAOhVVYOeZbK+jACW4j6e6Fh5wZv/0H/dr+8IK4OoM
          uCrQ7rafSGq42xlEwCg2slXkFzFP20ur1NauB2L+idMP1KYvQVeGFud2D/MrN4Ph
          IU9ZcLiTWxHTxCfc2tcTD7rfHf+jFneGyupFRqbTACdmRjCFoMctJr2WXYusej1R
          wH0hwyXgyOYRcWSDDbdcY+etYqnG2tRmCAs187uhZnDI92Pieqhi8C41eYChDor8
          sBstFXQs1ba5k77/aeYlSgKc379sv8eCOuYJeYJkSeWbiFBHJdo9PNpYqgy5wSkp
          s/gK24dFN4sqlA7fhQ4PrBd1b2KVbc544NZfOmQKFuR89p2wU3jDFyxuxSDcaDi8
          5m+89QARAQABiQI8BBgBCgAmAhsMFiEE6nN6Jb7y0w3Iu18f1XDIHTKUA4YFAmeF
          DGIFCQed1a0ACgkQ1XDIHTKUA4bPug/9EzqiEu5zr9XEDri+ETJXls4KFarLVA5b
          bjRCw/cLezYIr/JCf6MgocSD7ACxC7He2L/LW8spP4TWKUh0p/lXFmW7V3WxBSxt
          JvouDnmZPVOJEtCqADUGgDYPm9R6BwUCv3bYn23BUizNZSvNv83PhN3obN4YTKJO
          FmHk8tWKQotAy2ubjsWWmmc5+jpPwc4hFPLsUl9Zw7uMDhhEE+KZU27ykBA5FM87
          kEQ+atG7NYxDat7m6cFbnBO1uBqR1vUg6nYiQ88erBrkUTX/RbqBK2U6ZdlyA4/P
          edy1e89lLqXmAoJC5JjRZlT82PlnMwgQYomCJyJJfTWNaISa2bBCWnbdOGMlaeSU
          lp8hEJPV5ISeMux2fe+lxkkTJKAompUfHGH5D5JblF12j4oWd5MEO5yRmnQsIDCL
          S0EUmAa4iaid3xziIuCDwX5hzGDu+M06pC5JLKqToEfHFbz2zXbJKADYnxae3xbD
          tRSx8WjcNsu/H/rhgm8tBem6wbr5uP1EoIp6dV59CeGsIl35fNf4Zq4C8iGyKLsF
          rFskk/ZAdFv2SDjOq7D7W9F1zajkKERXzi6otqUfOOEoP1aCNCy+/RTnlAEKdo7l
          U+/vPZk/oVLQ2vzj9OkRsVxvkss9n9vQElWouLa+WMt+CpZZF+UyeBRXeE9StGa7
          lNmChhqrdIu5Ag0EY6md7gEQAN+6y69j+3NPpi+f/tMLudiuHsM0Y1bo3M5Tpczq
          EULOPEY1HuafeJ6C6rqzI0Bx3ypLdtUcEqpnM8RhScqj3otY8qMkfqu0mx3gqgSj
          ZCVbrRTxYMm3BMpUNqbZf2Sv+EMGL4uWiitxr2C4endy1jh+tkIGg0u8mwxEFkoy
          7FaVhQy8tGVI5Ot6b6RoZRJUwmR5J+KYW4Qu3bfo8QZG3zHQJmvU53N51+VhLXuM
          9Y3DiWGMhE0T8Z608Ry2mZ9tb6rjKMwaFm7nGWE6kwwERJ3MVmx0vPOmLpec22VU
          81Vkl9Em76/iLglY2XBymbitNszHgDq71mov2E/p6EFO426dyKkDJdCxMsyNxqDg
          DEdTtjvJ8IyO77LiLwPoV92pzi7EEWZ4cxBx9s3wpjCDlhC+tAsbJK4gV6fEdFNZ
          RtEY64Z97g1H30pZCDWs+AlmilJ+UCGGT6RY2bSQ7Z2JBSv4DMyq8Bzaiy71qF9Q
          pcE0rwz3EaZnaY7FGfsjlZy6vKM9sUMwdPIC8eun5II5gGgUH6Q9A1+uMnY5Cb8P
          hgGDwTQePMROQgNFXb6i5N9lUoDYQ42wpldnJsEBCGd7xJNUbjbQEbmt3OR/OlDq
          KWHrhQw/PEcKxRIRmVCV8jT2T85k7cuskJ6B4YbfdiJwHPnGEBxL7jLy1ea0G7vX
          6kcXABEBAAGJAjwEGAEKACYCGyAWIQTqc3olvvLTDci7Xx/VcMgdMpQDhgUCZ4UM
          fQUJB53VjwAKCRDVcMgdMpQDhml1EACMnI+Hw7PDLs+qKIsAO/aXWwkNUc8dL9iI
          2wXZYpsCVzEiTpLeodFtmz/D1CpZLvzjGCnbgpkX0dSrMYPm6a6c892QXinAnaHd
          KzXLncXqQOicyWjH1oh51TZDlYQl3gjtEu06UXREC6JjEMCzULvPV1M58vQiOD37
          0SCdfEj86QBFOS6jCqat9VFkDvhC+M/Jpd5ANRqNisdr69nAlwp7qJ1+H0JuR+/+
          NCcK6vbUTrNxxQDkbnQbjOhM9UgvBEITF3EJXga4GgrZuBeTcl305sNr9FbuNSvW
          aO9XDkMu7MHsgeO0hRwIgIVPTLkAlO/plRs5yAcY4ayRGMlDYTZGS/1rr78hSeiE
          oudajykE8wUvKkk6ZYtM9Rj7ZosLbFXLtoWz8v2Uq3Ve2UqveIG1V/FnrYOfa8ot
          nYdSPFidtG9j+Ujreq3KzaUqLEY52cTKm0jRTsHjsd2pxObuJ9VYPAJSs5+O1J0C
          2jmQA2ngwZ3J+jpRcRBMshFaytSfUGLWtTsc/aeQOQh0jo1L6gRMLedlzzVRPa7h
          DYQXUFNKVoPRgScsS/TduG5ULyJU0iaxV/3HObKwXAuhgcTYaH7ctuwem+Xny/dA
          6QqorR6ekGk268XzF2ZNWtm6dIT8dynlQ/hxA2J8cASvp5mUyO+lGLjrbv0UOPrU
          g/tmD4yVBQ==
          =GI+7
          -----END PGP PUBLIC KEY BLOCK-----
        '';
      }
    ];
  };
}
