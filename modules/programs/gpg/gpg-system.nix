{
  programs.gnupg.agent.enableSSHSupport = true;
  programs.gnupg.agent.enableExtraSocket = true;

  systemd.user.tmpfiles.rules = [
    "d %t/gnupg 0700 - - -"
  ];
}
