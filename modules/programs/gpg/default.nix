{
  flake.homeManagerModules.programs-gpg = ./gpg.nix;
  flake.nixosModules.programs-gpg = ./gpg-system.nix;
}
