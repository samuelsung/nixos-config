{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;
  bluraySupport = osConfig.bluraySupport.enable or false;
  cfg = config.programs.vlc;
in
{
  options.programs.vlc = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
      description = "vlc";
    };

    aacs.enable = mkOption {
      type = types.bool;
      default = bluraySupport;
      description = "aacs support";
    };

    bdplus.enable = mkOption {
      type = types.bool;
      default = bluraySupport;
      description = "bdplus support";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      (pkgs.vlc.override {
        libbluray = pkgs.libbluray.override {
          withAACS = cfg.aacs.enable;
          withBDplus = cfg.bdplus.enable;
        };
      })
    ];
  };
}
