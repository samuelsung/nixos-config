localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ config, lib, pkgs, ... }:

let
  inherit (lib) concatStringsSep mkIf mkOption types;
  cfg = config.programs.fetch;
in
{
  options.programs.fetch.enable = mkOption {
    type = types.bool;
    default = config.utils.basics.enable;
    description = "enable fetch applications";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      pfetch
      neofetch
    ] ++ [
      inputs'.fetchfetch.packages.fetchfetch
    ];

    # PF_INFO="ascii title os host kernel wm shell editor uptime memory palette" ${pkgs.dash}/bin/dash ${pkgs.pfetch}/bin/pfetch
    programs.zsh.initExtraBeforeCompInit =
      let
        remainders = [
          "Focus on yourself, not others."
          # https://www.youtube.com/shorts/SvRv6nk3tPs
          "Nobody cares how hard you work."
        ];
      in
      ''
        FF_REMAINDER="${concatStringsSep ";" remainders}" ${inputs'.fetchfetch.packages.fetchfetch}/bin/fetchfetch
      '';

    home.file.".config/neofetch/config.conf".source = ./neofetch-config.conf;
  };
})
