{ flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-fetch = importApply ./fetch.nix { inherit moduleWithSystem; };
}
