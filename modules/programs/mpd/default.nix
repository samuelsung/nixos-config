{ flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-mpd = importApply ./mpd.nix { inherit inputs; };
}
