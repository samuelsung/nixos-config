localFlake:

{ pkgs, config, lib, osConfig, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge optionalString;
  cfg = config.services.mpd;
  move = path: pkgs.runCommand "move"
    {
      inherit path;
      nativeBuildInputs = with pkgs; [ findutils ];
    }
    ''
      mkdir "$out";
      find $path -maxdepth 1 | grep \.m3u8$ | grep -o \.\*\.m3u | while read p; do {
        cp "$p""8" "$out/$(basename $p)"
      }; done;
    '';
in
mkMerge [
  { services.mpd.enable = mkDefault (config.utils.basics.enable && config.desktop.enable); }

  (mkIf cfg.enable {
    home.packages = with pkgs; [
      mpc_cli
    ];

    services.mpd = {
      extraConfig = ''
        auto_update "yes"
        replaygain "auto"

        ${optionalString osConfig.services.pipewire.enable ''
          audio_output {
            type "pipewire"
            name "Pipewire Playback"
          }
        ''}
      '';
      network.port = 5000 + osConfig.users.users.${config.home.username}.uid;
      playlistDirectory = move localFlake.inputs.music-playlist;
    };
  })
]
