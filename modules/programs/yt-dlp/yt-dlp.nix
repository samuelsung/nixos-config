{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.programs.yt-dlp;
in
{
  programs.yt-dlp.enable = mkDefault (config.utils.basics.enable && config.desktop.enable);

  home.packages = mkIf cfg.enable [
    pkgs.youtube-dl-mpv
  ];
}
