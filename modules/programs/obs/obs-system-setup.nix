{ config, lib, ... }:

let
  inherit (builtins) any attrValues;
  inherit (lib) mkIf;
  anyOneUseObs = any
    (userCfg: userCfg.programs.obs-studio.enable)
    (attrValues config.home-manager.users);
in
mkIf anyOneUseObs {
  boot.kernelModules = [ "v4l2loopback" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];

  hardware.pulseaudio.extraConfig = ''
    load-module module-null-sink sink_name=OBSSink sink_properties="device.description='OBSSink'"
    load-module module-loopback source=OBSSink.monitor
  '';

  services.pipewire.extraConfig.pipewire."95-obs-sinks" = {
    context.modules = [
      {
        name = "libpipewire-module-loopback";
        args = {
          audio.position = [ "FL" "FR" ];
          capture.props = {
            node.name = "obssink-input";
            node.description = "OBSSink Input";
            media.class = "Audio/Sink";
            audio.channels = 2;
            audio.position = [ "FL" "FR" ];
          };
          playback.props = {
            node.name = "obssink-output";
            node.description = "OBSSink";
          };
        };
      }
    ];
  };
}
