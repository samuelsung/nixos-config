{
  flake.homeManagerModules.programs-obs = ./obs.nix;
  flake.homeManagerModules.programs-obs-samuelsung = ./obs-samuelsung.nix;
  flake.nixosModules.programs-obs-system-setup = ./obs-system-setup.nix;
}
