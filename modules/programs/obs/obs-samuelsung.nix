{ pkgs, ... }:

{
  home.file.".config/obs-studio/themes" = {
    force = true;
    source = "${pkgs.catppuccin-obs}/themes";
  };
}
