{ config, lib, osConfig, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge optionalString;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.obs-studio;
in
mkMerge [
  {
    programs.obs-studio.enable =
      mkDefault (config.utils.creatives.enable && config.desktop.enable);
  }

  (mkIf cfg.enable {
    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/obs-studio"
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/obs-studio"
    ];
  })
]
