{ config, lib, ... }:

let
  inherit (lib) mkIf;

  cfg = config.utils.communications.vesktop;
in
{
  services.picom.opacityRules = mkIf cfg.enable [
    "94:class_g = 'vesktop'"
  ];

  wayland.windowManager.hyprland.settings.windowrulev2 = mkIf cfg.enable [
    "opacity 0.94,class:(vesktop)"
  ];
}
