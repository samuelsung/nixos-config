{
  flake.homeManagerModules.programs-discord = ./discord.nix;
  flake.homeManagerModules.programs-discord-samuelsung = ./discord-samuelsung.nix;
  flake.homeManagerModules.programs-vesktop = ./vesktop.nix;
  flake.homeManagerModules.programs-vesktop-samuelsung = ./vesktop-samuelsung.nix;
}
