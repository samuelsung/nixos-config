{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf mkOption types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.communications.discord;
  json = pkgs.formats.json { };

  applySettings =
    let
      attr.openasar.css = cfg.openasar.css;
    in
    pkgs.writeShellScript "applySettings" ''
      config_path=${config.home.homeDirectory}/.config/discord/settings.json
      current_config=$(${pkgs.coreutils}/bin/cat "$config_path")

      ${pkgs.jq}/bin/jq -s '
        def deepmerge(a;b):
          reduce b[] as $item (a;
            reduce ($item | keys_unsorted[]) as $key (.;
              $item[$key] as $val | ($val | type) as $type | .[$key] = if ($type == "object") then
                deepmerge({}; [if .[$key] == null then {} else .[$key] end, $val])
              elif ($type == "array") then
                (.[$key] + $val | unique)
              else
                $val
              end)
            );
          deepmerge({}; .)' \
        <(echo "$current_config") \
        ${json.generate "attr" attr} \
        >"$config_path"
    '';
in
{
  options.utils.communications.discord = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        enable discord
      '';
    };

    package = mkOption {
      type = types.package;
      default = pkgs.unstable.discord;
      description = "package for discord";
    };

    finalPackage = mkOption {
      type = types.package;
      default = cfg.package.override {
        withOpenASAR = cfg.openasar.enable;
        withVencord = cfg.vencord.enable;
      };
    };

    openasar = {
      enable = mkEnableOption "openasar support";

      css = mkOption {
        type = types.str;
        default = "";
        description = "openasar css";
      };
    };

    vencord.enable = mkEnableOption "vencord support";
  };

  config = mkIf cfg.enable {
    home.packages = [
      cfg.finalPackage
    ];

    systemd.user.services.discord = {
      Unit = {
        Description = pkgs.master.discord.name;
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" ];
      };

      Install.WantedBy = [ "graphical-session.target" ];

      Service = {
        ExecStartPre = "${applySettings}";
        ExecStart = "${cfg.finalPackage}/bin/discord --start-minimized";
        Restart = "always";
        RestartSec = 3;
      };
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/discord"
      ];
      allowOther = true;
    };
  };
}
