{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf mkOption types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.communications.vesktop;
in
{
  options.utils.communications.vesktop = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.communications.enable && config.desktop.enable;
      description = ''
        enable vesktop
      '';
    };

    package = mkOption {
      type = types.package;
      default = pkgs.unstable.vesktop;
      description = "package for vesktop";
    };

    finalPackage = mkOption {
      type = types.package;
      default = cfg.package.override {
        inherit (cfg) withSystemVencord;
      };
    };

    withSystemVencord = mkEnableOption "with nixpkgs' vencord";
  };

  config = mkIf cfg.enable {
    home.packages = [
      cfg.finalPackage
    ];

    systemd.user.services.vesktop = {
      Unit = {
        Description = cfg.finalPackage.name;
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" ];
      };

      Install.WantedBy = [ "graphical-session.target" ];

      Service = {
        ExecStart = "${cfg.finalPackage}/bin/vesktop --start-minimized";
        Restart = "always";
        RestartSec = 3;
      };
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/vesktop"
      ];
      allowOther = true;
    };
  };
}
