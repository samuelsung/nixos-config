{ config, lib, pkgs, ... }:

let
  inherit (builtins) readFile;
  inherit (lib) mkIf;

  cfg = config.utils.communications.discord;
in
{
  services.picom.opacityRules = mkIf cfg.enable [
    "94:class_g = 'discord'"
  ];

  wayland.windowManager.hyprland.settings.windowrulev2 = mkIf cfg.enable [
    "opacity 0.94,class:(discord)"
  ];

  utils.communications.discord = {
    openasar.enable = false;
    openasar.css = readFile "${pkgs.catppuccin-discord}/catppuccin-mocha-mauve.theme.css";
    vencord.enable = false;
  };
}
