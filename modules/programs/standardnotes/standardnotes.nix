{ pkgs, lib, config, osConfig, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.standardnotes;
in
{
  options.programs.standardnotes.enable = mkOption {
    default = config.utils.basics.enable && config.desktop.enable;
    type = types.bool;
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      standardnotes
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/Standard Notes"
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.config/Standard Notes"
    ];
  };
}
