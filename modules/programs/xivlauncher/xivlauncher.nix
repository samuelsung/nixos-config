{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) mkIf mkOption optionalString types;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.games.xivlauncher;
in
{
  options.utils.games.xivlauncher.enable = mkOption {
    type = types.bool;
    default = config.utils.games.enable;
    description = ''
      enable xivlauncher
    '';
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.desktop.enable;
        message = ''
          desktop support is needed for xivlauncher
        '';
      }
    ];

    home.packages = with pkgs; [
      gshade_installer
      xivlauncher
    ];

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        { directory = ".xlcore"; method = "symlink"; }
        { directory = ".xlcore2"; method = "symlink"; }
        { directory = ".xlcore3"; method = "symlink"; }
        { directory = ".local/share/reshade"; method = "symlink"; }
      ];
      allowOther = true;
    };

    home.backups.paths = [
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.xlcore"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.xlcore2"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.xlcore3"
      "${optionalString impermanence "/persist"}/home/${config.home.username}/.local/share/reshade"
    ];

    systemd.user.tmpfiles.rules =
      mkIf cfg.enable [
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.xlcore 0755 - - -"
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.xlcore2 0755 - - -"
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.xlcore3 0755 - - -"
        "d ${optionalString impermanence "/persist/"}${config.home.homeDirectory}/.local/share/reshade 0755 - - -"
      ];
  };
}
