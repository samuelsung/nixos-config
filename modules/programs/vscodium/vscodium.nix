{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge concatMapStringsSep;
  cfg = config.programs.vscode;
in
mkMerge [
  { programs.vscode.enable = mkDefault (config.desktop.enable && config.utils.developments.enable); }

  (mkIf cfg.enable {
    services.picom.opacityRules = [
      "94:class_g = 'VSCodium'"
    ];

    wayland.windowManager.hyprland.settings.windowrulev2 = mkIf cfg.enable [
      "opacity 0.94,class:(VSCodium)"
    ];

    programs.vscode.package = pkgs.vscodium;
    # Version of Visual Studio Code to install.
    # Type: package
    # Default: (build of vscode-1.51.1)
    # Example:
    # pkgs.vscodium
    # Declared by:
    # <home-manager/modules/programs/vscode.nix> 

    programs.vscode.extensions = with pkgs.vscode-extensions; [
      asvetliakov.vscode-neovim
      catppuccin.catppuccin-vsc
      fwcd.kotlin
      mathiasfrohlich.kotlin
      redhat.vscode-xml
      rust-lang.rust
      vscjava.vscode-java-debug
      vscjava.vscode-java-dependency
      vscjava.vscode-java-test
      vscjava.vscode-maven
      wayou.vscode-todo-highlight
      eamodio.gitlens
      catppuccin.catppuccin-vsc-icons
      dbaeumer.vscode-eslint
      redhat.java
      yzhang.markdown-all-in-one
    ];

    # programs.vscode.haskell.enable
    # Whether to enable Haskell integration for Visual Studio Code.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/programs/vscode/haskell.nix> 

    # programs.vscode.haskell.hie.enable
    # Whether to enable Haskell IDE engine integration.
    # Type: boolean
    # Default: true
    # Declared by:
    # <home-manager/modules/programs/vscode/haskell.nix> 

    # programs.vscode.haskell.hie.executablePath
    # The path to the Haskell IDE Engine executable.
    # Because hie-nix is not packaged in Nixpkgs, you need to add it as an overlay or set this option. Example overlay configuration:
    # nixpkgs.overlays = [
    #   (self: super: { hie-nix = import ~/src/hie-nix {}; })
    # ]
    # Type: path
    # Default: "${pkgs.hie-nix.hies}/bin/hie-wrapper"
    # Example:
    # (import ~/src/haskell-ide-engine {}).hies + "/bin/hie-wrapper";
    # Declared by:
    # <home-manager/modules/programs/vscode/haskell.nix> 

    # programs.vscode.keybindings
    # Keybindings written to Visual Studio Code's keybindings.json.
    # Type: list of submodules
    # Default: [ ]
    # Example:
    # [
    #   {
    #     key = "ctrl+c";
    #     command = "editor.action.clipboardCopyAction";
    #     when = "textInputFocus";
    #   }
    # ]
    # Declared by:
    # <home-manager/modules/programs/vscode.nix> 

    # programs.vscode.keybindings.*.command
    # The VS Code command to execute.
    # Type: string
    # Example: "editor.action.clipboardCopyAction"
    # Declared by:
    # <home-manager/modules/programs/vscode.nix> 

    # programs.vscode.keybindings.*.key
    # The key or key-combination to bind.
    # Type: string
    # Example: "ctrl+c"
    # Declared by:
    # <home-manager/modules/programs/vscode.nix> 

    # programs.vscode.keybindings.*.when
    # Optional context filter.
    # Type: string
    # Default: ""
    # Example: "textInputFocus"
    # Declared by:
    # <home-manager/modules/programs/vscode.nix> 

    programs.vscode.userSettings = {
      # fonts
      "editor.fontFamily" = "'Recursive Mono Casual Static','Noto Color Emoji'";
      "editor.fontWeight" = 500;
      "editor.fontSize" = 16;
      "editor.fontLigatures" = concatMapStringsSep ", " (s: "'${s}'") [
        # enable ligatures
        "dlig"
        # simplified 'a'
        "ss01"
        # simplified 'g'
        "ss02"
        # simplified 'f'
        "ss03"
        # simplified 'i'
        "ss04"
        # simplified 'l'
        "ss05"
        # simplified 'r'
        "ss06"
        # simplified itatlic diaponals
        # "ss07"
        # No-serif 'L', 'Z'
        "ss08"
        # simplified 6, 9
        "ss09"
        # dotted zero
        "ss10"
        # simplified 1
        "ss11"
        # simplified @
        "ss12"
      ];

      "editor.lineNumbers" = "relative";
      "editor.minimap.enabled" = false;
      "editor.renderWhitespace" = "boundary";
      "editor.tabSize" = 2;
      "editor.detectIndentation" = false;
      "editor.trimAutoWhitespace" = true;
      "editor.suggestSelection" = "first";
      "editor.wordWrap" = "on";
      "editor.renderControlCharacters" = true;
      "window.menuBarVisibility" = "toggle";
      "workbench.editor.centeredLayoutAutoResize" = false;

      "workbench.colorTheme" = "Catppuccin Mocha";
      "workbench.preferredLightColorTheme" = "Catppuccin Mocha";
      "workbench.iconTheme" = "catppuccin-mocha";

      "search.smartCase" = true;
      "files.trimFinalNewlines" = true;
      "files.trimTrailingWhitespace" = true;

      "editor.formatOnSave" = true;
      "editor.codeActionsOnSave" = {
        "source.fixAll" = "always";
      };

      "java.semanticHighlighting.enabled" = true;
      "java.refactor.renameFromFileExplorer" = "autoApply";
      "java.project.importHint" = true;
      "java.configuration.checkProjectSettingsExclusions" = true;

      "vscode-neovim.neovimExecutablePaths.linux" = "${config.programs.neovim-flake.vscode.build.package}/bin/nvim";
      "extensions.experimental.affinity"."asvetliakov.vscode-neovim" = 1;

      "files.exclude" = {
        ".devenv" = true;
        ".direnv" = true;
      };

      "search.exclude" = {
        ".devenv" = true;
        ".direnv" = true;
      };

      "redhat.telemetry.enabled" = false;
      "java.jdt.ls.java.home" = "${pkgs.openjdk21}/lib/openjdk";
      "java.configuration.runtimes" = [
        {
          name = "JavaSE-11";
          path = "${pkgs.openjdk11}/lib/openjdk";
        }
        {
          name = "JavaSE-1.8";
          path = "${pkgs.openjdk8}/lib/openjdk";
        }
      ];

      "java.autobuild.enabled" = false;
      "java.import.generatesMetadataFilesAtProjectRoot" = false;
      "java.import.exclusions" = [
        "**/node_modules/**"
        "**/.direnv/**"
        "**/.devenv/**"
        "**/.git/**"
      ];
      "java.project.resourceFilters" = [
        "node_modules"
        "\\.direnv"
        "\\.devenv"
        "\\.git"
      ];

      "files.watcherExclude" = {
        "**/.git/objects/**" = true;
        "**/.devenv/**" = true;
        "**/.direnv/**" = true;
        "**/.git/subtree-cache/**" = true;
        "**/.hg/store/**" = true;
      };

      "[bat]" = {
        "editor.tabSize" = 4;
        "editor.insertSpaces" = true;
      };

      "[css]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[scss]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[html]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[javascript]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[javascriptreact]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[java]" = {
        "editor.insertSpaces" = true;
        "editor.defaultFormatter" = "redhat.java";
      };

      "[json]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[jsonc]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[jsonl]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[markdown]" = {
        "editor.tabSize" = 4;
        "editor.insertSpaces" = true;
      };

      "[sql]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[typescript]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[typescriptreact]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };

      "[xml]" = {
        "editor.tabSize" = 4;
        "editor.insertSpaces" = true;
      };

      "[yaml]" = {
        "editor.tabSize" = 2;
        "editor.insertSpaces" = true;
      };
    };
  })
]
