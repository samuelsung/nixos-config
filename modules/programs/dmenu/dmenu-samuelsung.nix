localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ lib, ... }:
let
  inherit (lib) concatMapAttrs mapAttrs;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;

  mapColors = mapAttrs (_: v: "#${hexOf v}");

  mapCatppuccin = flavour: with (mapColors flavour);
    {
      normfgcolor = text;
      normbgcolor = base;
      selfgcolor = base;
      selbgcolor = sky;
      promptfgcolor = base;
      promptbgcolor = green;
      selhighlightfgcolor = overlay0;
      selhighlightbgcolor = sky;
      normhighlightfgcolor = yellow;
      normhighlightbgcolor = base;
      outfgcolor = "#000000";
      outbgcolor = sky;
    };
in
{
  programs.dmenu.package =
    inputs'.dmenu.packages.dmenu-samuelsung;

  xresources.properties = concatMapAttrs
    (k: v: { "dmenu.${k}" = v; })
    (mapCatppuccin catppuccin.mocha);
})
