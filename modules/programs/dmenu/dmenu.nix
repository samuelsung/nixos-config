{ config, lib, pkgs, ... }:

let
  cfg = config.programs.dmenu;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.dmenu = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.dmenu;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package pkgs.dmenu-emoji ];
  };
}
