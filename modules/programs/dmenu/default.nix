{ colors, flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-dmenu = ./dmenu.nix;
  flake.homeManagerModules.programs-dmenu-samuelsung = importApply ./dmenu-samuelsung.nix { inherit colors moduleWithSystem; };
}
