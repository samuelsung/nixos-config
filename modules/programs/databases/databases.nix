{ config, lib, pkgs, osConfig, ... }:

let
  inherit (lib) types mkOption mkIf mkMerge;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.utils.developments.databases;
in
{
  options.utils.developments.databases.enable = mkOption {
    type = types.bool;
    default = config.utils.developments.enable;
    description = "enable development utils.";
  };

  config = mkIf cfg.enable (mkMerge [
    {
      home.packages = with pkgs; [
        mariadb
        sqlite
      ];
    }

    (mkIf config.desktop.enable {
      home.packages = with pkgs; [
        dbeaver-bin
      ];

      home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
        directories = [
          ".local/share/DBeaverData"
        ];
        allowOther = true;
      };
    })
  ]);
}
