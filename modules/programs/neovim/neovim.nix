localFlake:

{ config, lib, ... }:

let
  cfg = config.programs.neovim-flake;
  inherit (lib) mkDefault mkIf;
in
{
  imports = [
    localFlake.inputs.neovim-flake.homeManagerModules.default
  ];

  programs.neovim-flake.neovim.enable = mkDefault config.utils.basics.enable;
  programs.neovim-flake.neovim.defaultEditor = true;
  programs.neovim-flake.page.enable = mkDefault config.utils.basics.enable;
  programs.neovim-flake.page.defaultPager = true;
  programs.neovim-flake.neovim = {
    languages.markdown.plugins.enable = true;
  };

  # use nvim instead "${cfg.neovim.finalPackage}/bin/nvim"
  # so that it can be override when overriding nvim package
  home.binAliases = mkIf cfg.neovim.enable {
    v = "nvim";
    vi = "nvim";
    vim = "nvim";
  };
}
