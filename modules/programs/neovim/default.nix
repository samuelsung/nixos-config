{ flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-neovim = importApply ./neovim.nix { inherit inputs; };
}
