{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.programs.newsboat;
in
{
  home.packages = mkIf cfg.enable [
    pkgs.yt-dlp # downloading youtube
  ];

  programs.newsboat.enable =
    mkDefault (config.desktop.enable && config.utils.basics.enable);

  programs.newsboat.extraConfig = ''
    #show-read-feeds no
    auto-reload yes

    external-url-viewer "urlscan -dc -r 'linkhandler {}'"

    bind-key n down
    bind-key e up
    bind-key n next articlelist
    bind-key e prev articlelist
    bind-key N next-feed articlelist
    bind-key E prev-feed articlelist
    bind-key G end
    bind-key g home
    bind-key d pagedown
    bind-key u pageup
    bind-key i open
    bind-key h quit
    bind-key a toggle-article-read
    bind-key k next-unread
    bind-key K prev-unread
    bind-key D pb-download
    bind-key U show-urls
    bind-key x pb-delete

    browser linkhandler
    macro , open-in-browser
    macro t set browser "qndl" ; open-in-browser ; set browser linkhandler
    macro a set browser "tsp yt-dlp --add-metadata -xic -f bestaudio/best" ; open-in-browser ; set browser linkhandler
    macro v set browser "setsid -f mpv" ; open-in-browser ; set browser linkhandler
    macro w set browser "lynx" ; open-in-browser ; set browser linkhandler
    macro d set browser "dmenuhandler" ; open-in-browser ; set browser linkhandler
    macro c set browser "xsel -b <<<" ; open-in-browser ; set browser linkhandler
    macro C set browser "youtube-viewer --comments=%u" ; open-in-browser ; set browser linkhandler
    macro y set browser "youtube-dl-mpv %u" ; open-in-browser ; set browser linkhandler
    macro Y set browser "yt-dlp %u -o - | mpv -" ; open-in-browser ; set browser linkhandler
    macro p set browser "peertubetorrent %u 480" ; open-in-browser ; set browser linkhandler
    macro P set browser "peertubetorrent %u 1080" ; open-in-browser ; set browser linkhandler
  '';
}
