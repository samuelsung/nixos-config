localFlake:

{ lib, ... }:

let
  inherit (lib) mapAttrs;
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;

  mapColors = mapAttrs (_: v: "#${hexOf v}");

in
with (mapColors catppuccin.mocha);
{
  programs.alacritty.settings = {
    window = {
      opacity = 0.75;
    };

    font = {
      # NOTE(samuelsung): use the following command to list all available font
      # fc-list : family style
      normal = {
        family = "FiraCode Nerd Font Mono";
        style = "Medium";
      };
      bold = {
        family = "FiraCode Nerd Font Mono";
        style = "Bold";
      };
      italic = {
        family = "FiraCode Nerd Font Mono"; # sadly firacode does not have italic font...
        style = "Medium";
      };
      bold_italic = {
        family = "FiraCode Nerd Font Mono";
        style = "Bold";
      };
      size = 12;
    };

    colors = {
      primary = {
        background = base;
        foreground = text;
        # Bright and dim foreground colors
        dim_foreground = text;
        bright_foreground = text;
      };

      # Cursor colors
      cursor = {
        text = base;
        cursor = rosewater;
      };
      vi_mode_cursor = {
        text = base;
        cursor = lavender;
      };

      # Search colors
      search = {
        matches = {
          foreground = base;
          background = subtext0;
        };
      };

      focused_match = {
        foreground = base;
        background = green;
      };
      footer_bar = {
        foreground = base;
        background = subtext0;
      };

      # Keyboard regex hints
      hints = {
        start = {
          foreground = base;
          background = yellow;
        };
        end = {
          foreground = base;
          background = subtext0;
        };
      };

      # Selection colors
      selection = {
        text = base;
        background = rosewater;
      };

      # Normal colors
      normal = {
        black = surface1;
        inherit red;
        inherit green;
        inherit yellow;
        inherit blue;
        magenta = pink;
        cyan = teal;
        white = subtext1;
      };

      # Bright colors
      bright = {
        black = surface2;
        inherit red;
        inherit green;
        inherit yellow;
        inherit blue;
        magenta = pink;
        cyan = teal;
        white = subtext0;
      };

      # Dim colors
      dim = {
        black = surface1;
        inherit red;
        inherit green;
        inherit yellow;
        inherit blue;
        magenta = pink;
        cyan = teal;
        white = subtext1;
      };

      indexed_colors = [
        { index = 16; color = peach; }
        { index = 17; color = rosewater; }
      ];
    };
  };
}
