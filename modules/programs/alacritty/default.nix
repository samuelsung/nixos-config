{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.programs-alacritty = ./alacritty.nix;
  flake.homeManagerModules.programs-alacritty-samuelsung = importApply ./alacritty-samuelsung.nix {
    inherit colors;
  };
}
