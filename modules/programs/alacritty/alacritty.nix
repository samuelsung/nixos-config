{ config, lib, pkgs, ... }:

let
  inherit (lib) makeBinPath mkDefault;
  inherit (pkgs) alacritty writeShellScriptBin;
in
{
  programs.alacritty.enable = mkDefault (config.utils.basics.enable && config.desktop.enable);

  programs.alacritty.package = writeShellScriptBin "alacritty" ''
    #!/bin/sh
    PATH=${makeBinPath [ alacritty ]}:$PATH
    alacritty msg create-window $@ || exec alacritty $@
  '';
}
