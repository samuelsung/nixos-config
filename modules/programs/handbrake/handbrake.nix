{ config, lib, pkgs, ... }:

let
  cfg = config.programs.handbrake;
  inherit (lib) mkIf mkOption types;
in
{
  options.programs.handbrake = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.creatives.enable && config.desktop.enable;
    };

    package = mkOption {
      type = types.package;
      default = pkgs.handbrake;
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
  };
}
