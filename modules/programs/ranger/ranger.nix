{ config, lib, pkgs, ... }:

let
  cfg = config.programs.ranger;
  inherit (lib) mkDefault mkMerge mkIf optionals readFile;
in
mkMerge [
  { programs.ranger.enable = mkDefault config.utils.basics.enable; }

  (mkIf cfg.enable {
    home.packages = with pkgs; [
      ranger
    ] ++ (optionals config.desktop.enable [
      ueberzug
      ffmpegthumbnailer
      poppler_utils
    ]);

    home.binAliases = {
      r = "${pkgs.ranger}/bin/ranger";
    };

    programs.ranger.extraConfig = readFile ./rc.conf;
    xdg.configFile."ranger/rifle.conf".source = ./rifle.conf;
    xdg.configFile."ranger/scope.sh".source = ./scope.sh;
    xdg.configFile."ranger/commands.py".source = ./commands.py;
  })
]
