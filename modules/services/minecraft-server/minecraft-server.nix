{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf optionalString;
  impermanence = config.impermanence.enable or false;
  cfg = config.services.minecraft-server;
in
{
  services.minecraft-server = {
    package = pkgs.papermc;
    eula = true;
    openFirewall = true;
    dataDir = "${optionalString impermanence "/persist"}/var/lib/minecraft";
  };

  backups.paths = mkIf cfg.enable [
    cfg.dataDir
  ];
}
