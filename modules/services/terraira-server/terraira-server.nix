{ config, lib, ... }:

let
  inherit (lib) mkIf optionalString;
  cfg = config.services.terraria;
in
{
  services.terraria.secure = true;
  services.terraria.openFirewall = true;
  services.terraria.dataDir = "${optionalString config.impermanence.enable "/persist"}/srv/terraria";

  backups.paths = mkIf cfg.enable [
    cfg.dataDir
  ];
}
