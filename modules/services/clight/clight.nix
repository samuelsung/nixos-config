{ config, lib, ... }:

let
  inherit (lib) mkIf;
  cfg = config.services.clight;
in
mkIf cfg.enable {
  location.provider = "geoclue2";

  services.clight.settings = {
    backlight = {
      trans_fixed = 1000;
      ac_timeouts = [ 1 1 1 ];
      batt_timeouts = [ 1 1 1 ];
    };

    sensor = {
      ac_regression_points = [ 0.01 0.08 0.15 0.20 0.35 0.43 0.56 0.75 0.85 0.95 1.0 ];
      batt_regression_points = [ 0.01 0.08 0.15 0.20 0.35 0.43 0.56 0.75 0.85 0.95 1.0 ];
    };
    gamma.disabled = true;
  };

  home-manager.sharedModules = [{
    programs.waybar.settings = {
      backlight.on-click = "systemctl restart --user clight.service";
    };
  }];
}
