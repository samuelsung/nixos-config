{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
mkIf config.desktop.enable {
  # dconf
  programs.dconf.enable = true;
}
