{ lib, config, pkgs, ... }:

let
  inherit (lib)
    concatStringsSep
    flatten
    listToAttrs
    mapAttrsToList
    mkIf
    mkOption
    nameValuePair
    optionals
    optionalString
    types;
  cfg = config.services.gitlab-runner;

  encryptedJson = (builtins.fromJSON (
    builtins.readFile cfg.sopsFile
  )).gitlab-runner;

  # NOTICE(samuelsung): it seems that old runners will fail to be unregistered if the service name gets to long...
  # "repo" is not need to be exact, can be shorten if to long
  getServiceName = repo: tag: "${repo}-${tag}";

  getSopsKey = repo: tag: "${repo}-${tag}-gr";

  flattenNames = flatten (
    mapAttrsToList
      (tag: repos:
        (mapAttrsToList
          (repo: _: { inherit tag repo; })
          repos)
      )
      encryptedJson
  );

  genNixRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    dockerImage = "alpine";
    dockerVolumes = [
      "/nix/store:/nix/store:ro"
      "/nix/var/nix/db:/nix/var/nix/db:ro"
      "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
    ] ++ (optionals (cfg.githubToken != null) [
      "${cfg.githubToken}:${cfg.githubToken}:ro"
    ]);
    dockerDisableCache = true;
    preBuildScript = pkgs.writeScript "setup-container" ''
      mkdir -p -m 0755 /nix/var/log/nix/drvs
      mkdir -p -m 0755 /nix/var/nix/gcroots
      mkdir -p -m 0755 /nix/var/nix/profiles
      mkdir -p -m 0755 /nix/var/nix/temproots
      mkdir -p -m 0755 /nix/var/nix/userpool
      mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
      mkdir -p -m 1777 /nix/var/nix/profiles/per-user
      mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
      mkdir -p -m 0700 "$HOME/.nix-defexpr"
      mkdir -p -m 0755 /etc/nix
      cat <<EOF > /etc/nix/nix.conf
      experimental-features = nix-command flakes
      ${optionalString (cfg.githubToken != null) "access-tokens = github.com=''$(cat ${cfg.githubToken})"}
      EOF
      . ${pkgs.nix}/etc/profile.d/nix.sh
      ${pkgs.nix}/bin/nix-env -i ${concatStringsSep " " (with pkgs; [
        nix
        cacert
        git
        openssh
      ])}
      ${pkgs.nix}/bin/nix-channel --add ${config.system.defaultChannel} nixpkgs
      ${pkgs.nix}/bin/nix-channel --update nixpkgs
    '';
    environmentVariables = {
      ENV = "/etc/profile";
      USER = "root";
      NIX_REMOTE = "daemon";
      PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
      NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
    };
    maximumTimeout = 60 * 60 * 3;
    tagList = [ "nix" ];
  };

  # runner for building in docker via host's nix-daemon
  # nix store will be readable in runner, might be insecure
  # runner for building docker images
  genDockerRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    dockerImage = "docker:stable";
    dockerVolumes = [
      "/var/run/docker.sock:/var/run/docker.sock"
    ];
    tagList = [ "docker-images" ];
  };

  # runner for executing stuff on host system (very insecure!)
  # make sure to add required packages (including git!)
  # to `environment.systemPackages`
  genShellRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    executor = "shell";
    tagList = [ "shell" ];
  };

  # runner for everything else
  genDefaultRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    dockerImage = "debian:stable";
  };
in
{
  options.services.gitlab-runner = {
    sopsFile = mkOption {
      type = types.path;
      description = ''
        the port of the bitwarden server using.
      '';
    };

    githubToken = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = ''
        github token for access github.com in nix
      '';
    };
  };

  config = mkIf (cfg.enable && cfg.sopsFile != null) {
    virtualisation.docker.enable = true;

    sops.secrets = listToAttrs (map
      ({ tag, repo }: nameValuePair (getSopsKey repo tag) {
        inherit (cfg) sopsFile;
        key = "gitlab-runner/${tag}/${repo}";
        mode = "0400";
      })
      flattenNames
    );

    services.gitlab-runner = {
      settings.concurrent = 10;

      services = listToAttrs (
        map
          ({ tag, repo }:
            let
              serviceName = getServiceName repo tag;
              sopsKey = getSopsKey repo tag;
            in
            nameValuePair serviceName (
              if (tag == "nix") then
              # File should contain at least these two variables:
              # `CI_SERVER_URL`
              # `REGISTRATION_TOKEN`
                genNixRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
              else if (tag == "docker") then
                genDockerRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
              else if (tag == "shell") then
                genShellRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
              else
                genDefaultRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
            ))
          flattenNames
      );
    };
  };
}
