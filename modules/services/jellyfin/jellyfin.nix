{ lib, ... }:

let
  inherit (lib) mkDefault;
in
{
  services.jellyfin.openFirewall = mkDefault true;
}
