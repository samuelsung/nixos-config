{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf mkOption types;

  cfg = config.services.refresh-tmux-sessionizer-cache;
in
{
  options.services.refresh-tmux-sessionizer-cache = {
    enable = mkOption {
      type = types.bool;
      default = config.programs.tmux.enable && config.utils.basics.enable && config.utils.developments.enable;
      description = "periodic update tmux-sessionizer cache";
    };

    frequency = mkOption {
      type = types.str;
      default = "*:0/5";
    };
  };

  config = mkIf cfg.enable {
    systemd.user.services."refresh-tmux-sessionizer-cache" = mkIf cfg.enable {
      Unit = {
        Description = "refreshing tmux sessionizer cache";
        After = [ "network.target" ];
      };
      Install.WantedBy = [ "default.target" ];
      Service = {
        Type = "oneshot";
        ExecStart = "${pkgs.tmux-sessionizer}/bin/tmux-sessionizer -r";
      };
    };

    systemd.user.timers."refresh-tmux-sessionizer-cache" = mkIf cfg.enable {
      Unit = {
        Description = "periodic refreshing tmux sessionizer cache";
      };
      Timer = {
        Unit = "refresh-tmux-sessionizer-cache.service";
        OnCalendar = cfg.frequency;
      };
      Install.WantedBy = [ "timers.target" ];
    };
  };
}
