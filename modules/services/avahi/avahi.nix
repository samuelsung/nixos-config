{ config, lib, ... }:

let
  inherit (lib) mkIf;
  cfg = config.services.avahi;
in
mkIf (cfg.allowInterfaces != null && cfg.allowInterfaces != [ ]) {
  services.avahi = {
    enable = true;
    inherit (config.networking) hostName;
    nssmdns4 = true;
    publish = {
      enable = true;
      addresses = true;
      domain = true;
      userServices = true;
    };
  };
}
