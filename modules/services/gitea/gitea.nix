{ config, lib, pkgs, ... }:

let
  inherit (lib) concatStringsSep mkDefault mkIf optionalString;
  cfg = config.services.gitea;
in
{
  services.gitea = {
    package = pkgs.gitea;
    appName = mkDefault "${config.networking.hostName}: private gitea service";
    stateDir = "${optionalString config.impermanence.enable "/persist"}/var/lib/gitea";
    useWizard = false;
    settings.service.DISABLE_REGISTRATION = mkDefault true;
    settings.ui.THEMES = concatStringsSep "," [
      "catppuccin-latte-rosewater"
      "catppuccin-latte-flamingo"
      "catppuccin-latte-pink"
      "catppuccin-latte-mauve"
      "catppuccin-latte-red"
      "catppuccin-latte-maroon"
      "catppuccin-latte-peach"
      "catppuccin-latte-yellow"
      "catppuccin-latte-green"
      "catppuccin-latte-teal"
      "catppuccin-latte-sky"
      "catppuccin-latte-sapphire"
      "catppuccin-latte-blue"
      "catppuccin-latte-lavender"
      "catppuccin-frappe-rosewater"
      "catppuccin-frappe-flamingo"
      "catppuccin-frappe-pink"
      "catppuccin-frappe-mauve"
      "catppuccin-frappe-red"
      "catppuccin-frappe-maroon"
      "catppuccin-frappe-peach"
      "catppuccin-frappe-yellow"
      "catppuccin-frappe-green"
      "catppuccin-frappe-teal"
      "catppuccin-frappe-sky"
      "catppuccin-frappe-sapphire"
      "catppuccin-frappe-blue"
      "catppuccin-frappe-lavender"
      "catppuccin-macchiato-rosewater"
      "catppuccin-macchiato-flamingo"
      "catppuccin-macchiato-pink"
      "catppuccin-macchiato-mauve"
      "catppuccin-macchiato-red"
      "catppuccin-macchiato-maroon"
      "catppuccin-macchiato-peach"
      "catppuccin-macchiato-yellow"
      "catppuccin-macchiato-green"
      "catppuccin-macchiato-teal"
      "catppuccin-macchiato-sky"
      "catppuccin-macchiato-sapphire"
      "catppuccin-macchiato-blue"
      "catppuccin-macchiato-lavender"
      "catppuccin-mocha-rosewater"
      "catppuccin-mocha-flamingo"
      "catppuccin-mocha-pink"
      "catppuccin-mocha-mauve"
      "catppuccin-mocha-red"
      "catppuccin-mocha-maroon"
      "catppuccin-mocha-peach"
      "catppuccin-mocha-yellow"
      "catppuccin-mocha-green"
      "catppuccin-mocha-teal"
      "catppuccin-mocha-sky"
      "catppuccin-mocha-sapphire"
      "catppuccin-mocha-blue"
      "catppuccin-mocha-lavender"
    ];
    lfs.enable = mkDefault true;
    dump.type = mkDefault "tar.gz";
  };

  systemd.user.tmpfiles = mkIf cfg.enable {
    users.gitea.rules = [
      "L ${cfg.stateDir}/custom/public/css - - - - ${pkgs.catppuccin-gitea}"
    ];
  };

  backups.paths = mkIf cfg.enable [
    cfg.stateDir
  ];
}
