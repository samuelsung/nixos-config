{ config, lib, pkgs, ... }:
let
  inherit (lib) mkDefault mkForce mkIf mkOption types;
  inherit (types) listOf str;
  cfg = config.services.dunst;
in
{
  options.services.dunst = {
    systemd.targets = mkOption {
      type = listOf str;
      default = [ "graphical-session.target" ];
      example = [ "sway-session.target" ];
      description = "the systemd targets that will automatically start the service";
    };
  };

  config = {
    services.dunst.enable = mkDefault config.desktop.enable;

    systemd.user.services.dunst = {
      Install.WantedBy = cfg.systemd.targets;
      Service = {
        Type = mkForce "simple";
        BusName = mkForce "";
      };
    };

    home.packages = mkIf cfg.enable [
      pkgs.libnotify
    ];
  };
}
