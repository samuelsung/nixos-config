{ colors, flake-parts-lib, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.services-dunst = ./dunst.nix;
  flake.homeManagerModules.services-dunst-samuelsung = importApply
    ./dunst-samuelsung.nix
    { inherit colors; };
}
