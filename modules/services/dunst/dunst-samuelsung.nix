localFlake:

let
  inherit (localFlake.colors) catppuccin;
  inherit (localFlake.colors.lib) hexOf;
in
{
  services.dunst.systemd.targets = [ "x-session.target" ];
  services.dunst.settings = {
    global = {
      follow = "keyboard";
      enable_posix_regex = true;

      width = 350;
      origin = "top-right";
      offset = "5x33";

      icon_corner_radius = 0;
      corner_radius = 0;
      indicate_hidden = true;
      transparency = 10; # NOTE(samuelsung): this only apply to X

      separator_height = 2;
      padding = 8;
      horizontal_padding = 8;
      text_icon_padding = 0;
      gap_size = 0;
      # Define a color for the separator.
      # possible values are:
      #  * auto: dunst tries to find a color fitting to the background;
      #  * foreground: use the same color as the foreground;
      #  * frame: use the same color as the frame;
      #  * anything else will be interpreted as a X color.
      separator_color = "frame";
      frame_width = 2;
      frame_color = "#${hexOf catppuccin.mocha.mauve}";
      sort = true;
      idle_threshold = 120;
      font = "FiraCode 12";
      line_height = 0;
      # The format of the message.  Possible variables are:
      #   %a  appname
      #   %s  summary
      #   %b  body
      #   %i  iconname (including its path)
      #   %I  iconname (without its path)
      #   %p  progress value if set ([  0%] to [100%]) or nothing
      #   %n  progress value if set without any extra characters
      #   %%  Literal %
      # Markup is allowed
      format = "<b>%s</b>\n%b";
      vertical_alignment = "center";
      show_age_threshold = 20;

      # icon_path (default: "/usr/share/icons/gnome/16x16/status/:/usr/share/icons/gnome/16x16/devices/")
      # icon_theme (default: "Adwaita", example: "Adwaita, breeze")
      # enable_recursive_icon_lookup (default: false)

      history_length = 2000;

      dmenu = "dmenu -p dunst";
      browser = "xdg-open";
      markup = "full";
    };

    experimental = {
      per_monitor_dpi = false;
    };

    shortcuts = {
      close = "ctrl+space";
      close_all = "ctrl+shift+space";
      history = "ctrl+grave";
      context = "ctrl+shift+period";
    };

    urgency_low = {
      background = "#${hexOf catppuccin.mocha.base}";
      foreground = "#${hexOf catppuccin.mocha.text}";
      timeout = 5;
    };

    urgency_normal = {
      background = "#${hexOf catppuccin.mocha.base}";
      foreground = "#${hexOf catppuccin.mocha.text}";
      timeout = 5;
    };

    urgency_critical = {
      background = "#${hexOf catppuccin.mocha.base}";
      foreground = "#${hexOf catppuccin.mocha.text}";
      frame_color = "#${hexOf catppuccin.mocha.peach}";
      timeout = 0;
    };
  };
}
