{ config, lib, pkgs, ... }:

let
  inherit (lib) makeBinPath mkEnableOption mkIf mkOption optionals types;
  inherit (types) lines listOf str;
  json = pkgs.formats.json { };

  cfg = config.services.sway-notification-center;
in
{
  options.services.sway-notification-center = {
    enable = mkEnableOption "sway-notification-center service";

    config = mkOption {
      inherit (json) type;
      default = { };
      description = "config";
    };

    css = mkOption {
      type = lines;
      default = "";
      description = "css";
    };

    systemd.enable = mkEnableOption "systemd support for sway-notification-center";

    systemd.targets = mkOption {
      type = listOf str;
      default = [ "graphical-session.target" ];
      example = [ "sway-session.target" ];
      description = "the systemd targets that will automatically start the service";
    };

    waylandDisplay = mkOption {
      type = str;
      default = "";
      description =
        "Set the service's {env}`WAYLAND_DISPLAY` environment variable.";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [
      pkgs.libnotify
      pkgs.swaynotificationcenter
    ];

    systemd.user.services.swaync = {
      Unit = {
        Description = "Simple notification daemon with a GUI built for Sway";
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" ];
      };

      Install.WantedBy = cfg.systemd.targets;

      Service = {
        Type = "simple";
        ExecStart = "${pkgs.swaynotificationcenter}/bin/swaync -s ${pkgs.writeText "style.css" cfg.css} -c ${json.generate "config.json" cfg.config}";
        Environment = [
          # coreutils is needed for the example script
          "PATH=${makeBinPath [ pkgs.coreutils pkgs.swaynotificationcenter ]}"
        ] ++ (optionals (cfg.waylandDisplay != "") [
          "WAYLAND_DISPLAY=${cfg.waylandDisplay}"
        ]);
      };
    };
  };
}
