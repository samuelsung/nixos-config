{ pkgs, ... }:

{
  services.sway-notification-center.config = {
    positionX = "right";
    positionY = "top";

    control-center-margin-top = 0;
    control-center-margin-bottom = 0;
    control-center-margin-right = 0;
    control-center-margin-left = 0;

    timeout = 10;
    timeout-low = 5;
    timeout-critical = 0;

    notification-window-width = 500;
    keyboard-shortcuts = true;
    image-visibility = "always";
    transition-time = 200;
    hide-on-clear = false;
    hide-on-action = true;

    fit-to-screen = true;
    # control-center-height = 500;
    # control-center-width = 500;

    widgets = [
      "title"
      "dnd"
      # "label"
      # "buttons-grid"
      "mpris"
      # "volume"
      # "inhibitors"
      "notifications"
      # "menubar"
    ];

    widget-config = {
      title = {
        text = "Notification Center";
        clear-all-button = true;
        button-text = "󰆴 Clear All";
      };

      dnd.text = "Do Not Disturb";

      label = {
        text = "Label Text";
        max-lines = 1;
      };

      mpris = {
        image-size = 96;
        image-radius = 12;
      };

      menubar."menu#power" = {
        label = "Power";
        position = "right"; # enum: ["right", "left"]
        # animation-type: enum: ["slide_down", "slide_up", "none"]
        # animation-duration:
        # default: 250
        # description: Duration of animation in milliseconds
        actions = [
          {
            label = "Shut down";
            command = "systemctl poweroff";
          }
        ];
      };

      buttons-grid.actions = [
        {
          label = "Shut down";
          command = "systemctl poweroff";
        }
      ];

      volume = {
        label = "Volume";
        show-per-app = true;
        empty-list-label = "No active sink input";
        expand-button-label = "⇧";
        collapse-button-label = "⇩";
        icon-size = 24;
        # animation-type: enum: ["slide_down", "slide_up", "none"]
        # animation-duration = 250;
      };

      backlight = {
        # label = "Brightness";
        # device = "intel_backlight";
        # subsystem = enum: ["backlight", "leds"]
        # min = 0;
      };

      inhibitors = {
        text = "Inhibitors";
        clear-all-button = true;
        button-text = "Clear All";
      };
    };
  };

  services.sway-notification-center.css = builtins.readFile "${pkgs.catppuccin-swaync}/mocha.css";
}
