{ config, lib, ... }:

let
  inherit (lib) mapAttrs mkIf mkOption types;
  cfg = config.services.nginx;
in
{
  options.services.nginx.reverseProxyTargets = mkOption {
    type = with types; attrsOf str;
    default = { };
    description = ''
      the mapping of the reverseproxy
    '';
  };

  config = mkIf (cfg.enable && cfg.reverseProxyTargets != { }) {
    networking.firewall = {
      allowedTCPPorts = [ 80 443 ];
    };

    security.acme.acceptTerms = true;

    services.nginx.virtualHosts =
      let
        common = {
          locations."/" = {
            extraConfig = ''
              # unlimited upload size in nginx (so the setting in application applies)
              client_max_body_size 0;

              # proxy timeout should match the timeout value set in /etc/webapps/gitlab/puma.rb
              proxy_read_timeout 60;
              proxy_connect_timeout 60;
              proxy_redirect off;

              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forwarded-Ssl on;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header X-Forwarded-Proto $scheme;
            '';
          };

        };
        SSL = {
          enableACME = true;
          forceSSL = true;
        };
      in
      mapAttrs
        (_address: target: (
          SSL //
          common // {
            locations."/".proxyPass = target;
          }
        ))
        cfg.reverseProxyTargets;
  };
}
