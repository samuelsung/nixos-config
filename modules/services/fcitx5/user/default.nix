{ lib, config, pkgs, osConfig, ... }:

let
  inherit (lib) mkIf;
in
mkIf config.desktop.enable {
  i18n.inputMethod.enabled = "fcitx5";
  i18n.inputMethod.fcitx5 = {
    addons = with pkgs; [
      fcitx5-mozc
      fcitx5-rime
      fcitx5-chinese-addons
      catppuccin-fcitx5
    ];
  };

  home.file.".local/share/fcitx5/rime/default.custom.yaml" = {
    force = true;
    source = ./default.custom.yaml;
  };
  home.file.".config/fcitx5/profile" = {
    force = true;
    source = ./profile;
  };
  home.file.".config/fcitx5/config" = {
    force = true;
    source = ./config;
  };
  home.file.".config/fcitx5/conf/classicui.conf" = {
    force = true;
    source = ./classicui.conf;
  };

  home.persistence."/persist/${config.home.homeDirectory}" = mkIf osConfig.impermanence.enable {
    directories = [
      ".local/share/fcitx5/rime"
    ];
    allowOther = true;
  };
}
