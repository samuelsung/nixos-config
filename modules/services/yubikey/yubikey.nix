{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge mkOption types;
  cfg = config.programs.yubikey;
in
{
  options.programs.yubikey = {
    enable = mkOption {
      type = types.bool;
      default = config.desktop.enable;
      description = "yubikey";
    };
  };

  config = mkIf cfg.enable {
    services.pcscd.enable = mkDefault true;
    services.udev.packages = [ pkgs.yubikey-personalization ];

    environment.systemPackages = mkMerge [
      [
        pkgs.yubikey-manager
        pkgs.yubikey-personalization
        pkgs.yubico-piv-tool
      ]
      (mkIf config.desktop.enable [
        pkgs.yubikey-manager-qt
        pkgs.yubikey-personalization-gui
        pkgs.yubioath-flutter
      ])
    ];
  };
}
