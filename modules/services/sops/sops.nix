{ config, lib, pkgs, ... }:

let
  inherit (lib) attrNames concatMapStringsSep mkIf mkMerge mkOption optionalString types;

  cfg = config.secrets.sops;

  sshKeyPath = "${optionalString config.impermanence.enable "/persist"}/etc/ssh/ssh_host_ed25519_key";

  configureScript = pkgs.writeShellScript "root-sops-key" ''
    mkdir -p /root/.config/sops/age
    install -d -m 0700 -o root /root/.config/sops/age
    cat ${sshKeyPath} | ${pkgs.ssh-to-age}/bin/ssh-to-age -private-key > /root/.config/sops/age/keys.txt
    chown root /root/.config/sops/age/keys.txt
    chmod 400 /root/.config/sops/age/keys.txt
  '';
in
{
  options.secrets.sops.enable = mkOption {
    type = types.bool;
    default = config.secrets.enable;
    description = "secrets with sops";
  };

  config = mkMerge [
    {
      assertions = [{
        assertion = !cfg.enable -> config.sops.secrets == { };
        message = "Enable config.secrets.enable or remove ${concatMapStringsSep ", " (name: "sops.${name}") (attrNames config.sops.secrets)}";
      }];

      environment.systemPackages = with pkgs; [
        sops
        ssh-to-age
        age
      ];
    }

    (mkIf cfg.enable {
      sops.age.sshKeyPaths = [ sshKeyPath ];

      systemd.services.root-sops-key = {
        description = "setup root's sops key file";
        documentation = [ ];
        after = [ "local-fs.target" ];
        requires = [ "local-fs.target" ];
        wantedBy = [ "multi-user.target" ];
        restartIfChanged = true;
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
          ExecStart = "${configureScript}";
        };
      };
    })
  ];
}
