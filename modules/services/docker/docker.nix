{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
  cfg = config.virtualisation.docker;
in
mkIf cfg.enable {
  environment.systemPackages = [
    pkgs.docker-compose
  ];

  users.users.samuelsung = mkIf config.users.presets.samuelsung.enable {
    extraGroups = [ "docker" ];
  };

  users.users.samuelsung_play = mkIf config.users.presets.samuelsung_play.enable {
    extraGroups = [ "docker" ];
  };

  environment.persistence."/persist" = mkIf config.impermanence.enable {
    hideMounts = true;
    directories = [
      "/var/lib/docker"
    ];
  };
}
