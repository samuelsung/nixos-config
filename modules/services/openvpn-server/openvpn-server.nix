# NOTE(samuelsung): to generate hmac secret, run:
# openvpn --genkey secret <name> NOTE(samuelsung): to verify the cert is signed by the given ca, run:
# openssl verify -CAfile <ca.crt> <client/srever.crt>

{ pkgs, config, lib, ... }:

with lib;
let
  cfg = config.services.openvpn.presets;

  gen-openvpn-config = pkgs.writeShellScriptBin "gen-openvpn-config" ''
    if [ "$(whoami)" != root ]; then
      echo "Please run this script as root" > /dev/stderr
      exit 1
    fi

    currentIp=$(ip address show "${cfg.ext-dev}" | grep "inet" | head --lines 1 | awk '{print $2}' | cut -f1 -d'/');

    cat <<EOL
    client

    dev tun
    proto udp
    remote ''${currentIp} ${toString cfg.port}

    <ca>
    $(cat ${config.sops.secrets."openvpn-ca-cert".path})
    </ca>
    cert client.crt
    key client.key

    cipher AES-256-CBC
    auth SHA512
    # TLS Security
    <tls-crypt>
    $(cat ${config.sops.secrets."openvpn-hmac".path})
    </tls-crypt>
    tls-version-min 1.2

    keepalive 20 60

    remote-cert-tls server

    resolv-retry infinite

    nobind

    persist-tun
    persist-key

    # Redirect all Connection through OpenVPN Server
    redirect-gateway def1

    mute-replay-warnings
    verb 3
  '';
in
{
  options.services.openvpn.presets = {
    enable = mkEnableOption "the pre-defined server";

    ext-dev = mkOption {
      type = types.str;
      description = ''
        the external device for the openvpn server.
      '';
    };

    vpn-dev = mkOption {
      type = types.str;
      default = "tun0";
      description = ''
        the internal device for the openvpn server.
      '';
    };

    port = mkOption {
      type = types.int;
      default = 1194;
      description = ''
        the port for the openvpn server.
      '';
    };

    internalIp = mkOption {
      type = types.str;
      default = "10.5.0.0";
      description = ''
        the internal ip for the openvpn server.
      '';
    };

    internalMask = mkOption {
      type = types.str;
      default = "255.255.255.0";
      description = ''
        the netmask for internal network for the openvpn server.
      '';
    };

    caCertSopsFile = mkOption {
      type = types.path;
      description = ''
        the sops file of the ca cert for the openvpn server.
      '';
    };

    certSopsFile = mkOption {
      type = types.path;
      description = ''
        the sops file of the cert for the openvpn server.
      '';
    };

    keySopsFile = mkOption {
      type = types.path;
      description = ''
        the sops file of the key for the openvpn server.
      '';
    };

    hmacSopsFile = mkOption {
      type = types.path;
      description = ''
        the sops file of the hmac for the openvpn server.
      '';
    };

    dhSopsFile = mkOption {
      type = types.path;
      description = ''
        the sops file of the dh for the openvpn server.
      '';
    };

    withPamAuth = mkOption {
      type = types.bool;
      default = false;
      description = ''
        whether enable pam auth for the openvpn server.
      '';
    };
  };

  config = mkIf cfg.enable {
    sops.secrets = {
      "openvpn-ca-cert" = {
        sopsFile = cfg.caCertSopsFile;
        format = "binary";
        mode = "0400";
      };
      "openvpn-server-cert" = {
        sopsFile = cfg.certSopsFile;
        format = "binary";
        mode = "0400";
      };
      "openvpn-server-key" = {
        sopsFile = cfg.keySopsFile;
        format = "binary";
        mode = "0400";
      };
      "openvpn-hmac" = {
        sopsFile = cfg.hmacSopsFile;
        format = "binary";
        mode = "0400";
      };
      "openvpn-dh" = {
        sopsFile = cfg.dhSopsFile;
        format = "binary";
        mode = "0400";
      };
    };

    # sudo systemctl start nat
    networking.nat = {
      enable = true;
      externalInterface = cfg.ext-dev;
      internalInterfaces = [ cfg.vpn-dev ];
    };
    networking.firewall.trustedInterfaces = [ cfg.vpn-dev ];
    networking.firewall.allowedUDPPorts = [ cfg.port ];

    environment.systemPackages = [
      pkgs.openvpn
      pkgs.easyrsa
      pkgs.openssl
      gen-openvpn-config
    ]; # for key generation

    services.openvpn.servers.openvpn-server.config = ''
      # OpenVPN Port, Protocol, and the Tun
      port ${toString cfg.port}
      proto udp
      dev ${cfg.vpn-dev}

      # Network Configuration - Internal network
      server ${cfg.internalIp} ${cfg.internalMask}

      persist-tun
      persist-key

      # DNS
      push "dhcp-option DNS 1.1.1.1"
      push "dhcp-option DNS 1.0.0.1"

      # Enable multiple clients to connect with the same certificate key
      duplicate-cn

      # OpenVPN Server Certificate - CA, server key and certificate
      ca ${config.sops.secrets."openvpn-ca-cert".path}
      cert ${config.sops.secrets."openvpn-server-cert".path}
      key ${config.sops.secrets."openvpn-server-key".path}
      dh ${config.sops.secrets."openvpn-dh".path}

      auth SHA512
      # TLS Security
      tls-server
      tls-crypt ${config.sops.secrets."openvpn-hmac".path}
      tls-version-min 1.2

      # Data encryption cipher
      cipher AES-256-CBC

      # Don't negotiate ciphers
      ncp-disable

      # Other Configuration
      keepalive 20 60

      # Require client certificates to have the correct extended key usage
      remote-cert-tls client

      # Reject connections without certificates
      verify-client-cert require

      ${if cfg.withPamAuth then ''
        plugin ${pkgs.openvpn}/lib/openvpn/plugins/openvpn-plugin-auth-pam.so login
      '' else ""}

      # Drop privilege
      user ${config.users.users.nobody.name}
      group ${config.users.groups.nogroup.name}

      # OpenVPN Log
      log-append /var/log/openvpn.log
      verb 3
    '';
  };
}
