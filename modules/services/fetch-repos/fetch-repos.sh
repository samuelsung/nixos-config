#!/bin/sh

json=$(cat "$1")

debug() {
  if [ "$DEBUG" = 1 ]; then {
    echo "$1" 1>&2
  }; fi
}

error() {
  echo "$1" 1>&2
  exit 1
}

for_json() {
  echo "$json" | jq "${@}"
}

get_repos() {
  for_json -r 'keys[]'
}

setup_remotes() {
  repo_path=$1

  (
    debug "cd into $repo_path"
    cd "$repo_path" || error "cannot cd into ${repo_path}"

    target_remotes=$(for_json -r ".\"${repo_path}\".remotes | keys[]")

    echo "$target_remotes" | while read -r remote; do {
      fetch_remote_url=$(for_json -r ".\"${repo_path}\".remotes.\"${remote}\".fetch // \"\"")
      push_remote_url=$(for_json -r ".\"${repo_path}\".remotes.\"${remote}\".push // \"\"")

      if git remote | grep --silent "^${remote}$"; then {
        git remote set-url "${remote}" "${fetch_remote_url}"

        git remote set-url --push "${remote}" "${push_remote_url}"
      }; else {
        git remote add "${remote}" "${fetch_remote_url}"

        git remote set-url --push "${remote}" "${push_remote_url}"
      }; fi
    }; done

    git remote | while read -r remote; do {
      if ! echo "$target_remotes" | grep --silent "^$remote$"; then {
        debug "cleaning up remote ${remote}"
        git remote remove "${remote}"
      }; fi
    }; done

    if [ "$DEBUG" = 1 ]; then {
      git remote --verbose 1>&2
    }; fi
  ) || exit 1
}

fetch_remotes() {
  (
    debug "cd into $repo_path"
    cd "$repo_path" || error "cannot cd into ${repo_path}"

    git fetch --all --prune
  ) || exit 1
}

clone_repo() {
  repo_path=$1
  bare=$(for_json ".\"${repo_path}\".bare // false")
  first_remote=$(for_json -r ".\"${repo_path}\".remotes | keys | first // \"\"")
  first_fetch_remote_url=$(for_json -r ".\"${repo_path}\".remotes.\"${first_remote}\".fetch // \"\"")

  if [ "$first_remote" = "" ] || [ "$first_fetch_remote_url" = "" ]; then {
    error "there is no remote configured for ${repo_path}, exiting..."
  }; fi

  debug "cloning ${repo_path}... (bare: ${bare}, remote: ${first_remote}, url: ${first_fetch_remote_url})"

  if [ "$bare" = true ]; then {
    git clone "${first_fetch_remote_url}" "${repo_path}" --bare

    (
      cd "$repo_path" || error "cannot cd into ${repo_path}"

      git remote remove "${first_remote}"
    )

  }; else {
    git clone "${first_fetch_remote_url}" "${repo_path}"
  }; fi
}

ROOT=${ROOT:-$HOME/repos}

cd "$ROOT" || error "failed to cd into $ROOT"

get_repos | while read -r repo_path; do {
  if [ ! -e "$repo_path" ]; then {
    debug "$repo_path non-exist"
    clone_repo "$repo_path"

    setup_remotes "$repo_path"

    fetch_remotes "$repo_path"
  }; else {
    debug "$repo_path exist"

    setup_remotes "$repo_path"

    fetch_remotes "$repo_path"
  }; fi
}; done
