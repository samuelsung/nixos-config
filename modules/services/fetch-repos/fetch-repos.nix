{ config, lib, pkgs, ... }:

let
  inherit (builtins) toJSON;
  inherit (lib) concatMapAttrs filterAttrs makeBinPath
    mkIf mkOption optionalAttrs removePrefix
    replaceStrings types;
  inherit (lib.strings) sanitizeDerivationName;

  cfg = config.services.fetch-repos;

  sanitizeName = name:
    replaceStrings
      [ "." ] [ "-" ]
      (sanitizeDerivationName (removePrefix "/" name));

  fetch-repos = pkgs.runCommand "fetch-repos" { buildInputs = with pkgs; [ bash makeWrapper ]; } ''
    mkdir -p $out/bin
    cp ${./fetch-repos.sh} $out/bin/fetch-repos
    patchShebangs $out/bin/fetch-repos

    wrapProgram $out/bin/fetch-repos --prefix PATH : ${with pkgs; makeBinPath [
      coreutils git gnugrep jq openssh
    ]}
  '';

  writeJson = name: obj:
    pkgs.writeTextFile { inherit name; text = toJSON obj; };

  publicRepos = concatMapAttrs
    (_: folder: optionalAttrs folder.enable (
      concatMapAttrs
        (_: repo: optionalAttrs (!repo.private) {
          ${repo.repoPath} = repo;
        })
        (filterAttrs (k: _: k != "enable") folder)
    ))
    config.repos;

  jsons = {
    ".repos.public.json" = publicReposJson;
  } // cfg.additional;

  publicReposJson = writeJson ".repos.public.json" publicRepos;
in
{
  options.services.fetch-repos = {
    enable = mkOption {
      type = types.bool;
      default = config.utils.basics.enable && config.utils.developments.enable;
      description = "periodic fetch-repos";
    };

    additional = mkOption {
      type = with types; attrsOf path;
      default = { };
    };

    frequency = mkOption {
      type = types.str;
      default = "*:0/5";
    };
  };

  config = mkIf cfg.enable {
    home.file.".config/fetch-repos/.repos.public.json".source = publicReposJson;

    systemd.user.services = concatMapAttrs
      (fileName: jsonPath: {
        "fetch-repos-${sanitizeName fileName}" = {
          Unit = {
            Description = "fetch-repos for fileName";
            After = [ "network.target" ];
          };
          Install.WantedBy = [ "default.target" ];
          Service = {
            Environment = [ "DEBUG=1" "ROOT=\"${config.home.homeDirectory}/repos\"" ];
            Type = "oneshot";
            ExecStart = "${fetch-repos}/bin/fetch-repos ${jsonPath}";
          };
        };
      })
      jsons;

    systemd.user.timers = concatMapAttrs
      (fileName: _: {
        "fetch-repos-${sanitizeName fileName}" = {
          Unit = {
            Description = "periodic fetch-repos for ${fileName}";
          };
          Timer = {
            Unit = "fetch-repos-${sanitizeName fileName}.service";
            OnCalendar = cfg.frequency;
          };
          Install.WantedBy = [ "timers.target" ];
        };
      })
      jsons;
  };
}
