{ config, lib, pkgs, ... }:

let
  cfg = config.virtualisation;
  inherit (lib) mkEnableOption mkIf mkMerge;
in
{
  options.virtualisation.libvirtd = {
    iscsi.enable = mkEnableOption "iscsi support of libvirtd";
    looking-glass.enable = mkEnableOption "looking-glass with libvirtd";
  };

  config = mkIf cfg.libvirtd.enable (
    mkMerge [
      {
        programs.dconf.enable = true;
        security.polkit.enable = true;

        environment.persistence."/persist" = mkIf config.impermanence.enable {
          hideMounts = true;
          directories = [
            "/var/lib/libvirt"
          ];
        };

        users.users.samuelsung = mkIf config.users.presets.samuelsung.enable {
          extraGroups = [ "libvirtd" ];
        };

        users.users.samuelsung_play = mkIf config.users.presets.samuelsung_play.enable {
          extraGroups = [ "libvirtd" ];
        };
      }

      (mkIf cfg.libvirtd.iscsi.enable {
        nixpkgs.config.packageOverrides = pkgs: {
          libvirt = pkgs.libvirt.override { enableIscsi = true; };
        };

        environment.systemPackages = with pkgs; [
          openiscsi
        ];

        systemd.services.iscsid = {
          description = "iSCSI daemon";
          path = [ pkgs.openiscsi ];
          script = "iscsid -f";
          wantedBy = [ "multi-user.target" ];
        };
      })

      (mkIf cfg.libvirtd.looking-glass.enable {
        environment.systemPackages = with pkgs; [
          (scream.override {
            pulseSupport = true;
          })

          unstable.looking-glass-client
        ];

        networking.firewall.allowedUDPPorts = [
          4010 ## for scream audio
        ];
      })
    ]
  );
}
