{ config, lib, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.services.vaultwarden;
in
{
  networking.firewall.allowedTCPPorts = mkIf cfg.enable [
    cfg.config.rocketPort
  ];

  services.vaultwarden = {
    config = {
      rocketPort = mkDefault 8000;
      signupsAllowed = mkDefault false;
    };
  };
}
