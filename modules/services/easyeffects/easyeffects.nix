{ config, lib, osConfig, pkgs, ... }:

let
  inherit (lib) concatMapAttrs flip mkDefault mkIf mkOption;

  json = pkgs.formats.json { };
  cfg = config.services.easyeffects;
in
{
  options.services.easyeffects = {
    presetSettings = mkOption {
      inherit (json) type;
      default = { };
    };
  };

  config.services.easyeffects.enable = mkDefault osConfig.services.pipewire.enable;

  config.services.easyeffects.preset = mkIf
    (cfg.presetSettings ? default)
    "default";

  config.home.file = flip concatMapAttrs cfg.presetSettings (name: settings: {
    ".config/easyeffects/output/${name}.json".source = json.generate "${name}.json" settings;
  });
}
