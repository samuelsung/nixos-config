{ config, lib, ... }:

let
  inherit (lib) concatStringsSep mkDefault mkIf mkForce;

  cfg = config.services.openssh;
in
{
  services.openssh = {
    settings = {
      Macs = [
        "umac-128-etm@openssh.com"
        "hmac-sha2-256-etm@openssh.com"
        "hmac-sha2-512-etm@openssh.com"
      ];

      KexAlgorithms = [
        "diffie-hellman-group14-sha256"
        "diffie-hellman-group16-sha512"
        "diffie-hellman-group18-sha512"
        "diffie-hellman-group-exchange-sha256"
        "curve25519-sha256"
        "curve25519-sha256@libssh.org"
      ];

      HostKeyAlgorithms = concatStringsSep "," [
        "rsa-sha2-512"
        "rsa-sha2-256"
        "ssh-ed25519"
      ];

      X11Forwarding = false;
      PermitRootLogin = mkForce "no";
      PasswordAuthentication = false;

      Protocol = 2;

      # Auth
      PubkeyAuthentication = true;
      ChallengeResponseAuthentication = false;
      KerberosAuthentication = false;
      GSSAPIAuthentication = false;

      ## Default is disable, just to be safe
      IgnoreRhosts = true;
      PermitEmptyPasswords = false;
      MaxAuthTries = 3;
      MaxSessions = 2;
      ClientAliveCountMax = 2;

      # Disable unuse service
      AllowTcpForwarding = mkDefault false;
      AllowAgentForwarding = mkDefault false;
      PermitTunnel = mkDefault false;
    };

    ports = [ 939 ]; # use port 939 instead of 22

    hostKeys = mkIf config.impermanence.enable [
      {
        path = "/persist/etc/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "/persist/etc/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };

  environment.etc = mkIf (cfg.enable && config.impermanence.enable) {
    "ssh/ssh_host_rsa_key".source = "/persist/etc/ssh/ssh_host_rsa_key";
    "ssh/ssh_host_rsa_key.pub".source = "/persist/etc/ssh/ssh_host_rsa_key.pub";
    "ssh/ssh_host_ed25519_key".source = "/persist/etc/ssh/ssh_host_ed25519_key";
    "ssh/ssh_host_ed25519_key.pub".source = "/persist/etc/ssh/ssh_host_ed25519_key.pub";
  };
}
