{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
  cfg = config.hardware.pulseaudio;
in
mkIf cfg.enable {
  environment.systemPackages = with pkgs; [
    pulsemixer
    playerctl
  ];

  hardware.pulseaudio = {
    package = pkgs.pulseaudioFull;
  };

  home-manager.sharedModules = [{
    windowManager.shortcuts = {
      incVolumn = {
        action = "${pkgs.pulsemixer}/bin/pulsemixer --change-volume +10";
        keys = [{ key = "XF86XK_AudioRaiseVolume"; }];
      };

      descVolumn = {
        action = "${pkgs.pulsemixer}/bin/pulsemixer --change-volume -10";
        keys = [{ key = "XF86XK_AudioLowerVolume"; }];
      };

      playPause = {
        action = "${pkgs.playerctl}/bin/playerctl play-pause";
        keys = [{ key = "XF86XK_AudioPlay"; }];
      };

      toggleMute = {
        action = "${pkgs.pulsemixer}/bin/pulsemixer --toggle-mute";
        keys = [{ key = "XF86XK_AudioMute"; }];
      };
    };
  }];
}
