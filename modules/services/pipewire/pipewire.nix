{ config, lib, pkgs, ... }:

let
  inherit (lib) mkDefault mkIf;
  cfg = config.services.pipewire;
in
mkIf cfg.enable {
  security.rtkit.enable = mkDefault true;
  services.pipewire = {
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;

    jack.enable = true;
  };

  home-manager.sharedModules = [
    (args:
      {
        windowManager.shortcuts = {
          incVolumn = {
            action = "${pkgs.pulsemixer}/bin/pulsemixer --change-volume +10";
            keys = [{ key = "XF86XK_AudioRaiseVolume"; }];
          };

          descVolumn = {
            action = "${pkgs.pulsemixer}/bin/pulsemixer --change-volume -10";
            keys = [{ key = "XF86XK_AudioLowerVolume"; }];
          };

          playPause = {
            action = "${pkgs.playerctl}/bin/playerctl play-pause";
            keys = [{ key = "XF86XK_AudioPlay"; }];
          };

          toggleMute = {
            action = "${pkgs.pulsemixer}/bin/pulsemixer --toggle-mute";
            keys = [{ key = "XF86XK_AudioMute"; }];
          };
        };

        home.persistence."/persist/${args.config.home.homeDirectory}" = mkIf config.impermanence.enable {
          directories = [
            ".local/state/wireplumber"
          ];
          allowOther = true;
        };
      }
    )
  ];
}
