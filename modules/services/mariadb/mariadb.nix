{ pkgs, ... }:

{
  services.mysql = {
    package = pkgs.mariadb;
  };
}
