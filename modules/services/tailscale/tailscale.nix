{ config, lib, ... }:

let
  inherit (lib) mkBefore mkDefault mkIf mkOption mkMerge;
  inherit (lib.types) nullOr str;
  cfg = config.services.tailscale;
  impermanence = config.impermanence.enable or false;
in
{
  options.services.tailscale.loginServer = mkOption {
    type = nullOr str;
    default = null;
    description = "login server of tailscale pointing to";
  };

  config = {
    services.tailscale.extraUpFlags = mkMerge [
      (mkIf (cfg.loginServer != null) (mkBefore [
        "--login-server"
        cfg.loginServer
      ]))
      [ "--accept-routes" ]
    ];

    services.tailscale.useRoutingFeatures = mkDefault "client";

    environment.persistence."/persist" = mkIf (cfg.enable && impermanence) {
      hideMounts = true;
      directories = [
        "/var/lib/tailscale"
      ];
    };
  };
}
