{ flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.web-qutebrowser = importApply
    ./qutebrowser.nix
    { inherit moduleWithSystem; };
}
