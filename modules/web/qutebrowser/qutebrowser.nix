localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ config, pkgs, lib, osConfig, ... }:

let
  inherit (lib) concatMap concatStringsSep flatten listToAttrs mkDefault mkIf mkMerge nameValuePair optionalAttrs optionalString;
  cfg = config.programs.qutebrowser;
  impermanence = osConfig.impermanence.enable or false;
in
mkMerge [
  { programs.qutebrowser.enable = mkDefault (config.desktop.enable && config.web.enable); }
  (mkIf cfg.enable {
    xdg.mimeApps.associations.added = {
      "application/x-extension-htm" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-html" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-shtml" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-xht" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-xhtml" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/xhtml+xml" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "text/html" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/chrome" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/http" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/https" = [ "org.qutebrowser.qutebrowser.desktop" ];
    };

    xdg.mimeApps.defaultApplications = mkIf (config.web.default == "qutebrowser") {
      "application/x-extension-htm" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-html" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-shtml" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-xht" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/x-extension-xhtml" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "application/xhtml+xml" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "text/html" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/chrome" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/http" = [ "org.qutebrowser.qutebrowser.desktop" ];
      "x-scheme-handler/https" = [ "org.qutebrowser.qutebrowser.desktop" ];
    };

    programs.qutebrowser = {
      enableDefaultBindings = false;

      package = pkgs.qutebrowser;

      searchEngines =
        let
          defaultSearchEngine = config.web.searchEngines.default;
        in
        (listToAttrs (concatMap
          (engine: map
            (alias: nameValuePair alias (engine.template "{}"))
            engine.aliases
          )
          config.web.searchEngines.list
        )) // (optionalAttrs (defaultSearchEngine != null) {
          DEFAULT = defaultSearchEngine.template "{}";
        });

      extraConfig = ''
        import theme
        theme.setup(c, 'mocha', True)
      '';

      settings =
        {
          tabs.select_on_remove = "last-used";
          tabs.last_close = "close";

          colors.webpage.preferred_color_scheme = "dark";
          content.webrtc_ip_handling_policy = "default-public-interface-only";

          content.headers.do_not_track = true;

          url.default_page = "https://codeberg.org";

          content.blocking.adblock.lists = [
            "https://easylist.to/easylist/easylist.txt"
            "https://easylist.to/easylist/easyprivacy.txt"
            "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt"
            "https://easylist.to/easylist/fanboy-annoyance.txt"
            "https://secure.fanboy.co.nz/fanboy-annoyance.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/annoyances.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badlists.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badware.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2020.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2021.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2022.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2023.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/lan-block.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/legacy.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/privacy.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/quick-fixes.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/resource-abuse.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/ubol-filters.txt"
            "https://github.com/uBlockOrigin/uAssets/raw/master/filters/unbreak.txt"
          ];

          content.blocking.method = "both";
        };

      keyBindings = {
        normal = {
          # navigation
          oo = "cmd-set-text -s :open"; # open in current tab
          ot = "cmd-set-text -s :open -t"; # open in new tab
          oT = "cmd-set-text :open -t -r {url:pretty}"; # open in new tab, with current url
          A = "cmd-set-text :open {url:pretty}"; # edit the url
          U = "cmd-set-text :open {url:pretty} ;; rl-beginning-of-line ;; repeat 3 rl-forward-word";

          ox = "cmd-set-text -s :open -b";
          oX = "cmd-set-text :open -b -r {url:pretty}";
          ow = "cmd-set-text -s :open -w";
          oW = "cmd-set-text :open -w {url:pretty}";

          "<Ctrl+w>t" = "open -t";
          "<Ctrl+w>w" = "open -w";
          "<Ctrl+w>p" = "open -p";

          "/" = "cmd-set-text /";
          "?" = "cmd-set-text ?";
          ":" = "cmd-set-text :";

          h = "scroll left";
          n = "scroll down";
          e = "scroll up";
          i = "scroll right";

          gg = "scroll-to-perc 0";
          G = "scroll-to-perc";

          I = "tab-next";
          H = "tab-prev";

          k = "search-next";
          K = "search-prev";

          co = "tab-only";

          "<Escape>" = "clear-keychain ;; search ;; fullscreen --leave";

          # "<Ctrl+w>" = "tab-close";
          # "<Ctrl+Shift+w>" = "close";

          "<Ctrl+w>1" = "tab-focus -n 1";
          "<Ctrl+w>2" = "tab-focus -n 2";
          "<Ctrl+w>3" = "tab-focus -n 3";
          "<Ctrl+w>4" = "tab-focus -n 4";
          "<Ctrl+w>5" = "tab-focus -n 5";
          "<Ctrl+w>6" = "tab-focus -n 6";
          "<Ctrl+w>7" = "tab-focus -n 7";
          "<Ctrl+w>8" = "tab-focus -n 8";
          "<Ctrl+w>9" = "tab-focus -n 9";

          "<Ctrl+w>i" = "tab-next";
          "<Ctrl+w>h" = "tab-prev";

          "<Ctrl+n>" = "zoom-out";
          "<Ctrl+e>" = "zoom-in";
          "<Ctrl+r>" = "zoom";

          "<Ctrl+b>" = "cmd-set-text -sr :tab-focus";
          ZQ = "tab-close";
          ZZ = "tab-close";

          ZAQ = "quit";
          ZAZ = "quit --save";

          gm = "tab-move";
          gh = "tab-move -";
          gi = "tab-move +";
          gc = "tab-clone";

          r = "reload";
          R = "reload -f";

          E = "back";
          gE = "back -t";
          GE = "back -w";

          N = "forward";
          gN = "forward -t";
          GN = "forward -w";

          "<F11>" = "fullscreen";

          ff = "hint";
          ft = "hint all tab";
          fw = "hint all window";
          fb = "hint all tab-bg";
          # "ff" = "hint all tab-fg";
          fh = "hint all hover";
          fp = "hint images";
          fP = "hint images tab";
          fo = "hint links fill :open {hint-url}";
          fO = "hint links fill :open -t -r {hint-url}";
          fy = "hint links yank";
          fY = "hint links yank-primary";
          fr = "hint --rapid links tab-bg";
          fR = "hint --rapid links window";
          fd = "hint links download";
          fi = "hint inputs";
          fI = "hint inputs --first";

          l = "undo";
          L = "undo -w";

          u = "mode-enter insert";
          v = "mode-enter caret";
          V = "mode-enter caret ;; selection-toggle --line";
          "`" = "mode-enter set_mark";
          "'" = "mode-enter jump_mark";

          yy = "yank";
          yY = "yank -s";
          yt = "yank title";
          yT = "yank title -s";
          yd = "yank domain";
          yD = "yank domain -s";
          yp = "yank pretty-url";
          yP = "yank pretty-url -s";
          ym = "yank inline [{title}]({url})";
          yM = "yank inline [{title}]({url}) -s";

          pp = "open -- {clipboard}";
          pP = "open -- {primary}";
          Pp = "open -t -- {clipboard}";
          PP = "open -t -- {primary}";
          wp = "open -w -- {clipboard}";
          wP = "open -w -- {primary}";

          bb = "cmd-set-text -s :bookmark-load";
          bt = "cmd-set-text -s :bookmark-load -t";
          bw = "cmd-set-text -s :bookmark-load -w";

          sf = "save";
          ss = "cmd-set-text -s :set";
          sl = "cmd-set-text -s :set -t";
          sk = "cmd-set-text -s :bind";
          "[[" = "navigate prev";
          "]]" = "navigate next";
          "{{" = "navigate prev -t";
          "}}" = "navigate next -t";

          gu = "navigate up";
          gU = "navigate up -t";

          wi = "devtools";
          wIh = "devtools left";
          wIj = "devtools bottom";
          wIk = "devtools top";
          wIl = "devtools right";
          wIw = "devtools window";
          wIf = "devtools-focus";

          gd = "download";
          ad = "download-cancel";
          cd = "download-clear";

          gf = "view-source";

          gt = "cmd-set-text -s :tab-select";

          "<Ctrl+Tab>" = "tab-focus last";
          "<Ctrl+^>" = "tab-focus last";
          "<Ctrl+v>" = "mode-enter passthrough";

          # "<Ctrl+f>" = "scroll-page 0 1";
          # "<Ctrl+b>" = "scroll-page 0 -1";
          # "<Ctrl+d>" = "scroll-page 0 0.5";
          # "<Ctrl+u>" = "scroll-page 0 -0.5";
          g0 = "tab-focus 1";
          "g^" = "tab-focus 1";
          "g$" = "tab-focus -1";
          "<Ctrl+h>" = "home";
          "<Ctrl+s>" = "stop";
          "<Ctrl+Alt+p>" = "print";
          Ss = "set";
          Sb = "bookmark-list --jump";
          Sq = "bookmark-list";
          Sh = "history";
          "<Return>" = "selection-follow";
          "<Ctrl+Return>" = "selection-follow -t";
          "." = "repeat-command";
          "<Ctrl+p>" = "tab-pin";
          m = "tab-mute";
          gD = "tab-give";

          q = "macro-record";
          "@" = "macro-run";

          tsh = "config-cycle -p -t -u *://{url:host}/* content.javascript.enabled ;; reload";
          tSh = "config-cycle -p -u *://{url:host}/* content.javascript.enabled ;; reload";
          tsH = "config-cycle -p -t -u *://*.{url:host}/* content.javascript.enabled ;; reload";
          tSH = "config-cycle -p -u *://*.{url:host}/* content.javascript.enabled ;; reload";
          tsu = "config-cycle -p -t -u {url} content.javascript.enabled ;; reload";
          tSu = "config-cycle -p -u {url} content.javascript.enabled ;; reload";
          tph = "config-cycle -p -t -u *://{url:host}/* content.plugins ;; reload";
          tPh = "config-cycle -p -u *://{url:host}/* content.plugins ;; reload";
          tpH = "config-cycle -p -t -u *://*.{url:host}/* content.plugins ;; reload";
          tPH = "config-cycle -p -u *://*.{url:host}/* content.plugins ;; reload";
          tpu = "config-cycle -p -t -u {url} content.plugins ;; reload";
          tPu = "config-cycle -p -u {url} content.plugins ;; reload";
          tih = "config-cycle -p -t -u *://{url:host}/* content.images ;; reload";
          tIh = "config-cycle -p -u *://{url:host}/* content.images ;; reload";
          tiH = "config-cycle -p -t -u *://*.{url:host}/* content.images ;; reload";
          tIH = "config-cycle -p -u *://*.{url:host}/* content.images ;; reload";
          tiu = "config-cycle -p -t -u {url} content.images ;; reload";
          tIu = "config-cycle -p -u {url} content.images ;; reload";
          tch = "config-cycle -p -t -u *://{url:host}/* content.cookies.accept all no-3rdparty never ;; reload";
          tCh = "config-cycle -p -u *://{url:host}/* content.cookies.accept all no-3rdparty never ;; reload";
          tcH = "config-cycle -p -t -u *://*.{url:host}/* content.cookies.accept all no-3rdparty never ;; reload";
          tCH = "config-cycle -p -u *://*.{url:host}/* content.cookies.accept all no-3rdparty never ;; reload";
          tcu = "config-cycle -p -t -u {url} content.cookies.accept all no-3rdparty never ;; reload";
          tCu = "config-cycle -p -u {url} content.cookies.accept all no-3rdparty never ;; reload";
        };

        caret = {
          v = "selection-toggle";
          V = "selection-toggle --line";
          "<Space>" = "selection-toggle";
          "<Ctrl+Space>" = "selection-drop";
          c = "mode-enter normal";
          e = "move-to-next-line";
          n = "move-to-prev-line";
          i = "move-to-next-char";
          h = "move-to-prev-char";
          # e = "move-to-end-of-word";
          w = "move-to-next-word";
          b = "move-to-prev-word";
          o = "selection-reverse";
          "]" = "move-to-start-of-next-block";
          "[" = "move-to-start-of-prev-block";
          "}" = "move-to-end-of-next-block";
          "{" = "move-to-end-of-prev-block";
          "0" = "move-to-start-of-line";
          "$" = "move-to-end-of-line";
          gg = "move-to-start-of-document";
          G = "move-to-end-of-document";
          Y = "yank selection -s";
          y = "yank selection";
          "<Return>" = "yank selection";
          H = "scroll left";
          E = "scroll down";
          N = "scroll up";
          I = "scroll right";
          "<Escape>" = "mode-leave";
        };

        command = {
          "<Ctrl+p>" = "command-history-prev";
          "<Ctrl+n>" = "command-history-next";
          "<Up>" = "completion-item-focus --history prev";
          "<Down>" = "completion-item-focus --history next";
          "<Shift+Tab>" = "completion-item-focus prev";
          "<Tab>" = "completion-item-focus next";
          "<Ctrl+Tab>" = "completion-item-focus next-category";
          "<Ctrl+Shift+Tab>" = "completion-item-focus prev-category";
          "<PgDown>" = "completion-item-focus next-page";
          "<PgUp>" = "completion-item-focus prev-page";
          "<Ctrl+d>" = "completion-item-del";
          "<Shift+Del>" = "completion-item-del";
          "<Ctrl+c>" = "completion-item-yank";
          "<Ctrl+Shift+c>" = "completion-item-yank --sel";
          "<Return>" = "command-accept";
          "<Ctrl+Return>" = "command-accept --rapid";
          "<Ctrl+b>" = "rl-backward-char";
          "<Ctrl+f>" = "rl-forward-char";
          "<Alt+b>" = "rl-backward-word";
          "<Alt+f>" = "rl-forward-word";
          "<Ctrl+a>" = "rl-beginning-of-line";
          "<Ctrl+e>" = "rl-end-of-line";
          "<Ctrl+u>" = "rl-unix-line-discard";
          "<Ctrl+k>" = "rl-kill-line";
          "<Alt+d>" = "rl-kill-word";
          "<Ctrl+w>" = "rl-rubout \" ";
          "<Ctrl+Shift+w>" = "rl-filename-rubout";
          "<Alt+Backspace>" = "rl-backward-kill-word";
          "<Ctrl+y>" = "rl-yank";
          "<Ctrl+?>" = "rl-delete-char";
          "<Ctrl+h>" = "rl-backward-delete-char";
          "<Escape>" = "mode-leave";
        };
        hint = {
          "<Return>" = "hint-follow";
          "<Ctrl+r>" = "hint --rapid links tab-bg";
          "<Ctrl+f>" = "hint links";
          "<Ctrl+b>" = "hint all tab-bg";
          "<Escape>" = "mode-leave";
        };
        insert = {
          "<Ctrl+e>" = "edit-text";
          "<Shift+Ins>" = "insert-text -- {primary}";
          "<Escape>" = "mode-leave";
          "<Shift+Escape>" = "fake-key <Escape>";
        };
        passthrough = {
          "<Shift+Escape>" = "mode-leave";
        };
        prompt = {
          "<Return>" = "prompt-accept";
          "<Ctrl+x>" = "prompt-open-download";
          "<Ctrl+p>" = "prompt-open-download --pdfjs";
          "<Shift+Tab>" = "prompt-item-focus prev";
          "<Up>" = "prompt-item-focus prev";
          "<Tab>" = "prompt-item-focus next";
          "<Down>" = "prompt-item-focus next";
          "<Alt+y>" = "prompt-yank";
          "<Alt+Shift+y>" = "prompt-yank --sel";
          "<Ctrl+b>" = "rl-backward-char";
          "<Ctrl+f>" = "rl-forward-char";
          "<Alt+b>" = "rl-backward-word";
          "<Alt+f>" = "rl-forward-word";
          "<Ctrl+a>" = "rl-beginning-of-line";
          "<Ctrl+e>" = "rl-end-of-line";
          "<Ctrl+u>" = "rl-unix-line-discard";
          "<Ctrl+k>" = "rl-kill-line";
          "<Alt+d>" = "rl-kill-word";
          "<Ctrl+w>" = "rl-rubout \" ";
          "<Ctrl+Shift+w>" = "rl-filename-rubout";
          "<Alt+Backspace>" = "rl-backward-kill-word";
          "<Ctrl+?>" = "rl-delete-char";
          "<Ctrl+h>" = "rl-backward-delete-char";
          "<Ctrl+y>" = "rl-yank";
          "<Escape>" = "mode-leave";
        };
        register = {
          "<Escape>" = "mode-leave";
        };
        yesno = {
          "<Return>" = "prompt-accept";
          y = "prompt-accept yes";
          n = "prompt-accept no";
          Y = "prompt-accept --save yes";
          N = "prompt-accept --save no";
          "<Alt+y>" = "prompt-yank";
          "<Alt+Shift+y>" = "prompt-yank --sel";
          "<Escape>" = "mode-leave";
        };
      };
    };

    home.file.".config/qutebrowser/theme.py".source = inputs'.misc.packages.catppuccin-qutebrowser;

    home.file.".config/qutebrowser/bookmarks/urls".text =
      let
        mapBookmark = section: { name, keywords, url, ... }:
          "${url} ${concatStringsSep "," keywords} ${optionalString (section != "") "${section}/"}${name}";

        mapFolder = { name, children, ... }:
          map
            (child:
              if child.url == null
              then mapFolder child
              else mapBookmark name child)
            children;
      in
      concatStringsSep "\n" (flatten (map mapFolder config.web.bookmarks));

    # NOTE(samuelsung): this is a tmp fix for the cursor theme not showing properly.
    # https://github.com/NixOS/nixpkgs/issues/204693
    programs.zsh.sessionVariables = {
      LD_LIBRARY_PATH = "${pkgs.xorg.libXcursor}/lib";
    };

    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".local/share/qutebrowser"
      ];
      allowOther = true;
    };
  })
])
