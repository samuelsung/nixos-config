{ config, pkgs, lib, ... }:

let
  inherit (pkgs) runCommand;
  inherit (lib) concatMapStringsSep imap0 mkIf optionals optionalAttrs optionalString;
  inherit (builtins) toJSON;

  mapEngine = order:
    { name
    , aliases
    , home
    , template
    , jsonSuggestionsTemplate
    , xmlSuggestionsTemplate
    , base64Icon
    , ...
    }: {
      "_name" = name;
      "_isAppProvided" = false;
      "_metaData" = {
        "loadPathHash" = "";
        "order" = order;
        "alias" = concatMapStringsSep "," (a: "@${a}") aliases;
        "hidden" = false;
      };
      "_loadPath" = "[other]addEngineWithDetails:set-via-user";
      "description" = name;
      "__searchForm" = home;
      "_iconURL" = optionalString (base64Icon != null) ''data:image/png;base64,${base64Icon}'';
      "_urls" = [{
        params = [ ];
        rels = [ ];
        template = template "{searchTerms}";
      }] ++ (optionals (jsonSuggestionsTemplate != null)) [{
        params = [ ];
        rels = [ ];
        template = jsonSuggestionsTemplate "{searchTerms}";
        type = "application/x-suggestions+json";
      }] ++ (optionals (xmlSuggestionsTemplate != null) [{
        params = [ ];
        rels = [ ];
        template = xmlSuggestionsTemplate "{searchTerms}";
        type = "application/x-suggestions+xml";
      }]);
      "_orderHint" = null;
      "_telemetryId" = null;
      "_updateInterval" = null;
      "_updateURL" = null;
      "_iconUpdateURL" = null;
      "_filePath" = null;
      "_extensionID" = null;
      "_locale" = null;
      "_definedAliases" = [ ];
    };

  mapSearchFile = engines:
    let
      defaultEngine = config.web.searchEngines.default;
    in
    toJSON {
      version = 6;
      engines = (lib.imap0 mapEngine engines) ++ [
        {
          "_name" = "Google";
          "_isAppProvided" = true;
          "_metaData"."hidden" = true;
        }

        {
          "_name" = "Bing";
          "_isAppProvided" = true;
          "_metaData"."hidden" = true;
        }

        {
          "_name" = "Amazon.com";
          "_isAppProvided" = true;
          "_metaData"."hidden" = true;
        }

        {
          "_name" = "Amazon.com";
          "_isAppProvided" = true;
          "_metaData"."hidden" = true;
        }
      ];
      metaData = {
        useSavedOrder = true;
      } // (optionalAttrs (defaultEngine != null) {
        current = defaultEngine.name;
        hash = defaultEngine.firefoxDefaultHash;
      });
    };

  writeMozlz4 = engines: runCommand "search.json.mozlz4a"
    {
      config = mapSearchFile engines;
      preferLocalBuild = true;
      allowSubstitutes = false;
      nativeBuildInputs = with pkgs; [ mozlz4a jq ];
    }
    ''
      echo "$config" | jq --compact-output > compact
      mozlz4a compact "$out"
    '';

  cfg = config.programs.firefox;
in
{
  home.file = mkIf cfg.enable {
    ".mozilla/firefox/default/search.json.mozlz4" = {
      source = writeMozlz4 config.web.searchEngines.list;
      force = true;
    };
  };
}
