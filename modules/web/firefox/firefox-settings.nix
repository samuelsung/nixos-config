{ config, pkgs, lib, ... }:

let
  inherit (builtins) head toJSON;
  inherit (lib)
    concatMap
    concatMapStrings
    concatMapStringsSep
    concatStringsSep
    take
    optionalAttrs
    optionalString
    replaceStrings;

  collectPinnedBookmarks = item:
    if (item.url == null)
    then concatMap collectPinnedBookmarks item.children
    else if item.pinned
    then [ item ]
    else [ ];

  escapeXML = replaceStrings [ ''"'' "'" "<" ">" "&" ] [
    "&quot;"
    "&apos;"
    "&lt;"
    "&gt;"
    "&amp;"
  ];

  entryMapper = entry:
    ''
      <DT><A HREF="${escapeXML entry.url}" ADD_DATE="0" LAST_MODIFIED="0"${
        optionalString (entry.keywords != [ ])
        " SHORTCUTURL=\"${escapeXML (concatStringsSep "," entry.keywords)}\""
      }>${escapeXML entry.name}</A>
    '';

  folderMapper = { name, ... }@entry:
    if (entry.url or null) == null then
      ''
        <DT><H3>${escapeXML name}</H3>
        <DL><p>
          ${concatMapStrings folderMapper entry.children}
        </DL><p>
      ''
    else entryMapper entry;

  mkBookmarkFile =
    { toolbar ? [ ]
    , menu ? [ ]
    }:
    pkgs.writeText "firefox-bookmarks.html" ''
      <!DOCTYPE NETSCAPE-Bookmark-file-1>
      <!-- This is an automatically generated file.
        It will be read and overwritten.
        DO NOT EDIT! -->
      <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
      <TITLE>Bookmarks</TITLE>
      <H1>Bookmarks Menu</H1>
      <DL><p>
        ${concatMapStrings folderMapper menu}
        <DT><H3 ADD_DATE="0" LAST_MODIFIED="0" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks Toolbar</H3>
        <DL><p>
          ${concatMapStrings folderMapper [{ name = ""; children = toolbar; }]}
        </DL><p>
      </DL>
    '';

in
{
  programs.firefox.oneTimeSettings = {
    # UI

    ## Hide toolbars
    "browser.toolbars.bookmarks.visibility" = "never";

    ## Inspect
    "devtools.toolbox.host" = "window";

    # Downloads

    ## Downloads will go to the Downloads folder
    "browser.download.folderList" = 1;

    ## Ask download location everytime
    "browser.download.useDownloadDir" = false;

    # Findbar

    ## match part of a word
    "findbar.entireword" = false;

    ## highlight all match result
    "findbar.highlightAll" = true;

    # BOOKMARKS
    "browser.bookmarks.file" = "${mkBookmarkFile { toolbar = config.web.bookmarks; }}";
    "browser.places.importBookmarksHTML" = true;
  };

  programs.firefox.permanentSettings = {
    # UI

    ## Placeholder name in the url bar
    "browser.urlbar.placeholderName" = "DuckDuckGo";
    "browser.urlbar.placeholderName.private" = "DuckDuckGo";

    ## New Tab

    ### Show search bar
    "browser.newtabpage.activity-stream.showSearch" = true;

    ### Show top sites
    "browser.newtabpage.activity-stream.feeds.topsites" = true;

    ### Hide sponsored sites
    "browser.newtabpage.activity-stream.showSponsored" = false;
    "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;

    ### Hide recent activity
    "browser.newtabpage.activity-stream.feeds.section.highlights" = false;

    ### Show 1 row of top sites
    "browser.newtabpage.activity-stream.topSitesRows" = 1;

    ### Disable tips
    "browser.newtabpage.activity-stream.feeds.snippets" = false;

    ### Clear default topsites
    "browser.newtabpage.activity-stream.default.sites" = "";

    ### Top sites blocks
    "browser.newtabpage.blocked" =
      let
        blockList = [
          "T9nJot5PurhJSy8n038xGA==" # twitter
          "4gPpjkxgZzXPVtuEoAL9Ig==" # facebook
          "gLv0ja2RYVgxKdp0I5qwvA==" # reddit
          "26UbzFJ7qT9/4DhodHKA1Q==" # youtube
          "11J3zf/vmVpGrlm9rvHbhg==" # google
          "BRX66S9KVyZQ1z3AIk0A7w==" # amazon
        ];
      in
      ''{${concatMapStringsSep "," (hash: ''"${hash}":1'') blockList}}'';

    ### pinned suggestions
    "browser.newtabpage.pinned" = toJSON (take 8 (
      map
        ({ url, keywords, ... }: {
          inherit url;
          label = head keywords;
        })
        (concatMap collectPinnedBookmarks config.web.bookmarks)
    ));

    ## Toolbar
    ### Don't show the import bookmark from other browser button in the toolbar
    "browser.bookmarks.addedImportButton" = true;

    # BEHAVIOR
    ## don't check is default browser on start up
    "browser.shell.checkDefaultBrowser" = false;

    ## Resume previous browser session
    ## 0=blank, 1=home, 2=last visited page, 3=resume previous session
    "browser.startup.page" = 3;

    ## Show homepage and new windows as firefox home
    "browser.startup.homepage" = "about:home";

    ## Show new tab as firefox home
    ## true=about:home, false=about:blank
    "browser.newtabpage.enabled" = true;

    ## Preload the newtab page so we do not need to wait
    "browser.newtab.preload" = true;

    ## do not warn user when it can be resume
    "browser.sessionstore.warnOnQuit" = false;

    ## Ctrl+Tab cycles through tabs in recently used order
    "browser.ctrlTab.sortByRecentlyUsed" = false;

    ## Open a new tab when click on a link
    "browser.link.open_newwindow" = 3;

    ## Open a new tab without switch to it by default
    "browser.tabs.loadInBackground" = true;

    ## Disable DRM
    "media.eme.enabled" = false;

    ## Use default performance settings
    "browser.preferences.defaultPerformanceSettings.enabled" = true;

    ## Disable auto scrolling
    "general.autoScroll" = false;

    ## Enable smooth scrolling
    "general.smoothScroll" = true;

    ## Always use the cursor keys to navigate within pages
    "accessibility.browsewithcaret" = false;

    ## Search for text when you start typing
    "accessibility.typeaheadfind" = false;

    ## Control media via keyboard, headset, or virtual interface
    "media.hardwaremediakeys.enabled" = true;

    ## Enable picture-in-picture video controls
    "media.videocontrols.picture-in-picture.video-toggle.enabled" = true;

    ## Recommend features as you browse
    "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features" = false;

    ## Recommend extensions as you browse
    "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons" = false;

    ## Ask to save logins and passwords for websites
    "signon.rememberSignons" = false;
    ### Autofill logins and passwords
    "signon.autofillForms" = false;
    ### Suggest and generate strong passwords
    "signon.generation.enabled" = false;
    ### disable formless login capture for Password Manager [FF51+]
    "signon.formlessCapture.enabled" = false;
    ### Show alerts about passwords for breached websites
    "signon.management.page.breach-alerts.enabled" = false;

    ### limit (or disable) HTTP authentication credentials dialogs triggered by sub-resources [FF41+]
    ### hardens against potential credentials phishing
    ### 0 = don't allow sub-resources to open HTTP authentication credentials dialogs
    ### 1 = don't allow cross-origin sub-resources to open HTTP authentication credentials dialogs
    ### 2 = allow sub-resources to open HTTP authentication credentials dialogs (default)
    "network.auth.subresource-http-auth-allow" = 1;

    ### 0906: enforce no automatic authentication on Microsoft sites [FF91+] [WINDOWS 10+]
    ### [SETTING] Privacy & Security>Logins and Passwords>Allow Windows single sign-on for...
    ### [1] https://support.mozilla.org/kb/windows-sso
    "network.http.windows-sso.enabled" = false;

    ### enable https only mode
    "dom.security.https_only_mode" = true;
    "dom.security.https_only_mode_ever_enabled" = true;

    # SEARCH
    ## Allow add search engines
    ## See https://bugzilla.mozilla.org/show_bug.cgi?id=1195005
    "browser.urlbar.update2.engineAliasRefresh" = true;

    ## When using the address bar, suggest:
    ### bookmarks
    "browser.urlbar.suggest.bookmark" = true;
    ### search engines
    "browser.urlbar.suggest.engines" = true;
    ### history
    "browser.urlbar.suggest.history" = false;
    ### open tabes
    "browser.urlbar.suggest.openpage" = true;
    ### shortcuts
    "browser.urlbar.suggest.topsites" = true;

    # GEOLOCATION

    ## 0201: use Mozilla geolocation service instead of Google if permission is granted [FF74+]
    ## Optionally enable logging to the console (defaults to false)
    "geo.provider.network.url" = "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%";
    ## "geo.provider.network.logging.enabled", true); # [HIDDEN PREF]
    ## 0202: disable using the OS's geolocation service
    "geo.provider.ms-windows-location" = false; # [WINDOWS]
    "geo.provider.use_corelocation" = false; # [MAC]
    "geo.provider.use_gpsd" = false; # [LINUX]

    ## 0203: disable region updates
    ## [1] https://firefox-source-docs.mozilla.org/toolkit/modules/toolkit_modules/Region.html
    "browser.region.network.url" = ""; # [FF78+]
    "browser.region.update.enabled" = false; # [FF79+]

    # TELEMETRY
    ## 0105: disable some Activity Stream items
    ## Activity Stream is the default homepage/newtab based on metadata and browsing behavior
    ## [SETTING] Home>Firefox Home Content>...  to show/hide what you want
    "browser.newtabpage.activity-stream.feeds.telemetry" = false;
    "browser.newtabpage.activity-stream.telemetry" = false;
    "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
    "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
    "browser.newtabpage.activity-stream.feeds.discoverystreamfeed" = false; # [FF66+]

    ## 0330: disable new data submission [FF41+]
    ## If disabled, no policy is shown or upload takes place, ever
    ## [1] https://bugzilla.mozilla.org/1195552
    "datareporting.policy.dataSubmissionEnabled" = false;
    ## 0331: disable Health Reports
    ## [SETTING] Privacy & Security>Firefox Data Collection & Use>Allow Firefox to send technical... data
    "datareporting.healthreport.uploadEnabled" = false;
    ## 0332: disable telemetry
    ## The "unified" pref affects the behaviour of the "enabled" pref
    ## - If "unified" is false then "enabled" controls the telemetry module
    ## - If "unified" is true then "enabled" only controls whether to record extended data
    ## [NOTE] "toolkit.telemetry.enabled" is now LOCKED to reflect prerelease (true or release builds (false [2]
    ## [1] https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/internals/preferences.html
    ## [2] https://medium.com/georg-fritzsche/data-preference-changes-in-firefox-58-2d5df9c428b5
    "toolkit.telemetry.unified" = false;
    "toolkit.telemetry.enabled" = false; # see [NOTE]
    "toolkit.telemetry.server" = "data:,";
    "toolkit.telemetry.archive.enabled" = false;
    "toolkit.telemetry.newProfilePing.enabled" = false; # [FF55+]
    "toolkit.telemetry.shutdownPingSender.enabled" = false; # [FF55+]
    "toolkit.telemetry.updatePing.enabled" = false; # [FF56+]
    "toolkit.telemetry.bhrPing.enabled" = false; # [FF57+] Background Hang Reporter
    "toolkit.telemetry.firstShutdownPing.enabled" = false; # [FF57+]
    ## 0333: disable Telemetry Coverage
    ## [1] https://blog.mozilla.org/data/2018/08/20/effectively-measuring-search-in-firefox/
    "toolkit.telemetry.coverage.opt-out" = true; # [HIDDEN PREF]
    "toolkit.coverage.opt-out" = true; # [FF64+] [HIDDEN PREF]
    "toolkit.coverage.endpoint.base" = "";
    ## 0334: disable PingCentre telemetry (used in several System Add-ons [FF57+]
    ## Defense-in-depth: currently covered by 0331
    "browser.ping-centre.telemetry" = false;

    # STUDIES
    ## 0340: disable Studies
    ## [SETTING] Privacy & Security>Firefox Data Collection & Use>Allow Firefox to install and run studies ***/
    "app.shield.optoutstudies.enabled" = false;
    ## 0341: disable Normandy/Shield [FF60+]
    ## Shield is a telemetry system that can push and test "recipes"
    ## [1] https://mozilla.github.io/normandy/
    "app.normandy.enabled" = false;
    "app.normandy.api_url" = "";

    # CRASH REPORTS
    ## 0350: disable Crash Reports
    "breakpad.reportURL" = "";
    "browser.tabs.crashReporting.sendReport" = false; # [FF44+]
    # "browser.crashReports.unsubmittedCheck.enabled" = false; # [FF51+] [DEFAULT: false]
    ## 0351: enforce no submission of backlogged Crash Reports [FF58+]
    ## [SETTING] Privacy & Security>Firefox Data Collection & Use>Allow Firefox to send backlogged crash reports  ***/
    "browser.crashReports.unsubmittedCheck.autoSubmit2" = false; # [DEFAULT: false]

    # OTHER
    ## 0360: disable Captive Portal detection
    ## [1] https://www.eff.org/deeplinks/2017/08/how-captive-portals-interfere-wireless-security-and-privacy
    "captivedetect.canonicalURL" = "";
    "network.captive-portal-service.enabled" = false; # [FF52+]
    ## 0361: disable Network Connectivity checks [FF65+]
    ## [1] https://bugzilla.mozilla.org/1460537
    "network.connectivity-service.enabled" = false;
    ## 0362: enforce disabling of Web Compatibility Reporter [FF56+]
    ## Web Compatibility Reporter adds a "Report Site Issue" button to send data to Mozilla
    "extensions.webcompat-reporter.enabled" = false; # [DEFAULT: false]

  } // (optionalAttrs (config.programs.firefox.proxy != null) {
    # PROXY
    # Manual proxy configuration
    "network.proxy.type" = 1;

    ## share http and https proxy
    "network.proxy.share_proxy_settings" = true;

    ## http setting

    "network.proxy.http" = "127.0.0.1";
    "network.proxy.http_port" = 8118;

    ## https setting
    "network.proxy.ssl" = "127.0.0.1";
    "network.proxy.ssl_port" = 8118;

    ## disable skip proxy url
    "network.proxy.no_proxies_on" = "";

    ## socks setting
    "network.proxy.socks" = "";
    "network.proxy.socks_port" = 0;
    "network.proxy.socks_version" = 5;

    ## do not proxy dns when using socks v5
    "network.proxy.socks_remote_dns" = false;

    ## prompt for authentication if password is saved
    "signon.autologin.proxy" = false;

    ## disable DNS over HTTPS
    "network.trr.mode" = 5;
    "network.trr.uri" = "";
    "network.trr.custom_uri" = "";
    "network.trr.default_provider_uri" = "";

  });
}
