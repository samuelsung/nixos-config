{ pkgs, lib, config, osConfig, ... }:

let
  inherit (builtins) toJSON;
  inherit (lib) concatStrings mapAttrsToList mkDefault mkIf mkMerge mkOption types;

  cfg = config.programs.firefox;
in
{
  options.programs.firefox.permanentSettings = mkOption {
    type = with types; attrsOf (oneOf [ number str bool ]);
  };

  options.programs.firefox.oneTimeSettings = mkOption {
    type = with types; attrsOf (oneOf [ number str bool ]);
  };

  options.programs.firefox.proxy = mkOption {
    type = with types; nullOr (submodule {
      options.httpProxy = mkOption {
        type = types.str;
      };
      options.httpPort = mkOption {
        type = types.int;
      };
      options.httpsProxy = mkOption {
        type = types.str;
      };
      options.httpsPort = mkOption {
        type = types.int;
      };
    });
    default = null;
  };

  imports = [
    ./firefox-settings.nix
    ./firefox-searchEngines.nix
  ];

  config = mkMerge [
    { programs.firefox.enable = mkDefault (config.desktop.enable && config.web.enable); }

    (mkIf cfg.enable {
      xdg.mimeApps.associations.added = {
        "application/x-extension-htm" = [ "firefox.desktop" ];
        "application/x-extension-html" = [ "firefox.desktop" ];
        "application/x-extension-shtml" = [ "firefox.desktop" ];
        "application/x-extension-xht" = [ "firefox.desktop" ];
        "application/x-extension-xhtml" = [ "firefox.desktop" ];
        "application/xhtml+xml" = [ "firefox.desktop" ];
        "text/html" = [ "firefox.desktop" ];
        "x-scheme-handler/chrome" = [ "firefox.desktop" ];
        "x-scheme-handler/http" = [ "firefox.desktop" ];
        "x-scheme-handler/https" = [ "firefox.desktop" ];
      };

      xdg.mimeApps.defaultApplications = mkIf (config.web.default == "firefox") {
        "application/x-extension-htm" = [ "firefox.desktop" ];
        "application/x-extension-html" = [ "firefox.desktop" ];
        "application/x-extension-shtml" = [ "firefox.desktop" ];
        "application/x-extension-xht" = [ "firefox.desktop" ];
        "application/x-extension-xhtml" = [ "firefox.desktop" ];
        "application/xhtml+xml" = [ "firefox.desktop" ];
        "text/html" = [ "firefox.desktop" ];
        "x-scheme-handler/chrome" = [ "firefox.desktop" ];
        "x-scheme-handler/http" = [ "firefox.desktop" ];
        "x-scheme-handler/https" = [ "firefox.desktop" ];
      };

      programs.firefox.package =
        pkgs.firefox.override {
          extraPrefs =
            let
              mapLockPref = prefs: concatStrings (mapAttrsToList
                (name: value: ''
                  lockPref("${name}", ${toJSON value});
                '')
                prefs);
            in
            mapLockPref cfg.permanentSettings;
        };

      programs.firefox.profiles = {
        default = {
          id = 0;
          isDefault = true;
          settings = cfg.oneTimeSettings;

          extensions = with pkgs.nur.repos.rycee.firefox-addons; [
            privacy-badger
            vimium
            h264ify
            bitwarden
            reduxdevtools
            react-devtools
            ublock-origin
          ];
        };
      };

      home.persistence."/persist/${config.home.homeDirectory}" = mkIf osConfig.impermanence.enable {
        directories = [
          ".mozilla"
        ];
        allowOther = true;
      };
    })
  ];
}
