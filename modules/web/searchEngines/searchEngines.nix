{ config, lib, ... }:

let
  inherit (builtins) match;
  inherit (lib) filter head mkIf mkOption types;
  cfg = config.web.searchEngines;

  searchEngineType = types.submodule (args: {
    options.name = mkOption {
      type = types.str;
    };

    options.aliases = mkOption {
      type = with types; listOf str;
    };

    options.home = mkOption {
      type = types.str;
      default =
        let
          matchUrl = match "(https?://[a-zA-Z1-9\.]+)/.*";
        in
        head (matchUrl (args.config.template ""));
    };

    options.template = mkOption {
      type = with types; functionTo str;
    };

    options.jsonSuggestionsTemplate = mkOption {
      type = with types;
        nullOr (functionTo str);
      default = null;
    };

    options.xmlSuggestionsTemplate = mkOption {
      type = with types; nullOr (functionTo str);
      default = null;
    };

    options.base64Icon = mkOption {
      type = with types; nullOr str;
      default = null;
    };

    options.default = mkOption {
      type = types.bool;
      default = false;
    };

    options.firefoxDefaultHash = mkOption {
      type = with types; nullOr str;
      default = null;
    };
  });
in
{
  options.web.searchEngines = {
    default = mkOption {
      type = with types; nullOr searchEngineType;
      default =
        let
          defaults = filter ({ default, ... }: default) cfg.list;
        in
        if (defaults == [ ]) then null else head defaults;
      readOnly = true;
    };

    list = mkOption {
      type = types.listOf searchEngineType;
    };
  };

  config = mkIf config.web.enable {
    assertions = map
      ({ name, default, firefoxDefaultHash, ... }: {
        assertion = default -> firefoxDefaultHash != null;
        message = ''
          Default engine ${name} require firefoxDefaultHash.
        '';
      })
      cfg.list;
  };
}
