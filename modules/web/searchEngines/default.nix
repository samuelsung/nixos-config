{
  flake.homeManagerModules.web-searchEngines = ./searchEngines.nix;
  flake.homeManagerModules.web-searchEngines-samuelsung = ./searchEngines-samuelsung.nix;
}
