{ config, lib, ... }:

let
  inherit (lib) mkOption optionals types;
  images = import ./images.nix;
  cfg = config.web.searchEngines;
in
{
  options.web.searchEngines = {
    withGaming = mkOption {
      type = types.bool;
      default = true;
    };

    withSocialMedias = mkOption {
      type = types.bool;
      default = true;
    };

    withShopping = mkOption {
      type = types.bool;
      default = true;
    };
  };

  config = {
    web.searchEngines.list = [
      {
        name = "DuckDuckGo";
        aliases = [ "d" "duckduckgo" ];
        template = key: "https://duckduckgo.com/?q=${key}";
        base64Icon = images.duckduckgo;
        default = true;
        firefoxDefaultHash = "SAwnRqalCPASel0MooiVGYhRsMN6LGAxjSZ0SFJCZIY=";
      }

      {
        name = "NixOS packages";
        aliases = [ "n" "nix" "nixpkgs" ];
        template = key: "https://search.nixos.org/packages?query=${key}";
        base64Icon = images.nix;
      }

      {
        name = "NixOS options";
        aliases = [ "no" "nixopt" "nixoptions" ];
        template = key: "https://search.nixos.org/options?query=${key}";
        base64Icon = images.nix;
      }

      {
        name = "NixOS Wiki (en)";
        aliases = [ "nw" "nixwiki" "nixoswiki" ];
        template = key:
          "https://nixos.wiki/index.php?title=Special:Search&search=${key}";
        jsonSuggestionsTemplate = key:
          "https://nixos.wiki/api.php?action=opensearch&search=${key}&namespace=0";
        xmlSuggestionsTemplate = key:
          "https://nixos.wiki/api.php?action=opensearch&format=xml&search=${key}&namespace=0";
        base64Icon = images.nix;
      }

      {
        name = "ArchWiki (en)";
        aliases = [ "aw" "arch" "archwiki" ];
        template = key:
          "https://wiki.archlinux.org/index.php?title=Special:Search&search=${key}";
        jsonSuggestionsTemplate = key:
          "https://wiki.archlinux.org/api.php?action=opensearch&search=${key}&namespace=0|3000";
        xmlSuggestionsTemplate = key:
          "https://wiki.archlinux.org/api.php?action=opensearch&format=xml&search=${key}&namespace=0|3000";
        base64Icon = images.arch;
      }

      {
        name = "GitLab";
        aliases = [ "gl" "gitlab" ];
        template = key:
          "https://gitlab.com/search?search=${key}";
        base64Icon = images.gitlab;
      }

      {
        name = "GitHub";
        aliases = [ "gh" "github" ];
        template = key:
          "https://github.com/search?q=${key}&ref=opensearch";
        base64Icon = images.github;
      }

      {
        name = "Yarn";
        aliases = [ "yarn" ];
        template = key:
          "https://yarnpkg.com/?q=${key}";
        base64Icon = images.yarn;
      }

      {
        name = "npm";
        aliases = [ "npm" ];
        template = key:
          "https://www.npmjs.com/search?q=${key}";
        base64Icon = images.npm;
      }

      {
        name = "Cargo";
        aliases = [ "crates" "cargo" ];
        template = key:
          "https://crates.io/search?q=${key}";
        base64Icon = images.cargo;
      }

      {
        name = "Docs.rs";
        aliases = [ "docsrs" ];
        template = key:
          "https://docs.rs/releases/search?query=${key}";
        base64Icon = images.docsrs;
      }

      {
        name = "Hoogle";
        aliases = [ "hoogle" ];
        template = key:
          "https://hoogle.haskell.org/?hoogle=${key}";
        jsonSuggestionsTemplate = key:
          "https://hoogle.haskell.org/?hoogle=${key}&mode=suggest";
        base64Icon = images.hoggle;
      }

      {
        name = "Wikipedia (en)";
        aliases = [ "w" "wiki" "wikien" ];
        template = key:
          "https://en.wikipedia.org/wiki/Special:Search?search=${key}&go=Go&ns0=1";
        base64Icon = images.wiki;
      }

      {
        name = "Wikipedia (ja)";
        aliases = [ "wikijp" ];
        template = key:
          "https://ja.wikipedia.org/w/index.php?title=%E7%89%B9%E5%88%A5:%E6%A4%9C%E7%B4%A2&search=${key}";
        base64Icon = images.wiki;
      }
    ] ++ (optionals cfg.withGaming [
      {
        name = "Final Fantasy XIV The Lodestone";
        aliases = [ "lodestone" ];
        template = key:
          "https://jp.finalfantasyxiv.com/lodestone/community/search/?q=${key}";
        base64Icon = images.lodestone;
      }
      {
        name = "FF14攻略 - Game8";
        aliases = [ "game8ff14" ];
        template = key:
          "https://game8.jp/ff14/search?q=${key}";
        base64Icon = images.game8;
      }
    ]) ++ (optionals cfg.withSocialMedias [
      {
        name = "YouTube";
        aliases = [ "youtube" ];
        template = key:
          "https://www.youtube.com/results?search_query=${key}&utm_source=opensearch";
        base64Icon = images.youtube;
      }

      {
        name = "Twitter";
        aliases = [ "twitter" ];
        template = key:
          "https://twitter.com/search?q=${key}";
        base64Icon = images.twitter;
      }

      {
        name = "Reddit";
        aliases = [ "reddit" ];
        template = key:
          "https://www.reddit.com/search/?q=${key}";
        base64Icon = images.reddit;
      }

      {
        name = "LIHKG";
        aliases = [ "lihkg" ];
        template = key:
          "https://lihkg.com/search?q=${key}";
        base64Icon = images.lihkg;
      }
    ]) ++ (optionals cfg.withShopping [
      {
        name = "Amazon Search Suggestions";
        aliases = [ "amazon" "amazonjp" ];
        template = key:
          "http://amazon.co.jp/s?k=${key}";
        jsonSuggestionsTemplate = key:
          "http://completion.amazon.co.jp/search/complete?method=completion&q=${key}&search-alias=aps&mkt=6";
        base64Icon = images.amazon;
      }
    ]);
  };
}
