({ config, lib, ... }:
let
  inherit (lib) mkEnableOption mkOption types;
in
{
  options.web.enable = mkEnableOption "web related modules";
  options.web.default = mkOption {
    type = types.enum [ "firefox" "qutebrowser" ];
    default = "qutebrowser";
    description = "default browser";
  };

  config.assertions = [{
    assertion = config.web.enable -> {
      firefox = config.programs.firefox.enable;
      qutebrowser = config.programs.qutebrowser.enable;
    }.${config.web.default};
    message = "default browser ${config.web.default} is not enabled.";
  }];
})
