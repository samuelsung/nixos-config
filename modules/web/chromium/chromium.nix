{ pkgs, lib, config, osConfig, ... }:

let
  inherit (lib) mkDefault mkIf mkMerge;
  impermanence = osConfig.impermanence.enable or false;
  cfg = config.programs.chromium;
in
mkMerge [
  {
    programs.chromium.enable = mkDefault (config.desktop.enable && config.web.enable);
    programs.chromium.package = pkgs.ungoogled-chromium;
  }

  (mkIf cfg.enable {
    home.persistence."/persist/${config.home.homeDirectory}" = mkIf impermanence {
      directories = [
        ".config/chromium"
      ];
      allowOther = true;
    };
  })
]
