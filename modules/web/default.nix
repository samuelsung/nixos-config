{
  flake.homeManagerModules.web = ./web.nix;

  imports = [
    ./bookmarks
    ./chromium
    ./firefox
    ./qutebrowser
    ./searchEngines
    ./zen
  ];
}
