{ lib, ... }:

let
  inherit (lib) mkOption types;

  bookmarkModule = types.submodule {
    options = {
      name = mkOption {
        type = types.str;
        description = "name of the folder/bookmark";
      };

      children = mkOption {
        default = [ ];
        type = with types; listOf bookmarkModule;
        description = "children of the folder";
      };

      keywords = mkOption {
        default = [ ];
        type = with types; listOf str;
        description = "keywords of the bookmark";
      };

      url = mkOption {
        default = null;
        type = with types; nullOr str;
        description = "url of the bookmark";
      };

      pinned = mkOption {
        type = types.bool;
        default = false;
      };
    };
  };
in
{
  options.web.bookmarks = mkOption {
    default = [ ];
    type = types.listOf bookmarkModule;
    description = "a set of bookmarks";
  };
}
