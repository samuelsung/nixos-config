{
  web.bookmarks = [
    {
      name = "work";
      children = [
        { name = "ifs-gitlab"; keywords = [ "ifsgitlab" ]; url = "http://scm-server.ifshk.com/"; }
        { name = "ifs-homepage"; keywords = [ "ifshomepage" ]; url = "http://homepage.ifshk.com/"; }
        { name = "ifs-tsm"; keywords = [ "ifstsm" ]; url = "http://tsm.ifshk.com:8080/login/"; }
        { name = "ifs-odoo"; keywords = [ "ifsodoo" ]; url = "http://comweb.ifshk.com"; }
        { name = "ifs-webmail"; keywords = [ "ifswebmail" ]; url = "https://webmail.ifshk.com"; }
        { name = "Jira"; keywords = [ "jira" ]; url = "https://ifshk.atlassian.net/jira/your-work"; }
      ];
    }

    {
      name = "nix";
      children = [
        {
          name = "repos";
          children = [
            { name = "NixOS/nixpkgs"; keywords = [ "nixpkgs" ]; url = "https://github.com/NixOS/nixpkgs"; pinned = true; }
            { name = "qutebrowser/qutebrowser"; keywords = [ "qutebrowser" ]; url = "https://github.com/qutebrowser/qutebrowser"; }
            { name = "Mic92/sops-nix"; keywords = [ "sopsnix" ]; url = "https://github.com/Mic92/sops-nix"; }
            { name = "mozilla/sops"; keywords = [ "sops" ]; url = "https://github.com/mozilla/sops"; }
            { name = "nix-community/nur-combined"; keywords = [ "nurcombined" ]; url = "https://github.com/nix-community/nur-combined"; }
          ];
        }

        { name = "Nixpkgs Search"; keywords = [ "nixsearch" ]; url = "https://search.nixos.org"; }
      ];
    }

    {
      name = "linux";
      children = [
        {
          name = "Arch Wiki";
          keywords = [ "archwiki" ];
          url = "https://wiki.archlinux.org/";
          pinned = true;
        }
      ];
    }

    {
      name = "references";
      children = [
        {
          name = "JS/TS";
          children = [
            { name = "Typescript Handbook"; keywords = [ "typescript" ]; url = "https://www.typescriptlang.org/docs/handbook/intro.html"; }
            { name = "MDN"; keywords = [ "mdn" ]; url = "https://developer.mozilla.org/en-US/"; }
            { name = "fp-ts"; keywords = [ "fpts" ]; url = "https://gcanti.github.io/fp-ts/"; }
            { name = "io-ts"; keywords = [ "iots" ]; url = "https://github.com/gcanti/io-ts/blob/master/index.md"; }
            { name = "i18next"; keywords = [ "i18next" ]; url = "https://www.i18next.com/"; }
            { name = "date-fns"; keywords = [ "datefns" ]; url = "https://date-fns.org/docs/Getting-Started"; }
            { name = "Mock Service Worker"; keywords = [ "msw" ]; url = "https://mswjs.io/docs/"; }
            { name = "Redux"; keywords = [ "redux" ]; url = "https://redux.js.org/api/api-reference"; }
            { name = "Redux Toolkit"; keywords = [ "rtk" ]; url = "https://redux-toolkit.js.org/introduction/getting-started"; }
            { name = "RTK Query"; keywords = [ "rtkquery" ]; url = "https://redux-toolkit.js.org/rtk-query/overview"; }
          ];
        }

        {
          name = "React";
          children = [
            { name = "react-hook-form"; keywords = [ "reacthookform" ]; url = "https://react-hook-form.com/"; }
            { name = "MUI"; keywords = [ "mui" ]; url = "https://mui.com/material-ui/"; }
            { name = "react-i18next"; keywords = [ "reacti18next" ]; url = "https://react.i18next.com/"; }
            { name = "react-redux"; keywords = [ "reactredux" ]; url = "https://react-redux.js.org/api/hooks"; }
            { name = "React Router"; keywords = [ "reactrouter" ]; url = "https://reactrouter.com/en/main"; }
          ];
        }

        {
          name = "Solid";
          children = [
            { name = "SolidJS"; keywords = [ "solid" ]; url = "https://www.solidjs.com"; }
            { name = "solid-icons"; keywords = [ "solidicons" ]; url = "https://solid-icons.vercel.app"; }
            { name = "solid-router"; keywords = [ "solidrouter" ]; url = "https://github.com/solidjs/solid-router"; }
            { name = "solid-i18next"; keywords = [ "solidi18next" ]; url = "https://github.com/mbarzda/solid-i18next"; }
          ];
        }

        {
          name = "CSS";
          children = [
            { name = "TailwindCSS"; keywords = [ "tailwind" ]; url = "https://tailwindcss.com/docs"; }
            { name = "Color Designer - Tools"; keywords = [ "colordesignertools" ]; url = "https://colordesigner.io/tools"; }
            { name = "palettte"; keywords = [ "palettte" ]; url = "https://palettte.app"; }
            { name = "figma"; keywords = [ "figma" ]; url = "https://www.figma.com"; }
            { name = "Nord Theme - Colors and Palettes"; keywords = [ "nord" ]; url = "https://www.nordtheme.com/docs/colors-and-palettes"; }
          ];
        }

        {
          name = "ZMK";
          children = [
            { name = "ZMK Firmware Doc"; keywords = [ "zmk" ]; url = "https://zmk.dev/docs"; }
          ];
        }
      ];
    }

    {
      name = "entertainment";
      children = [

        {
          name = "games";
          children = [

            {
              name = "ファイナルファンタジーXIV";
              children = [
                { name = "The Lodestone"; keywords = [ "lodestone" ]; url = "https://jp.finalfantasyxiv.com/lodestone/"; }
                { name = "FF14攻略 - Game8"; keywords = [ "game8ff14" ]; url = "https://game8.jp/ff14"; }
                { name = "コニーのタルト"; keywords = [ "connie" ]; url = "https://connietarte.com/"; }
                { name = "ff14logs"; keywords = [ "fflogs" ]; url = "https://ja.fflogs.com"; }
                { name = "xivanalysis"; keywords = [ "xivanalysis" ]; url = "https://xivanalysis.com"; }
                { name = "MIRAPRISNAP"; keywords = [ "miraprisnap" ]; url = "https://mirapri.com/"; }
                { name = "HOUSING SNAP"; keywords = [ "housingsnap" ]; url = "https://housingsnap.com/"; }
                { name = "光之收藏家"; keywords = [ "ffxivsc" ]; url = "https://www.ffxivsc.cn/#/glamourList?job=0-0&race=0&sex=0&sort=0&time=0"; }
              ];
            }
          ];
        }

        { name = "keybr.com"; keywords = [ "keybr" ]; url = "https://www.keybr.com"; }
        { name = "monkeytype"; keywords = [ "monkeytype" ]; url = "https://monkeytype.com/"; }
      ];
    }

    {
      name = "shopping";
      children = [
        { name = "Amazon JP"; keywords = [ "amazonjp" ]; url = "https://www.amazon.co.jp/"; }
        { name = "樂郵集運"; keywords = [ "lotpost" ]; url = "https://www.lotpost.com/"; }
      ];
    }

    {
      name = "banking";
      children = [
        {
          name = "恆生e-banking";
          keywords = [ "hangseng" "hangsang" ];
          url = "https://e-banking.hangseng.com/";
          pinned = true;
        }
        {
          name = "Aeon";
          keywords = [ "aeon" ];
          url = "https://www.aeon.com.hk/netmember/message.do";
          pinned = true;
        }
        {
          name = "PPS";
          keywords = [ "pps" ];
          url = "https://www.ppshk.com/pps/pps2/revamp2/template/pc/login_c.jsp";
          pinned = true;
        }
      ];
    }

    {
      name = "social";
      children = [
        { name = "Discord Web"; keywords = [ "discord" ]; url = "https://discord.com/channels/@me"; }
        { name = "Microsoft Teams"; keywords = [ "teams" ]; url = "https://teams.microsoft.com"; }
        { name = "Proton Mail"; keywords = [ "protonmail" ]; url = "https://mail.proton.me"; pinned = true; }
      ];
    }

    {
      name = "scm services";
      children = [
        { name = "Codeberg"; keywords = [ "codeberg" ]; url = "https://codeberg.org/"; }
        { name = "gitlab"; keywords = [ "gitlab" ]; url = "https://gitlab.com/"; pinned = true; }
        { name = "GitHub"; keywords = [ "github" ]; url = "https://github.com/"; pinned = true; }
        { name = "selfhost gitlab"; keywords = [ "gitlabself" ]; url = "https://gitlab.samuelsung1998.net/"; }
      ];
    }
  ];
}
