{ flake-parts-lib, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.homeManagerModules.web-zen = importApply
    ./zen.nix
    { inherit moduleWithSystem; };
}
