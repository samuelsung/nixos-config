localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ lib, config, osConfig, ... }:

let
  inherit (lib) mkEnableOption mkDefault mkIf mkMerge mkOption types;
  inherit (types) package;

  cfg = config.programs.zen-browser;
in
{
  options.programs.zen-browser = {
    enable = mkEnableOption "zen browser";

    package = mkOption {
      description = "package for zen browser";
      type = package;
      inherit (inputs'.zen-browser-flake.packages) default;
    };
  };


  config = mkMerge [
    { programs.zen-browser.enable = mkDefault (config.desktop.enable && config.web.enable); }

    (mkIf cfg.enable {
      home.packages = [
        cfg.package
      ];

      home.persistence."/persist/${config.home.homeDirectory}" = mkIf osConfig.impermanence.enable {
        directories = [
          ".zen"
        ];
        allowOther = true;
      };
    })
  ];
})
