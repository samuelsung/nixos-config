{ lib, samuelsung-lib, ... }:

{
  _module.args.samuelsung-lib = {
    misc = import ./misc.nix;
    tree = import ./tree.nix { inherit lib samuelsung-lib; };
  };
  flake.homeManagerModules.samuelsung-lib._module.args = {
    inherit samuelsung-lib;
  };

  flake.nixosModules.samuelsung-lib._module.args = {
    inherit samuelsung-lib;
  };
}
