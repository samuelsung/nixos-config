{ lib, samuelsung-lib, ... }:

let
  inherit (lib) flip filter findFirst head partition;
  inherit (builtins) length tail;
  inherit (samuelsung-lib.tree) groupBy'' listToTree;
in
{
  # (V => V => bool) => V[] => V[][]
  groupBy'' = sameGroup: list:
    if list == [ ] then [ ]
    else
      let
        sameWithFirst = partition (sameGroup (head list)) list;
        inherit (sameWithFirst) right wrong;
      in
      [ right ] ++ (groupBy'' sameGroup wrong);

  # Tree<K, V> = {
  #   key: K;
  #   value: V | null;
  #   children: Tree<K, V>[];
  # }
  # (V => K[]) => (K => K => bool) => V[] => Tree<K, V>[]
  listToTree = pathOf: keyEq: _list:
    let
      list = filter (v: (pathOf v) != [ ]) _list;
      firstKeyOf = v: head (pathOf v);
      sameFirstLevel = a: b: keyEq (head (pathOf a)) (head (pathOf b));
      grouped = groupBy'' sameFirstLevel list;
    in
    flip map grouped (vs:
      let
        v = head vs; # vs is non empty
        isTopLevel = v: length (pathOf v) == 1;
        childrenPathOf = v: tail (pathOf v);
        inherit (partition isTopLevel vs) right wrong;
      in
      {
        key = firstKeyOf v;
        value = findFirst (_: true) null right;
        children = listToTree childrenPathOf keyEq wrong;
      });
}
