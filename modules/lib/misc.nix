{
  min = a: b: if a < b then a else b;
  mod = a: b: a - (b * (a / b));
}
