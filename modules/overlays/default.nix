{ inputs, ... }:

let
  packageOverlays = name: source: _: prev: {
    ${name} = import source {
      inherit (prev) system config;
    };
  };
in

{
  flake.nixosModules.overlays.nixpkgs.overlays = [
    (packageOverlays "master" inputs.nixpkgs-master)
    (packageOverlays "unstable" inputs.nixpkgs-unstable)
    inputs.misc.overlays.default
    inputs.nur.overlays.default
  ];

  flake.homeManagerModules.overlays.nixpkgs.overlays = [
    (packageOverlays "master" inputs.nixpkgs-master)
    (packageOverlays "unstable" inputs.nixpkgs-unstable)
    inputs.misc.overlays.default
    inputs.nur.overlays.default
  ];
}
