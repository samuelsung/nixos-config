{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  environment.etc = {
    "wireplumber/main.lua.d/51-ifi-zen-dac-device-rename.lua".text =
      mkIf config.services.pipewire.wireplumber.enable ''
        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_output.usb-iFi__by_AMR__iFi__by_AMR__HD_USB_Audio_0003-00.analog-stereo"
          }}},
          apply_properties = {
            ["node.description"] = "iFi Zen DAC",
          },
        })
      '';
  };
}
