{ config
, lib
, pkgs
, modulesPath
, ...
}:

let
  inherit (lib)
    mkDefault
    mkForce;
in
{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  system.build.linode = import ../builders/make-nix-only-disk-image.nix {
    inherit lib config pkgs;
    partitionTableType = "none";
    format = "raw";
    additionalSpace = "64M";
    postVM = ''
      ${pkgs.pigz}/bin/pigz -9 $out/nixos.img
    '';
  };

  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "defaults" "size=2G" "mode=755" ]; # NOTICE(samuelsung): has to set mode or some software such as sshd will not be happy about it.
  };

  fileSystems."/nix" = {
    device = "/dev/disk/by-label/nixos";
    fsType = "ext4";
    autoResize = true;
  };

  # Enable LISH
  boot = {
    # Add kernel modules detected by nixos-generate-config:
    initrd.availableKernelModules = [
      "virtio_pci"
      "virtio_scsi"
      "ahci"
      "sd_mod"
    ];

    growPartition = true;

    # Set up LISH serial connection:
    kernelParams = [ "console=ttyS0,19200n8" ];

    loader = {
      # Increase timeout to allow LISH connection:
      timeout = mkForce 10;

      grub = {
        enable = true;
        forceInstall = true;
        mirroredBoots = [{
          devices = [ "nodev" ];
          path = "/nix/boot";
        }];
        fsIdentifier = "label";

        # Allow serial connection for GRUB to be able to use LISH:
        extraConfig = ''
          serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
          terminal_input serial;
          terminal_output serial
        '';

        # Link /nix/boot/grub2 to /nix/boot/grub:
        extraInstallCommands = ''
          ${pkgs.coreutils}/bin/ln -fs /nix/boot/grub /nix/boot/grub2
        '';

        # Remove GRUB splash image:
        splashImage = null;
      };
    };
  };

  # Hardware option detected by nixos-generate-config:
  hardware.cpu.amd.updateMicrocode = mkDefault config.hardware.enableRedistributableFirmware;

  # Install diagnostic tools for Linode support:
  environment.systemPackages = with pkgs; [
    inetutils
    mtr
    sysstat
    linode-cli
  ];

  networking = {
    enableIPv6 = true;
    tempAddresses = "disabled";
    useDHCP = true;
    usePredictableInterfaceNames = false;
    interfaces.eth0 = {
      tempAddress = "disabled";
      useDHCP = true;
    };
  };

  services.qemuGuest.enable = true;
}
