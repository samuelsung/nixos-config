{ inputs }:

{ config, lib, modulesPath, ... }:

let
  inherit (lib) mkIf;
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    inputs.nixos-hardware.nixosModules.common-cpu-amd-pstate
    inputs.nixos-hardware.nixosModules.common-cpu-amd
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.nixos-hardware.nixosModules.common-pc
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.enable = true;
  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "ahci"
    "nvme"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [
    "dm-snapshot"
    "vfio"
    "vfio_iommu_type1"
    "vfio_pci"
  ];

  boot.kernelModules = [ "kvm-amd" ];

  environment.etc = {
    "wireplumber/main.lua.d/51-tr3960x-device.lua".text =
      mkIf config.services.pipewire.wireplumber.enable ''
        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_output.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_2__sink"
          }}},
          apply_properties = {
            ["node.description"] = "On-Board S/PDIF Output",
          },
        })

        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_output.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_1__sink"
          }}},
          apply_properties = {
            ["node.description"] = "On-Board Front Headphones",
          },
        })

        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_output.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT__sink"
          }}},
          apply_properties = {
            ["node.description"] = "On-Board Speakers",
          },
        })

        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_input.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_2__source"
          }}},
          apply_properties = {
            ["node.description"] = "On-Board Front Mic",
          },
        })

        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_input.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_1__source"
          }}},
          apply_properties = {
            ["node.description"] = "On-Board Mic",
          },
        })

        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_input.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_0__source"
          }}},
          apply_properties = {
            ["node.description"] = "On-Board Line-In",
          },
        })
      '';
  };
}
