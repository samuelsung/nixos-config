{ config, lib, pkgs, modulesPath, ... }:

let
  inherit (lib) mkDefault mkForce;
in
{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  system.build.qcow = import ../builders/make-nix-only-disk-image.nix {
    inherit lib config pkgs;
    partitionTableType = "none";
    format = "qcow2";
    additionalSpace = "64M";
  };

  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "defaults" "size=2G" "mode=755" ]; # NOTICE(samuelsung): has to set mode or some software such as sshd will not be happy about it.
  };

  fileSystems."/nix" = {
    device = "/dev/disk/by-label/nixos";
    fsType = "ext4";
    autoResize = true;
  };

  boot = {
    initrd.enable = true;
    initrd.availableKernelModules = [
      "ata_piix"
      "uhci_hcd"
      "ahci"
      "xhci_pci"
      "virtio_pci"
      "sr_mod"
      "virtio_blk"
      "iso9660"
    ];

    growPartition = true;

    kernelParams = [ "console=ttyS0" ];

    loader = {
      timeout = mkForce 10;

      grub = {
        enable = true;
        forceInstall = true;
        mirroredBoots = [{
          devices = [ "/dev/vda" ];
          path = "/nix/boot";
        }];
        fsIdentifier = "label";

        # Allow serial connection for GRUB
        extraConfig = ''
          serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
          terminal_input serial;
          terminal_output serial
        '';
      };
    };
  };

  networking.useDHCP = mkDefault true;

  services.qemuGuest.enable = true;
}
