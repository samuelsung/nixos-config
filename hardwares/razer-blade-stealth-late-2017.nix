{ inputs }:

{ modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    inputs.nixos-hardware.nixosModules.common-cpu-intel
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.enable = true;
  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "nvme"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];

  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  boot.kernelParams = [
    # Fix screen flicking issue
    # SOURCE(samuelsung): https://github.com/rolandguelle/razer-blade-stealth-linux/blob/master/other-hardware-models.md
    "intel_idle.max_cstate=4"

    # Fix suspend loop
    # SOURCE(samuelsung): https://github.com/rolandguelle/razer-blade-stealth-linux/blob/master/ubuntu-18-04.md#12-suspend-loop
    "button.lid_init_state=open"
  ];
}
