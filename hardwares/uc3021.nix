{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  environment.etc = {
    "wireplumber/main.lua.d/51-uc3021-rename.lua".text =
      mkIf config.services.pipewire.wireplumber.enable ''
        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_input.usb-ATEN_UC3021_20000130041415-02.analog-stereo"
          }}},
          apply_properties = {
            ["node.description"] = "UC3021 Stereo Input",
          },
        })
      '';
  };
}
