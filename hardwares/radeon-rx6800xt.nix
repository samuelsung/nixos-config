{ inputs }:

{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  imports = [
    inputs.nixos-hardware.nixosModules.common-gpu-amd
  ];

  environment.etc = {
    "wireplumber/main.lua.d/51-radeon-rx6800xt-rename.lua".text =
      mkIf config.services.pipewire.wireplumber.enable ''
        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_output.pci-0000_03_00.1.hdmi-stereo-extra2"
          }}},
          apply_properties = {
            ["node.description"] = "Radeon RX 6800XT HDMI/DP Audio",
          },
        })
      '';
  };
}
