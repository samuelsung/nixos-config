{ flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  flake.nixosModules = {
    hardware-b525 = ./b525.nix;
    hardware-ifi-zen-dac = ./ifi-zen-dac.nix;
    hardware-iso = ./iso.nix;
    hardware-linode = ./linode.nix;
    hardware-qcow = ./qcow.nix;
    hardware-radeon-rx6800xt = importApply ./radeon-rx6800xt.nix { inherit inputs; };
    hardware-razer-blade-stealth-late-2017 = importApply ./razer-blade-stealth-late-2017.nix { inherit inputs; };
    hardware-scarlett-4i4-3rd-gen = ./scarlett-4i4-3rd-gen.nix;
    hardware-tr3960x = importApply ./tr3960x.nix { inherit inputs; };
    hardware-uc3021 = ./uc3021.nix;
    hardware-vm = ./vm.nix;
    hardware-vultr = ./vultr.nix;
    hardware-xps-15-9500-nvidia = importApply ./xps-15-9500-nvidia.nix { inherit inputs; };
  };
}
