{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  environment.etc = {
    "wireplumber/main.lua.d/51-b525-rename.lua".text =
      mkIf config.services.pipewire.wireplumber.enable ''
        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_input.usb-046d_B525_HD_Webcam_CD357410-00.mono-fallback"
          }}},
          apply_properties = {
            ["node.description"] = "B525 HD Mic mono",
          },
        })
      '';
  };
}
