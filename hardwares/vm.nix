{ lib, modulesPath, ... }:

let
  inherit (lib) mkForce;
in
{
  imports = [
    (modulesPath + "/virtualisation/qemu-vm.nix")
  ];

  security.sudo.wheelNeedsPassword = mkForce false;
  services.getty.autologinUser = "samuelsung";

  users.users.root = {
    initialHashedPassword = mkForce "";
    hashedPassword = mkForce null;
    password = mkForce null;
    hashedPasswordFile = mkForce null;
    initialPassword = mkForce null;
  };


  users.presets.samuelsung.enable = true;
  users.users.samuelsung = {
    initialHashedPassword = mkForce "";
    hashedPassword = mkForce null;
    password = mkForce null;
    hashedPasswordFile = mkForce null;
    initialPassword = mkForce null;
  };
  users.presets.samuelsung_play.enable = true;
  users.users.samuelsung_play = {
    initialHashedPassword = mkForce "";
    hashedPassword = mkForce null;
    password = mkForce null;
    hashedPasswordFile = mkForce null;
    initialPassword = mkForce null;
  };

  virtualisation.cores = 4;
  virtualisation.memorySize = 8192;
}
