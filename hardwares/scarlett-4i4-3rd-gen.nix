{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  environment.etc = {
    "wireplumber/main.lua.d/51-scarlett-4i4-3rd-gen-rename.lua".text =
      mkIf config.services.pipewire.wireplumber.enable ''
        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_output.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-40"
          }}},
          apply_properties = {
            ["node.description"] = "Scarlett 4i4 4.0",
          },
        })

        table.insert(alsa_monitor.rules, {
          matches = {{{
            "node.name",
            "equals",
            "alsa_input.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-21"
          }}},
          apply_properties = {
            ["node.description"] = "Scarlett 4i4 2.1 Input",
          },
        })
      '';
  };

  services.pipewire.extraConfig.pipewire."91-scarlett-4i4-3rd-gen-inst" = {
    context.modules = [
      {
        name = "libpipewire-module-loopback";
        args = {
          node.description = "Scarlett 4i4 INST";
          capture.props = {
            node.name = "capture.scarlett_4i4_inst";
            audio.position = [ "FL" ];
            stream.dont-remix = true;
            target.object = "alsa_input.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-21";
            node.passive = true;
          };
          playback.props = {
            node.name = "scarlett_4i4_inst";
            media.class = "Audio/Source";
            audio.position = [ "MONO" ];
          };
        };
      }
      {
        name = "libpipewire-module-loopback";
        args = {
          node.description = "Scarlett 4i4 Mic";
          capture.props = {
            node.name = "capture.4i4_mic";
            audio.position = [ "FR" ];
            stream.dont-remix = true;
            target.object = "alsa_input.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-21";
            node.passive = "true";
          };
          playback.props = {
            node.name = "scarlett_4i4_mic";
            media.class = "Audio/Source";
            audio.position = [ "MONO" ];
          };
        };
      }
    ];
  };
}
