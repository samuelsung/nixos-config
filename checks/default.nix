{ config, lib, ... }:

let
  inherit (lib) concatMapAttrs;
in
{
  perSystem = { self', ... }: {
    checks = self'.packages
      // (concatMapAttrs
      (name: nixosConfiguration: {
        "nixosConfigurations-${name}" =
          nixosConfiguration.config.system.build.toplevel;
      })
      config.flake.nixosConfigurations);
  };
}
