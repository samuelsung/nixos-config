{ config, lib, ... }:

let
  inherit (lib)
    attrValues
    concatMap
    concatMapAttrs
    concatStringsSep
    lessThan
    sort
    unique;
in
{
  perSystem = { pkgs, ... }:
    let
      sortedUnique = xs: sort lessThan (unique xs);

      packagesInConfig = cfg:
        let
          hmPackages = concatMap
            ({ home, ... }: home.packages)
            (attrValues (cfg.home-manager.users or { }));
        in
        cfg.environment.systemPackages ++ hmPackages;

      mkPackageList = packages: pkgs.writeText "package-list" (
        concatStringsSep "\n" (sortedUnique (map (p: p.name) packages))
      );
    in
    {
      apps = concatMapAttrs
        (name: nixosConfiguration: {
          "${name}-installed-packages".program = pkgs.writeScriptBin
            "${name}-installed-packages"
            "cat ${mkPackageList (packagesInConfig nixosConfiguration.config)}";
        })
        (config.flake.nixosConfigurations or { });
    };
}
