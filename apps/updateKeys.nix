{
  perSystem = { pkgs, ... }:
    {
      apps.update-keys.program = pkgs.writeScriptBin "update-keys" ''
        #!${pkgs.runtimeShell}

        ${pkgs.gnused}/bin/sed '/path_regex/!d;s/.*path_regex: //g' .sops.yaml | while read -r p; do {
          ${pkgs.findutils}/bin/find . | ${pkgs.gnugrep}/bin/grep "$p" | while read -r f; do {
            ${pkgs.sops}/bin/sops updatekeys -y "$f"
          }; done;
        }; done;
      '';
    };
}
