{ config, flake-parts-lib, inputs, moduleWithSystem, ... }:

let
  inherit (flake-parts-lib) importApply;
  inherit (inputs.nixpkgs.lib) nixosSystem;
in
{
  flake.nixosConfigurations.tr3960x = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./3960x { inherit config; }) ];
  };

  flake.nixosConfigurations.blade-stealth = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./blade-stealth { inherit config; }) ];
  };

  flake.nixosConfigurations.xps-15-9500 = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./xps-15-9500 { inherit config moduleWithSystem; }) ];
  };

  flake.nixosConfigurations.hephaistos = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./hephaistos { inherit config; }) ];
  };

  flake.nixosConfigurations.minimal-iso = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./minimal-iso { inherit config; }) ];
  };

  flake.packages.x86_64-linux.minimal-iso =
    config.flake.nixosConfigurations.minimal-iso.config.system.build.isoImage;

  flake.nixosConfigurations.desktop-iso = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./desktop-iso { inherit config; }) ];
  };

  flake.packages.x86_64-linux.desktop-iso =
    config.flake.nixosConfigurations.desktop-iso.config.system.build.isoImage;

  flake.nixosConfigurations.gpg-iso = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./gpg-iso { inherit config; }) ];
  };

  flake.packages.x86_64-linux.gpg-iso =
    config.flake.nixosConfigurations.gpg-iso.config.system.build.isoImage;

  flake.nixosConfigurations.minimal-vm = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./minimal-vm { inherit config; }) ];
  };

  flake.packages.x86_64-linux.minimal-vm =
    config.flake.nixosConfigurations.minimal-vm.config.system.build.vm;

  flake.nixosConfigurations.server-vm = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./server-vm { inherit config; }) ];
  };

  flake.packages.x86_64-linux.server-vm =
    config.flake.nixosConfigurations.server-vm.config.system.build.vm;

  flake.nixosConfigurations.desktop-vm = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./desktop-vm { inherit config; }) ];
  };

  flake.packages.x86_64-linux.desktop-vm =
    config.flake.nixosConfigurations.desktop-vm.config.system.build.vm;

  flake.nixosModules.iso-base = importApply ./iso-base { inherit moduleWithSystem; };

  flake.nixosModules.linode-base = importApply ./linode-base { inherit config; };

  flake.nixosModules.qcow-base = importApply ./qcow-base { inherit config; };

  flake.nixosConfigurations.sankureddo = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./sankureddo { inherit config; }) ];
  };

  flake.packages.x86_64-linux.sankureddo =
    config.flake.nixosConfigurations.sankureddo.config.system.build.qcow;

  flake.nixosConfigurations.paparimo = nixosSystem {
    system = "x86_64-linux";
    modules = [ (importApply ./paparimo { inherit config; }) ];
  };

  flake.packages.x86_64-linux.paparimo =
    config.flake.nixosConfigurations.paparimo.config.system.build.linode;
}
