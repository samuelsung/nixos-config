localFlake:

{ config, lib, pkgs, ... }:

let
  inherit (lib) mkForce;
in
{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.iso-base
    localFlake.config.flake.nixosModules.hardware-iso
  ];
  networking.hostName = "gpg-iso";
  system.stateVersion = lib.trivial.release;

  services.openssh.enable = true;
  programs.gnupg.agent.enable = true;

  desktop.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  services.pipewire.enable = true;

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.nix.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.nix.enable = true;
    utils.managements.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };

  users.presets.samuelsung.enable = true;

  # security settings
  boot.kernelParams = [ "copytoram" ]; # ensure nothing is written to the usb stick
  # disable networking
  boot.blacklistedKernelModules = [ "af_packet" ]; # disable network setting
  boot.initrd.network.enable = false;
  networking.dhcpcd.enable = false;
  networking.dhcpcd.allowInterfaces = [ ];
  networking.interfaces = { };
  networking.firewall.enable = true;
  networking.useDHCP = false;
  networking.useNetworkd = false;
  networking.wireless.enable = false;
  hardware.bluetooth.enable = mkForce false;
  networking.networkmanager.enable = mkForce false;

  boot.tmp.cleanOnBoot = true;
  boot.kernel.sysctl = { "kernel.unprivileged_bpf_disabled" = 1; };

  services.qemuGuest.enable = true;

  environment.systemPackages = with pkgs; [
    # Testing
    ent

    # Password generation tools
    diceware
    pwgen

    # Miscellaneous tools
    cfssl
    pcsctools
  ];

  environment.interactiveShellInit = ''
    unset HISTFILE
  '';
}
