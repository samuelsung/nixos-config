localFlake:

{ config, pkgs, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-razer-blade-stealth-late-2017
    localFlake.config.flake.nixosModules.secrets-common-host
    localFlake.config.flake.nixosModules.secrets-common-interactive-host
  ];

  networking.hostName = "blade-stealth";
  system.stateVersion = "21.05";

  hardware.bluetooth.enable = true;
  networking.firewall.allowedTCPPorts = [ 3000 ];
  networking.networkmanager.enable = true;
  services.avahi.allowInterfaces = [ "wifi0" ];
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  programs.gnupg.agent.enable = true;
  secrets.enable = true;

  desktop.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  programs.hyprland.enable = true;
  services.wayland.enable = true;
  services.pipewire.enable = true;
  webcam.enable = true;

  virtualisation.docker.enable = true;
  virtualisation.libvirtd.enable = true;

  home-manager.users.samuelsung = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;
    };

    programs.notes-samuelsung.enable = true;

    repos.hyprland.enable = true;
    repos.misc.enable = true;
    repos.nix.enable = true;
    repos.personal.enable = true;
    repos.react.enable = true;
    repos.vim.enable = true;
    repos.zmk.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };
  home-manager.users.samuelsung_play = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;
    };

    programs.notes-samuelsung.enable = true;

    repos.entertainment.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.games.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };

  users.presets.samuelsung.enable = true;

  users.presets.samuelsung_play.enable = true;

  programs.light.enable = true;
  services.clight.enable = true;

  networking.wireguard.enable = true;

  services.samba.enable = true;

  networking.fix-interfaces.wifi0 = "26:a5:af:3c:4e:31";

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.blacklistedKernelModules = [ "pcspkr" ];

  system.fsPackages = with pkgs; [ sshfs ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/8f6d99ee-fc37-49a9-8dc2-66f50750b328";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/D2AD-D135";
    fsType = "vfat";
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/48fb62c7-d40f-4ff2-a33f-0a45e3d6d8e3"; }
  ];
}
