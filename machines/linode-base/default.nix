localFlake:

{ lib, ... }:

let
  inherit (lib) mkForce;
in
{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-linode
    localFlake.config.flake.nixosModules.secrets-common-host
    localFlake.config.flake.nixosModules.secrets-common-remote-host
  ];

  impermanence.enable = true;
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  secrets.enable = true;

  sops.age.sshKeyPaths = mkForce [ ];
  sops.gnupg.sshKeyPaths = mkForce [ ];
  sops.age.keyFile = "/age/key.txt";

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      # treesitter runs about 600M uncompressed cause it needs gcc and nodejs
      plugins.treesitter.enable = mkForce false;
      languages.markdown.plugins.enable = mkForce false;
      plugins.neorg.enable = mkForce false; # it requires treesitter to run
    };
    programs.neovim-flake.page = {
      plugins.treesitter.enable = mkForce false;
      plugins.neorg.enable = mkForce false;
    };
    utils.basics.enable = true;
  };

  users.presets.samuelsung.enable = true;

  fileSystems."/age" = {
    device = "/dev/sdc";
    fsType = "ext4";
    options = [ "ro" ];
    neededForBoot = true;
  };

  fileSystems."/persist" = {
    device = "/dev/sdd";
    fsType = "ext4";
    neededForBoot = true;
  };

  fileSystems."/persist/var/log" = {
    device = "/dev/sde";
    fsType = "ext4";
    neededForBoot = true;
  };

  fileSystems."/persist/var/lib/nixos" = {
    device = "/dev/sdf";
    fsType = "ext4";
    neededForBoot = true;
  };

  swapDevices = [{ device = "/dev/sdb"; }];
}
