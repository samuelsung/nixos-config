localFlake:

{ config, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-vultr
    localFlake.config.flake.nixosModules.secrets-common-host
    localFlake.config.flake.nixosModules.secrets-common-remote-host
  ];

  networking.hostName = "hephaistos";
  system.stateVersion = "22.11";
  time.timeZone = "Asia/Tokyo";

  impermanence.enable = true;
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  secrets.enable = true;

  home-manager.users.samuelsung = {
    utils.basics.enable = true;
  };

  users.presets.samuelsung.enable = true;

  services.minecraft-server.enable = true;
  services.minecraft-server.jvmOpts = "-Xmx1536M -Xms1536M";

  boot.blacklistedKernelModules = [ "pcspkr" ];

  # Disk /dev/vda: 60 GiB, 64424509440 bytes, 125829120 sectors
  # Units: sectors of 1 * 512 = 512 bytes
  # Sector size (logical/physical): 512 bytes / 512 bytes
  # I/O size (minimum/optimal): 512 bytes / 512 bytes
  # Disklabel type: gpt
  # Disk identifier: 8EC9273D-175A-2446-8E0F-86295F527175
  #
  # Device        Start       End   Sectors  Size Type             Label
  # /dev/vda1      2048      4095      2048    1M BIOS boot
  # /dev/vda2      4096   1052671   1048576  512M EFI System       BOOT
  # /dev/vda3   1052672  17829887  16777216    8G Linux swap       SWAP
  # /dev/vda4  17829888 125827071 107997184 51.5G Linux filesystem

  # sudo zpool create \
  #  -o ashift=12 \
  #  -o autotrim=off \ # use services.zfs.trim.enable = true
  #  -R /mnt \
  #  -O canmount=off \
  #  -O mountpoint=none \
  #  -O acltype=posixacl \
  #  -O compression=zstd \
  #  -O dnodesize=auto \
  #  -O normalization=formD \
  #  -O relatime=on \ # do not update access time if just read the file
  #  -O xattr=sa \
  #  rpool \
  #  /dev/vda4

  # NOTE(samuelsung): To prevent run out of disk space
  # sudo zfs create -o refreservation=1G -o mountpoint=none rpool/reserved

  # NOTE(samuelsung): Create a file system:
  # NOTE(samuelsung): setting the canmount=off mountpoint=/ will let all of its descendent to inherit the perent mountpoint without actually mounting the perent.
  # SEE(samuelsung): https://docs.oracle.com/cd/E19120-01/open.solaris/817-2271/gdrcf/index.html
  # In this case, rpool/nixos will not be mounted but rpool/nixos/nix will be mounted to /nix
  # sudo zfs create -o canmount=off -o mountpoint=/ rpool/nixos
  # sudo zfs create -o canmount=on rpool/nixos/nix
  # sudo zfs create -o canmount=on rpool/nixos/persist

  networking.hostId = "a56cac5d";

  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.devNodes = "/dev/";

  # NOTE(samuelsung): Using NixOS on a ZFS root file system might result in the boot error external pointer tables not supported when the number of hardlinks in the nix store gets very high. This can be avoided by adding this option to your configuration.nix file:
  boot.loader.grub.copyKernels = true;

  services.udev.extraRules = ''
    ACTION=="add|change", KERNEL=="sd[a-z]*[0-9]*|mmcblk[0-9]*p[0-9]*|nvme[0-9]*n[0-9]*p[0-9]*", ENV{ID_FS_TYPE}=="zfs_member", ATTR{../queue/scheduler}="none"
  ''; # zfs already has its own scheduler. without this the computer will freeze for a second when i nix build something.

  # ZFS maintenance settings.
  services.zfs.trim.enable = true;
  services.zfs.autoScrub.enable = true;
  services.zfs.autoScrub.pools = [ "rpool" ];

  # Set Max ARC size => 2GB == 2147483648 Bytes
  # Set Min ARC size => 1GB == 1073741824 Bytes
  boot.extraModprobeConfig = ''
    options zfs zfs_arc_max=2147483648
    options zfs zfs_arc_min=1073741824
  '';

  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "defaults" "size=2G" "mode=755" ]; # NOTICE(samuelsung): has to set mode or some software such as sshd will not be happy about it.
  };

  fileSystems."/nix" = {
    device = "rpool/nixos/nix";
    fsType = "zfs";
    options = [ "zfsutil" ];
  };

  fileSystems."/persist" = {
    device = "rpool/nixos/persist";
    fsType = "zfs";
    options = [ "zfsutil" ];
    neededForBoot = true;
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/BOOT";
    fsType = "vfat";
    options = [ "X-mount.mkdir" ];
  };

  swapDevices = [
    { device = "/dev/disk/by-partuuid/d97c6f4c-972c-ed49-aae3-ad31d9a4f2b1"; randomEncryption = true; } # SWAP
  ];

  networking.fix-interfaces = {
    lan0 = "56:00:04:42:a3:e7";
  };

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future.
  networking.useDHCP = false;
  networking.interfaces.lan0.useDHCP = true;
}
