localFlake:

{ lib, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-vm
  ];
  networking.hostName = "minimal-vm";
  system.stateVersion = lib.trivial.release;

  networking.networkmanager.enable = true;
  services.openssh.enable = true;
  programs.gnupg.agent.enable = true;

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.nix.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.developments.nix.enable = true;
  };
  home-manager.users.samuelsung_play = {
    programs.neovim-flake.neovim = {
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.nix.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.developments.nix.enable = true;
  };
}
