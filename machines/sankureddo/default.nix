localFlake:


{ config, lib, ... }:

let
  inherit (lib) mkOverride;
in
{
  imports = [
    localFlake.config.flake.nixosModules.secrets-common-interactive-host
    localFlake.config.flake.nixosModules.qcow-base
  ];
  networking.hostName = "sankureddo";
  system.stateVersion = "23.11";

  boot.kernelModules = [ "kvm-amd" ];

  services.openssh.settings = {
    AllowTcpForwarding = true;
    StreamLocalBindUnlink = true;
    AllowAgentForwarding = true;
  };
  networking.fix-interfaces.lan0 = "a2:ad:e1:85:9a:24";
  networking.interfaces.lan0.useDHCP = true;
  networking.enableIPv6 = false;

  home-manager.users.samuelsung = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;

      # revert qcow-base
      plugins.treesitter.enable = mkOverride 40 true;
      languages.markdown.plugins.enable = mkOverride 40 true;
      plugins.neorg.enable = mkOverride 40 true;
    };

    programs.notes-samuelsung.enable = true;

    repos.hyprland.enable = true;
    repos.misc.enable = true;
    repos.nix.enable = true;
    repos.personal.enable = true;
    repos.react.enable = true;
    repos.vim.enable = true;
    repos.zmk.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
  };

  services.avahi.allowInterfaces = [ "lan0" ];
}
