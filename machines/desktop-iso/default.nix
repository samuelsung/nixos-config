localFlake:

{ lib, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.iso-base
    localFlake.config.flake.nixosModules.hardware-iso
  ];
  networking.hostName = "desktop-iso";
  system.stateVersion = lib.trivial.release;

  hardware.bluetooth.enable = true;
  networking.networkmanager.enable = true;
  services.openssh.enable = true;
  programs.gnupg.agent.enable = true;

  desktop.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  programs.hyprland.enable = true;
  services.wayland.enable = true;
  services.pipewire.enable = true;

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.nix.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.nix.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    programs.chromium.enable = false;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };

  users.presets.samuelsung.enable = true;
}
