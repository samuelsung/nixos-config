localFlake:


{ config, ... }:

let
  palworldPort = 8211;
  palworldQueryPort = 27015;
in
{
  imports = [
    localFlake.config.flake.nixosModules.linode-base
  ];
  networking.hostName = "paparimo";
  system.stateVersion = "23.11";

  sops.secrets."palworld-server-docker.env" = {
    format = "binary";
    mode = "0400";
    sopsFile = ./palworld-server-docker.env.paparimo-host-secret;
    owner = config.users.users.palworld.name;
    group = config.users.groups.nogroup.name;
  };

  users.users.palworld = {
    group = "palworld";
    uid = 1010;
    isSystemUser = true;
  };

  users.groups.palworld.gid = 1010;

  virtualisation.oci-containers.containers = {
    palworld = {
      image = "thijsvanloef/palworld-server-docker";
      volumes = [
        "/persist/var/lib/palworld:/palworld/"
      ];

      ports = [
        "${toString palworldPort}:${toString palworldPort}/udp"
        "${toString palworldQueryPort}:${toString palworldQueryPort}/udp"
      ];

      environment = {
        PUID = toString config.users.users.palworld.uid;
        PGID = toString config.users.groups.palworld.gid;
        PORT = toString palworldPort;
        QUERY_PORT = toString palworldQueryPort;
        PLAYERS = "16";
        MULTITHREADING = "true";
        RCON_ENABLED = "true";
        RCON_PORT = "25575";
        TZ = "Asia/Hong_Kong";
        COMMUNITY = "false";
        UPDATE_ON_BOOT = "true";
        BACKUP_ENABLED = "true";
        DELETE_OLD_BACKUPS = "true";
        OLD_BACKUP_DAYS = "90";
        BACKUP_CRON_EXPRESSION = "0 0 * * *";

        DIFFICULTY = "Normal";
      };

      environmentFiles = [
        config.sops.secrets."palworld-server-docker.env".path
      ];
    };
  };

  networking.firewall = {
    allowedUDPPorts = [ palworldPort palworldQueryPort ];
  };

  environment.persistence."/persist" = {
    hideMounts = true;
    directories = [
      "/var/lib/containers"
      "/var/lib/palworld"
    ];
  };
}
