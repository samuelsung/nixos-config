localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ config, lib, pkgs, ... }:

let
  inherit (lib) mkForce;
in
{
  home-manager.sharedModules = [
    {
      desktop.wallpapers.package =
        let image = inputs'.wallpapers.packages.wallpapers.passthru.files.danbooru-4804170;
        in
        pkgs.runCommand "single-wallpaper" { } ''
          mkdir $out
          cp "${image}" "$out/${image.name}"
        '';
    }
  ];
})
