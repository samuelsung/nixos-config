localFlake:

{ config, lib, ... }:

let
  inherit (lib) mkForce mkIf;
in
{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-qcow
    localFlake.config.flake.nixosModules.secrets-common-host
    localFlake.config.flake.nixosModules.secrets-common-remote-host
  ];

  impermanence.enable = true;
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  secrets.enable = true;

  sops.age.sshKeyPaths = mkForce [ ];
  sops.gnupg.sshKeyPaths = mkForce [ ];
  sops.age.keyFile = "/age/key.txt";

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      # treesitter runs about 600M uncompressed cause it needs gcc and nodejs
      plugins.treesitter.enable = mkForce false;
      languages.markdown.plugins.enable = mkForce false;
      plugins.neorg.enable = mkForce false; # it requires treesitter to run
    };
    programs.neovim-flake.page = {
      plugins.treesitter.enable = mkForce false;
      plugins.neorg.enable = mkForce false;
    };
    utils.basics.enable = true;
  };

  users.presets.samuelsung.enable = true;

  fileSystems."/age" = mkIf config.secrets.enable {
    device = "/dev/disk/by-label/ISOIMAGE";
    fsType = "iso9660";
    options = [ "ro" ];
    neededForBoot = true;
  };

  fileSystems."/persist" = mkIf config.impermanence.enable {
    device = "/dev/disk/by-label/PERSIST";
    fsType = "ext4";
    neededForBoot = true;
    autoResize = true;
  };

  fileSystems."/persist/var/log" = mkIf config.impermanence.enable {
    device = "/dev/disk/by-label/PERSISTVARLOG";
    fsType = "ext4";
    neededForBoot = true;
    autoResize = true;
  };

  fileSystems."/persist/var/lib/nixos" = mkIf config.impermanence.enable {
    device = "/dev/disk/by-label/PERSISTVARLIBNIXOS";
    fsType = "ext4";
    neededForBoot = true;
    autoResize = true;
  };

  swapDevices = mkIf config.impermanence.enable
    [{ device = "/persist/swapfile"; size = 8 * 1024; }];
}
