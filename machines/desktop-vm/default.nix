localFlake:

{ lib, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-vm
  ];
  networking.hostName = "desktop-vm";
  system.stateVersion = lib.trivial.release;

  networking.networkmanager.enable = true;
  services.openssh.enable = true;
  programs.gnupg.agent.enable = true;

  desktop.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  programs.hyprland.enable = true;
  services.wayland.enable = true;
  services.pipewire.enable = true;

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };
  home-manager.users.samuelsung_play = {
    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };
}
