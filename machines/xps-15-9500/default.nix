localFlake:

localFlake.moduleWithSystem ({ inputs' }:

{ config, pkgs, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-xps-15-9500-nvidia
    localFlake.config.flake.nixosModules.secrets-common-host
    localFlake.config.flake.nixosModules.secrets-common-interactive-host
  ];
  networking.hostName = "xps-15-9500";
  system.stateVersion = "23.11";

  hardware.video.monitors = [
    { name.x = "eDP-1"; h = 1920; v = 1200; x = 0; y = 0; refreshRate.x = 60; }
  ];

  hardware.bluetooth.enable = true;
  networking.firewall.allowedTCPPorts = [ 3000 ];
  impermanence.enable = true;
  networking.networkmanager.enable = true;
  services.avahi.allowInterfaces = [ "wifi0" ];
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  programs.gnupg.agent.enable = true;
  secrets.enable = true;

  desktop.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  programs.hyprland.enable = true;
  services.wayland.enable = true;
  services.pipewire.enable = true;
  webcam.enable = true;

  virtualisation.docker.enable = true;
  virtualisation.libvirtd.enable = true;

  home-manager.users.samuelsung = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;
    };

    programs.notes-samuelsung.enable = true;

    repos.hyprland.enable = true;
    repos.misc.enable = true;
    repos.nix.enable = true;
    repos.personal.enable = true;
    repos.react.enable = true;
    repos.vim.enable = true;
    repos.zmk.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;

    desktop.wallpapers.package = inputs'.wallpapers.packages.wallpapers-sfw;
  };
  home-manager.users.samuelsung_play = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;
    };

    programs.notes-samuelsung.enable = true;

    repos.entertainment.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.games.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;

    desktop.wallpapers.package = inputs'.wallpapers.packages.wallpapers-sfw;
  };

  users.presets.samuelsung.enable = true;

  users.presets.samuelsung_play.enable = true;

  # Minimal list of modules to use the EFI system partition and the YubiKey
  boot.initrd.kernelModules = [ "vfat" "nls_cp437" "nls_iso8859-1" "usbhid" ];

  # Enable support for the YubiKey PBA
  boot.initrd.luks.yubikeySupport = true;

  # Configuration to use your Luks device
  boot.initrd.luks.devices = {
    "nixos-enc" = {
      device = "/dev/disk/by-uuid/5b84fa56-9410-421a-8564-a87dbd72f194";
      preLVM = true; # You may want to set this to false if you need to start a network service first
      yubikey = {
        slot = 1;
        twoFactor = true; # Set to false if you did not set up a user password.
        storage = {
          device = "/dev/disk/by-uuid/6176-004A";
        };
      };
    };
  };

  programs.light.enable = true;
  services.clight.enable = true;

  networking.wireguard.enable = true;

  services.samba.enable = true;

  networking.fix-interfaces.wifi0 = "04:6c:59:0b:3a:65";

  boot.blacklistedKernelModules = [ "pcspkr" ];

  system.fsPackages = with pkgs; [ sshfs ];

  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "defaults" "size=5G" "mode=755" ]; # NOTICE(samuelsung): has to set mode or some software such as sshd will not be happy about it.
  };

  fileSystems."/nix" =
    {
      device = "/dev/disk/by-uuid/a6c6bed1-a439-414d-a62f-450bdd2985d7";
      fsType = "btrfs";
      options = [ "subvol=nix" "compress=zstd" ];
    };

  fileSystems."/persist" =
    {
      device = "/dev/disk/by-uuid/a6c6bed1-a439-414d-a62f-450bdd2985d7";
      fsType = "btrfs";
      options = [ "subvol=persist" "compress=zstd" ];
      neededForBoot = true;
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/6176-004A";
      fsType = "vfat";
    };

  swapDevices =
    [{ device = "/dev/disk/by-uuid/de00e2ec-948d-477d-9d44-13a7f840b087"; }];

  powerManagement.powertop.enable = true;
  powerManagement.enable = true;
  services.thermald.enable = true;
  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

      CPU_ENERGY_PERF_POLICY_ON_BAT = "balance_power";
      CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
    };
  };

  environment.systemPackages = with pkgs; [
    powertop
  ];
})
