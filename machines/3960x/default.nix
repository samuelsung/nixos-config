localFlake:

{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;
in
{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-b525
    localFlake.config.flake.nixosModules.hardware-ifi-zen-dac
    localFlake.config.flake.nixosModules.hardware-radeon-rx6800xt
    localFlake.config.flake.nixosModules.hardware-scarlett-4i4-3rd-gen
    localFlake.config.flake.nixosModules.hardware-tr3960x
    localFlake.config.flake.nixosModules.hardware-uc3021
    localFlake.config.flake.nixosModules.secrets-common-host
    localFlake.config.flake.nixosModules.secrets-common-interactive-host
  ];

  networking.hostName = "tr3960x";
  system.stateVersion = "23.11";

  hardware.video.monitors = [
    { name.x = "DP-3"; h = 1920; v = 1080; x = 0; y = 60; refreshRate.x = 60; }
    {
      name.x = "HDMI-1";
      name.w = "HDMI-A-1";
      h = 2560;
      v = 1440;
      x = 1920;
      y = 0;
      refreshRate.x = 120;
      refreshRate.w = 144;
      primary = true;
    }
    { name.x = "DP-2"; h = 1920; v = 1080; x = 4480; y = 105; refreshRate.x = 60; }
    { name.x = "DP-1"; h = 1920; v = 1080; x = 6400; y = 250; refreshRate.x = 60; rotation = 90; }
  ];

  hardware.bluetooth.enable = true;
  impermanence.enable = true;
  networking.firewall.allowedTCPPorts = [ 3000 ];
  networking.networkmanager.enable = true;
  services.avahi.allowInterfaces = [ "lan0" ];
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  programs.gnupg.agent.enable = true;
  secrets.enable = true;

  desktop.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  programs.hyprland.enable = true;
  services.wayland.enable = true;
  services.pipewire.enable = true;
  webcam.enable = true;
  bluraySupport.enable = true;

  virtualisation.docker.enable = true;
  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.iscsi.enable = true;
  virtualisation.libvirtd.looking-glass.enable = true;

  home-manager.users.samuelsung = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;
    };

    programs.notes-samuelsung.enable = true;

    repos.hyprland.enable = true;
    repos.misc.enable = true;
    repos.nix.enable = true;
    repos.personal.enable = true;
    repos.react.enable = true;
    repos.vim.enable = true;
    repos.zmk.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };
  home-manager.users.samuelsung_play = {
    imports = [ localFlake.config.flake.homeManagerModules.secrets-common-interactive-samuelsung ];

    programs.neovim-flake.neovim = {
      modules.debugger.enable = true;
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.clang.plugins.enable = true;
      languages.css.plugins.enable = true;
      languages.docker.plugins.enable = true;
      languages.html.plugins.enable = true;
      languages.java.plugins.enable = true;
      languages.js.plugins.enable = true;
      languages.json.plugins.enable = true;
      languages.kotlin.plugins.enable = true;
      languages.latex.plugins.enable = true;
      languages.markdown.plugins.enable = true;
      languages.nix.plugins.enable = true;
      languages.ocaml.plugins.enable = true;
      languages.python.plugins.enable = true;
      languages.rust.plugins.enable = true;
      languages.shell.plugins.enable = true;
      languages.spelling.plugins.enable = true;
      languages.sql.plugins.enable = true;
      languages.wgsl.plugins.enable = true;
    };

    programs.notes-samuelsung.enable = true;

    repos.entertainment.enable = true;

    utils.basics.enable = true;
    utils.communications.enable = true;
    utils.developments.enable = true;
    utils.games.enable = true;
    utils.creatives.enable = true;
    utils.managements.enable = true;
    web.enable = true;
    wayland.windowManager.hyprland.enable = true;
    xsession.windowManager.xmonad.enable = true;
  };

  users.presets.samuelsung.enable = true;

  users.presets.samuelsung_play.enable = true;

  # Minimal list of modules to use the EFI system partition and the YubiKey
  boot.initrd.kernelModules = [ "vfat" "nls_cp437" "nls_iso8859-1" "usbhid" ];

  # Enable support for the YubiKey PBA
  boot.initrd.luks.yubikeySupport = true;

  # Configuration to use your Luks device
  boot.initrd.luks.devices = {
    "nixos-enc" = {
      device = "/dev/disk/by-uuid/8ea85f73-a9c1-4f64-bef9-c82c2f92848d";
      preLVM = true; # You may want to set this to false if you need to start a network service first
      yubikey = {
        slot = 1;
        twoFactor = true; # Set to false if you did not set up a user password.
        storage = {
          device = "/dev/disk/by-uuid/8A9D-84E5";
        };
      };
    };
  };

  networking.wireguard.enable = true;
  services.samba.enable = true;

  networking.fix-interfaces.lan0 = "a8:5e:45:54:24:ce";

  boot.blacklistedKernelModules = [ "pcspkr" ];

  boot.kernelParams = [
    "amd_iommu=on"
    "vfio_iommu_type1.allow_unsafe_interrupts=1"
  ];

  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "defaults" "size=5G" "mode=755" ]; # NOTICE(samuelsung): has to set mode or some software such as sshd will not be happy about it.
  };

  fileSystems."/nix" =
    {
      device = "/dev/disk/by-uuid/320a4179-78ed-4bb3-ab22-19427f2490ba";
      fsType = "btrfs";
      options = [ "subvol=nix" "compress=zstd" ];
    };

  fileSystems."/persist" =
    {
      device = "/dev/disk/by-uuid/320a4179-78ed-4bb3-ab22-19427f2490ba";
      fsType = "btrfs";
      options = [ "subvol=persist" "compress=zstd" ];
      neededForBoot = true;
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/8A9D-84E5";
      fsType = "vfat";
      options = [ "fmask=0022" "dmask=0022" ];
    };

  system.fsPackages = with pkgs; [ sshfs ];

  systemd.user.tmpfiles.users.samuelsung.rules = [
    "L /home/samuelsung/music - - - - /home/samuelsung/vault/library/audio/music"
  ];

  systemd.user.tmpfiles.users.samuelsung_play.rules = [
    "L /home/samuelsung_play/music - - - - /home/samuelsung_play/vault/library/audio/music"
  ];

  swapDevices =
    [{ device = "/dev/disk/by-uuid/40f78ce0-3d09-4388-92ec-c51609432cd1"; }];

  hardware.pulseaudio.daemon.config.default-sample-channels = 2;

  # Map front-left,right to anothor audio device.
  hardware.pulseaudio.extraConfig = ''
    load-module module-remap-sink sink_name=Stereo sink_properties="device.description='Stereo'" remix=no master=alsa_output.usb-Generic_USB_Audio-00.HiFi___ucm0005.hw_ALC1220VBDT__sink channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
  '';

  services.pipewire.extraConfig.pipewire."92-low-latency.conf" = {
    context.properties = {
      default.clock.rate = 48000;
      default.clock.quantum = 512;
      default.clock.min-quantum = 512;
      default.clock.max-quantum = 512;
    };
  };

  services.pipewire.extraConfig.pipewire-pulse."92-low-latency" = {
    pulse.properties = {
      pulse.min.req = "512/48000";
      pulse.default.req = "512/48000";
      pulse.max.req = "512/48000";
      pulse.min.quantum = "512/48000";
      pulse.max.quantum = "512/48000";
    };
    stream.properties = {
      node.latency = "512/48000";
      resample.quality = 1;
    };
  };

  environment.etc = {
    "wireplumber/main.lua.d/99-alsa-lowlatency.lua".text = mkIf config.services.pipewire.wireplumber.enable ''
      table.insert(alsa_monitor.rules, {
        matches = {
          {{
            "node.name",
            "equals",
            "alsa_input.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-21"
          }},
          {{
            "node.name",
            "equals",
            "alsa_output.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-40"
          }},
        },
        apply_properties = {
          -- ["audio.format"] = "S32LE",
          ["audio.rate"] = "48000", -- for USB soundcards it should be twice your desired rate
          ["api.alsa.period-size"] = 512, -- defaults to 1024, tweak by trial-and-error
          -- ["api.alsa.disable-batch"] = true, -- generally, USB soundcards use the batch mode
        },
      })
    '';

    "wireplumber/main.lua.d/52-3960x-disable.lua".text = mkIf config.services.pipewire.wireplumber.enable ''
      table.insert(alsa_monitor.rules, {
        matches = {
          {{
            "node.name",
            "equals",
            "alsa_input.usb-046d_B525_HD_Webcam_CD357410-00.mono-fallback"
          }},
          {{
            "node.name",
            "equals",
            "alsa_output.pci-0000_03_00.1.hdmi-stereo-extra2"
          }},
          {{
            "node.name",
            "equals",
            "alsa_output.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_2__sink"
          }},
          {{
            "node.name",
            "equals",
            "alsa_output.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_1__sink"
          }},
          {{
            "node.name",
            "equals",
            "alsa_output.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT__sink"
          }},
          {{
            "node.name",
            "equals",
            "alsa_input.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_2__source"
          }},
          {{
            "node.name",
            "equals",
            "alsa_input.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_1__source"
          }},
          {{
            "node.name",
            "equals",
            "alsa_input.usb-Generic_USB_Audio-00.HiFi__hw_ALC1220VBDT_0__source"
          }},
        },
        apply_properties = {
          ["node.disabled"] = true,
        },
      })
    '';
  };

  services.pipewire.extraConfig.pipewire."90-main-output" = {
    context.modules = [{
      name = "libpipewire-module-combine-stream";
      args = {
        combine.mode = "sink";
        node.name = "main-output";
        node.description = "Main Output";
        combine.latency-compensate = false; # if true, match latencies by adding delays
        combine.props = {
          audio.position = [ "FL" "FR" ];
        };
        stream.props = { };
        stream.rules = [{
          matches = [
            { node.name = "alsa_output.usb-iFi__by_AMR__iFi__by_AMR__HD_USB_Audio_0003-00.analog-stereo"; }
            { node.name = "alsa_output.usb-Focusrite_Scarlett_4i4_USB_D8K27VC181E243-00.analog-surround-40"; }
          ];
          actions = {
            create-stream = {
              combine.audio.position = [ "FL" "FR" ];
              audio.position = [ "FL" "FR" ];
            };
          };
        }];
      };
    }];
  };
}
