localFlake:

{ lib, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.hardware-vm
  ];
  networking.hostName = "server-vm";
  system.stateVersion = lib.trivial.release;

  services.openssh.enable = true;
  programs.gnupg.agent.enable = true;

  home-manager.users.samuelsung = {
    utils.basics.enable = true;
    utils.developments.nix.enable = true;
  };
  home-manager.users.samuelsung_play = {
    utils.basics.enable = true;
    utils.developments.nix.enable = true;
  };
}
