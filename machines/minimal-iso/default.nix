localFlake:

{ lib, ... }:

{
  imports = [
    localFlake.config.flake.nixosModules.default
    localFlake.config.flake.nixosModules.iso-base
    localFlake.config.flake.nixosModules.hardware-iso
  ];
  networking.hostName = "minimal-iso";
  system.stateVersion = lib.trivial.release;

  hardware.bluetooth.enable = true;
  networking.networkmanager.enable = true;
  services.openssh.enable = true;
  programs.gnupg.agent.enable = true;

  home-manager.users.samuelsung = {
    programs.neovim-flake.neovim = {
      modules.lsp.enable = true;
      modules.snippets.enable = true;

      languages.nix.plugins.enable = true;
    };
    utils.basics.enable = true;
    utils.developments.nix.enable = true;
    utils.managements.enable = true;
  };

  users.presets.samuelsung.enable = true;
}
