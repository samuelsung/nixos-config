{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  sops.defaultSopsFile = ./common.host.yaml;

  sops.secrets."samuelsung-passwd" = {
    sopsFile = ./common.host.yaml;
    key = "passwd/samuelsung";
    mode = "0400";
    neededForUsers = true;
  };

  sops.secrets."root-passwd" = {
    sopsFile = ./common.host.yaml;
    key = "passwd/root";
    mode = "0400";
    neededForUsers = true;
  };

  users.users.root = {
    hashedPasswordFile = config.sops.secrets."root-passwd".path;
    initialPassword = null;
  };

  users.users.samuelsung = mkIf config.users.presets.samuelsung.enable {
    hashedPasswordFile = config.sops.secrets."samuelsung-passwd".path;
    initialPassword = null;
  };

  users.users.samuelsung_play = mkIf config.users.presets.samuelsung_play.enable {
    hashedPasswordFile = config.sops.secrets."samuelsung-passwd".path;
    initialPassword = null;
  };

  sops.secrets."no_scope_github_token" = {
    sopsFile = ./common.host.yaml;
    key = "no_scope_github_token";
    mode = "400";
  };

  sops.templates."nix-github-com-extra-access-token.conf".content = ''
    extra-access-tokens = github.com=${config.sops.placeholder."no_scope_github_token"}
  '';

  nix.extraOptions = ''
    !include ${config.sops.templates."nix-github-com-extra-access-token.conf".path}
  '';

  services.gitlab-runner.githubToken = config.sops.secrets."no_scope_github_token".path;

  sops.secrets."restic-tataru-repo" = {
    key = "restic-tataru-repo";
    mode = "0400";
    sopsFile = ./common.host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets."restic-tataru-password" = {
    key = "restic-tataru-password";
    mode = "0400";
    sopsFile = ./common.host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets."restic-tataru-env" = {
    key = "restic-tataru-env";
    mode = "0400";
    sopsFile = ./common.host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  services.restic.backups.tataru-backup = {
    inherit (config.backups) paths exclude;

    initialize = true;
    passwordFile = config.sops.secrets."restic-tataru-password".path;
    environmentFile = config.sops.secrets."restic-tataru-env".path;
    repositoryFile = config.sops.secrets."restic-tataru-repo".path;
    timerConfig = {
      OnCalendar = "00:05";
      RandomizedDelaySec = "5h";
    };
  };
}
