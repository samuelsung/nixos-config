{ config, lib, ... }:

let
  inherit (lib) mkOverride;
in
{
  sops.secrets."samuelsung-remote-tailscale-authkey" = {
    key = "tailscale-authkey";
    mode = "0400";
    sopsFile = ./common.remote-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  services.tailscale = {
    loginServer = mkOverride 1000 "https://headscale.helios.samuelsung.net";
    authKeyFile = mkOverride 1000 config.sops.secrets."samuelsung-remote-tailscale-authkey".path;
  };
}
