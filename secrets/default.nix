{
  flake.homeManagerModules.secrets-common-interactive-samuelsung = ./common.interactive-samuelsung.nix;
  flake.nixosModules.secrets-common-host = ./common.host.nix;
  flake.nixosModules.secrets-common-interactive-host = ./common.interactive-host.nix;
  flake.nixosModules.secrets-common-remote-host = ./common.remote-host.nix;
}
