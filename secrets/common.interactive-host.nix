{ config, lib, ... }:

let
  inherit (lib) concatMapAttrs mkIf mkOverride;

  primaryGroup = username:
    let
      inherit (config.users) groups users;
    in
    groups.${users.${username}.group};
in
{
  sops.secrets."samuelsung-interactive-tailscale-authkey" = {
    key = "tailscale-authkey";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  services.tailscale = {
    loginServer = mkOverride 500 "https://headscale.helios.samuelsung.net";
    authKeyFile = mkOverride 500 config.sops.secrets."samuelsung-interactive-tailscale-authkey".path;
  };

  sops.secrets.age-samuelsung = mkIf config.users.presets.samuelsung.enable {
    key = "age-samuelsung";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.samuelsung.name;
    group = config.users.groups.nogroup.name;
  };

  home-manager.users.samuelsung = mkIf config.users.presets.samuelsung.enable {
    sops.age.keyFile = config.sops.secrets.age-samuelsung.path;
  };

  sops.secrets.age-samuelsung_play = mkIf config.users.presets.samuelsung_play.enable {
    key = "age-samuelsung";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.samuelsung_play.name;
    group = config.users.groups.nogroup.name;
  };

  home-manager.users.samuelsung_play = mkIf config.users.presets.samuelsung_play.enable {
    sops.age.keyFile = config.sops.secrets.age-samuelsung_play.path;
  };

  sops.secrets.expressvpn-username = {
    key = "expressvpn-username";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.expressvpn-password = {
    key = "expressvpn-password";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.expressvpn-jp-ca = {
    key = "expressvpn-jp-ca";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.expressvpn-jp-cert = {
    key = "expressvpn-jp-cert";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.expressvpn-jp-key = {
    key = "expressvpn-jp-key";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.expressvpn-jp-tls-auth = {
    key = "expressvpn-jp-tls-auth";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.templates."expressvpn-env-file".content = ''
    EXPRESSVPN_USERNAME=${config.sops.placeholder."expressvpn-username"}
    EXPRESSVPN_PASSWORD=${config.sops.placeholder."expressvpn-password"}
  '';

  sops.secrets.garemarudo-route1 = {
    key = "garemarudo-route1";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-route2 = {
    key = "garemarudo-route2";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-remote = {
    key = "garemarudo-remote";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-username = {
    key = "garemarudo-username";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-password = {
    key = "garemarudo-password";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-ca = {
    key = "garemarudo-ca";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-cert = {
    key = "garemarudo-cert";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.secrets.garemarudo-key = {
    key = "garemarudo-key";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  sops.templates."garemarudo-env-file".content = ''
    GAREMARUDO_ROUTE1=${config.sops.placeholder."garemarudo-route1"}
    GAREMARUDO_ROUTE2=${config.sops.placeholder."garemarudo-route2"}
    GAREMARUDO_REMOTE=${config.sops.placeholder."garemarudo-remote"}
    GAREMARUDO_USERNAME=${config.sops.placeholder."garemarudo-username"}
    GAREMARUDO_PASSWORD=${config.sops.placeholder."garemarudo-password"}
  '';

  networking.networkmanager.ensureProfiles.profiles =
    let
      uuids = {
        tokyo = "8e4a514d-5fea-4ddb-bc96-c05184769fa0";
        osaka = "8e4a514d-5fea-4ddb-bc96-c05184769fa1";
        shibuya = "8e4a514d-5fea-4ddb-bc96-c05184769fa2";
        yokohama = "8e4a514d-5fea-4ddb-bc96-c05184769fa3";
      };
    in
    (concatMapAttrs
      (name: uuid: {
        "expressvpn-${name}" = {
          connection = { autoconnect = "false"; id = "expressvpn-${name}"; type = "vpn"; inherit uuid; };
          ipv4 = { method = "auto"; };
          ipv6 = { addr-gen-mode = "stable-privacy"; method = "auto"; };
          proxy = { };
          vpn = {
            auth = "SHA512";
            ca = config.sops.secrets.expressvpn-jp-ca.path;
            cert = config.sops.secrets.expressvpn-jp-cert.path;
            cert-pass-flags = "0";
            cipher = "AES-256-GCM";
            comp-lzo = "no-by-default";
            connection-type = "password-tls";
            dev = "tun";
            fragment-size = "1300";
            key = config.sops.secrets.expressvpn-jp-key.path;
            mssfix = "1200";
            ns-cert-type = "server";
            password-flags = "0";
            remote = "japan-${name}-ca-version-2.expressnetw.com:1195";
            remote-random = "yes";
            service-type = "org.freedesktop.NetworkManager.openvpn";
            ta = config.sops.secrets.expressvpn-jp-tls-auth.path;
            ta-dir = "1";
            tunnel-mtu = "1500";
            username = "$EXPRESSVPN_USERNAME";
            verify-x509-name = "name-prefix:Server";
          };
          vpn-secrets = { password = "$EXPRESSVPN_PASSWORD"; };
        };
      })
      uuids) // {
      garemarudo = {
        connection = { autoconnect = "false"; id = "garemarudo"; type = "vpn"; uuid = "16dc6167-b978-48d1-afa2-838db11f9c24"; };
        ipv4 = { ignore-auto-routes = "true"; method = "auto"; never-default = "true"; route1 = "$GAREMARUDO_ROUTE1"; route2 = "$GAREMARUDO_ROUTE2"; };
        ipv6 = { addr-gen-mode = "stable-privacy"; method = "auto"; };
        proxy = { };
        vpn = {
          auth = "SHA256";
          ca = config.sops.secrets.garemarudo-ca.path;
          cert = config.sops.secrets.garemarudo-cert.path;
          cert-pass-flags = "0";
          data-ciphers = "AES-256-CBC";
          cipher = "AES-256-CBC";
          connection-type = "password-tls";
          dev = "tun";
          float = "yes";
          key = config.sops.secrets.garemarudo-key.path;
          password-flags = "0";
          ping = "10";
          ping-restart = "60";
          proto-tcp = "yes";
          remote = "$GAREMARUDO_REMOTE";
          reneg-seconds = "28800";
          service-type = "org.freedesktop.NetworkManager.openvpn";
          tls-version-min = "1.2";
          username = "$GAREMARUDO_USERNAME";
        };
        vpn-secrets = { password = "$GAREMARUDO_PASSWORD"; };
      };
    };

  networking.networkmanager.ensureProfiles.environmentFiles = [
    config.sops.templates."expressvpn-env-file".path
    config.sops.templates."garemarudo-env-file".path
  ];

  # ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF8T/u/gmQOwk0XOCa73GQz/7xdYxWzm1qaUIQkdPAgg
  sops.secrets."tataru-vault-ssh-key" = {
    key = "tataru-vault-ssh-key";
    mode = "0400";
    sopsFile = ./common.interactive-host.yaml;
    owner = config.users.users.root.name;
    group = config.users.groups.nogroup.name;
  };

  fileSystems."/home/samuelsung/vault" = mkIf config.users.presets.samuelsung.enable {
    device = "vault_user@tataru:/";
    fsType = "fuse.sshfs";
    options = [
      "port=939"
      "idmap=user"
      "allow_other"
      "uid=${toString config.users.users.samuelsung.uid}"
      "gid=${toString (primaryGroup "samuelsung").gid}"
      "noauto"
      "x-systemd.automount"
      "_netdev"
      "ServerAliveInterval=15"
      "identityfile=${config.sops.secrets."tataru-vault-ssh-key".path}"
    ];
  };

  fileSystems."/home/samuelsung_play/vault" = mkIf config.users.presets.samuelsung_play.enable {
    device = "vault_user@tataru:/";
    fsType = "fuse.sshfs";
    options = [
      "port=939"
      "idmap=user"
      "allow_other"
      "uid=${toString config.users.users.samuelsung_play.uid}"
      "gid=${toString (primaryGroup "samuelsung_play").gid}"
      "noauto"
      "x-systemd.automount"
      "_netdev"
      "ServerAliveInterval=15"
      "identityfile=${config.sops.secrets."tataru-vault-ssh-key".path}"
    ];
  };
}
