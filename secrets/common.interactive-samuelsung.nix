{ config, lib, ... }:

let
  inherit (lib) mkIf;
in
{
  sops.secrets."ssh-samuelsung-private" = {
    key = "ssh-samuelsung-private";
    sopsFile = ./common.interactive-samuelsung.yaml;
    path = "${config.xdg.dataHome}/secrets/ssh-samuelsung-private";
    mode = "0400";
  };

  programs.ssh.includes = [
    config.sops.secrets."ssh-samuelsung-private".path
  ];

  sops.secrets."secret-samuelsung-notes-secret" = {
    key = "secret-samuelsung-notes-secret";
    sopsFile = ./common.interactive-samuelsung.yaml;
    path = "${config.xdg.dataHome}/secrets/secret-samuelsung-notes-secret";
    mode = "0400";
  };

  programs.ssh.matchBlocks = mkIf (config.programs.notes-samuelsung.enable or false) {
    notes-gitea-samuelsung-net = {
      host = "notes-gitea.samuelsung.net";
      hostname = "gitea.samuelsung.net";
      port = 443;
      user = "gitea";
      checkHostIP = true;
      identityFile = config.sops.secrets.secret-samuelsung-notes-secret.path;
      extraOptions.Preferredauthentications = "publickey";
    };
  };

  repos.notes = mkIf (config.programs.notes-samuelsung.enable or false) {
    notes-samuelsung.remotes.origin.fetch =
      "gitea@notes-gitea.samuelsung.net:samuelsung/notes.git";
  };

  sops.secrets."lab-secret-samuelsung" = {
    key = "lab-secret";
    sopsFile = ./common.interactive-samuelsung.yaml;
    path = "${config.xdg.dataHome}/secrets/lab-secret-samuelsung_common";
    mode = "0400";
  };

  programs.lab.secretFile =
    config.sops.secrets."lab-secret-samuelsung".path;

  sops.secrets."tea-secret-samuelsung_common-config-yml" = {
    key = "tea-secret-samuelsung";
    sopsFile = ./common.interactive-samuelsung.yaml;
    path = "${config.xdg.dataHome}/secrets/tea-secret-samuelsung_common-config-yml";
    mode = "0400";
  };

  programs.tea.secretFile =
    config.sops.secrets."tea-secret-samuelsung_common-config-yml".path;

  sops.secrets."joplin-s3-secret-samuelsung" = {
    key = "joplin-s3-secret-samuelsung";
    sopsFile = ./common.interactive-samuelsung.yaml;
    mode = "0400";
    path = "${config.xdg.dataHome}/secrets/joplin-s3-secret-samuelsung";
  };

  programs.joplin.sync = {
    enable = true;
    type = 8;
    secretPath = config.sops.secrets."joplin-s3-secret-samuelsung".path;
  };
}
