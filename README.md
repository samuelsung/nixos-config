# My personal NixOS configuration
Configuration for personal desktops, notebooks and self-host servers.

# References (Thank you 🐸)
## Manuals
- [home-manager's options](https://nix-community.github.io/home-manager/options.html)

## Guides
- [NixOS Flakes Wiki](https://nixos.wiki/wiki/Flakes)
- [Nix Flakes, Part 3: Managing NixOS systems - Eelco Dolstra](https://www.tweag.io/blog/2020-07-31-nixos-flakes/)
- [NixOS Configuration with Flakes - jordanisaacs](https://jdisaacs.com/series/nixos-desktop/)
- [The working programmer’s guide to setting up Haskell projects - jonascarpay](https://jonascarpay.com/posts/2021-01-28-haskell-project-template.html)
- [Shell Scripts with Nix - Jon Sangster](https://ertt.ca/nix/shell-scripts/)
- [OpenSSH security and hardening - Linux Audit](https://linux-audit.com/audit-and-harden-your-ssh-configuration)
- [sshd_config - How to configure the OpenSSH server - www.ssh.com](https://www.ssh.com/academy/ssh/sshd_config)
- [openssh - mozilla](https://infosec.mozilla.org/guidelines/openssh.html)
- [Arch security wiki](https://wiki.archlinux.org/title/security)
- [Arch openssh wiki](https://wiki.archlinux.org/title/OpenSSH)
- [Ask for a password in POSIX-compliant shell? - stackexchange](https://unix.stackexchange.com/questions/222974/ask-for-a-password-in-posix-compliant-shell)
- [Shell Stlye Guide - google](https://google.github.io/styleguide/shellguide.html)
- [Parameter Expansion - The Open Group Base Specifications Issue](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_02)
- [Here Documents](https://linux.die.net/abs-guide/here-docs.html)
- [getopt, getopts or manual parsing - what to use when I want to support both short and long options?](https://unix.stackexchange.com/questions/62950/getopt-getopts-or-manual-parsing-what-to-use-when-i-want-to-support-both-shor)
- [How to autorebase MRs in GitLab CI - Marcin Wosinek](https://how-to.dev/how-to-autorebase-mrs-in-gitlab-ci)

## Repos
- [Mic92's dotfiles repo](https://github.com/Mic92/dotfiles)
- [jordanisaacs's dotfiles repo](https://github.com/jordanisaacs/dotfiles)
- [jordanisaacs's dwm repo](https://github.com/jordanisaacs/dwm-flake)
- [arkenfox's user.js](https://github.com/arkenfox/user.js)
- [de956's browser-privacy](https://github.com/de956/browser-privacy)
- [DeterminateSystems's update-flake-lock](https://github.com/DeterminateSystems/update-flake-lock)

## Projects
- [flake-compat](https://github.com/edolstra/flake-compat)
- [sops-nix](https://github.com/Mic92/sops-nix)
- [NixOS hardware repo](https://github.com/NixOS/nixos-hardware)
- [update-flake-lock](https://github.com/DeterminateSystems/update-flake-lock)
