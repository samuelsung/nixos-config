{ config, inputs, lib, ... }:

let
  inherit (lib) mkForce optionals;
in
{
  imports = [ inputs.devenv.flakeModule ];
  perSystem = ctx@{ system, inputs', pkgs, ... }: {
    checks = {
      pre-commit = ctx.config.devenv.shells.default.pre-commit.run;
    };

    devenv.shells.default = {
      packages = [
        inputs'.misc.packages.ssh-keygen-from-keymap
      ] ++ (optionals (system == "x86_64-linux") [
        pkgs.appimage-run
        pkgs.steam-run
      ]);

      pre-commit = {
        src = ../.;
        hooks = {
          deadnix.enable = true;
          nixpkgs-fmt.enable = true;
          statix.enable = true;
          shfmt.enable = true;
          shfmt.entry = mkForce "${pkgs.shfmt}/bin/shfmt -ci -s -bn -i 2 -l -w";
          shfmt.excludes = [
            "p10k\\.zsh"
            "secret\\.sh"
          ];
          shellcheck.enable = true;
          shellcheck.entry = mkForce "${pkgs.shellcheck}/bin/shellcheck -x";

          commitlint = inputs.commitlint-config.hook.${system};
        };
      };
    };
  };
}
